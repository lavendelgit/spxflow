if (out.ManualNetPriceCheckerinDocumentCurrency){
    return out.ManualNetPriceCheckerinDocumentCurrency
}
else if(out.InterfacerPriceCheckerinDocumentCurrency){
    return out.InterfacerPriceCheckerinDocumentCurrency
}
else if(out.CustomerPriceCheckerinDocumentCurrency && !out.ManualNetPriceCheckerinDocumentCurrency && !out.InterfacerPriceCheckerinDocumentCurrency){
    return  out.CustomerPriceCheckerinDocumentCurrency - (out.VolumeDiscountinDocumentCurrency?Math.abs(out.VolumeDiscountinDocumentCurrency):0) -
            (out.PromotionaldiscountsinDocumentCurrency?Math.abs(out.PromotionaldiscountsinDocumentCurrency):0) -
            (out.ManualDiscountinDocumentCurrency?Math.abs(out.ManualDiscountinDocumentCurrency):0)
}else if(out.GloballistpriceinDocumentCurrency && !out.ManualNetPriceCheckerinDocumentCurrency && !out.InterfacerPriceCheckerinDocumentCurrency){
    return out.GloballistpriceinDocumentCurrency - (out.CurrentRow?.RegionalDiscountinDocumentCurrency?Math.abs(out.CurrentRow?.RegionalDiscountinDocumentCurrency):0) - (out.CustomerprogramDiscountinDocumentCurrency?Math.abs(out.CustomerprogramDiscountinDocumentCurrency):0)- (out.VolumeDiscountinDocumentCurrency?Math.abs(out.VolumeDiscountinDocumentCurrency):0) -
            (out.PromotionaldiscountsinDocumentCurrency?Math.abs(out.PromotionaldiscountsinDocumentCurrency):0) -
            (out.ManualDiscountinDocumentCurrency?Math.abs(out.ManualDiscountinDocumentCurrency):0)
}
return 0