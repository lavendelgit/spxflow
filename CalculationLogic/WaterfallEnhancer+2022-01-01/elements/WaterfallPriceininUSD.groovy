if (out.ManualNetPriceCheckerinUSD){
    return out.ManualNetPriceCheckerinUSD
}
else if(out.InterfacerPriceCheckerinUSD){
    return out.InterfacerPriceCheckerinUSD
}
else if(out.CustomerPriceCheckerinUSD && !out.ManualNetPriceCheckerinUSD && !out.InterfacerPriceCheckerinUSD){
    return  out.CustomerPriceCheckerinUSD - (out.VolumeDiscountinUSD?Math.abs(out.VolumeDiscountinUSD):0) -
            (out.PromotionaldiscountsinUSD?Math.abs(out.PromotionaldiscountsinUSD):0) -
            (out.ManualDiscountinUSD?Math.abs(out.ManualDiscountinUSD):0)
}else if(out.GloballistpriceinUSD && !out.ManualNetPriceCheckerinUSD && !out.InterfacerPriceCheckerinUSD){
    return out.GloballistpriceinUSD - (out.CurrentRow?.RegionalDiscountinUSD?Math.abs(out.CurrentRow?.RegionalDiscountinUSD):0) - (out.CustomerprogramDiscountinUSD?Math.abs(out.CustomerprogramDiscountinUSD):0)- (out.VolumeDiscountinUSD?Math.abs(out.VolumeDiscountinUSD):0) -
            (out.PromotionaldiscountsinUSD?Math.abs(out.PromotionaldiscountsinUSD):0) -
            (out.ManualDiscountinUSD?Math.abs(out.ManualDiscountinUSD):0)
}
return 0