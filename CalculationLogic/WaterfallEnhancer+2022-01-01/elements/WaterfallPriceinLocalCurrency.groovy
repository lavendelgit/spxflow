if (out.ManualNetPriceCheckerinLocalCurrency){
    return out.ManualNetPriceCheckerinLocalCurrency
}
else if(out.InterfacerPriceCheckerinLocalCurrency){
    return out.InterfacerPriceCheckerinLocalCurrency
}
else if(out.CustomerPriceCheckerinLocalCurrency && !out.ManualNetPriceCheckerinLocalCurrency && !out.InterfacerPriceCheckerinLocalCurrency){
    return  out.CustomerPriceCheckerinLocalCurrency - (out.VolumeDiscountinLocalCurrency?Math.abs(out.VolumeDiscountinLocalCurrency):0) -
            (out.PromotionaldiscountsinLocalCurrency?Math.abs(out.PromotionaldiscountsinLocalCurrency):0) -
            (out.ManualDiscountinLocalCurrency?Math.abs(out.ManualDiscountinLocalCurrency):0)
}else if(out.GloballistpriceinLocalCurrency && !out.ManualNetPriceCheckerinLocalCurrency && !out.InterfacerPriceCheckerinLocalCurrency){
    return out.GloballistpriceinLocalCurrency - (out.CurrentRow?.RegionalDiscountinLocalCurrency?Math.abs(out.CurrentRow?.RegionalDiscountinLocalCurrency):0) - (out.CustomerprogramDiscountinLocalCurrency?Math.abs(out.CustomerprogramDiscountinLocalCurrency):0)- (out.VolumeDiscountinLocalCurrency?Math.abs(out.VolumeDiscountinLocalCurrency):0) -
            (out.PromotionaldiscountsinLocalCurrency?Math.abs(out.PromotionaldiscountsinLocalCurrency):0) -
            (out.ManualDiscountinLocalCurrency?Math.abs(out.ManualDiscountinLocalCurrency):0)
}
return 0