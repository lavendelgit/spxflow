/**
 * Based on the selected configuration prepares a proper Map containing the user default values that will be saved
 * to the SIP_DefaultFilters PP table.
 */
def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig
String selectedConfiguration = api.getElement("SelectedConfiguration")
Map fetchedInput

switch (selectedConfiguration) {
    case commonsConstConfig.GENERAL_FILTER_NAME:
        fetchedInput = ExecutorUtils.fetchDefaultConfiguratorInput()
        break
    case commonsConstConfig.OUTLIERS_NAME:
        fetchedInput = ExecutorUtils.fetchOutliersInputs()
        break
    case commonsConstConfig.REVENUE_AND_MARGIN_NAME:
        fetchedInput = ExecutorUtils.fetchRevenueAndMarginInputs()
        break
    case commonsConstConfig.REVENUE_AND_MARGIN_MAP_NAME:
        fetchedInput = ExecutorUtils.fetchRevenueAndMarginMapInputs()
        break
    case commonsConstConfig.WATERFALL_NAME:
        fetchedInput = ExecutorUtils.fetchWaterfallInputs()
        break
    case commonsConstConfig.COMPARISON_WATERFALL_NAME:
        fetchedInput = ExecutorUtils.fetchWaterfallComparisonInputs()
        break
    case commonsConstConfig.MARGIN_BREAKDOWN_NAME:
        fetchedInput = ExecutorUtils.fetchMarginBreakdownInputs()
        break
    case commonsConstConfig.REVENUE_BREAKDOWN_NAME:
        fetchedInput = ExecutorUtils.fetchRevenueBreakdownInputs()
        break
    case commonsConstConfig.PRODUCT_CUSTOMER_CAUSALITY_NAME:
        fetchedInput = ExecutorUtils.fetchProductCustomerCausalityInputs()
        break
    default:
        return
}

ExecutorUtils.saveDefaultFilterConfiguration(selectedConfiguration, fetchedInput)