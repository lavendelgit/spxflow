/**
 * Retrieves the value that has been selected in the SelectedConfiguration entry from the SIP_DefaultFilterConfiguratorInput logic
 */
return api.input(libs.SIP_Dashboards_Commons.ConstConfig.DEFAULT_CONFIGURATOR_CONFIG.INPUTS.DASHBOARD_SELECTION.UNIQUE_KEY)