import net.pricefx.formulaengine.DatamartContext

/**
 * Get data from simulation to show on dashboard
 * @param configuration
 * @param detailViewDashboardParams
 * @return
 */
List calculateMetricsPerProduct(Map configuration, Map detailViewDashboardParams) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def queryUtils = libs.CustomerInsights.QueryUtils

  List basicQuerySimulationFilters = detailViewDashboardParams.getBasicQuerySimulationFilter(configuration)
  DatamartContext.Query query = queryHelper.createQueryForSumValuePerProduct(configuration, basicQuerySimulationFilters)
  String sql = """ SELECT productId                            AS 'productId',               
                          revenue                              AS 'revenue',                   
                          margin                               AS 'margin',                 
                          quantity                             AS 'quantity',        
                          transactionAmount                    AS 'transactionAmount',  
                          productClassificationByQuantity      AS 'productClassificationByQuantity',
                          productClassificationByMarginPercent AS 'productClassificationByMarginPercent'
                   FROM T1 
                   ORDER BY productId """

  return queryUtils.getFullQueryData(sql, query)
}
/**
 * get info of customer:
 *    Revenue/Margin/Quantity: base on inputParamYTD --> year to date
 *    others : base on inputParamL12M --> last 12M
 * @param configuration
 * @param detailViewDashboardParams
 * @param inputParamL12M
 * @return
 */
Map getSummaryInfoForCustomerDetailView(Map configuration, Map detailViewDashboardParams) {
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils
  def classificationUtils = libs.CustomerInsights.Classification
  def dateUtils = libs.CustomerInsights.DateUtils
  def constant = libs.CustomerInsights.Constant

  List getValueFromDatamartFilters = detailViewDashboardParams.getBasicQueryDatamartFilter(configuration)
  Map data = calculationCommonUtils.getSummaryValuesFromOriginalSource(configuration, getValueFromDatamartFilters)
  if (data.revenue) {
    //calculate YTD trend
    Map timePeriod = constant.TIME_PERIOD
    Map YTDPeriod = dateUtils.getPeriod(detailViewDashboardParams.getCurrentDate(), timePeriod.YTD)
    Map previousYTDPeriod = dateUtils.addYearsToPeriod(YTDPeriod, -1)//last year
    List YTDFilters = detailViewDashboardParams.getCustomPeriodQueryDatamartFilter(configuration, YTDPeriod.startDate, YTDPeriod.endDate)
    List previousYTDFilters = detailViewDashboardParams.getCustomPeriodQueryDatamartFilter(configuration, previousYTDPeriod.startDate, previousYTDPeriod.endDate)

    Map trendYTD = calculationCommonUtils.calculateYTDTrendFromOriginalSource(configuration, YTDFilters, previousYTDFilters)
    data.customerYTDRevenueTrend = trendYTD.revenueYTDTrend
    data.customerYTDMarginTrend = trendYTD.marginYTDTrend
    data.customerYTDQuantityTrend = trendYTD.quantityYTDTrend
    //calculate L12M trend
    Map L12MPeriod = dateUtils.getPeriod(detailViewDashboardParams.getCurrentDate(), timePeriod.L12M)
    List L12MFilters = detailViewDashboardParams.getCustomPeriodQueryDatamartFilter(configuration, L12MPeriod.startDate, L12MPeriod.endDate)

    Map trendL12M = calculationCommonUtils.calculateL12MTrendForCustomerFromOriginalSource(configuration, L12MFilters, L12MPeriod)
    BigDecimal customerHealthScore = classificationUtils.calculateHealthScore(configuration, trendL12M.revenueTrend, trendL12M.marginTrend)
    data.customerRevenueTrend = trendL12M.revenueTrend
    data.customerMarginTrend = trendL12M.marginTrend
    data.customerQuantityTrend = trendL12M.quantityTrend
    data.customerHealthScore = customerHealthScore
  }

  return data
}
/**
 *
 * @param inputGroupedDataByMonth
 * @param months
 * @param valueType
 * @return
 */
Map getRegressionFactorFromMapData(Map inputGroupedDataByMonth, List months, String valueType) {
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils

  List dataInputForRegression = convertToListForCalculateRegression(inputGroupedDataByMonth, months, valueType)

  return calculationCommonUtils.calculateRegression(dataInputForRegression)
}
/**
 * convert from Map to List
 *     Map structure ["2019-M01": [ProductId: 'MB-00001' ,Revenue: 100, Margin: 10, Quantity: 80, TransactionAmount: 50]
 *     List structure [[x:'value of x', y:'value of y'],...]
 *     x : step number of Month in Map (from 1...)
 *     y : revenue/margin/quantity/transaction amount of month
 * @param inputGroupedDataByMonth
 * @return
 */
protected List convertToListForCalculateRegression(Map inputGroupedDataByMonth, List months, String valueType) {
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils

  List data = []
  Map currentItem
  BigDecimal value
  for (int i = 0; i < months?.size(); i++) {
    currentItem = inputGroupedDataByMonth.get(months.getAt(i))
    value = currentItem ? calculationCommonUtils.getValueBaseOnType(currentItem, valueType, false) : BigDecimal.ZERO
    data.add([step: (i + 1), value: value])
  }

  return data
}
/**
 * Get sum value from L12M to current date for each month
 * @param configuration
 * @param detailViewDashboardParams
 * @return
 */
List getDataForRegressionAndRevenueMargin(Map configuration, Map detailViewDashboardParams) {
  def dateUtils = libs.CustomerInsights.DateUtils
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils
  def configurationUtils = libs.CustomerInsights.Configuration

  Integer lastMonthAmount = configurationUtils.getLastMonthAmountFromConfiguration(configuration)
  Map period = dateUtils.getPreviousPeriodFromDate(detailViewDashboardParams.getCurrentDate(), lastMonthAmount)
  List queryFilters = detailViewDashboardParams.getCustomPeriodQuerySimulationFilter(configuration, period.startDate, detailViewDashboardParams.getEndDate())

  return calculationCommonUtils.getSumValueOnEachMonth(configuration, queryFilters)
}
/**
 * Get sum of revenue from list
 * @param data
 * @return
 */
protected BigDecimal getSumRevenue(List data) {
  return (data?.sum { (it.revenue as BigDecimal) }) ?: BigDecimal.ZERO
}
/**
 * Get data for revenue breakdown chart
 * @param configuration
 * @param detailViewDashboardParams
 * @return
 */
Map getDataForRevenueBreakdownChart(Map configuration, Map detailViewDashboardParams) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def constant = libs.CustomerInsights.Constant
  def dateUtils = libs.CustomerInsights.DateUtils

  Integer decimalPlaces = constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_02
  BigDecimal lastRevenue, lostBusiness, newBusiness, priceEffect, volumeEffect, portfolioMixEffect, otherEffects, currentRevenue
  List basicQuerySimulationFilters = detailViewDashboardParams.getBasicQuerySimulationFilter(configuration)
  DatamartContext.Query currentPeriodQuery = queryHelper.createQueryForSumValuePerCustomerAndProduct(configuration, basicQuerySimulationFilters)
  Integer lastMonthAmount = libs.CustomerInsights.Configuration.getLastMonthAmountFromConfiguration(configuration)
  Map previousPeriod = dateUtils.addMonthsToPeriod(detailViewDashboardParams.getPeriod(), (-1) * lastMonthAmount)
  DatamartContext.Query previousPeriodQuery = queryHelper.createQueryForSumValuePerCustomerAndProduct(configuration,
      detailViewDashboardParams.getCustomPeriodQuerySimulationFilter(configuration, previousPeriod.startDate, previousPeriod.endDate))

  lastRevenue = getTotalRevenueFromQuery(previousPeriodQuery)
  currentRevenue = getTotalRevenueFromQuery(currentPeriodQuery)
  List lostBusinessInfo = getLostOrNewBusinessInfoFromQuery(previousPeriodQuery, currentPeriodQuery)
  List newBusinessInfo = getLostOrNewBusinessInfoFromQuery(currentPeriodQuery, previousPeriodQuery)
  lostBusiness = (-1) * getSumRevenue(lostBusinessInfo)
  newBusiness = getSumRevenue(newBusinessInfo)
  List commonBusinessInfo = getCommonBusinessInfoFromQuery(previousPeriodQuery, currentPeriodQuery)
  priceEffect = getPriceEffect(commonBusinessInfo)
  volumeEffect = getVolumeEffect(commonBusinessInfo)
  portfolioMixEffect = getPortfolioMixEffect(commonBusinessInfo)
  otherEffects = currentRevenue - (lastRevenue + lostBusiness + priceEffect + volumeEffect + portfolioMixEffect + newBusiness)
  otherEffects = libs.SharedLib.RoundingUtils.round(otherEffects, decimalPlaces)

  return [lastRevenue       : lastRevenue,
          lostBusiness      : lostBusiness,
          priceEffect       : priceEffect,
          volumeEffect      : volumeEffect,
          portfolioMixEffect: portfolioMixEffect,
          otherEffects      : otherEffects,
          newBusiness       : newBusiness,
          currentRevenue    : currentRevenue]
}
/**
 * Get sum of revenue from query
 * @param DatamartContext.Query
 * @return
 */
protected BigDecimal getTotalRevenueFromQuery(DatamartContext.Query query) {
  if (!query) {
    return BigDecimal.ZERO
  }

  String sqlStatement = "SELECT SUM(revenue) as 'revenue' FROM T1"
  List data = api.getDatamartContext().executeSqlQuery(sqlStatement, query)?.collect()

  return data?.getAt(0)?.revenue ?: BigDecimal.ZERO
}
/**
 * Get Lost or New business Info from 2 query
 *   To get 'LOST Business' -> first param is Previous Period Query, second param is Current Period Query
 *   To get 'NEW Business' -> first param is Current Period Query, second param is Previous Period Query
 * @param currentPeriodRevenueBreakdownTable
 * @param previousPeriodRevenueBreakdownTable
 * @return
 */
protected List getLostOrNewBusinessInfoFromQuery(DatamartContext.Query previousQuery, DatamartContext.Query currentQuery) {
  def queryUtils = libs.CustomerInsights.QueryUtils

  if (!previousQuery || !currentQuery) {
    return null
  }

  String sqlStatement = """SELECT T1.customerId      as 'customerId',
                                  T1.productId       as 'productId',
                                  T1.revenue         as 'revenue',
                                  T1.quantity        as 'quantity',
                                  T1.revenuePerUnit  as 'revenuePerUnit'
                          FROM T1 LEFT OUTER JOIN T2 ON T1.productId = T2.productId AND T1.customerId = T2.customerId
                          WHERE T2.productId IS NULL AND T2.customerId IS NULL
                          ORDER BY T1.customerId, T1.productId """

  return queryUtils.getFullQueryData(sqlStatement, previousQuery, currentQuery)
}
/**
 * Get Common Business Info
 * @param currentPeriodRevenueBreakdownTable
 * @param previousPeriodRevenueBreakdownTable
 * @return
 */
protected List getCommonBusinessInfoFromQuery(DatamartContext.Query previousQuery, DatamartContext.Query currentQuery) {
  def queryUtils = libs.CustomerInsights.QueryUtils

  if (!previousQuery || !currentQuery) {
    return null
  }

  String sqlStatement = """SELECT   T1.customerId     AS 'customerId', 
                                    T1.productId      AS 'productId',
                                    T1.revenue        AS 'previousRevenue', 
                                    T1.quantity       AS 'previousQuantity', 
                                    T1.revenuePerUnit AS 'previousRevenuePerUnit', 
                                    T2.revenue        AS 'currentRevenue', 
                                    T2.quantity       AS 'currentQuantity', 
                                    T2.revenuePerUnit AS 'currentRevenuePerUnit'
                           FROM     T1 INNER JOIN T2 
                                   ON T1.productId=T2.productId AND T1.customerId=T2.customerId
                           ORDER BY T1.customerId, T1.productId """

  return queryUtils.getFullQueryData(sqlStatement, previousQuery, currentQuery)
}

protected BigDecimal getPriceEffect(List commonBusiness) {
  BigDecimal priceEffect = commonBusiness?.sum {
    ((it.currentRevenuePerUnit ?: 0.0) - (it.previousRevenuePerUnit ?: 0.0)) * (it.currentQuantity ?: 0.0)
  }

  return priceEffect ?: BigDecimal.ZERO
}

protected BigDecimal getVolumeEffect(List commonBusiness) {
  BigDecimal volumeEffect = commonBusiness?.sum {
    if (it.previousQuantity) {
      return ((it.currentQuantity ?: 0.0) - (it.previousQuantity ?: 0.0)) * (it.previousRevenue ?: 0.0) / (it.previousQuantity)
    } else {
      return BigDecimal.ZERO
    }
  }

  return volumeEffect ?: BigDecimal.ZERO
}

protected BigDecimal getPortfolioMixEffect(List commonBusiness) {
  BigDecimal totalCurrentQuantity = BigDecimal.ZERO, totalPreviousQuantity = BigDecimal.ZERO
  commonBusiness.each {
    totalCurrentQuantity += (it.currentQuantity ?: BigDecimal.ZERO)
    totalPreviousQuantity += (it.previousQuantity ?: BigDecimal.ZERO)
  }

  BigDecimal mixEffect, currentQuantityPercent, previousQuantityPercent
  if (totalCurrentQuantity && totalPreviousQuantity) {
    mixEffect = commonBusiness.sum {
      currentQuantityPercent = (it.currentQuantity ?: BigDecimal.ZERO) / totalCurrentQuantity
      previousQuantityPercent = (it.PreviousQuantity ?: BigDecimal.ZERO) / totalPreviousQuantity
      (it.PreviousRevenuePerUnit ?: BigDecimal.ZERO) * (currentQuantityPercent - previousQuantityPercent)
    }
    mixEffect *= totalCurrentQuantity
  }

  return mixEffect ?: BigDecimal.ZERO
}