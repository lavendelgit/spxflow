/**
 * init new instant detail view dashboard parameter
 * @param customerId
 * @param timeFilter
 * @param currency
 * @param dimension
 * @param period
 * @param currentDate
 * @param productClassByQuantities
 * @param productClassByHealthScores
 * @return
 */
Map newDetailViewDashboardParam(String customerId,
                                String timeFilter,
                                Map currency,
                                Map dimension,
                                Map period,
                                Date currentDate,
                                List productClassByQuantities,
                                List productClassByHealthScores) {
  Map detailViewParam = libs.CustomerInsights.CommonDashboardParameter.commonDashboardParam(timeFilter, currency, period, currentDate)
  detailViewParam << [
      getCustomerId                       : { return customerId },
      getDimension                        : { return dimension },
      getProductClassByQuantities         : { return productClassByQuantities },
      getProductClassByHealthScores       : { return productClassByHealthScores },
      getBasicQuerySimulationFilter       : { Map _configuration ->
        return generateBasicQuerySimulationFilter(_configuration, detailViewParam)
      },
      getCustomPeriodQuerySimulationFilter: { Map _configuration, Date _startDate, Date _endDate ->
        return generateCustomPeriodQuerySimulationFilter(_configuration, _startDate, _endDate, detailViewParam)
      },
      getBasicQueryDatamartFilter         : { Map _configuration ->
        return generateBasicQueryDatamartFilter(_configuration, detailViewParam)
      },
      getCustomPeriodQueryDatamartFilter  : { Map _configuration, Date _startDate, Date _endDate ->
        return generateCustomPeriodQueryDatamartFilter(_configuration, _startDate, _endDate, detailViewParam)
      }
  ]

  return detailViewParam
}

//**********************************************************
protected List generateBasicQuerySimulationFilter(Map configuration, Map detailViewParam) {
  return generateCustomPeriodQuerySimulationFilter(configuration, detailViewParam.getStartDate(), detailViewParam.getEndDate(), detailViewParam)
}

protected List generateCustomPeriodQuerySimulationFilter(Map configuration, Date startDate, Date endDate, Map detailViewParam) {
  List<Filter> filters = []

  filters.addAll(buildCustomerSimulationFilter(configuration, detailViewParam.getCustomerId()))
  filters.addAll(detailViewParam.getPeriodSimulationFilter(configuration, startDate, endDate))
  filters.addAll(buildDimensionSimulationFilter(detailViewParam.getDimension()))
  filters.addAll(buildProductClassSimulationFilter(configuration,
      detailViewParam.getProductClassByQuantities(),
      detailViewParam.getProductClassByHealthScores()))

  return filters
}

protected List buildCustomerSimulationFilter(Map configuration, String customerId) {
  return customerId ? [Filter.equal(configuration.CustomerIDFieldName, customerId)] : []
}

protected List buildDimensionSimulationFilter(Map dimension) {
  return dimension ? [Filter.equal(dimension.FieldName, dimension.Value)] : []
}

protected List buildProductClassSimulationFilter(Map configuration, List productClassByQuantities, List productClassByHealthScores) {
  List filters = []

  if (productClassByQuantities) {
    filters.add(Filter.in(configuration.ProductClassificationByQuantityFieldName, productClassByQuantities))
  }

  if (productClassByHealthScores) {
    filters.add(Filter.in(configuration.ProductClassificationByHealthScoreFieldName, productClassByHealthScores))
  }

  return filters
}
//**********************************************************
protected List generateBasicQueryDatamartFilter(Map configuration, Map detailViewParam) {
  return generateCustomPeriodQueryDatamartFilter(configuration, detailViewParam.getStartDate(), detailViewParam.getEndDate(), detailViewParam)
}

protected List generateCustomPeriodQueryDatamartFilter(Map configuration, Date startDate, Date endDate, Map detailViewParam) {
  List<Filter> filters = []

  filters.addAll(detailViewParam.getPeriodDatamartFilter(configuration, startDate, endDate))
  filters.addAll(buildDimensionDatamartFilter(configuration, detailViewParam))
  filters.addAll(buildCustomerDatamartFilter(configuration, detailViewParam.getCustomerId()))
  filters.addAll(buildProductClassDatamartFilter(configuration, detailViewParam))

  return filters
}

protected List buildDimensionDatamartFilter(Map configuration, Map detailViewParam) {
  def calculationUtil = libs.CustomerInsights.CalculationCommonUtils

  Map originalSource = configuration.OriginalSource
  List<Filter> filters = []

  if (detailViewParam.getDimension()?.FieldName) { // in case dimension is selected
    Map customerAndProducts = calculationUtil.getCustomerAndProductBaseOnInput(configuration, detailViewParam.getBasicQuerySimulationFilter(configuration))

    if (customerAndProducts.customerIds != null) { //will process with []
      filters.add(Filter.in(originalSource.CustomerIDFieldName, customerAndProducts.customerIds))
    }

    if (customerAndProducts.productIds != null) { //will process with []
      filters.add(Filter.in(originalSource.ProductIDFieldName, customerAndProducts.productIds))
    }
  }

  return filters
}

protected List buildCustomerDatamartFilter(Map configuration, String customerId) {
  Map originalSource = configuration.OriginalSource

  return customerId ? [Filter.equal(originalSource.CustomerIDFieldName, customerId)] : []
}

protected List buildProductClassDatamartFilter(Map configuration, Map detailViewParam) {
  def calculationUtil = libs.CustomerInsights.CalculationCommonUtils

  Map originalSource = configuration.OriginalSource
  List<Filter> filters = []

  if (detailViewParam.getProductClassByQuantities() || detailViewParam.getProductClassByHealthScores()) {
    Map customerAndProducts = calculationUtil.getCustomerAndProductBaseOnInput(configuration, detailViewParam.getBasicQuerySimulationFilter(configuration))
    filters.add(Filter.in(originalSource.ProductIDFieldName, customerAndProducts.productIds))
  }

  return filters
}