/**
 * init new instant customer portfolio dashboard parameter
 * @param customerId
 * @param productAttribute
 * @param topProduct
 * @param timeFilter
 * @param currency
 * @param period
 * @param currentDate
 * @param productClassByQuantities
 * @param productClassByHealthScores
 * @return
 */
Map newPortfolioDashboardParam(String customerId,
                               String productAttribute,
                               Integer topProduct,
                               String timeFilter,
                               Map currency,
                               Map period,
                               Date currentDate,
                               List productClassByQuantities,
                               List productClassByHealthScores) {
  Map portfolioParam = libs.CustomerInsights.CommonDashboardParameter.commonDashboardParam(timeFilter, currency, period, currentDate)
  portfolioParam << [
      getCustomerId                         : { return customerId },
      getTopProduct                         : { return topProduct },
      getProductAttribute                   : { return productAttribute },
      getProductClassByQuantities           : { return productClassByQuantities },
      getProductClassByHealthScores         : { return productClassByHealthScores },
      getValueOnSegmentQuerySimulationFilter: { Map _configuration, String segment ->
        return generateValueOnSegmentQuerySimulationFilter(_configuration, segment, portfolioParam)
      },
      getBasicQuerySimulationFilter         : { Map _configuration ->
        return generateBasicQuerySimulationFilter(_configuration, portfolioParam)
      }
  ]

  return portfolioParam
}

//**********************************************************
protected List generateValueOnSegmentQuerySimulationFilter(Map configuration, String customerSegment, Map portfolioParam) {
  List<Filter> filters = []

  filters.addAll(portfolioParam.getPeriodSimulationFilter(configuration, portfolioParam.getStartDate(), portfolioParam.getEndDate()))
  filters.addAll(buildProductClassSimulationFilter(configuration,
      portfolioParam.getProductClassByQuantities(),
      portfolioParam.getProductClassByHealthScores()))
  filters.addAll(buildCustomerSegmentSimulationFilter(configuration, customerSegment))

  return filters
}

protected List generateBasicQuerySimulationFilter(Map configuration, Map portfolioParam) {
  List<Filter> filters = []

  filters.addAll(buildCustomerSimulationFilter(configuration, portfolioParam.getCustomerId()))
  filters.addAll(portfolioParam.getPeriodSimulationFilter(configuration, portfolioParam.getStartDate(), portfolioParam.getEndDate()))
  filters.addAll(buildProductClassSimulationFilter(configuration,
      portfolioParam.getProductClassByQuantities(),
      portfolioParam.getProductClassByHealthScores()))

  return filters
}

protected List buildCustomerSimulationFilter(Map configuration, String customerId) {
  return customerId ? [Filter.equal(configuration.CustomerIDFieldName, customerId)] : []
}

protected List buildProductClassSimulationFilter(Map configuration, List productClassByQuantities, List productClassByHealthScores) {
  List filters = []

  if (productClassByQuantities) {
    filters.add(Filter.in(configuration.ProductClassificationByQuantityFieldName, productClassByQuantities))
  }

  if (productClassByHealthScores) {
    filters.add(Filter.in(configuration.ProductClassificationByHealthScoreFieldName, productClassByHealthScores))
  }

  return filters
}

protected List buildCustomerSegmentSimulationFilter(Map configuration, String customerSegment) {
  return customerSegment ? [Filter.equal(configuration.CustomerSegmentFieldName, customerSegment)] : []
}