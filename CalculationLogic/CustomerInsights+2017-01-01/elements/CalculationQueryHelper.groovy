import net.pricefx.formulaengine.DatamartContext

/*
   Create Queries for Calculation element
   Source: Simulation/Datamart
 */
/**
 * create Query object to query some customer data from Simulation
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValuePerCustomer(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map resultFields = [customerId                         : configuration.CustomerIDFieldName,
                      customerName                       : configuration.CustomerNameFieldName,
                      revenue                            : queryUtils.createSQLForSum(configuration.InvoicePriceFieldName),
                      margin                             : queryUtils.createSQLForSum(configuration.MarginFieldName),
                      quantity                           : queryUtils.createSQLForSum(configuration.QuantityFieldName),
                      avgMarginPercent                   : "${queryUtils.createSQLForSum(configuration.MarginFieldName)} / ${queryUtils.createSQLForSum(configuration.InvoicePriceFieldName)}",
                      customerHealthScore                : queryUtils.createSQLForAverage(configuration.CustomerHealthScoreFieldName),
                      customerRevenueTrend               : queryUtils.createSQLForAverage(configuration.CustomerRevenueTrendFieldName),
                      customerMarginTrend                : queryUtils.createSQLForAverage(configuration.CustomerMarginTrendFieldName),
                      customerQuantityTrend              : queryUtils.createSQLForAverage(configuration.CustomerQuantityTrendFieldName),
                      customerYTDRevenueTrend            : queryUtils.createSQLForAverage(configuration.CustomerYTDRevenueTrendFieldName),
                      customerYTDMarginTrend             : queryUtils.createSQLForAverage(configuration.CustomerYTDMarginTrendFieldName),
                      customerYTDQuantityTrend           : queryUtils.createSQLForAverage(configuration.CustomerYTDQuantityTrendFieldName),
                      customerClassificationByRevenue    : configuration.CustomerClassificationByRevenueFieldName,
                      customerClassificationByHealthScore: configuration.CustomerClassificationByHealthScoreFieldName,
                      customerSegment                    : configuration.CustomerSegmentFieldName,
                      transactionAmount                  : queryUtils.createSQLForSum(configuration.TransactionAmountFieldName),
                      buyingProductAmount                : "COUNT_DISTINCT(${configuration.ProductIDFieldName})"]

  List orderBy = ["revenue DESC"]

  return buildQuery(configuration, queryFilters, resultFields, orderBy, true)
}
/**
 * create Query to count buying products
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForCountBuyingProduct(Map configuration, List queryFilters) {
  Map resultFields = [buyingProductAmount: "COUNT_DISTINCT(${configuration.ProductIDFieldName})"]

  return buildQuery(configuration, queryFilters, resultFields, null, true)
}
/**
 * create Query to Sum some values Per product
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValuePerProduct(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map resultFields = [productId                           : configuration.ProductIDFieldName,
                      revenue                             : queryUtils.createSQLForSum(configuration.InvoicePriceFieldName),
                      margin                              : queryUtils.createSQLForSum(configuration.MarginFieldName),
                      quantity                            : queryUtils.createSQLForSum(configuration.QuantityFieldName),
                      transactionAmount                   : queryUtils.createSQLForSum(configuration.TransactionAmountFieldName),
                      productClassificationByQuantity     : configuration.ProductClassificationByQuantityFieldName,
                      productClassificationByMarginPercent: configuration.ProductClassificationByMarginPercentFieldName]
  List orderBy = [configuration.ProductIDFieldName]

  return buildQuery(configuration, queryFilters, resultFields, orderBy, true)
}
/**
 * create Query to Sum some values on each Month Per Customer
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValueOnEachMonthPerCustomer(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map resultFields = [customerId       : configuration.CustomerIDFieldName,
                      customerName     : configuration.CustomerNameFieldName,
                      customerSegment  : configuration.CustomerSegmentFieldName,
                      month            : configuration.PricingMonthFieldName,
                      revenue          : queryUtils.createSQLForSum(configuration.InvoicePriceFieldName),
                      margin           : queryUtils.createSQLForSum(configuration.MarginFieldName),
                      quantity         : queryUtils.createSQLForSum(configuration.QuantityFieldName),
                      transactionAmount: queryUtils.createSQLForSum(configuration.TransactionAmountFieldName)]
  List orderBy = [configuration.CustomerIDFieldName, configuration.PricingMonthFieldName]

  return buildQuery(configuration, queryFilters, resultFields, orderBy, true)
}
/**
 * create Query to Sum some values on Per Product And Customer
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValuePerCustomerAndProduct(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map resultFields = [customerId                          : configuration.CustomerIDFieldName,
                      productId                           : configuration.ProductIDFieldName,
                      customerSegment                     : configuration.CustomerSegmentFieldName,
                      productClassificationByMarginPercent: configuration.ProductClassificationByMarginPercentFieldName,
                      revenue                             : queryUtils.createSQLForSum(configuration.InvoicePriceFieldName),
                      quantity                            : queryUtils.createSQLForSum(configuration.QuantityFieldName),
                      revenuePerUnit                      : "${queryUtils.createSQLForSum(configuration.InvoicePriceFieldName)}/${queryUtils.createSQLForSum(configuration.QuantityFieldName)}",
  ]
  List orderBy = [configuration.CustomerIDFieldName, configuration.ProductIDFieldName]

  return buildQuery(configuration, queryFilters, resultFields, orderBy, true)
}
/**
 * create Query to Sum some values Per product and Customer Class
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValuePerProductAndCustomerClass(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map resultFields = [productId                           : configuration.ProductIDFieldName,
                      productName                         : configuration.ProductNameFieldName,
                      customerClassificationByRevenue     : configuration.CustomerClassificationByRevenueFieldName,
                      avgPriceByCustomerClass             : queryUtils.createSQLForAverage(configuration.ProductAvgPriceByCustomerClassFieldName),
                      overallPrice                        : queryUtils.createSQLForAverage(configuration.ProductOverallPriceFieldName),
                      productClassificationByMarginPercent: configuration.ProductClassificationByMarginPercentFieldName,
                      revenue                             : queryUtils.createSQLForSum(configuration.InvoicePriceFieldName),
                      margin                              : queryUtils.createSQLForSum(configuration.MarginFieldName),
                      marginPercent                       : "${queryUtils.createSQLForSum(configuration.MarginFieldName)}/${queryUtils.createSQLForSum(configuration.InvoicePriceFieldName)}",
                      quantity                            : queryUtils.createSQLForSum(configuration.QuantityFieldName)]

  return buildQuery(configuration, queryFilters, resultFields, null, true)
}
/**
 * create Query to get some Info per Product
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForGetInfoPerProduct(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map resultFields = [productId         : configuration.ProductIDFieldName,
                      productName       : configuration.ProductNameFieldName,
                      revenue           : queryUtils.createSQLForSum(configuration.InvoicePriceFieldName),
                      margin            : queryUtils.createSQLForSum(configuration.MarginFieldName),
                      marginPercent     : "${queryUtils.createSQLForSum(configuration.MarginFieldName)}/${queryUtils.createSQLForSum(configuration.InvoicePriceFieldName)}",
                      quantity          : queryUtils.createSQLForSum(configuration.QuantityFieldName),
                      revenueTrend      : queryUtils.createSQLForAverage(configuration.ProductAndCustomerRevenueTrendFieldName),
                      marginTrend       : queryUtils.createSQLForAverage(configuration.ProductAndCustomerMarginTrendFieldName),
                      quantityTrend     : queryUtils.createSQLForAverage(configuration.ProductAndCustomerQuantityTrendFieldName),
                      revenueTrendYTD   : queryUtils.createSQLForAverage(configuration.ProductAndCustomerYTDRevenueTrendFieldName),
                      marginTrendYTD    : queryUtils.createSQLForAverage(configuration.ProductAndCustomerYTDMarginTrendFieldName),
                      quantityTrendYTD  : queryUtils.createSQLForAverage(configuration.ProductAndCustomerYTDQuantityTrendFieldName),
                      avgPriceByCustomer: queryUtils.createSQLForAverage(configuration.ProductAvgPriceByCustomerFieldName),
                      productHealthScore: queryUtils.createSQLForAverage(configuration.ProductHealthScoreFieldName)]

  return buildQuery(configuration, queryFilters, resultFields, null, true)
}
/**
 * create Query to get value of Product attribute
 * @param configuration
 * @param productAttribute
 * @return
 */
DatamartContext.Query createQueryForGetProductAttribute(Map configuration, String productAttribute) {
  Map productSourceMapping = libs.CustomerInsights.ProductUtils.getSourceFieldMappingOfProductDataSource()
  String attribute = productSourceMapping?.get(productAttribute)?.getAt(0)?.name
  DatamartContext datamartContext = api.getDatamartContext()
  def datasource = datamartContext.getDataSource("Product") // default get from DM
  DatamartContext.Query query = datamartContext.newQuery(datasource, false)
  String productIdFieldName = configuration.OriginalSource.ProductIDFieldName
  query.select(productIdFieldName, "productId")
  if (attribute) {
    String unknown = libs.CustomerInsights.Constant.DEFAULT_VALUE.UNKNOWN
    query.select("IFNULL(${attribute},'${unknown}')", "productAttribute")
  }

  return query
}
/**
 * create Query to summary values from original source (datamart)
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSummaryValuesFromOriginalSource(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map originalSource = configuration.OriginalSource
  Map resultFields = [revenue : queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName),
                      margin  : queryUtils.createSQLForSum(originalSource.MarginFieldName),
                      quantity: queryUtils.createSQLForSum(originalSource.QuantityFieldName)]

  return buildQueryForOriginalSource(configuration, queryFilters, resultFields, null, true)
}
/**
 * create Query to summary values on each period (quarter/month) from original source (datamart)
 * @param configuration
 * @param queryFilters
 * @param periodName
 * @return
 */
DatamartContext.Query createQueryForSummaryValuesOnEachPeriodFromOriginalSource(Map configuration, List queryFilters, String periodName) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map originalSource = configuration.OriginalSource
  String pricingDateFieldName = originalSource.PricingDateFieldName
  String localPeriodName = periodName ?: libs.CustomerInsights.Constant.TIME_PERIOD.MONTH
  String periodFieldName = pricingDateFieldName + localPeriodName

  Map resultFields = [(localPeriodName.toLowerCase()): periodFieldName,
                      revenue                        : queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName),
                      margin                         : queryUtils.createSQLForSum(originalSource.MarginFieldName),
                      quantity                       : queryUtils.createSQLForSum(originalSource.QuantityFieldName)]

  return buildQueryForOriginalSource(configuration, queryFilters, resultFields, null, true)
}
/**
 * build a query based on params input with source = Simulation
 * @param configuration
 * @param queryFilters
 * @param resultFields
 * @param orderBy
 * @param rollup
 * @return
 */
protected DatamartContext.Query buildQuery(Map configuration, List queryFilters, Map resultFields, List orderBy, boolean rollup = false) {
  String sourceType = configuration.SourceType
  String sourceName = configuration.SourceName

  return libs.CustomerInsights.QueryUtils.getContextQuery(sourceType, sourceName, resultFields, rollup, orderBy, *queryFilters)
}
/**
 * build a query based on dashboard params input with source = datamart
 * @param configuration
 * @param queryFilters
 * @param resultFields
 * @param orderBy
 * @param rollup
 * @return
 */
protected DatamartContext.Query buildQueryForOriginalSource(Map configuration, List queryFilters, Map resultFields, List orderBy, boolean rollup = false) {
  Map originalSource = configuration.OriginalSource
  String sourceType = originalSource.SourceType
  String sourceName = originalSource.SourceName

  return libs.CustomerInsights.QueryUtils.getContextQuery(sourceType, sourceName, resultFields, rollup, orderBy, *queryFilters)
}
