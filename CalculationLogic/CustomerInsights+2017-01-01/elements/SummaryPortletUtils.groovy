Map getSummaryOpportunities(List data) {
  def roundingUtil = libs.SharedLib.RoundingUtils
  if (!data) {
    return [:]
  }

  BigDecimal pricingOpportunity = BigDecimal.ZERO, sellingOpportunity = BigDecimal.ZERO
  BigDecimal upSell, crossSell
  for (item in data) {
    pricingOpportunity += (item.revenueBelowTarget ?: BigDecimal.ZERO)
    upSell = roundingUtil.round((item.upSell ?: BigDecimal.ZERO), 0)
    crossSell = roundingUtil.round((item.crossSell ?: BigDecimal.ZERO), 0)
    sellingOpportunity += (upSell + crossSell)
  }

  return [pricingOpportunity: pricingOpportunity,
          sellingOpportunity: sellingOpportunity,
          opportunity       : (pricingOpportunity + sellingOpportunity)]
}

def createControllerWithHtmlTable(Map configuration, def inputParams) {
  Map localParam
  if (inputParams instanceof List) {
    localParam = mappingTableData(inputParams)
  } else {
    localParam = inputParams
  }
  String html = generateHtmlTable(configuration, localParam)
  def controller = api.newController()
  controller.addHTML(html)

  return controller
}

String generateHtmlTable(Map configuration, Map inputParams) {
  String labelStyle = libs.CustomerInsights.Constant.SUMMARY_PORTLET_LABEL.STYLE
  Map generalSection = getDataGeneralSectionWithFormat(configuration, inputParams.general)
  Map column01 = getDataColumnWithFormat(configuration, inputParams.column01)
  Map column02 = getDataColumnWithFormat(configuration, inputParams.column02)
  Map column03 = getDataColumnWithFormat(configuration, inputParams.column03)

  String html = """
<table style='width:100%;max-width:950px'>
       <tr>
          <td>
             <div style='${labelStyle}'>${generalSection.customer.label}</div>
          </td>
          <td colspan='6' style='word-wrap:break-word; white-space:normal'>
             <div style='${getValueHTMLStyle(null, true, true)}'>
                  ${generalSection.customer.name}
             </div>
          </td>
       </tr>
       <tr>
          <td>
             <div style='${labelStyle}'>${generalSection.category.label}</div>
          </td>
          <td colspan='6'>
             <div style='${getValueHTMLStyle(null, false, true)}'>
                  ${generalSection.category.name}
             </div>
          </td>
       </tr>
       <tr>
          <td>
             <div style='${labelStyle}'>${generalSection.customerSegment.label}</div>
          </td>
          <td colspan='6'>
             <div style='${getValueHTMLStyle(null, false, true)}'>
                  ${generalSection.customerSegment.name}
             </div>
          </td>
       </tr>
       <tr>
          <td>
             <div style='${labelStyle}'>${generalSection.healthScore.label}</div>
          </td>
          <td>
             <div style='${getValueHTMLStyle(generalSection.healthScore.format.color, true, false)}'>
                   ${generalSection.healthScore.format.value}
             </div>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
       </tr>
       <tr style='height:14px'></tr>
       <tr>
          <td>
             <div style='${labelStyle}'>${column01.row01.label}</div>
          </td>
          <td>
             <div style='${getValueHTMLStyle(column01.row01.format.color, true, false)}'>
                  ${column01.row01.format.value}
             </div>
          </td>
          <td></td>
          <td>
             <div style='${labelStyle}'>${column02.row01.label}</div>
          </td>
          <td>
             <div style='${getValueHTMLStyle(column02.row01.format.color, true, false)}'>
                  ${column02.row01.format.value}
             </div>
          </td>
          <td></td>
          <td>
             <div style='${labelStyle}'>${column03.row01.label}</div>
          </td>
          <td>
             <div style='${getValueHTMLStyle(column03.row01.format.color, true, false)}'>
                  ${column03.row01.format.value}
             </div>
          </td>
       </tr>
       <tr>
          <td>
             <div style='${labelStyle}'>${column01.row02.label}</div>
          </td>
          <td>
             <div style='${getValueHTMLStyle(column01.row02.format.color, true, false)}'>
                  ${column01.row02.format.value}
             </div>
          </td>
          <td></td>
          <td>
             <div style='${labelStyle}'>${column02.row02.label}</div>
          </td>
          <td>
             <div style='${getValueHTMLStyle(column02.row02.format.color, true, false)}'>
                  ${column02.row02.format.value}
             </div>
          </td>
          <td></td>
          <td>
             <div style='${labelStyle}'>${column03.row02.label}</div>
          </td>
          <td>
             <div style='${getValueHTMLStyle(column03.row02.format.color, true, false)}'>
                  ${column03.row02.format.value}
             </div>
          </td>
       </tr>
       <tr>
          <td style='width:20%'>
             <div style='${labelStyle}'>${column01.row03.label}</div>
          </td>
          <td style='width:7%;max-width:80px'>
             <div style='${getValueHTMLStyle(column01.row03.format.color, true, false)}'>
                  ${column01.row03.format.value}
             </div>
          </td>
          <td style='width:8%'></td>
          <td style='width:24%'>
             <div style='${labelStyle}'>${column02.row03.label}</div>
          </td>
          <td style='width:7%;max-width:80px'>
             <div style='${getValueHTMLStyle(column02.row03.format.color, true, false)}'>
                  ${column02.row03.format.value}
             </div>
          </td>
          <td style='width:8%'></td>
          <td style='width:18%'>
             <div style='${labelStyle}'>${column03.row03.label}</div>
          </td>
          <td style='width:8%;max-width:80px'>
             <div style='${getValueHTMLStyle(column03.row03.format.color, true, false)}'>
                  ${column03.row03.format.value}
             </div>
          </td>
       </tr>
       <tr style='height:14px'></tr>
</table>"""

  return html
}

String makeMultiCustomerLabel(List customers) {
  Map summaryPortletLabel = libs.CustomerInsights.Constant.SUMMARY_PORTLET_LABEL
  String label = ""
  Integer customerSize = customers?.size() ?: 0
  Integer currentIndex = 0
  Map customer
  String suffix
  while ((label.length() < summaryPortletLabel.MAX_LENGTH) && currentIndex < customerSize) {
    customer = customers.getAt(currentIndex)
    suffix = currentIndex == (customerSize - 1) ? "" : ", "
    label += makeSingleCustomerLabel(customer.customerId, customer.customerName) + suffix
    currentIndex++
  }

  if (currentIndex < customerSize) {
    label += summaryPortletLabel.EXCESS_LABEL_SUFFIX
  }

  return label
}

String makeSingleCustomerLabel(String customerId, String customerName) {
  if (customerId == null || customerName == null) {
    return libs.CustomerInsights.Constant.SUMMARY_PORTLET_LABEL.EMPTY_VALUE
  }

  return "${customerId} (${customerName})"
}

Map initTableItem(String label, def value, boolean isFormattingRequired, boolean isTrend) {
  return [label               : label,
          value               : value,
          isFormattingRequired: isFormattingRequired,
          isTrend             : isTrend]
}

Map initHealthScoreItem(String label, BigDecimal value) {
  return initTableItem(label, value, true, false)
}

Map initTextItem(String label, String value) {
  return initTableItem(label, value, false, false)
}

Map initTrendItem(String label, BigDecimal value) {
  return initTableItem(label, value, true, true)
}

Map initNumberItem(String label, BigDecimal value) {
  return initTableItem(label, value, false, false)
}

protected Map getDataGeneralSectionWithFormat(Map configuration, Map generalData) {
  Map summaryPortletLabel = libs.CustomerInsights.Constant.SUMMARY_PORTLET_LABEL
  String customerLabel = generalData.customer.label
  String customerName = generalData.customer.value != null ? generalData.customer.value : summaryPortletLabel.EMPTY_VALUE
  String categoryLabel = generalData.category.label
  //just check null, empty is still shown
  String categoryName = generalData.category.value != null ? generalData.category.value : summaryPortletLabel.EMPTY_VALUE
  String customerSegmentName = generalData.customerSegment.value != null ? generalData.customerSegment.value : summaryPortletLabel.EMPTY_VALUE
  String customerSegmentLabel = generalData.customerSegment.label
  String healthScoreLabel = generalData.healthScore.label
  Map healthScoreFormat = getValueWithFormat(configuration,
      generalData.healthScore?.value,
      generalData.healthScore.isFormattingRequired,
      generalData.healthScore.isTrend)

  return [customer       : [label: customerLabel, name: customerName],
          category       : [label: categoryLabel, name: categoryName],
          customerSegment: [label: customerSegmentLabel, name: customerSegmentName],
          healthScore    : [label: healthScoreLabel, format: healthScoreFormat]]
}

protected Map getDataColumnWithFormat(Map configuration, Map columnData) {
  return [row01: getDataRowWithFormat(configuration, columnData.row01),
          row02: getDataRowWithFormat(configuration, columnData.row02),
          row03: getDataRowWithFormat(configuration, columnData.row03)]
}

protected Map getDataRowWithFormat(Map configuration, Map rowData) {
  String rowLabel = rowData.label
  Map rowFormat = getValueWithFormat(configuration, rowData.value, rowData.isFormattingRequired, rowData.isTrend)

  return [label: rowLabel, format: rowFormat]
}

protected Map getValueWithFormat(Map configuration, BigDecimal inputValue, boolean isFormattingRequired = false, boolean isTrend = false) {
  def constant = libs.CustomerInsights.Constant
  def dataTable = libs.CustomerInsights.DataTable
  if (inputValue == null) {
    return [value: constant.SUMMARY_PORTLET_LABEL.EMPTY_VALUE, color: null]
  }
  //get threshold value
  BigDecimal trendUpperValue = configuration.CONFIGURATION.TREND_UPPER.value.getAt(0) as BigDecimal
  BigDecimal trendLowerValue = configuration.CONFIGURATION.TREND_LOWER.value.getAt(0) as BigDecimal
  BigDecimal healthScoreUpperValue = configuration.CONFIGURATION.CUSTOMER_HEALTH_SCORE_UPPER.value.getAt(0) as BigDecimal
  BigDecimal healthScoreLowerValue = configuration.CONFIGURATION.CUSTOMER_HEALTH_SCORE_LOWER.value.getAt(0) as BigDecimal
  //get format values
  Map format
  String value, color

  if (!isFormattingRequired) {
    value = formatValueWithoutSymbol(inputValue)
    color = null
  } else if (isTrend) {
    format = dataTable.getFormatBasedOnRangeValue(trendUpperValue, trendLowerValue, inputValue)
    value = format.symbol + dataTable.formatValueBaseType(inputValue, constant.VALUE_TYPE_CODE.PERCENT)
    color = format.textColor
  } else {
    format = dataTable.getFormatBasedOnRangeValue(healthScoreUpperValue, healthScoreLowerValue, inputValue)
    value = format.symbol + dataTable.formatValueBaseType(inputValue, constant.VALUE_TYPE_CODE.ABSOLUTE)
    color = format.textColor
  }

  return [value: value,
          color: color]
}

protected String formatValueWithoutSymbol(BigDecimal inputValue) {
  def constant = libs.CustomerInsights.Constant
  def roundingUtils = libs.SharedLib.RoundingUtils
  Integer decimalPlaces = libs.CustomerInsights.Constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_00

  return api.formatNumber(constant.FORMAT.NON_DECIMAL, roundingUtils.round(inputValue, decimalPlaces))
}

protected String getValueHTMLStyle(String color, boolean isBold = false, boolean isLeftAlign = false) {
  String align = isLeftAlign ? "left" : "right"
  String fontWeight = isBold ? "bold" : "normal"

  return "color:${color};font-weight:${fontWeight};font-size:14px;text-align:${align}"
}

protected Map mappingTableData(List inputTableData) {
  Map tableData = [:]
  //general section
  Map general = [:]
  general.customer = inputTableData.getAt(0)
  general.category = inputTableData.getAt(1)
  general.customerSegment = inputTableData.getAt(2)
  general.healthScore = inputTableData.getAt(3)
  tableData.general = general
  //column-01
  Map column01 = [:]
  column01.row01 = value = inputTableData.getAt(4)
  column01.row02 = value = inputTableData.getAt(5)
  column01.row03 = value = inputTableData.getAt(6)
  tableData.column01 = column01
  //column-02
  Map column02 = [:]
  column02.row01 = value = inputTableData.getAt(7)
  column02.row02 = value = inputTableData.getAt(8)
  column02.row03 = value = inputTableData.getAt(9)
  tableData.column02 = column02
  //column-03
  Map column03 = [:]
  column03.row01 = value = inputTableData.getAt(10)
  column03.row02 = value = inputTableData.getAt(11)
  column03.row03 = value = inputTableData.getAt(12)
  tableData.column03 = column03

  return tableData
}