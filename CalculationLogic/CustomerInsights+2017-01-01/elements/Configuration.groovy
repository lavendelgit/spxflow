import net.pricefx.formulaengine.DatamartContext

/**
 * Read data in AP with name ="customer-insights-accelerator" and push into a map
 * @return Map
 */
Map getAdvancedConfigurationOption() {
  def advancedConfigurationObj = readAPObject(libs.CustomerInsights.Constant.CID_AP_NAME)

  return putAPDataToMap(advancedConfigurationObj)
}

protected def readAPObject(String uniqueName) {
  def advancedConf = api.find("AP", Filter.equal("uniqueName", uniqueName))
  if (!advancedConf?.value) {
    api.addWarning("AP: ${uniqueName} does not exist")
    return null
  }
  String advancedConfigurationJSON = advancedConf.value.first()
  def advancedConfigurationValue
  try {
    advancedConfigurationValue = api.jsonDecode(advancedConfigurationJSON)
  } catch (exception) {
    api.addWarning("AP: ${uniqueName} is not in JSON format")
  }

  return advancedConfigurationValue
}

protected Map putAPDataToMap(Map advancedConfigurationObj) {
  Map timePeriod = libs.CustomerInsights.Constant.TIME_PERIOD

  if (!advancedConfigurationObj) {
    return [:]
  }

  Map configuration = [:]
  //DM Original Info
  Map originalSource = [:]
  originalSource.SourceType = advancedConfigurationObj.OriginalSourceType ?: null
  originalSource.SourceName = advancedConfigurationObj.OriginalSourceName ?: null
  originalSource.PricingDateFieldName = advancedConfigurationObj.OriginalPricingDateFieldName ?: null
  originalSource.CustomerIDFieldName = advancedConfigurationObj.OriginalCustomerIDFieldName ?: null
  originalSource.CustomerNameFieldName = advancedConfigurationObj.OriginalCustomerNameFieldName ?: null
  originalSource.ProductIDFieldName = advancedConfigurationObj.OriginalProductIDFieldName ?: null
  originalSource.ProductNameFieldName = advancedConfigurationObj.OriginalProductNameFieldName ?: null
  originalSource.InvoicePriceFieldName = advancedConfigurationObj.OriginalInvoicePriceFieldName ?: null
  originalSource.MarginFieldName = advancedConfigurationObj.OriginalMarginFieldName ?: null
  originalSource.QuantityFieldName = advancedConfigurationObj.OriginalQuantityFieldName ?: null
  configuration.OriginalSource = originalSource
  //--DMSIM info
  configuration.SourceType = advancedConfigurationObj.SourceType ?: null
  configuration.SourceName = advancedConfigurationObj.SourceName ?: null
  configuration.ProductIDFieldName = advancedConfigurationObj.ProductIDFieldName ?: originalSource.ProductIDFieldName
  configuration.ProductNameFieldName = advancedConfigurationObj.ProductNameFieldName ?: originalSource.ProductNameFieldName
  configuration.CustomerIDFieldName = advancedConfigurationObj.CustomerIDFieldName ?: originalSource.CustomerIDFieldName
  configuration.CustomerNameFieldName = advancedConfigurationObj.CustomerNameFieldName ?: originalSource.CustomerNameFieldName
  configuration.InvoicePriceFieldName = advancedConfigurationObj.InvoicePriceFieldName ?: null
  configuration.MarginFieldName = advancedConfigurationObj.MarginFieldName ?: null
  configuration.PricingDateFieldName = advancedConfigurationObj.PricingDateFieldName ?: originalSource.PricingDateFieldName
  configuration.QuantityFieldName = advancedConfigurationObj.QuantityFieldName ?: null
  configuration.TransactionAmountFieldName = advancedConfigurationObj.TransactionAmountFieldName ?: null
  //---add Pricing month/quarter/year
  if (configuration.PricingDateFieldName) {
    configuration.PricingMonthFieldName = configuration.PricingDateFieldName + timePeriod.MONTH
    configuration.PricingQuarterFieldName = configuration.PricingDateFieldName + timePeriod.QUARTER
    configuration.PricingYearFieldName = configuration.PricingDateFieldName + timePeriod.YEAR
  }

  return configuration
}

protected Map getSimulationMapping() {
  Map configuration = [:]
  configuration.CustomerHealthScoreFieldName = "Sim_CustomerHealthScore"
  configuration.CustomerClassificationByRevenueFieldName = "Sim_CustomerClassificationByRevenue"
  configuration.CustomerClassificationByHealthScoreFieldName = "Sim_CustomerClassificationByHealthScore"
  configuration.CustomerRevenueTrendFieldName = "Sim_CustomerRevenueTrend"
  configuration.CustomerMarginTrendFieldName = "Sim_CustomerMarginTrend"
  configuration.CustomerQuantityTrendFieldName = "Sim_CustomerQuantityTrend"
  configuration.CustomerYTDRevenueTrendFieldName = "Sim_CustomerYTDRevenueTrend"
  configuration.CustomerYTDMarginTrendFieldName = "Sim_CustomerYTDMarginTrend"
  configuration.CustomerYTDQuantityTrendFieldName = "Sim_CustomerYTDQuantityTrend"
  configuration.ProductAndCustomerRevenueTrendFieldName = "Sim_ProductAndCustomerRevenueTrend"
  configuration.ProductAndCustomerMarginTrendFieldName = "Sim_ProductAndCustomerMarginTrend"
  configuration.ProductAndCustomerQuantityTrendFieldName = "Sim_ProductAndCustomerQuantityTrend"
  configuration.ProductAndCustomerYTDRevenueTrendFieldName = "Sim_ProductAndCustomerYTDRevenueTrend"
  configuration.ProductAndCustomerYTDMarginTrendFieldName = "Sim_ProductAndCustomerYTDMarginTrend"
  configuration.ProductAndCustomerYTDQuantityTrendFieldName = "Sim_ProductAndCustomerYTDQuantityTrend"
  configuration.ProductAvgPriceByCustomerClassFieldName = "Sim_ProductAvgPriceByCustomerClassification"
  configuration.ProductAvgPriceByCustomerFieldName = "Sim_ProductAvgPriceByCustomer"
  configuration.ProductOverallPriceFieldName = "Sim_ProductOverallPrice"
  configuration.ProductClassificationByQuantityFieldName = "Sim_ProductClassificationByQuantity"
  configuration.ProductClassificationByMarginPercentFieldName = "Sim_ProductClassificationByMarginPercent"
  configuration.CustomerSegmentFieldName = "Sim_CustomerSegment"
  configuration.ProductHealthScoreFieldName = "Sim_ProductHealthScore"
  configuration.ProductClassificationByHealthScoreFieldName = "Sim_ProductClassificationByHealthScore"

  return configuration
}
/**
 *
 * @param DatamartName
 * @return Map
 */
Map getDatasourceFields(String sourceType, String sourceName, String owner, Boolean isDimension, Boolean isMakeLabelAsKey = false) {
  def dataSource = libs.CustomerInsights.QueryUtils.getDataSourceConnection(sourceType, sourceName)
  boolean isOwnerValid, isDimensionValid
  List sourceFields = dataSource?.fc()?.fields ?: []
  List filteredFields = sourceFields?.findAll {
    isOwnerValid = owner ? (it.owningFC?.toLowerCase() == owner.toLowerCase()) : true
    isDimensionValid = (isDimension == null) ? true : (it.dimension == isDimension)

    return (isOwnerValid && isDimensionValid)
  }

  Map fields = filteredFields.collectEntries { item ->
    return isMakeLabelAsKey ? [(item.label): item.name] : [(item.name): item.label]
  }

  return fields
}
/**
 * Get unique values of queryFieldName from data source
 * @param sourceType
 * @param sourceName
 * @param queryFieldName
 * @return
 */
List getDistinctValueOfField(String sourceType, String sourceName, String queryFieldName) {
  def dataSourceConnection = libs.CustomerInsights.QueryUtils.getDataSourceConnection(sourceType, sourceName)
  DatamartContext datamartContext = api.getDatamartContext()
  DatamartContext.Query query = datamartContext.newQuery(dataSourceConnection, false)
  query.select(queryFieldName, "Data")
  query.selectDistinct()

  return datamartContext.executeQuery(query)?.getData()?.value?.collect()
}
/**
 * Get all configuration data in PP PFXTemplate_CustomerInsights_Configuration
 * Convert data into Map structure
 * e.g: data in PP
 *          Category (Key1)       Key Name (Key2)        Value (Key3)     IsDefault(Attribute1)   Order(Attribute2)
 *          MASTER_DATA           TIME_FILTER            MTD               YES                    1
 *          MASTER_DATA           TIME_FILTER            QTD                                      2
 *          MASTER_DATA           TIME_FILTER            YTD                                      3
 *          PRODUCT_HEALTH        SPECIALITY             40                                       1
 *          PRODUCT_HEALTH        COMMODITY              0                                        2
 *      Result Map
 *         [ MASTER_DATA    : [ TIME_FILTER : [
 *                                              [VALUE: "MTD", IS_DEFAULT: "YES", ORDER: 1],
 *                                              [VALUE: "QTD", IS_DEFAULT: null, ORDER: 2],
 *                                              [VALUE: "YTD", IS_DEFAULT: null, ORDER: 3]
 *                                            ]
 *                            ],
 *           PRODUCT_HEALTH : [ SPECIALITY : [
 *                                              [VALUE: 40, IS_DEFAULT: null, ORDER: 1]
 *                                           ],
 *                              COMMODITY  : [
 *                                             [VALUE: 0, IS_DEFAULT: null, ORDER: 2]
 *                                           ]
 *                            ]
 *          ]
 *
 * @return Map
 */
Map getConfigurationDataFromPP() {
  def constant = libs.CustomerInsights.Constant
  Map columnConfig = constant.CONFIGURATION_PP_CONFIG.COLUMN_CONFIG
  String configTableName = constant.CONFIGURATION_PP_CONFIG.NAME
  List rawData = api.findLookupTableValues(configTableName)
  if (!rawData) {
    return [:]
  }

  rawData.sort(true, { it.getAt(columnConfig.ORDER) })
  Map categoryGroup = rawData.groupBy { it.getAt(columnConfig.CATEGORY) }
  Map nameGroup
  Map finalMap = [:], categoryMap
  for (category in categoryGroup) {
    nameGroup = category.value.groupBy { it.getAt(columnConfig.NAME) }
    categoryMap = createMapDataForLevel2(nameGroup)
    finalMap.put(category.key, categoryMap)
  }

  return finalMap
}

protected Map createMapDataForLevel2(Map groupedMap) {
  Map dataMap = [:], valueGroup
  String valueColumn = libs.CustomerInsights.Constant.CONFIGURATION_PP_CONFIG.COLUMN_CONFIG.VALUE
  for (groupedMapItem in groupedMap) {
    valueGroup = groupedMapItem.value.groupBy { it.getAt(valueColumn) }
    dataMap.put(groupedMapItem.key, createListOfMapDataForNodeLevel(valueGroup))
  }

  return dataMap
}

protected List createListOfMapDataForNodeLevel(Map groupedMap) {
  List data = []
  Map columnConfig = libs.CustomerInsights.Constant.CONFIGURATION_PP_CONFIG.COLUMN_CONFIG
  Integer order
  for (groupedMapItem in groupedMap) {
    order = (groupedMapItem.value.getAt(columnConfig.ORDER).getAt(0) as Integer) ?: Integer.MAX_VALUE
    data.add([value    : groupedMapItem.key,
              isDefault: groupedMapItem.value.getAt(columnConfig.IS_DEFAULT).getAt(0),
              order    : order,
              label    : groupedMapItem.value.getAt(columnConfig.LABEL).getAt(0)])
  }

  return data.sort { a, b -> a.order <=> b.order }
}

Map getAllConfigurationData() {
  if (api.global.CID_CONFIGURATION_DATA) {
    return api.global.CID_CONFIGURATION_DATA
  }

  Map ppConfigData = getConfigurationDataFromPP()
  Map apConfigData = getAdvancedConfigurationOption()
  Map simulationConfigData = getSimulationMapping()
  Map customerSegment = [customerSegments: getCustomerSegmentConfiguration()]
  api.global.CID_CONFIGURATION_DATA = (apConfigData + ppConfigData + simulationConfigData + customerSegment)

  return api.global.CID_CONFIGURATION_DATA
}

Integer getLastMonthAmountFromConfiguration(Map configuration) {
  Integer lastMonthAmountDefault = libs.CustomerInsights.Constant.DEFAULT_VALUE.LAST_MONTH_AMOUNT

  return (configuration.MASTER_DATA.LAST_MONTH_AMOUNT.getAt(0).value ?: lastMonthAmountDefault) as Integer
}

Integer getNextMonthAmountFromConfiguration(Map configuration) {
  Integer lastMonthAmountDefault = libs.CustomerInsights.Constant.DEFAULT_VALUE.NEXT_MONTH_AMOUNT

  return (configuration.MASTER_DATA.NEXT_MONTH_AMOUNT.getAt(0).value ?: lastMonthAmountDefault) as Integer
}
/**
 * Get base currency of datamart
 * @param datamartName
 * @return
 */
String getBaseCurrencyOfDatamart(String datamartName) {
  def datamart = api.find("DM", 0, 1, null, Filter.equal("uniqueName", datamartName))

  return datamart?.getAt(0)?.baseCcyCode
}
/**
 * get REVENUE_WEIGHT and MARGIN_WEIGHT value from PP
 * Rule: REVENUE_WEIGHT + MARGIN_WEIGHT = 1
 * in case REVENUE_WEIGHT is not null and MARGIN_WEIGHT is null -> calculate MARGIN_WEIGHT = 1 - REVENUE_WEIGHT
 * in case REVENUE_WEIGHT is null and MARGIN_WEIGHT is not null -> calculate REVENUE_WEIGHT = 1 - MARGIN_WEIGHT
 * in case REVENUE_WEIGHT is null and MARGIN_WEIGHT is null -> REVENUE_WEIGHT =  MARGIN_WEIGHT = 0.5
 * @param configuration
 * @return
 */
Map getWeightedFactorFromConfiguration(Map configuration) {
  BigDecimal defaultValue = libs.CustomerInsights.Constant.DEFAULT_VALUE.REVENUE_WEIGHT
  BigDecimal revenueWeighted = (configuration.CONFIGURATION?.REVENUE_WEIGHT?.getAt(0)?.value) as BigDecimal
  BigDecimal marginWeighted = (configuration.CONFIGURATION?.MARGIN_WEIGHT?.getAt(0)?.value) as BigDecimal

  if (!revenueWeighted && !marginWeighted) {
    revenueWeighted = defaultValue
    marginWeighted = defaultValue
  } else if (!revenueWeighted) {
    revenueWeighted = BigDecimal.ONE - marginWeighted
  } else if (!marginWeighted) {
    marginWeighted = BigDecimal.ONE - revenueWeighted
  }

  return [revenueWeighted: revenueWeighted, marginWeighted: marginWeighted]
}
/**
 * get Customer Segment Setting from PP
 * @return
 */
List getCustomerSegmentConfiguration() {
  Map segmentInfo = getCustomerSegmentAttributesFromPP()

  return groupByCustomerSegmentAttributeBySourceType(segmentInfo)
}

protected List groupByCustomerSegmentAttributeBySourceType(Map segmentInfo) {
  List segments = []
  Map customerSegment
  for (segmentInfoItem in segmentInfo) {
    customerSegment = buildCustomerSegmentAttribute(segmentInfoItem.value)

    if (customerSegment) {
      segments.add(customerSegment)
    }
  }

  return segments
}

protected Map buildCustomerSegmentAttribute(List segmentInfo) {
  if (!segmentInfo) {
    return null
  }

  List customerSegmentAttributes = []
  String sourceType = segmentInfo.getAt(0).sourceType
  String sourceName = segmentInfo.getAt(0).sourceName

  for (segmentInfoItem in segmentInfo) {
    customerSegmentAttributes << [sourceField: segmentInfoItem.sourceField, isCustomerId: segmentInfoItem.isCustomerId]
  }

  return [customerSegmentAttributes: customerSegmentAttributes,
          sourceType               : sourceType,
          sourceName               : sourceName]
}
/**
 * get rows in PP 'PFXTemplate_CustomerInsights_Customer_Segment' with isActive = YES
 * group attributes by source
 * @return
 */
protected Map getCustomerSegmentAttributesFromPP() {
  def constant = libs.CustomerInsights.Constant
  Map segmentPPConfig = constant.CUSTOMER_SEGMENT_PP_CONFIG.COLUMN_CONFIG
  String tableName = constant.CUSTOMER_SEGMENT_PP_CONFIG.NAME

  List rows = api.findLookupTableValues(tableName, Filter.equal(segmentPPConfig.IS_ACTIVE, constant.CUSTOMER_SEGMENT_PP_CONFIG.IS_TRUE))
  List segmentAttributes = []

  for (row in rows) {
    segmentAttributes.add([sourceType  : row.getAt(segmentPPConfig.SOURCE_TYPE),
                           sourceName  : row.getAt(segmentPPConfig.SOURCE_NAME),
                           sourceField : row.getAt(segmentPPConfig.SOURCE_FIELD),
                           isCustomerId: row.getAt(segmentPPConfig.IS_CUSTOMER_ID)])
  }

  return segmentAttributes.groupBy { (it.sourceType + it.sourceName) }
}

Map getConfigurationCurrencyData() {
  def currency = libs.CustomerInsights.Constant.DEFAULT_VALUE.CURRENCY

  return [currencyCode  : currency.CURRENCY_CODE,
          currencySymbol: currency.CURRENCY_SYMBOL]
}

Map getDatamartCurrencyData(String datamartName) {
  String currencyCode = getBaseCurrencyOfDatamart(datamartName)
  String currencySymbol = libs.SIP_Dashboards_Commons.CurrencyUtils.getCurrencySymbol(currencyCode)
  Map currency = [currencyCode  : currencyCode,
                  currencySymbol: currencySymbol]

  return currencyCode ? currency : getConfigurationCurrencyData()
}