import groovy.transform.Field
import net.pricefx.formulaengine.DatamartContext

/*
  Convert from filter to list of ProductId
 */
@Field String PRODUCT_DATA_SOURCE_NAME = "Product"

List convertProductGroupInputToListOfProductId(def productGroup) {
  if (!productGroup) {
    return null
  }

  Filter filter = (productGroup instanceof Filter) ? productGroup : productGroup.asFilter()
  List products = libs.SharedLib.StreamUtils.stream("P", "sku", ["sku"], [filter])

  return products?.sku
}
/**
 * Get map of attributes of product
 * @param isLabelKey : true: label is key of Map / false: fieldName is key of Map
 * @return
 */
Map getProductAttributes(Boolean isLabelKey = false) {
  List productAttributeMeta = api.find("PAM")
  Map productAtt = [:]
  Map dimensionAttributes = getSourceFieldsFromProductDataSource()
  List sourceFields = dimensionAttributes.keySet() as List
  Map labelTranslations, productAttributeEntry
  String label, fieldName
  for (attributeMeta in productAttributeMeta) {
    if (!sourceFields.contains(attributeMeta.fieldName)) {
      continue
    }

    labelTranslations = api.jsonDecode(attributeMeta.labelTranslations)
    label = labelTranslations.getAt("") ?: attributeMeta.label //empty key is default locate label
    fieldName = attributeMeta.fieldName
    productAttributeEntry = isLabelKey ? [(label): dimensionAttributes.getAt(fieldName)] : [(dimensionAttributes.getAt(fieldName)): label]
    productAtt << productAttributeEntry
  }

  return productAtt
}
/**
 * Get map of sourceField from Product datasource which dimension == true
 * @return
 */
Map getSourceFieldsFromProductDataSource() {
  List fields = getProductSourceFields(PRODUCT_DATA_SOURCE_NAME, true)

  return fields?.collectEntries { item -> [(item.sourceField): item.name] }
}

Map getSourceFieldMappingOfProductDataSource() {
  List fields = getProductSourceFields(PRODUCT_DATA_SOURCE_NAME, true)

  return fields?.groupBy { it.name }
}

List getProductSourceFields(String dataSourceName, boolean isDimension) {
  DatamartContext datamartContext = api.getDatamartContext()
  def dataSource = datamartContext.getDataSource(dataSourceName)
  List fieldList = dataSource?.fc()?.fields ?: []

  return isDimension ? (fieldList.findAll { it.dimension == true }) : fieldList
}