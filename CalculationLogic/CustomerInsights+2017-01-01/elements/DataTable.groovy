import net.pricefx.common.api.FieldFormatType
import net.pricefx.server.dto.calculation.ResultMatrix

/**
 * Set style for cell of ResultMatrix
 * @param matrix
 * @param value
 * @param type
 * @return
 */
ResultMatrix.ResultMatrixStyledCell setStyledCell(ResultMatrix matrix, String value, String type) {
  Map tableDataType = libs.CustomerInsights.Constant.TABLE_DATA_TYPE
  ResultMatrix.ResultMatrixStyledCell styledCell

  switch (type) {
    case tableDataType.TOP:
      styledCell = setStyledCellForUp(matrix, value)
      break
    case tableDataType.WORST:
      styledCell = setStyledCellForDown(matrix, value)
      break
    case tableDataType.BOLD:
      styledCell = setStyledCellBasedOnInputFormat(matrix, value, [weight: "bold"])
      break
    default:
      styledCell = setStyledCellBasedOnInputFormat(matrix, value, [:])
  }

  return styledCell
}

protected ResultMatrix.ResultMatrixStyledCell setStyledCellForUp(ResultMatrix matrix, String value) {
  def constant = libs.CustomerInsights.Constant

  return setStyledCellBasedOnFormat(matrix, constant.FORMAT.ARROW.UP, constant.FORMAT.COLOR.HIGH, value)
}

protected ResultMatrix.ResultMatrixStyledCell setStyledCellForDown(ResultMatrix matrix, String value) {
  def constant = libs.CustomerInsights.Constant

  return setStyledCellBasedOnFormat(matrix, constant.FORMAT.ARROW.DOWN, constant.FORMAT.COLOR.LOW, value)
}

protected ResultMatrix.ResultMatrixStyledCell setStyledCellBasedOnFormat(ResultMatrix matrix, String symbol, String textColor, String value) {
  return matrix.styledCell(symbol + " " + value, textColor, null, "bold")
}

protected ResultMatrix.ResultMatrixStyledCell setStyledCellBasedOnInputFormat(ResultMatrix matrix, String value, Map format) {
  return matrix.styledCell(value, format?.textColor, format?.bgColor, format?.weight)
}

/**
 * Set format cell based on a value with a range
 * @param matrix
 * @param upperValue
 * @param lowerValue
 * @param value
 * @param type
 * @return
 */
ResultMatrix.ResultMatrixStyledCell setStyledCellBasedOnRangeValue(ResultMatrix matrix, BigDecimal upperValue, BigDecimal lowerValue, BigDecimal value, String type = "ABSOLUTE") {
  if (value == null) {
    return null
  }

  Map format = getFormatBasedOnRangeValue(upperValue, lowerValue, value)
  String formatValue = formatValueBaseType(value, type)

  return setStyledCellBasedOnFormat(matrix, format.symbol, format.textColor, formatValue)
}

protected String formatValueBaseType(BigDecimal value, String type) {
  def roundingUtils = libs.SharedLib.RoundingUtils
  def constant = libs.CustomerInsights.Constant
  String valueFormat, numberFormat
  if (constant.VALUE_TYPE_CODE.PERCENT.equalsIgnoreCase(type)) {
    numberFormat = api.formatNumber(constant.FORMAT.DECIMAL, roundingUtils.round(100 * value, constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_02))
    valueFormat = value > 0 ? ('+' + numberFormat) : numberFormat
    valueFormat += "%"
  } else {
    numberFormat = api.formatNumber(constant.FORMAT.NON_DECIMAL, roundingUtils.round(value, constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_00))
    valueFormat = value > 0 ? ('+' + numberFormat) : numberFormat
  }

  return valueFormat
}
/**
 * Get top list and worst list from input list
 * return map with 3 elements:: Top list, Worst list and full list (input list)
 *    [ TOP: Top list,
 *      WORST: Worst list,
 *      ALL: input list ]
 * @param inputData
 * @param topValue
 * @return
 */
Map getTopAndWorst(List inputData, Integer topValue) {
  List topData = [], worstData = []
  Integer sizeOfData = inputData?.size() ?: 0

  if (sizeOfData == 0) {
    return [top  : [],
            worst: [],
            all  : []]
  }

  if (topValue >= sizeOfData) {
    topData = inputData
  } else if (sizeOfData <= (2 * topValue)) {
    topData = inputData.subList(0, topValue)
    worstData = inputData.subList(topValue, sizeOfData)
  } else {
    topData = inputData.subList(0, topValue)
    worstData = inputData.subList(sizeOfData - topValue, sizeOfData)
  }

  return [top  : topData,
          worst: worstData,
          all  : inputData]
}

ResultMatrix.ResultMatrixStyledCell setStyledCellByStyle(ResultMatrix matrix, String value, Map style) {
  return matrix.styledCell(value, style.textColor, style.bgColor, style.weight, style.alignment)
}

protected Map getFormatBasedOnRangeValue(BigDecimal upperValue, BigDecimal lowerValue, BigDecimal value) {
  if (value == null) {
    return null
  }

  def constant = libs.CustomerInsights.Constant
  def roundingUtils = libs.SharedLib.RoundingUtils
  Map upper = [symbol   : constant.FORMAT.ARROW.UP,
               textColor: constant.FORMAT.COLOR.HIGH]
  Map medium = [symbol   : constant.FORMAT.ARROW.HORIZONTAL,
                textColor: constant.FORMAT.COLOR.MEDIUM]
  Map lower = [symbol   : constant.FORMAT.ARROW.DOWN,
               textColor: constant.FORMAT.COLOR.LOW]

  Integer decimalPlaces = constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_03
  BigDecimal roundedValue = roundingUtils.round(value, decimalPlaces)
  BigDecimal roundedLowerValue = roundingUtils.round(lowerValue, decimalPlaces)
  BigDecimal roundedUpperValue = roundingUtils.round(upperValue, decimalPlaces)

  if (roundedValue <= roundedLowerValue) {
    return lower
  } else if (roundedValue >= roundedUpperValue) {
    return upper
  } else {
    return medium
  }

  return null
}
/**
 * metaData is a Map with structure
 *  [
 *   tableStructure : List of Map [name :'',label:'',formatType:'',formatRequired:true,thresholdValue: [upper:'',lower:''],dataAttributeName:''],
 *    preferenceName: '',
 *    embeddedDashboard: [eventName: '', columnValue:'', eventDataAttribute:''],
 *
 *    data: [topData: list of map data, worstData:list of map data]
 *  ]
 *
 * @param configuration
 * @param metaData
 * @return
 */
ResultMatrix generateDataTable(Map metaData) {
  Map tableStructure = createMatrixTableStructure(metaData.tableStructure)
  ResultMatrix matrixTable = api.newMatrix(tableStructure.values().toList())
  setupMatrixTableOptions(matrixTable, metaData)
  populateDataToMatrixTable(matrixTable, metaData)

  return matrixTable
}

protected Map createMatrixTableStructure(List tableStructure) {
  return tableStructure.collectEntries { return [(it.name): it.label] }
}

protected void setupMatrixTableOptions(ResultMatrix matrixTable, Map metaData) {
  List tableStructure = metaData.tableStructure
  for (column in tableStructure) {
    matrixTable.setColumnFormat(column.label, column.formatType)
  }
  matrixTable.setDisableSorting(true)

  String preferenceName = metaData.preferenceName
  if (preferenceName) {
    matrixTable.setPreferenceName(preferenceName)
  }

  Map embeddedDashboard = metaData.embeddedDashboard
  if (embeddedDashboard) {
    String event = api.dashboardWideEvent(embeddedDashboard.eventName)
    matrixTable.onRowSelection()
        .triggerEvent(event)
        .withColValueAsEventDataAttr(embeddedDashboard.columnValue, embeddedDashboard.eventDataAttribute)
  }
}

protected void populateDataToMatrixTable(ResultMatrix matrixTable, Map metaData) {
  if (!metaData.data) {
    return
  }

  Map tableDataType = libs.CustomerInsights.Constant.TABLE_DATA_TYPE
  List topData = metaData.data.topData
  List worstData = metaData.data.worstData
  List boldData = metaData.data.boldData
  List tableStructure = metaData.tableStructure
  addDataToTable(matrixTable, tableStructure, topData, tableDataType.TOP)
  addDataToTable(matrixTable, tableStructure, worstData, tableDataType.WORST)
  addDataToTable(matrixTable, tableStructure, boldData, tableDataType.BOLD)
}

protected void addDataToTable(ResultMatrix matrixTable, List tableStructure, List inputData, String tableDataType) {
  Map row
  for (dataItem in inputData) {
    row = createDataRow(matrixTable, tableStructure, dataItem, tableDataType)
    matrixTable.addRow(row)
  }
}

protected Map createDataRow(ResultMatrix matrix, List tableStructure, Map itemData, String formatType) {
  return tableStructure.collectEntries {
    return [(it.label): getValueWithCellStyle(matrix, it, itemData, formatType)]
  }
}

protected def getValueWithCellStyle(ResultMatrix matrix, Map columnConfiguration, Map inputData, String topWorstType) {
  def constant = libs.CustomerInsights.Constant
  def roundingUtils = libs.SharedLib.RoundingUtils
  Map thresholdValue = columnConfiguration.thresholdValue
  String attributeName = columnConfiguration.dataAttributeName
  Integer decimalPlaces = columnConfiguration.decimalPlaces
  String localFormatType = (columnConfiguration.formatType == FieldFormatType.PERCENT) ? constant.VALUE_TYPE_CODE.PERCENT : constant.VALUE_TYPE_CODE.ABSOLUTE

  def returnedValue
  if (!columnConfiguration.formatRequired) {
    returnedValue = (decimalPlaces != null) ? roundingUtils.round(inputData.getAt(attributeName), decimalPlaces) : inputData.getAt(attributeName)
  } else if (!thresholdValue) {
    returnedValue = setStyledCell(matrix, inputData.getAt(attributeName), topWorstType)
  } else {
    BigDecimal value = (decimalPlaces != null) ? roundingUtils.round(inputData.getAt(attributeName), decimalPlaces) : inputData.getAt(attributeName)
    returnedValue = setStyledCellBasedOnRangeValue(matrix, thresholdValue.upper, thresholdValue.lower, value, localFormatType)
  }

  return returnedValue
}

Map newColumnConfiguration() {
  Map configuration = [:]
  Map methods = [:]
  methods = [
      setName               : { String _name ->
        configuration.name = _name

        return methods
      },
      setLabel              : { String _label ->
        configuration.label = _label

        return methods
      },
      setFormatType         : { FieldFormatType _formatType ->
        configuration.formatType = _formatType

        return methods
      },
      setFormatRequired     : { Boolean _formatRequired ->
        configuration.formatRequired = _formatRequired

        return methods
      },
      setDecimalPlaces      : { Integer _decimalPlaces ->
        configuration.decimalPlaces = _decimalPlaces

        return methods
      },
      setDataAttributeName  : { String _dataAttributeName ->
        configuration.dataAttributeName = _dataAttributeName

        return methods
      },
      setThresholdValue     : { Map _thresholdValue ->
        configuration.thresholdValue = _thresholdValue

        return methods
      },
      getColumnConfiguration: {
        return configuration
      }
  ]

  return methods
}

Map buildThresholdValue(BigDecimal upper, BigDecimal lower) {
  if (upper == null && lower == null) {
    return null
  }

  return [upper: upper,
          lower: lower]
}

Map newTableMetaData() {
  Map metaData = [:]
  Map methods = [:]
  methods = [
      setTableStructure             : { List _tableStructure ->
        metaData.tableStructure = _tableStructure

        return methods
      },
      setTablePreferenceName        : { String _preferenceName ->
        metaData.preferenceName = _preferenceName

        return methods
      },
      setEmbeddedDashboardInfomation: { Map _embeddedDashboard ->
        metaData.embeddedDashboard = _embeddedDashboard

        return methods
      },
      setData                       : { Map _data ->
        metaData.data = _data

        return methods
      },
      getTableMetaData              : {
        return metaData
      }
  ]

  return methods
}

Map buildTopWorstData(List topData, List worstData) {
  if (topData == null && worstData == null) {
    return null
  }

  return [topData  : topData,
          worstData: worstData]
}

Map buildBoldData(List boldData) {
  if (boldData == null) {
    return null
  }

  return [boldData: boldData]
}

Map buildEmbeddedDashboardInfo(String eventName, String columnValue, String eventDataAttribute) {
  return [eventName         : eventName,
          columnValue       : columnValue,
          eventDataAttribute: eventDataAttribute]
}

Map createCustomerIdColumnConfiguration() {
  Map columnLabels = libs.CustomerInsights.Constant.COMMON_COLUMN_LABELS

  return newColumnConfiguration()
      .setName("CustomerId")
      .setLabel(columnLabels.CUSTOMER_ID)
      .setFormatType(FieldFormatType.TEXT)
      .setFormatRequired(false)
      .setDataAttributeName("customerId")
      .getColumnConfiguration()
}

Map createCustomerNameColumnConfiguration(boolean isFormatRequired = true) {
  Map columnLabels = libs.CustomerInsights.Constant.COMMON_COLUMN_LABELS

  return newColumnConfiguration()
      .setName("CustomerName")
      .setLabel(columnLabels.CUSTOMER_NAME)
      .setFormatType(FieldFormatType.TEXT)
      .setFormatRequired(isFormatRequired)
      .setDataAttributeName("customerName")
      .getColumnConfiguration()
}

Map createProductIdColumnConfiguration() {
  Map columnLabels = libs.CustomerInsights.Constant.COMMON_COLUMN_LABELS

  return newColumnConfiguration()
      .setName("ProductId")
      .setLabel(columnLabels.PRODUCT_ID)
      .setFormatType(FieldFormatType.TEXT)
      .setFormatRequired(false)
      .setDataAttributeName("productId")
      .getColumnConfiguration()
}

Map createProductNameColumnConfiguration(boolean isFormatRequired = true) {
  Map columnLabels = libs.CustomerInsights.Constant.COMMON_COLUMN_LABELS

  return newColumnConfiguration()
      .setName("ProductName")
      .setLabel(columnLabels.PRODUCT_NAME)
      .setFormatType(FieldFormatType.TEXT)
      .setFormatRequired(isFormatRequired)
      .setDataAttributeName("productName")
      .getColumnConfiguration()
}

Map createTextColumnConfiguration(String columnLabel, String dataAttributeName, boolean isFormatRequired = false) {
  return newColumnConfiguration()
      .setName(dataAttributeName)
      .setLabel(columnLabel)
      .setFormatType(FieldFormatType.TEXT)
      .setFormatRequired(isFormatRequired)
      .setDataAttributeName(dataAttributeName)
      .getColumnConfiguration()
}

Map createPercentColumnConfiguration(String columnLabel, String dataAttributeName) {
  return newColumnConfiguration()
      .setName(dataAttributeName)
      .setLabel(columnLabel)
      .setFormatType(FieldFormatType.PERCENT)
      .setFormatRequired(false)
      .setDataAttributeName(dataAttributeName)
      .getColumnConfiguration()
}

Map createTrendColumnConfiguration(configurationCategory, String columnLabel, String dataAttributeName) {
  BigDecimal trendUpperValue = configurationCategory.TREND_UPPER.value.getAt(0) as BigDecimal
  BigDecimal trendLowerValue = configurationCategory.TREND_LOWER.value.getAt(0) as BigDecimal
  Map thresholdValue = buildThresholdValue(trendUpperValue, trendLowerValue)

  return newColumnConfiguration()
      .setName(dataAttributeName)
      .setLabel(columnLabel)
      .setFormatType(FieldFormatType.PERCENT)
      .setFormatRequired(true)
      .setDataAttributeName(dataAttributeName)
      .setThresholdValue(thresholdValue)
      .getColumnConfiguration()
}

Map createHealthScoreColumnConfiguration(Map configurationCategory, String columnLabel, String dataAttributeName) {
  Map roundingDecimalPlaces = libs.CustomerInsights.Constant.ROUNDING_DECIMAL_PLACES
  BigDecimal healthScoreUpperValue = configurationCategory.CUSTOMER_HEALTH_SCORE_UPPER.value.getAt(0) as BigDecimal
  BigDecimal healthScoreLowerValue = configurationCategory.CUSTOMER_HEALTH_SCORE_LOWER.value.getAt(0) as BigDecimal

  return newColumnConfiguration()
      .setName(dataAttributeName)
      .setLabel(columnLabel)
      .setFormatType(FieldFormatType.NUMERIC)
      .setFormatRequired(true)
      .setDataAttributeName(dataAttributeName)
      .setDecimalPlaces(roundingDecimalPlaces.DECIMAL_PLACES_00)
      .setThresholdValue(buildThresholdValue(healthScoreUpperValue, healthScoreLowerValue))
      .getColumnConfiguration()
}

Map createNumberColumnConfiguration(String columnLabel, String dataAttributeName) {
  Map roundingDecimalPlaces = libs.CustomerInsights.Constant.ROUNDING_DECIMAL_PLACES

  return newColumnConfiguration()
      .setName(dataAttributeName)
      .setLabel(columnLabel)
      .setFormatType(FieldFormatType.NUMERIC)
      .setFormatRequired(false)
      .setDataAttributeName(dataAttributeName)
      .setDecimalPlaces(roundingDecimalPlaces.DECIMAL_PLACES_00)
      .getColumnConfiguration()
}

Map createMoneyColumnConfiguration(String columnLabel, String dataAttributeName) {
  return newColumnConfiguration()
      .setName(dataAttributeName)
      .setLabel(columnLabel)
      .setFormatType(FieldFormatType.MONEY)
      .setFormatRequired(false)
      .setDataAttributeName(dataAttributeName)
      .getColumnConfiguration()
}