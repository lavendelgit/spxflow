import net.pricefx.formulaengine.DatamartContext

/*
   Create Queries for Classification element
   Source: Datamart
 */
/**
 *  create query to get SUM of some values PER Product
 *  Source: Original Source (Datamart)
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValuePerProduct(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map originalSource = configuration.OriginalSource
  //create result fields
  Map resultFields = [productId: originalSource.ProductIDFieldName,
                      revenue  : queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName),
                      margin   : queryUtils.createSQLForSum(originalSource.MarginFieldName),
                      quantity : queryUtils.createSQLForSum(originalSource.QuantityFieldName)]
  List orderBy = ["revenue DESC"]

  return buildQueryFromOriginalSource(originalSource, queryFilters, resultFields, orderBy, true)
}
/**
 * create query to get SUM of some values PER each Month and each Product
 *  Source: Original Source (Datamart)
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValueOnEachMonthPerCustomer(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map timePeriod = libs.CustomerInsights.Constant.TIME_PERIOD
  Map originalSource = configuration.OriginalSource
  String monthFieldName = originalSource.PricingDateFieldName + timePeriod.MONTH
  //add result fields
  Map resultFields = [customerId: originalSource.CustomerIDFieldName,
                      month     : monthFieldName,
                      revenue   : queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName),
                      margin    : queryUtils.createSQLForSum(originalSource.MarginFieldName),
                      quantity  : queryUtils.createSQLForSum(originalSource.QuantityFieldName)]
  List orderBy = [originalSource.CustomerIDFieldName, monthFieldName]

  return buildQueryFromOriginalSource(originalSource, queryFilters, resultFields, orderBy, true)
}
/**
 * create query to get SUM of some values PER Customer
 *  Source: Original Source (Datamart)
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValuePerCustomer(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map originalSource = configuration.OriginalSource
  Map resultFields = [customerId: originalSource.CustomerIDFieldName,
                      revenue   : queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName),
                      margin    : queryUtils.createSQLForSum(originalSource.MarginFieldName),
                      quantity  : queryUtils.createSQLForSum(originalSource.QuantityFieldName)]
  List orderBy = ["revenue DESC"]

  return buildQueryFromOriginalSource(originalSource, queryFilters, resultFields, orderBy, true)
}
/**
 * create query to get SUM of some values PER each QUARTER and each PRODUCT and each CUSTOMER
 * Source: Original Source (Datamart)
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValueOnEachQuarterPerProductAndCustomer(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map timePeriod = libs.CustomerInsights.Constant.TIME_PERIOD
  Map originalSource = configuration.OriginalSource
  String quarterFieldName = originalSource.PricingDateFieldName + timePeriod.QUARTER
  Map resultFields = [customerId: originalSource.CustomerIDFieldName,
                      productId : originalSource.ProductIDFieldName,
                      quarter   : quarterFieldName,
                      revenue   : queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName),
                      margin    : queryUtils.createSQLForSum(originalSource.MarginFieldName),
                      quantity  : queryUtils.createSQLForSum(originalSource.QuantityFieldName)]
  List orderBy = [originalSource.ProductIDFieldName, originalSource.CustomerIDFieldName, quarterFieldName]

  return buildQueryFromOriginalSource(originalSource, queryFilters, resultFields, orderBy, true)
}
/**
 * create query to get SUM of some values PER Product and Customer
 * Source: Original Source (Datamart)
 * @param configuration
 * @param queryFilters
 * @return
 */
DatamartContext.Query createQueryForSumValuePerProductAndCustomer(Map configuration, List queryFilters) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  Map originalSource = configuration.OriginalSource
  Map resultFields = [customerId     : originalSource.CustomerIDFieldName,
                      productId      : originalSource.ProductIDFieldName,
                      revenue        : queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName),
                      margin         : queryUtils.createSQLForSum(originalSource.MarginFieldName),
                      quantity       : queryUtils.createSQLForSum(originalSource.QuantityFieldName),
                      avgInvoicePrice: "${queryUtils.createSQLForSum(originalSource.InvoicePriceFieldName)}/${queryUtils.createSQLForSum(originalSource.QuantityFieldName)}"]
  //create order by list
  List orderBy = [originalSource.ProductIDFieldName, originalSource.CustomerIDFieldName]

  return buildQueryFromOriginalSource(originalSource, queryFilters, resultFields, orderBy, true)
}

protected DatamartContext.Query buildQueryFromOriginalSource(Map originalSource, List queryFilters, Map resultFields, List orderBy, boolean rollup = false) {
  return libs.CustomerInsights.QueryUtils.getContextQuery(originalSource.SourceType, originalSource.SourceName, resultFields, rollup, orderBy, *queryFilters)
}