/**
 * init new instant global view dashboard parameter
 * @param customerGroup
 * @param timeFilter
 * @param kpi
 * @param topCustomer
 * @param currency
 * @param dimension
 * @param period
 * @param currentDate
 * @param customerClassByRevenues
 * @param customerClassByHealthScores
 * @return
 */
Map newGlobalViewDashboardParam(def customerGroup,
                                String timeFilter,
                                String kpi,
                                Integer topCustomer,
                                Map currency,
                                Map dimension,
                                Map period,
                                Date currentDate,
                                List customerClassByRevenues,
                                List customerClassByHealthScores) {
  Map globalViewParam = libs.CustomerInsights.CommonDashboardParameter.commonDashboardParam(timeFilter, currency, period, currentDate)
  globalViewParam << [
      getCustomerFilter                          : { return customerGroup?.asFilter() },
      getTopCustomer                             : { return topCustomer },
      getDimension                               : { return dimension },
      getKPI                                     : { return kpi },
      getCustomerClassByRevenues                 : { return customerClassByRevenues },
      getCustomerClassByHealthScores             : { return customerClassByHealthScores },
      getBuyingProductAmountQuerySimulationFilter: { Map _configuration ->
        return generateBuyingProductAmountQuerySimulationFilter(_configuration, globalViewParam)
      },
      getAllCustomerQuerySimulationFilter        : { Map _configuration ->
        return generateAllCustomerQuerySimulationFilter(_configuration, globalViewParam)
      },
      getCustomerQuerySimulationFilter           : { Map _configuration ->
        return generateBasicQuerySimulationFilter(_configuration, globalViewParam)
      },
      getBasicQuerySimulationFilter              : { Map _configuration ->
        return generateBasicQuerySimulationFilter(_configuration, globalViewParam)
      },
      getBasicQueryDatamartFilter                : { Map _configuration ->
        return generateCustomPeriodQueryDatamartFilter(_configuration, globalViewParam.getStartDate(), globalViewParam.getEndDate(), globalViewParam)
      },
      getCustomPeriodQueryDatamartFilter         : { Map _configuration, Date _startDate, Date _endDate ->
        return generateCustomPeriodQueryDatamartFilter(_configuration, _startDate, _endDate, globalViewParam)
      }
  ]

  return globalViewParam
}
//**********************************************************
protected List generateBasicQuerySimulationFilter(Map configuration, Map globalViewParam) {
  List<Filter> filters = []

  filters.addAll(globalViewParam.getPeriodSimulationFilter(configuration, globalViewParam.getStartDate(), globalViewParam.getEndDate()))
  filters.addAll(buildDimensionSimulationFilter(globalViewParam.getDimension()))
  filters.addAll(buildCustomerClassSimulationFilter(configuration,
      globalViewParam.getCustomerClassByRevenues(),
      globalViewParam.getCustomerClassByHealthScores()))
  filters.addAll(buildCustomerGroupSimulationFilter(configuration, globalViewParam.getCustomerFilter()))

  return filters
}

protected List generateBuyingProductAmountQuerySimulationFilter(Map configuration, Map globalViewParam) {
  List<Filter> filters = []

  filters.addAll(globalViewParam.getPeriodSimulationFilter(configuration, globalViewParam.getStartDate(), globalViewParam.getEndDate()))
  filters.addAll(buildDimensionSimulationFilter(globalViewParam.getDimension()))

  return filters
}

protected List generateAllCustomerQuerySimulationFilter(Map configuration, Map globalViewParam) {
  List<Filter> filters = []

  filters.addAll(globalViewParam.getPeriodSimulationFilter(configuration, globalViewParam.getStartDate(), globalViewParam.getEndDate()))
  filters.addAll(buildDimensionSimulationFilter(globalViewParam.getDimension()))
  filters.addAll(buildCustomerClassSimulationFilter(configuration,
      globalViewParam.getCustomerClassByRevenues(),
      globalViewParam.getCustomerClassByHealthScores()))

  return filters
}

protected List buildDimensionSimulationFilter(Map dimension) {
  return dimension ? [Filter.equal(dimension.FieldName, dimension.Value)] : []
}

protected List buildCustomerClassSimulationFilter(Map configuration, List customerClassByRevenues, List customerClassByHealthScores) {
  List filters = []

  if (customerClassByRevenues) { //List of Customer Class By Revenue
    filters.add(Filter.in(configuration.CustomerClassificationByRevenueFieldName, customerClassByRevenues))
  }

  if (customerClassByHealthScores) { //List of Customer Class By Health Score
    filters.add(Filter.in(configuration.CustomerClassificationByHealthScoreFieldName, customerClassByHealthScores))
  }

  return filters
}

protected List buildCustomerGroupSimulationFilter(Map configuration, Filter customerFilter) {
  List filters = []

  if (customerFilter) {
    List customerIds = libs.CustomerInsights.CustomerUtils.convertCustomerGroupInputToListOfCustomerId(customerFilter)
    filters.add(Filter.in(configuration.CustomerIDFieldName, customerIds))
  }

  return filters
}
//**********************************************************
protected List generateCustomPeriodQueryDatamartFilter(Map configuration, Date startDate, Date endDate, Map globalViewParam) {
  List<Filter> filters = []

  filters.addAll(globalViewParam.getPeriodDatamartFilter(configuration, startDate, endDate))
  filters.addAll(buildDimensionDatamartFilter(configuration, globalViewParam))
  filters.addAll(buildCustomerGroupDatamartFilter(configuration, globalViewParam))
  filters.addAll(buildCustomerClassDatamartFilter(configuration, globalViewParam))

  return filters
}

protected List buildDimensionDatamartFilter(Map configuration, Map globalViewParam) {
  def calculationUtil = libs.CustomerInsights.CalculationCommonUtils

  Map originalSource = configuration.OriginalSource
  List<Filter> filters = []

  if (globalViewParam.getDimension()?.FieldName) { // in case dimension is selected
    Map customerAndProducts = calculationUtil.getCustomerAndProductBaseOnInput(configuration, globalViewParam.getBasicQuerySimulationFilter(configuration))

    if (customerAndProducts.customerIds != null) { //will process with []
      filters.add(Filter.in(originalSource.CustomerIDFieldName, customerAndProducts.customerIds))
    }

    if (customerAndProducts.productIds != null) { //will process with []
      filters.add(Filter.in(originalSource.ProductIDFieldName, customerAndProducts.productIds))
    }
  }

  return filters
}

protected List buildCustomerGroupDatamartFilter(Map configuration, Map globalViewParam) {
  Map originalSource = configuration.OriginalSource
  List<Filter> filters = []

  if (globalViewParam.getCustomerFilter()) { // customer group
    Filter customerFilter = globalViewParam.getCustomerFilter()
    List customerIds = libs.CustomerInsights.CustomerUtils.convertCustomerGroupInputToListOfCustomerId(customerFilter)
    filters.add(Filter.in(originalSource.CustomerIDFieldName, customerIds))
  }

  return filters
}

protected List buildCustomerClassDatamartFilter(Map configuration, Map globalViewParam) {
  def calculationUtil = libs.CustomerInsights.CalculationCommonUtils

  Map originalSource = configuration.OriginalSource
  List<Filter> filters = []

  if (globalViewParam.getCustomerClassByRevenues() || globalViewParam.getCustomerClassByHealthScores()) {
    Map customerAndProducts = calculationUtil.getCustomerAndProductBaseOnInput(configuration, globalViewParam.getBasicQuerySimulationFilter(configuration))
    filters.add(Filter.in(originalSource.CustomerIDFieldName, customerAndProducts.customerIds))
  }

  return filters
}