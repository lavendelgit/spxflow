import net.pricefx.formulaengine.DatamartContext
import net.pricefx.formulaengine.DatamartQueryResult

String getOrderValue(String kpi = "") {
  Map attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  String orderByValue

  switch (kpi.toUpperCase()) {
    case attributeTypeCode.REVENUE:
      orderByValue = "revenue"
      break
    case attributeTypeCode.MARGIN_PERCENT:
      orderByValue = "avgmarginpercent"
      break
    case attributeTypeCode.VOLUME:
      orderByValue = "quantity"
      break
    case attributeTypeCode.HEALTH_SCORE:
      orderByValue = "customerhealthscore"
      break
    default:
      orderByValue = "revenue"
  }

  return orderByValue
}
/**
 * get values from datamart
 * @param configuration
 * @param queryFilters
 * @return
 */
Map getSummaryValuesFromOriginalSource(Map configuration, List queryFilters) {
  DatamartContext.Query query = libs.CustomerInsights.CalculationQueryHelper.createQueryForSummaryValuesFromOriginalSource(configuration, queryFilters)
  DatamartQueryResult result = api.getDatamartContext().executeQuery(query)

  return result?.getData()?.collect()?.getAt(0) ?: [:]
}
/**
 * calculate YTD Trend values from Datamart
 * @param configuration
 * @param YTDFilters
 * @param previousYTDFilters
 * @return
 */
Map calculateYTDTrendFromOriginalSource(Map configuration, List YTDFilters, List previousYTDFilters) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper

  DatamartContext.Query previousPeriodQuery = queryHelper.createQueryForSummaryValuesFromOriginalSource(configuration, previousYTDFilters)
  DatamartContext.Query currentPeriodQuery = queryHelper.createQueryForSummaryValuesFromOriginalSource(configuration, YTDFilters)
  String sqlYTDTrends = buildYTDTrendCalculationSQL()
  String sqlStatement = """ SELECT ${sqlYTDTrends}
                            FROM T2 CurrentPeriod, T1 PreviousPeriod """

  Map data = api.getDatamartContext().executeSqlQuery(sqlStatement, previousPeriodQuery, currentPeriodQuery)?.collect()?.getAt(0) ?: [:]

  return [revenueYTDTrend : data.revenueTrend,
          marginYTDTrend  : data.marginTrend,
          quantityYTDTrend: data.quantityTrend]
}
/**
 * calculate L12M Trend for Customer values from original source (datamart)
 * calculated by month
 * @param configuration
 * @param queryFilters
 * @param periodL12M
 * @return
 */
Map calculateL12MTrendForCustomerFromOriginalSource(Map configuration, List queryFilters, Map periodL12M) {
  def dateUtils = libs.CustomerInsights.DateUtils
  def constant = libs.CustomerInsights.Constant
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def queryUtils = libs.CustomerInsights.QueryUtils

  Map timePeriod = constant.TIME_PERIOD

  DatamartContext.Query summaryQuery = queryHelper.createQueryForSummaryValuesOnEachPeriodFromOriginalSource(configuration, queryFilters, timePeriod.MONTH)
  String monthNameOfEndDate = dateUtils.getMonthName(periodL12M.endDate)
  String sqlMonthlyData = """ SELECT  month                              AS 'month',
                                      revenue                            AS 'revenue',
                                      margin                             AS 'margin',
                                      quantity                           AS 'quantity',
                                      ROW_NUMBER() OVER (ORDER BY month) AS 'rownumber'
                              FROM T1 """
  String sqlL12MTrends = buildL12MTrendCalculationSQL(monthNameOfEndDate, timePeriod.MONTH)
  String sqlStatement = """ SELECT ${sqlL12MTrends}
                            FROM ( $sqlMonthlyData ) currentperiod LEFT JOIN 
                                 ( $sqlMonthlyData ) previousperiod 
                                ON  (currentperiod.rownumber - 1) = previousperiod.rownumber  """

  return queryUtils.getFullQueryData(sqlStatement, summaryQuery, summaryQuery)?.getAt(0)
}
/**
 * summarize data with group by Customer
 * @param configuration
 * @param queryFilters
 * @return
 */
Map getSumValuePerCustomer(Map configuration, List queryFilters) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper

  DatamartContext.Query query = queryHelper.createQueryForSumValuePerCustomer(configuration, queryFilters)
  DatamartQueryResult result = api.getDatamartContext().executeQuery(query)

  return result?.getData()?.collect()?.getAt(0) ?: [:]
}
/**
 * Calculate sum value on each month
 * @param configuration
 * @param queryFilters
 * @return
 */
List getSumValueOnEachMonth(Map configuration, List queryFilters) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def queryUtils = libs.CustomerInsights.QueryUtils

  DatamartContext.Query query = queryHelper.createQueryForSumValueOnEachMonthPerCustomer(configuration, queryFilters)
  String sql = """SELECT customerId        AS 'customerId',
                         customerName      AS 'customerName', 
                         customerSegment   AS 'customerSegment',
                         month             AS 'month',
                         revenue           AS 'revenue',
                         margin            AS 'margin',
                         IIF( revenue <> 0,
                              margin/revenue,  
                              0 )          AS 'marginPercent',
                         quantity          AS 'quantity',
                         transactionAmount AS 'transactionAmount'
                FROM T1 
                ORDER BY customerId, month """

  return queryUtils.getFullQueryData(sql, query)
}
/**
 * Detail process of function base on below link
 * https://www.statisticshowto.com/probability-and-statistics/regression-analysis/find-a-linear-regression-equation/#FindaLinear
 * Return factor will be applied to formulas: y = x*slope + interception
 * @param data
 * @return
 */
Map calculateRegression(List data) {

  List processData = data.collect {
    [x: it.step, y: it.value, xy: it.step * it.value, sqrX: it.step**2]
  }
  BigDecimal sumX = BigDecimal.ZERO
  BigDecimal sumY = BigDecimal.ZERO
  BigDecimal sumXY = BigDecimal.ZERO
  BigDecimal sumSqrX = BigDecimal.ZERO
  processData.each {
    sumX += it.x
    sumY += it.y
    sumXY += it.xy
    sumSqrX += it.sqrX
  }

  BigDecimal interception = BigDecimal.ZERO, slope = BigDecimal.ZERO
  Integer size = data.size()
  BigDecimal medianValue = (size * sumSqrX - sumX**2)

  if (medianValue) {
    slope = (size * sumXY - sumX * sumY) / medianValue
    interception = (sumY * sumSqrX - sumX * sumXY) / medianValue
  }

  return [slope: slope, interception: interception]
}
/**
 * Count distinct productId in a period
 * @param configuration
 * @param dashboardParams
 * @return
 */
BigDecimal getBuyingProductAmount(Map configuration, List queryFilters) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  DatamartContext.Query query = queryHelper.createQueryForCountBuyingProduct(configuration, queryFilters)
  Map resultData = api.getDatamartContext().executeQuery(query)?.getData()?.collect()?.getAt(0)

  return (resultData?.buyingProductAmount ?: BigDecimal.ZERO)
}
/**
 * get value based on type attribute Type
 * @param data
 * @param type
 * @param isPercentFormat
 * @return
 */
BigDecimal getValueBaseOnType(Map data, String type, boolean isPercentFormat = false) {
  def attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  if (!data || !type) {
    return BigDecimal.ZERO
  }

  BigDecimal value
  switch (type) {
    case attributeTypeCode.REVENUE:
      value = data.revenue
      break
    case attributeTypeCode.TRANSACTION_AMOUNT:
      value = data.transactionAmount
      break
    case attributeTypeCode.QUANTITY:
      value = data.quantity
      break
    case attributeTypeCode.MARGIN:
      value = data.margin
      break
    case attributeTypeCode.MARGIN_PERCENT:
      value = data.revenue ? (data.margin / data.revenue) : BigDecimal.ZERO
      value = isPercentFormat ? (100 * value) : value
      break
    default:
      value = data.revenue
  }

  return value
}
/**
 * calculate average value, not include null or zero item
 * @param data
 * @return
 */
BigDecimal getAverageValue(List data) {
  if (!data) {
    return BigDecimal.ZERO
  }

  List localData = data.findAll { it }
  int dataSize = localData.size()

  return dataSize > 0 ? localData.sum() / dataSize : BigDecimal.ZERO
}
/**
 * Get customer segment by customerId
 * @param configuration
 * @param customerId
 * @return
 */
String getCustomerSegmentByCustomer(Map configuration, String customerId) {
  def constant = libs.CustomerInsights.Constant
  def customerSegmentUtils = libs.CustomerInsights.CustomerSegmentUtils

  if (!customerId) {
    return customerId
  }

  String segmentOfCustomerCachedKey = "SEGMENT_OF_CUSTOMER_${customerId}"

  return libs.SharedLib.CacheUtils.getOrSet(segmentOfCustomerCachedKey, [], {
    Map customerSegment = customerSegmentUtils.getCustomerSegment(configuration.customerSegments, customerId)
    String segment = customerSegment?.getAt(customerId)?.getAt(0)?.customerSegment
    String unknown = constant.DEFAULT_VALUE.UNKNOWN

    return segment ?: unknown
  })
}
/**
 * build SQL to calculation YTD trends: revenueTrend, Margin trend, quantityTrend
 * @return
 */
String buildYTDTrendCalculationSQL() {
  String sql = """ ( CASE 
                          WHEN ( SUM(IFNULL(PreviousPeriod.revenue,0)) = 0 and SUM(IFNULL(CurrentPeriod.revenue,0)) = 0 ) THEN 0
                          WHEN ( SUM(IFNULL(PreviousPeriod.revenue,0)) = 0 and SUM(IFNULL(CurrentPeriod.revenue,0)) != 0 ) THEN 1
                          WHEN ( SUM(IFNULL(PreviousPeriod.revenue,0)) != 0 and SUM(IFNULL(CurrentPeriod.revenue,0)) != 0 ) THEN 
                               ( SUM(IFNULL(CurrentPeriod.revenue,0)) / SUM(IFNULL(PreviousPeriod.revenue,0)) - 1 )
                     END ) AS 'revenueTrend',
                   ( CASE 
                         WHEN ( SUM(IFNULL(PreviousPeriod.margin,0)) = 0 and SUM(IFNULL(CurrentPeriod.margin,0)) = 0 ) THEN 0
                         WHEN ( SUM(IFNULL(PreviousPeriod.margin,0)) = 0 and SUM(IFNULL(CurrentPeriod.margin,0)) != 0 ) THEN 1
                         WHEN ( SUM(IFNULL(PreviousPeriod.margin,0)) != 0 and SUM(IFNULL(CurrentPeriod.margin,0)) != 0 ) THEN 
                              ( SUM(IFNULL(CurrentPeriod.margin,0)) / SUM(IFNULL(PreviousPeriod.margin,0)) - 1 )
                     END ) AS 'marginTrend',
                   ( CASE 
                         WHEN ( SUM(IFNULL(PreviousPeriod.quantity,0)) = 0 and SUM(IFNULL(CurrentPeriod.quantity,0)) = 0 ) THEN 0
                         WHEN ( SUM(IFNULL(PreviousPeriod.quantity,0)) = 0 and SUM(IFNULL(CurrentPeriod.quantity,0)) != 0 ) THEN 1
                         WHEN ( SUM(IFNULL(PreviousPeriod.quantity,0)) != 0 and SUM(IFNULL(CurrentPeriod.quantity,0)) != 0 ) THEN 
                              ( SUM(IFNULL(CurrentPeriod.quantity,0)) / SUM(IFNULL(PreviousPeriod.quantity,0)) - 1 )
                     END ) AS 'quantityTrend' """

  return sql
}
/**
 * build SQL to calculation L12M trends: revenueTrend, Margin trend, quantityTrend based on periodType
 * @param periodName
 * @param periodType
 * @return
 */
String buildL12MTrendCalculationSQL(String periodName, String periodType) {
  Map timePeriod = libs.CustomerInsights.Constant.TIME_PERIOD
  String checkPeriodSQL

  if (timePeriod.MONTH == periodType) {
    checkPeriodSQL = "IIF( SUM((currentperiod.month < '${periodName}')::int) > 0, -1, 1)"
  } else {
    checkPeriodSQL = "IIF( SUM((currentperiod.quarter < '${periodName}')::int) > 0, -1, 1)"
  }

  String sql = """ IIF( SUM((currentperiod.revenue <> 0) :: int) > 1,
                        SUM (CASE
                                     WHEN currentperiod.revenue = 0 AND previousperiod.revenue = 0 THEN 0
                                     WHEN currentperiod.revenue != 0 AND previousperiod.revenue = 0 THEN 1
                                     WHEN currentperiod.revenue != 0 AND previousperiod.revenue != 0 THEN 
                                          currentperiod.revenue/previousperiod.revenue -1 
                             END) / SUM((currentperiod.revenue <> 0) :: int),
                        ${checkPeriodSQL}
                       ) AS 'revenueTrend',
                   IIF( SUM((currentperiod.margin <> 0) :: int) > 1,
                        SUM (CASE
                                     WHEN currentperiod.margin = 0 AND previousperiod.margin = 0 THEN 0
                                     WHEN currentperiod.margin != 0 AND previousperiod.margin = 0 THEN 1
                                     WHEN currentperiod.margin != 0 AND previousperiod.margin != 0 THEN 
                                          currentperiod.margin/previousperiod.margin -1 
                             END) / SUM((currentperiod.margin <> 0) :: int),
                        ${checkPeriodSQL}
                       ) AS 'marginTrend',
                   IIF( SUM((currentperiod.quantity <> 0) :: int) > 1,
                        SUM (CASE
                                     WHEN currentperiod.quantity = 0 AND previousperiod.quantity = 0 THEN 0
                                     WHEN currentperiod.quantity != 0 AND previousperiod.quantity = 0 THEN 1
                                     WHEN currentperiod.quantity != 0 AND previousperiod.quantity != 0 THEN 
                                          currentperiod.quantity/previousperiod.quantity -1 
                             END) / SUM((currentperiod.quantity <> 0) :: int),
                        ${checkPeriodSQL}
                       ) AS 'quantityTrend' """

  return sql
}
/**
 * Get list of CustomerId and ProductId from Simulation
 * @param configuration
 * @param filters
 * @return
 */
Map getCustomerAndProductBaseOnInput(Map configuration, List filters) {
  if (api.global.LIST_OF_CUSTOMER_AND_PRODUCT_BASED_ON_INPUT) {
    return api.global.LIST_OF_CUSTOMER_AND_PRODUCT_BASED_ON_INPUT
  }

  String sourceType = configuration.SourceType
  String sourceName = configuration.SourceName
  //add result fields
  Map resultFields = [customerId: configuration.CustomerIDFieldName,
                      productId : configuration.ProductIDFieldName]
  List orderBy = [configuration.CustomerIDFieldName, configuration.ProductIDFieldName]
  DatamartContext.Query query = libs.CustomerInsights.QueryUtils.getContextQuery(sourceType, sourceName, resultFields, false, orderBy, *filters)
  String sql = """ SELECT DISTINCT customerId AS 'customerId', 
                                   productId  AS 'productId' 
                   FROM T1
                   ORDER BY customerId, productId """
  List resultData = libs.CustomerInsights.QueryUtils.getFullQueryData(sql, query)

  Map customerAndProductData = [customerIds: resultData.customerId?.unique(),
                                productIds : resultData.productId?.unique()]
  api.global.LIST_OF_CUSTOMER_AND_PRODUCT_BASED_ON_INPUT = customerAndProductData

  return customerAndProductData
}