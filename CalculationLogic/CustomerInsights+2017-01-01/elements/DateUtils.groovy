//------DATE Utils----
/**
 * Get start date of month from inputDate. If param currentDate=null system will set currentDate = today
 * @param inputDate
 * @return StartDate of month
 */
Date getMonthStartDate(Date inputDate = new Date()) {
  Calendar calendar = Calendar.getInstance()
  calendar.set(inputDate[Calendar.YEAR], inputDate[Calendar.MONTH], 1)

  return setStartTime(calendar.getTime())
}
/**
 * Get end date of current month
 * @param inputDate
 * @return endDate of current month
 */
Date getMonthEndDate(Date inputDate = new Date()) {
  Calendar calendar = Calendar.getInstance()
  calendar.set(inputDate[Calendar.YEAR], inputDate[Calendar.MONTH], 1)
  calendar.add(Calendar.MONTH, 1)
  calendar.add(Calendar.DATE, -1)

  return setEndTime(calendar.getTime())
}
/**
 * Get start date of current quarter. If param inputDate=null system will set inputDate = today
 * @param inputDate
 * @return StartDate of current quarter
 */
Date getQuarterStartDate(Date inputDate = new Date()) {
  Calendar calendar = Calendar.getInstance()
  int month = inputDate[Calendar.MONTH]
  Integer quarterStartMonth = Math.floor(month / 3) * 3
  calendar.set(inputDate[Calendar.YEAR], quarterStartMonth, 1)

  return setStartTime(calendar.getTime())
}
/**
 * Get end date of current quarter. If param inputDate=null system will set inputDate = today
 * @param inputDate
 * @return endDate of current quarter
 */
Date getQuarterEndDate(Date inputDate = new Date()) {
  Calendar calendar = Calendar.getInstance()
  int month = inputDate[Calendar.MONTH]
  Integer quarterEndMonth = (Math.floor(month / 3) + 1) * 3 - 1
  calendar.set(inputDate[Calendar.YEAR], quarterEndMonth, 1)
  calendar.add(Calendar.MONTH, 1)
  calendar.add(Calendar.DATE, -1)

  return setEndTime(calendar.getTime())
}
/**
 * Get year start date of inputDate
 * @param inputDate
 * @return startDate of  year
 */
Date getYearStartDate(Date inputDate = new Date()) {
  Calendar calendar = Calendar.getInstance()
  calendar.set(inputDate[Calendar.YEAR], 0, 1)

  return setStartTime(calendar.getTime())
}
/**
 * Get end date of year from inputDate
 * @param inputDate
 * @return end date of current year
 */
Date getYearEndDate(Date inputDate = new Date()) {
  Calendar calendar = Calendar.getInstance()
  calendar.set(inputDate[Calendar.YEAR], 0, 1)
  calendar.add(Calendar.YEAR, 1)
  calendar.add(Calendar.DATE, -1)

  return setEndTime(calendar.getTime())
}
/**
 * add years
 * @param inputDate
 * @param yearAmount
 * @return
 */
Date addYears(Date inputDate, Integer yearAmount) {
  Calendar calendar = Calendar.getInstance()
  Date localInputDate = inputDate ?: new Date()
  calendar.setTime(localInputDate)
  calendar.add(Calendar.YEAR, yearAmount)

  return calendar.getTime()
}
/**
 * add months
 * @param inputDate
 * @param monthAmount
 * @return
 */
Date addMonths(Date inputDate, Integer monthAmount) {
  Calendar calendar = Calendar.getInstance()
  Date localInputDate = inputDate ?: new Date()
  calendar.setTime(localInputDate)
  calendar.add(Calendar.MONTH, monthAmount)

  return calendar.getTime()
}
/**
 * add days
 * @param inputDate
 * @param daysAmount
 * @return
 */
Date addDays(Date inputDate, Integer daysAmount) {
  Calendar calendar = Calendar.getInstance()
  Date localInputDate = inputDate ?: new Date()
  calendar.setTime(localInputDate)
  calendar.add(Calendar.DATE, daysAmount)

  return calendar.getTime()
}
/**
 * calculate number of days between two date
 * @param startDate
 * @param endDate
 * @return
 */
Integer getDaysDifference(Date startDate, Date endDate) {
  def datamartCalendar = api.getDatamartContext()?.calendar()

  return datamartCalendar.getDaysDiff(endDate?.toString(), startDate?.toString())
}
/**
 * periodType:
 *     YTD: Year To Date
 *     QTD: Quarter To Date
 *     MTD: Month To Date
 *     L12M: Last Twelve Month
 * @param currentDate
 * @param periodType
 * @return
 */
Map getPeriod(Date currentDate, String periodType) {
  Map timePeriod = libs.CustomerInsights.Constant.TIME_PERIOD

  if (!currentDate) {
    return [:]
  }

  Date startDate, endDate
  startDate = currentDate
  endDate = currentDate

  switch (periodType) {
    case timePeriod.MTD:
      startDate = getMonthStartDate(currentDate)
      break

    case timePeriod.QTD:
      startDate = getQuarterStartDate(currentDate)
      break

    case timePeriod.YTD:
      startDate = getYearStartDate(currentDate)
      break

    case timePeriod.L12M:
      Date lastStartDate = addMonths(currentDate, -12)
      startDate = getMonthStartDate(lastStartDate)
      endDate = addDays(getMonthStartDate(currentDate), -1)
      break

    case timePeriod.L6M:
      Date lastStartDate = addMonths(currentDate, -6)
      startDate = getMonthStartDate(lastStartDate)
      endDate = addDays(getMonthStartDate(currentDate), -1)
      break

    case timePeriod.L3M:
      Date lastStartDate = addMonths(currentDate, -3)
      startDate = getMonthStartDate(lastStartDate)
      endDate = addDays(getMonthStartDate(currentDate), -1)
      break

    default:
      startDate = getYearStartDate(currentDate)
  }

  return [startDate: startDate,
          endDate  : endDate]
}
/**
 * add years to period
 * @param inputPeriod
 * @param yearAmount
 * @return
 */
Map addYearsToPeriod(Map inputPeriod, Integer yearAmount) {
  if (!inputPeriod || !yearAmount) {
    return inputPeriod
  }

  Date startDate = addYears(getDateValue(inputPeriod.startDate), yearAmount)
  Date endDate = addYears(getDateValue(inputPeriod.endDate), yearAmount)

  return [startDate: startDate,
          endDate  : endDate]
}
/**
 * ad months to period
 * @param inputPeriod
 * @param monthAmount
 * @return
 */
Map addMonthsToPeriod(Map inputPeriod, Integer monthAmount) {
  if (!inputPeriod || !monthAmount) {
    return inputPeriod
  }

  Date startDate = addMonths(getDateValue(inputPeriod.startDate), monthAmount)
  Date endDate = addMonths(getDateValue(inputPeriod.endDate), monthAmount)

  return [startDate: startDate,
          endDate  : endDate]
}
/**
 * get list of months in period
 * @param period
 * @return
 */
List getMonthNamesInPeriod(Map period) {
  if (!period || !period.startDate || !period.endDate) {
    return null
  }

  return getMonthNamesInPeriod(period.startDate, period.endDate)
}
/**
 * get list of months in 2 days: fromDate, toDate
 * @param inputStartDate
 * @param inputEndDate
 * @return
 */
List getMonthNamesInPeriod(def inputStartDate, def inputEndDate) {
  Date startDate = getDateValue(inputStartDate)
  Date endDate = getDateValue(inputEndDate)
  Integer monthAmount = getMonthAmount(startDate, endDate)
  List monthNames = []
  for (int i = 0; i < monthAmount; i++) {
    monthNames.add(getMonthName(startDate))
    startDate = addMonths(startDate, 1)
  }

  return monthNames
}
/**
 * get month name of current date
 * @param inputDate
 * @return
 */
String getMonthName(Date inputDate) {
  if (!inputDate) {
    return null
  }

  int month = inputDate[Calendar.MONTH]
  String monthString = (month + 1).toString()
  String yearString = inputDate[Calendar.YEAR].toString()

  return month < 9 ? "$yearString-M0$monthString" : "$yearString-M$monthString"
}
/**
 * get quarter name of current date
 * @param inputDate
 * @return
 */
String getQuarterName(Date inputDate) {
  if (!inputDate) {
    return null
  }
  int month = inputDate[Calendar.MONTH]
  int year = inputDate[Calendar.YEAR]
  Integer quarter = Math.floor(month / 3) + 1

  return "$year-Q$quarter"
}
/**
 * get period from current date:
 * period:
 *       startDate: start date of previous x month of current date
 *       endDate: end date of previous month of current date
 * @param inputDate
 * @param inputMonthAmount
 * @return
 */
Map getPreviousPeriodFromDate(Date inputDate, Integer inputMonthAmount = 0) {
  if (!inputDate) {
    return null
  }

  Date startDate = addMonths(inputDate, (-1) * inputMonthAmount)
  Date startOfMonthStartDate = getMonthStartDate(startDate)
  Date endOfMonthEndDate = addDays(getMonthStartDate(inputDate), -1)

  return [startDate: startOfMonthStartDate,
          endDate  : endOfMonthEndDate]
}
/**
 *
 * @param inputDate
 * @param inputMonthAmount
 * @return
 */
Map getNextPeriodFromDate(Date inputDate, Integer inputMonthAmount = 0) {
  if (!inputDate) {
    return null
  }

  Date startOfMonthStartDate = getMonthStartDate(inputDate)
  Date endOfMonthEndDate = getMonthEndDate(addMonths(inputDate, inputMonthAmount))

  return [startDate: startOfMonthStartDate,
          endDate  : endOfMonthEndDate]
}
/**
 * format Date to String
 * @param inputDate
 * @return
 */
String formatDateToString(Date inputDate) {
  return (inputDate ? inputDate.format(libs.CustomerInsights.Constant.FORMAT.DATE) : null)
}
/**
 * format String to Date
 * @param inputString
 * @return
 */
Date parseStringToDate(String inputString) {
  return (inputString ? api.parseDate(libs.CustomerInsights.Constant.FORMAT.DATE, inputString) : null)
}

protected Date setStartTime(Date inputDate) {
  Calendar calendar = Calendar.getInstance()
  calendar.set(inputDate[Calendar.YEAR], inputDate[Calendar.MONTH], inputDate[Calendar.DATE], 0, 0, 0)
  calendar.set(Calendar.MILLISECOND, 0)

  return calendar.getTime()
}

protected Date setEndTime(Date inputDate) {
  Calendar calendar = Calendar.getInstance()
  calendar.set(inputDate[Calendar.YEAR], inputDate[Calendar.MONTH], inputDate[Calendar.DATE], 23, 59, 59)
  calendar.set(Calendar.MILLISECOND, 0)

  return calendar.getTime()
}
/**
 * if inputDate = null --> return current date (new Date())
 * if inputDate != null --> parse to Date
 * @param inputDate
 * @return
 */
Date getDateValue(def inputDate) {
  if (!inputDate) {
    return new Date()
  }

  return (inputDate instanceof Date) ? inputDate : parseStringToDate(inputDate)
}
/**
 * calculate month amount between startDate and endDate
 * return 0 if startDate=null or endDate=null
 * @param startDate
 * @param endDate
 * @return
 */
Integer getMonthAmount(Date startDate, Date endDate) {
  if (!startDate || !endDate) {
    return 0
  }

  return ((endDate[Calendar.YEAR] - startDate[Calendar.YEAR]) * 12 + (endDate[Calendar.MONTH] - startDate[Calendar.MONTH]) + 1)
}