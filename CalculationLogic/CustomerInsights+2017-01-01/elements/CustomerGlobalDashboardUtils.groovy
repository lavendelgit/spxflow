import net.pricefx.formulaengine.DatamartContext

/**
 * Get data from simulation to show on dashboard
 * @param configuration
 * @param customerGlobalViewParams
 * @return
 */
List calculateMetricsPerCustomer(Map configuration, Map customerGlobalViewParams) {
  def dateUtils = libs.CustomerInsights.DateUtils
  def constant = libs.CustomerInsights.Constant
  def queryUtils = libs.CustomerInsights.QueryUtils
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils

  List getBuyingProductAmountFilters = customerGlobalViewParams.getBuyingProductAmountQuerySimulationFilter(configuration)
  List getCustomerFilters = customerGlobalViewParams.getCustomerQuerySimulationFilter(configuration)
  List getAllCustomerFilters = customerGlobalViewParams.getAllCustomerQuerySimulationFilter(configuration)

  BigDecimal totalBuyingProductAmount = calculationCommonUtils.getBuyingProductAmount(configuration, getBuyingProductAmountFilters)
  DatamartContext.Query otherMetricQuery = queryHelper.createQueryForSumValuePerCustomer(configuration, getCustomerFilters)
  DatamartContext.Query transactionQuery = queryHelper.createQueryForSumValuePerCustomerAndProduct(configuration, getAllCustomerFilters)

  Integer numberOfDays = dateUtils.getDaysDifference(customerGlobalViewParams.getStartDate(), customerGlobalViewParams.getEndDate())
  String orderByValue = calculationCommonUtils.getOrderValue(customerGlobalViewParams.getKPI())
  String unknown = constant.DEFAULT_VALUE.UNKNOWN

  String sqlBasic = """SELECT COALESCE(TransactionData.customerSegment, '${unknown}')   AS 'customersegment',
                              TransactionData.productId                                 AS 'productid',
                              AVG(TransactionData.revenuePerUnit)                       AS 'unitprice',
                              AVG(TransactionData.revenue)                              AS 'revenue'    
                       FROM T1 TransactionData 
                       GROUP BY customersegment, productid"""
  String sqlUpSell_Sub_01 = """
        SELECT Trans.customerId                              AS 'customerid', 
               COALESCE(Trans.customerSegment,'${unknown}')  AS 'customersegment',
               Trans.productId                               AS 'productid',
               Trans.revenue                                 AS 'revenue',
               IIF( Trans.revenuePerUnit - AVG_SEGMENT_DATA.unitprice < 0,
                    (Trans.revenuePerUnit - AVG_SEGMENT_DATA.unitprice)*Trans.Quantity*(-1),
                    0 )                                      AS 'revenuebelowtarget',
               IIF( Trans.revenuePerUnit - AVG_SEGMENT_DATA.unitprice < 0,
                         Trans.revenue, 0 )                  AS 'revenueforrevenuebelowtargetpercent',
               IIF( Trans.quantity < AVG(Trans.quantity) OVER(PARTITION BY Trans.customerSegment, Trans.productId),
                     (AVG(Trans.quantity) OVER(PARTITION BY Trans.customerSegment, Trans.productId) - Trans.quantity)*Trans.revenuePerUnit,
                     0 )                   AS 'up_sell'
        FROM T1 Trans INNER JOIN (${sqlBasic}) AVG_SEGMENT_DATA 
          ON  COALESCE(Trans.customerSegment, '${unknown}') = COALESCE(AVG_SEGMENT_DATA.customersegment, '${unknown}') 
          AND Trans.productId = AVG_SEGMENT_DATA.productid"""

  String sqlUpSell = """ SELECT customerid                         AS 'customerid',
                                SUM(revenue)                       AS 'revenue',
                                SUM(revenuebelowtarget)            AS 'revenuebelowtarget',
                                IIF(SUM(revenueforrevenuebelowtargetpercent) <> 0,
                                    SUM(revenuebelowtarget)/SUM(revenueforrevenuebelowtargetpercent),
                                    0)                             AS 'revenuebelowtargetpercent',
                                SUM(up_sell)                       AS 'upsell'     
                         FROM     
                             ( ${sqlUpSell_Sub_01} )            
                          GROUP BY customerid """

  String sqlCrossSell = """SELECT RAW_DATA.customerId                                AS 'customerid', 
                                  ((TOTAL_SEG_DATA.revenue) - SUM(SEG_DATA.revenue)) AS 'crosssell'
                           FROM T1 RAW_DATA 
                           INNER JOIN       
                              (${sqlBasic}) SEG_DATA
                           ON COALESCE(RAW_DATA.customerSegment,'${unknown}') = SEG_DATA.customersegment AND RAW_DATA.productId = SEG_DATA.productid
                           LEFT JOIN
                           ( SELECT customersegment  AS 'customersegment', 
                                    SUM(revenue)     AS 'revenue'
                             FROM (${sqlBasic}) 
                             GROUP BY customersegment                             
                           ) TOTAL_SEG_DATA ON COALESCE(RAW_DATA.customerSegment,'${unknown}') = TOTAL_SEG_DATA.customersegment
                           GROUP BY  RAW_DATA.customerId,TOTAL_SEG_DATA.revenue """

  String sql = """ SELECT customerid                                 AS 'customerId',
                          revenue                                    AS 'revenue',
                          revenuebelowtarget                         AS 'revenueBelowTarget',
                          revenuebelowtargetpercent                  AS 'revenueBelowTargetPercent',
                          upsell                                     AS 'upSell',
                          crosssell                                  AS 'crossSell',
                          (TO_NUMBER(crosssell) + TO_NUMBER(upsell)) AS 'opportunity',
                          pricingopportunity                         AS 'pricingOpportunity',
                          buyingfrequency                            AS 'buyingFrequency',
                          buyingproductpercent                       AS 'buyingProductPercent',
                          customername                               AS 'customerName',
                          margin                                     AS 'margin',
                          quantity                                   AS 'quantity',
                          avgmarginpercent                           AS 'avgMarginPercent',
                          customerhealthscore                        AS 'customerHealthScore',
                          customerrevenuetrend                       AS 'customerRevenueTrend',
                          customerMarginTrend                        AS 'customerMarginTrend',
                          customerQuantityTrend                      AS 'customerQuantityTrend',
                          customerYTDRevenueTrend                    AS 'customerYTDRevenueTrend',
                          customerYTDMarginTrend                     AS 'customerYTDMarginTrend',
                          customerYTDQuantityTrend                   AS 'customerYTDQuantityTrend',
                          customerClassificationByRevenue            AS 'customerClassificationByRevenue',
                          customerClassificationByHealthScore        AS 'customerClassificationByHealthScore', 
                          transactionAmount                          AS 'transactionAmount',
                          buyingProductAmount                        AS 'buyingProductAmount'
                   FROM
                        ( SELECT up_sell_sql.customerid                  AS 'customerid',
                                 up_sell_sql.revenue                     AS 'revenue',
                                 up_sell_sql.revenuebelowtarget          AS 'revenuebelowtarget',
                                 up_sell_sql.revenuebelowtargetpercent   AS 'revenuebelowtargetpercent',
                                 up_sell_sql.upsell                      AS 'upsell',
                                 cross_sell_sql.crosssell                AS 'crosssell',
                                 (cross_sell_sql.crosssell + up_sell_sql.revenuebelowtarget)  AS 'pricingopportunity',
                                 IIF( ${numberOfDays} <> 0,
                                      TO_NUMBER(other_metric_sql.transactionAmount) / ${numberOfDays},
                                      0 )                                                    AS 'buyingfrequency',
                                 IIF( ${totalBuyingProductAmount} <> 0,
                                      TO_NUMBER(other_metric_sql.buyingProductAmount) / ${totalBuyingProductAmount},
                                      0 )                                             AS 'buyingproductpercent',
                              other_metric_sql.customerName                        AS 'customername',
                              other_metric_sql.margin                              AS 'margin',
                              other_metric_sql.quantity                            AS 'quantity',
                              other_metric_sql.avgMarginPercent                    AS 'avgmarginpercent',
                              other_metric_sql.customerHealthScore                 AS 'customerhealthscore',
                              other_metric_sql.customerRevenueTrend                AS 'customerrevenuetrend',
                              other_metric_sql.customerMarginTrend                 AS 'customermargintrend',
                              other_metric_sql.customerQuantityTrend               AS 'customerquantitytrend',
                              other_metric_sql.customerYTDRevenueTrend             AS 'customerytdrevenuetrend',
                              other_metric_sql.customerYTDMarginTrend              AS 'customerytdmargintrend',
                              other_metric_sql.customerYTDQuantityTrend            AS 'customerytdquantitytrend',
                              other_metric_sql.customerClassificationByRevenue     AS 'customerclassificationbyrevenue',
                              other_metric_sql.customerClassificationByHealthScore AS 'customerclassificationbyhealthscore',
                              other_metric_sql.transactionAmount                   AS 'transactionamount',
                              other_metric_sql.buyingProductAmount                 AS 'buyingproductamount'
                       FROM (${sqlUpSell}) up_sell_sql INNER JOIN (${sqlCrossSell}) cross_sell_sql
                         ON up_sell_sql.customerid = cross_sell_sql.customerid 
                         INNER JOIN T2 other_metric_sql ON up_sell_sql.customerid = other_metric_sql.customerId 
                       )
                   ORDER BY ${orderByValue} DESC , customerid ASC """

  return queryUtils.getFullQueryData(sql, transactionQuery, otherMetricQuery)
}
/**
 * get summary info for selected Customers
 * @param configuration
 * @param customerGlobalViewParams
 * @return
 */
Map getSummaryInfoForCustomerGlobalView(Map configuration, Map customerGlobalViewParams) {
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils

  List basicQueryDatamartFilters = customerGlobalViewParams.getBasicQueryDatamartFilter(configuration)
  Map data = calculationCommonUtils.getSummaryValuesFromOriginalSource(configuration, basicQueryDatamartFilters)

  if (data.revenue) { //just calculate and add YTD trend values if data is existed
    Map trendYTD = getYTDTrendValues(configuration, customerGlobalViewParams)
    data.customerYTDRevenueTrend = trendYTD.revenueYTDTrend
    data.customerYTDMarginTrend = trendYTD.marginYTDTrend
    data.customerYTDQuantityTrend = trendYTD.quantityYTDTrend
    data.customerHealthScore = getCustomerHealthScore(configuration, customerGlobalViewParams)
  }

  return data
}

protected Map getYTDTrendValues(Map configuration, Map customerGlobalViewParams) {
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils
  def dateUtils = libs.CustomerInsights.DateUtils
  def constant = libs.CustomerInsights.Constant

  Map YTDPeriod = dateUtils.getPeriod(customerGlobalViewParams.getCurrentDate(), constant.TIME_PERIOD.YTD)
  Map previousYTDPeriod = dateUtils.addYearsToPeriod(YTDPeriod, -1)//last year
  List YTDFilters = customerGlobalViewParams.getCustomPeriodQueryDatamartFilter(configuration, YTDPeriod.startDate, YTDPeriod.endDate)
  List previousYTDFilters = customerGlobalViewParams.getCustomPeriodQueryDatamartFilter(configuration, previousYTDPeriod.startDate, previousYTDPeriod.endDate)

  return calculationCommonUtils.calculateYTDTrendFromOriginalSource(configuration, YTDFilters, previousYTDFilters)
}

protected BigDecimal getCustomerHealthScore(Map configuration, Map customerGlobalViewParams) {
  def classificationUtils = libs.CustomerInsights.Classification
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils
  def dateUtils = libs.CustomerInsights.DateUtils
  def constant = libs.CustomerInsights.Constant

  Map L12MPeriod = dateUtils.getPeriod(customerGlobalViewParams.getCurrentDate(), constant.TIME_PERIOD.L12M)
  List L12MFilters = customerGlobalViewParams.getCustomPeriodQueryDatamartFilter(configuration, L12MPeriod.startDate, L12MPeriod.endDate)
  Map trendL12M = calculationCommonUtils.calculateL12MTrendForCustomerFromOriginalSource(configuration, L12MFilters, L12MPeriod)

  return classificationUtils.calculateHealthScore(configuration, trendL12M.revenueTrend, trendL12M.marginTrend)
}