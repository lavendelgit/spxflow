/**
 * Get customer segment from source types based on segmentSources
 * @param segmentSources
 * @return
 */
Map getCustomerSegment(List segmentSources, String customerIdInput = null) {
  List customerSegments = getCustomerSegmentDataForEachSource(segmentSources, customerIdInput)

  if (!customerSegments) {
    return [:]
  }

  Map firstSegment = customerSegments.getAt(0)
  if (customerSegments.size() == 1) {
    return firstSegment
  }

  Set customerIds = getSetOfCustomerIdFromSegments(customerSegments)
  Map finalMap = [:]
  for (customerId in customerIds) {
    finalMap << buildSegmentValue(customerId, customerSegments)
  }

  return finalMap
}

protected List getCustomerSegmentDataForEachSource(List segmentSources, String customerIdInput = null) {
  List customerSegments = []
  Map segmentData
  for (segmentSource in segmentSources) {
    segmentData = queryDataOfCustomerSegment(segmentSource, customerIdInput)
    customerSegments.add(segmentData)
  }

  return customerSegments
}

protected Map queryDataOfCustomerSegment(Map customerSegment, String customerIdInput = null) {
  def constant = libs.CustomerInsights.Constant
  def customerUtils = libs.CustomerInsights.CustomerUtils

  Map customerSegmentInfo = customerSegment
  if (!customerSegmentInfo.customerSegmentAttributes) {
    return null
  }

  List rawAttributes = customerSegmentInfo.customerSegmentAttributes //include customerId
  String sourceType = customerSegmentInfo.sourceType
  String sourceName = customerSegmentInfo.sourceName
  List rawData
  String customerId = "customerId" //default field name in C/CX
  List attributes = rawAttributes.findAll { it.isCustomerId == null }?.sourceField
  // just get attributes exclude customerId
  Map sourceTypeConfig = constant.SOURCE_TYPE

  switch (sourceType.toUpperCase()) {
    case sourceTypeConfig.C:
      rawData = customerUtils.getAllCustomerIdsWithOtherAttributes(attributes, customerIdInput)
      break
    case sourceTypeConfig.CX:
      rawData = customerUtils.getAllCustomerIdsWithOtherAttributesFromCX(sourceName, attributes, customerIdInput)
      break
    case sourceTypeConfig.DMDS:
      // customerId field name in data source
      customerId = rawAttributes.find {
        constant.CUSTOMER_SEGMENT_PP_CONFIG.IS_TRUE.equalsIgnoreCase(it.isCustomerId)
      }?.sourceField
      Filter filter
      if (customerIdInput) {
        filter = Filter.equal(customerId, customerIdInput)
      }
      rawData = customerUtils.getAllCustomerIdsWithOtherAttributesFromDataSource(sourceName, customerId, rawAttributes.sourceField, filter)
      break
    default:
      rawData = customerUtils.getAllCustomerIdsWithOtherAttributes(attributes, customerIdInput)
  }
  List processedData = mergeDataOfCustomerSegment(rawData, attributes)

  return processedData.groupBy { it.getAt(customerId) }
}

protected List mergeDataOfCustomerSegment(List rawData, List attributes) {
  Map defaultValue = libs.CustomerInsights.Constant.DEFAULT_VALUE
  List processedData = []
  List processedAttributes
  Map currentItem
  String unknown = defaultValue.UNKNOWN
  String separator = defaultValue.DASH_SEPARATOR

  for (item in rawData) {
    currentItem = item.clone()
    processedAttributes = attributes ? (attributes.collect { (item.getAt(it) ?: unknown) }) : [unknown]
    currentItem.customerSegment = processedAttributes.join(separator)
    processedData.add(currentItem)
  }

  return processedData
}
/**
 * build segment value from list of Map segment from different sources
 * to keep structure as Map segment from source, return map with:
 *     key is customerId
 *     value is a list just have 1 item
 * --> performance: because in function getCustomerSegment we return first item in case we just have one source
 * @param customerId
 * @param customerSegments
 * @return
 */
protected Map buildSegmentValue(String customerId, List customerSegments) {
  Map defaultValue = libs.CustomerInsights.Constant.DEFAULT_VALUE
  String separator = defaultValue.DASH_SEPARATOR
  String unknown = defaultValue.UNKNOWN

  if (!customerId) {
    return [:]
  }

  String segment, cumulativeSegment = ""
  int customerSegmentSize = customerSegments?.size() ?: 0

  for (int i = 0; i < customerSegmentSize; i++) {
    segment = (customerSegments.getAt(i).getAt(customerId)?.getAt(0)?.customerSegment ?: unknown)
    cumulativeSegment += (i == 0 ? segment : (separator + segment))
  }

  List value = [[customerId: customerId, customerSegment: cumulativeSegment]]

  return [(customerId): (value)]
}
/**
 * get set of CustomerId from segments
 * @param customerSegments
 * @return
 */
protected Set getSetOfCustomerIdFromSegments(List customerSegments) {
  Set customerIds = []

  for (Map customerSegmentItem in customerSegments) {
    customerIds.addAll(customerSegmentItem.keySet())
  }

  return customerIds
}