/**
 * create a list/dropdown list time filter
 * @param configuration
 * @return
 */
def createTimeFilter(Map configuration) {
  return createDropdownListFromConfiguration(configuration, libs.CustomerInsights.Constant.INPUT_ENTRY.TIME_FILTER.TYPE)
}
/**
 * create a inline configurator
 * @return
 */
def createDimensionFilter() {
  Map dimensionFilter = libs.CustomerInsights.Constant.DIMENSION_FILTER_CONFIG

  return api.inlineConfigurator(dimensionFilter.NAME, dimensionFilter.LOGIC_NAME)
}
/**
 * create dropdown list of top/worst. Get data from PP
 * @param inputName
 * @param topValues
 * @return
 */
def createTopWorstList(String inputName, List topValues) {
  def constant = libs.CustomerInsights.Constant
  List sortedTop = topValues?.sort { a, b -> a.order <=> b.order } ?: []
  Map defaultItem = sortedTop.find {
    constant.CONFIGURATION_PP_CONFIG.IS_TRUE.equalsIgnoreCase(it.isDefault)
  } ?: sortedTop.getAt(0)
  def userOption = api.option(inputName, sortedTop.value)
  setUserEntryDefaultValue(inputName, defaultItem.value)

  return userOption
}
/**
 * create  a KPI drop down list
 *
 * @param configuration
 * @return option
 */
def createKPIOption(Map configuration) {
  return createDropdownListFromConfiguration(configuration, libs.CustomerInsights.Constant.INPUT_ENTRY.KPI.TYPE)
}
/**
 * create dropdownlist input based on configuration
 * @param configuration
 * @param keyNameInput
 * @return
 */
protected def createDropdownListFromConfiguration(Map configuration, String keyNameInput) {
  def constant = libs.CustomerInsights.Constant
  List keyNames
  String dropdownName

  switch (keyNameInput) {
    case constant.INPUT_ENTRY.KPI.TYPE:
      keyNames = configuration.MASTER_DATA.KPI
      dropdownName = constant.INPUT_ENTRY.KPI.NAME
      break
    case constant.INPUT_ENTRY.TIME_FILTER.TYPE:
      keyNames = configuration.MASTER_DATA.TIME_FILTER
      dropdownName = constant.INPUT_ENTRY.TIME_FILTER.NAME
      break
    default:
      keyNames = configuration.MASTER_DATA.KPI
      dropdownName = constant.INPUT_ENTRY.KPI.NAME
  }

  List sortedKeyNames = keyNames.sort { a, b -> a.order <=> b.order }
  Map defaultItem = sortedKeyNames.find { constant.CONFIGURATION_PP_CONFIG.IS_TRUE.equalsIgnoreCase(it.isDefault) }
  Map optionLabel = createOptionLabel(sortedKeyNames)
  List optionValues = optionLabel.keySet() as List
  def userOption = api.option(dropdownName, optionValues, optionLabel)
  //set default value
  setUserEntryDefaultValue(dropdownName, defaultItem?.value)

  return userOption
}

protected Map createOptionLabel(List data) {
  String label

  return data.collectEntries { item ->
    label = item.label ?: item.value

    return [(item.value): label]
  }
}

protected void setUserEntryDefaultValue(String entryName, def defaultValue) {
  def param = api.getParameter(entryName)
  if (param && param.getValue() == null && defaultValue) {
    param.setRequired(true)
    param.setValue(defaultValue)
  }
}
/**
 *
 * @param configuration
 * @param dimension
 * @return
 */
Map getDimensionFilterLabel(Map configuration, Map dimension) {
  if (!dimension?.FieldName || !dimension?.Value) {
    return [fieldLabel: "", fieldValue: ""]
  }
  Map fields = libs.CustomerInsights.Configuration.getDatasourceFields(configuration.SourceType, configuration.SourceName, "[System]", true)
  String fieldLabel = fields.getAt(dimension.FieldName)

  return [fieldLabel: (fieldLabel + ":"), fieldValue: dimension.Value]
}
/**
 * create a multi choice input options
 * value of options queried from Simulation based on customerFilter
 * @param configuration
 * @param customerFilter
 * @return
 */
def createCustomerOrProductAttributeFilter(Map configuration, Map customerFilter, Boolean isMultiChoice = false) {
  List optionValues = getOptionValueByCustomerOrProductAttribute(configuration, customerFilter.TYPE)
  def customerAttributeFilter

  if (isMultiChoice) {
    customerAttributeFilter = api.options(customerFilter.NAME, optionValues)
  } else {
    customerAttributeFilter = api.option(customerFilter.NAME, optionValues)
  }

  return customerAttributeFilter
}

protected List getOptionValueByCustomerOrProductAttribute(Map configuration, String attributeType) {
  Map inputEntry = libs.CustomerInsights.Constant.INPUT_ENTRY
  Map customerGlobalViewFilter = inputEntry.CUSTOMER_GLOBAL_VIEW_FILTER
  Map customerDetailViewFilter = inputEntry.CUSTOMER_DETAIL_VIEW_FILTER
  List optionValues

  switch (attributeType) {
    case customerGlobalViewFilter.CUSTOMER_CLASSIFICATION_REVENUE.TYPE:
      optionValues = getAllValueFromCategory(configuration.CUSTOMER_CLASSIFICATION_REVENUE)
      break

    case customerGlobalViewFilter.CUSTOMER_CLASSIFICATION_HEALTHSCORE.TYPE:
      optionValues = getAllValueFromCategory(configuration.CUSTOMER_CLASSIFICATION_HEALTH_SCORE)
      break

    case customerDetailViewFilter.PRODUCT_CLASSIFICATION_QUANTITY.TYPE:
      optionValues = getAllValueFromCategory(configuration.PRODUCT_CLASSIFICATION_QUANTITY)
      break

    case customerDetailViewFilter.PRODUCT_CLASSIFICATION_MARGIN_PERCENT.TYPE:
      optionValues = getAllValueFromCategory(configuration.PRODUCT_CLASSIFICATION_MARGIN_PERCENT)
      break

    case customerDetailViewFilter.PRODUCT_CLASSIFICATION_HEALTHSCORE.TYPE:
      optionValues = getAllValueFromCategory(configuration.PRODUCT_CLASSIFICATION_HEALTH_SCORE)
      break
    
    default:
      optionValues = []
  }

  return optionValues
}
/**
 * get list value of category in PP PFXTemplate_CustomerInsights_Configuration
 * @param category
 * @return
 */
protected List getAllValueFromCategory(Map category) {
  List values = []

  for (categoryItem in category) {
    values.add(categoryItem.value?.getAt(0)?.label)
  }

  return values.unique()
}
