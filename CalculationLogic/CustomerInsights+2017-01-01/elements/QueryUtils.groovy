import net.pricefx.formulaengine.DatamartContext
import net.pricefx.formulaengine.DatamartQueryResult

DatamartQueryResult queryDataSource(String sourceType, String sourceName, Map resultFields, boolean rollup, List orderByList, Filter... filters) {
  DatamartContext.Query query = getContextQuery(sourceType, sourceName, resultFields, rollup, orderByList, *filters)

  return api.getDatamartContext().executeQuery(query)
}

DatamartContext.Query getContextQuery(String sourceType, String sourceName, Map resultFields, boolean rollup, List orderByList, Filter... filters) {
  def dataSourceConnection = getDataSourceConnection(sourceType, sourceName)
  DatamartContext.Query query = api.getDatamartContext().newQuery(dataSourceConnection, rollup)
  for (field in resultFields) {
    query.select(field.value, field.key)
  }
  query.where(filters)
  for (orderBy in orderByList) {
    query.orderBy(orderBy)
  }

  return query
}

String createSQLForSum(String fieldName) {
  return "SUM(IFNULL(${fieldName},0))"
}

String createSQLForAverage(String fieldName) {
  return "AVG(IFNULL(${fieldName},0))"
}

protected def getDataSourceConnection(String sourceType, String sourceName) {
  DatamartContext datamartContext = api.getDatamartContext()
  Map sourceTypeConstant = libs.CustomerInsights.Constant.SOURCE_TYPE
  boolean isDataSource = sourceTypeConstant.DMDS.equalsIgnoreCase(sourceType)

  return isDataSource ? datamartContext.getDataSource(sourceName) : datamartContext.getDatamart(sourceName)
}

List getFullQueryData(String sqlStatement, DatamartContext.Query... query) {
  Map queryConfiguration = libs.CustomerInsights.Constant.QUERY_CONFIG
  Integer limitedRow = queryConfiguration.LIMITED_ROW_AMOUNT
  Integer maxInterval = queryConfiguration.MAX_INTERVAL
  Integer counter = 0, offset = 0
  String localSQLStatement
  List finalData = [], offsetData
  while (counter < maxInterval && (counter == 0 || (offsetData?.size() == limitedRow))) {
    localSQLStatement = sqlStatement + " LIMIT $limitedRow OFFSET $offset "
    offsetData = api.getDatamartContext().executeSqlQuery(localSQLStatement, query)?.collect()
    finalData.addAll(offsetData)
    offset += limitedRow
    counter++
  }

  return finalData
}