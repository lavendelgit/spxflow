import net.pricefx.formulaengine.DatamartContext

/**
 * Get average prices of product based on Customer Classification
 * Data is queried from DSIM with params in inputParams
 * @param configuration
 * @param portfolioDashboardParams
 * @return
 */
List getAverageInvoicePricePerProduct(Map configuration, Map portfolioDashboardParams) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def queryUtils = libs.CustomerInsights.QueryUtils

  Map customerClassRevenue = configuration.CUSTOMER_CLASSIFICATION_REVENUE
  Set customerClasses = customerClassRevenue?.keySet()
  String customerClassFields = ""
  String avgPriceByCustomerClassSQL = """ SELECT 
                               RawDataQuery.productId                            AS 'productid',
                               RawDataQuery.productName                          AS 'productname',
                               AVG(RawDataQuery.overallPrice)                    AS 'overallprice',
                               RawDataQuery.productClassificationByMarginPercent AS 'productclassificationbymarginpercent',
                               SUM(RawDataQuery.revenue)                         AS 'revenue',
                               SUM(RawDataQuery.margin)                          AS 'margin',
                               SUM(RawDataQuery.quantity)                        AS 'quantity' """
  for (customerClass in customerClasses) {
    avgPriceByCustomerClassSQL += ", AVG(CASE WHEN RawDataQuery.customerClassificationByRevenue = '${customerClass}' THEN RawDataQuery.avgPriceByCustomerClass END) AS ${customerClass} "
    customerClassFields += ", ${customerClass}  AS '${customerClass}' "
  }
  avgPriceByCustomerClassSQL += """ FROM T1 RawDataQuery 
             GROUP BY productId, productName, productClassificationByMarginPercent """

  String avgPricePerCustomerSQL = """SELECT productId          AS 'productid',
                                            avgPriceByCustomer AS 'avgpricebycustomer'
                                     FROM T2 """
  String sql = """ SELECT AVG_PRICE_BY_CLASS_SQL.productid                            AS 'productId',
                          AVG_PRICE_BY_CLASS_SQL.productname                          AS 'productName',
                          AVG_PRICE_BY_CLASS_SQL.overallprice                         AS 'overallPrice',
                          AVG_PRICE_BY_CLASS_SQL.productclassificationbymarginpercent AS 'productClassificationByMarginPercent',
                          AVG_PRICE_BY_CUSTOMER_SQL.avgpricebycustomer                AS 'avgPriceByCustomer'
                          ${customerClassFields}
                   FROM (${avgPriceByCustomerClassSQL}) AVG_PRICE_BY_CLASS_SQL INNER JOIN (${avgPricePerCustomerSQL}) AVG_PRICE_BY_CUSTOMER_SQL
                    ON AVG_PRICE_BY_CLASS_SQL.productid = AVG_PRICE_BY_CUSTOMER_SQL.productid
                    ORDER BY AVG_PRICE_BY_CLASS_SQL.overallprice DESC , AVG_PRICE_BY_CLASS_SQL.productId ASC"""

  List basicQuerySimulationFilters = portfolioDashboardParams.getBasicQuerySimulationFilter(configuration)
  List nonSegmentQuerySimulationFilters = portfolioDashboardParams.getValueOnSegmentQuerySimulationFilter(configuration, null)
  DatamartContext.Query rawDataQuery = queryHelper.createQueryForSumValuePerProductAndCustomerClass(configuration, nonSegmentQuerySimulationFilters)
  DatamartContext.Query avgPriceQuery = queryHelper.createQueryForGetInfoPerProduct(configuration, basicQuerySimulationFilters)

  return queryUtils.getFullQueryData(sql, rawDataQuery, avgPriceQuery)
}
/**
 * Get Product Health and Trends data
 * @param configuration
 * @param portfolioDashboardParams
 * @return
 */
List getProductHealthData(Map configuration, Map portfolioDashboardParams) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def queryUtils = libs.CustomerInsights.QueryUtils

  List nonSegmentQuerySimulationFilters = portfolioDashboardParams.getBasicQuerySimulationFilter(configuration)
  DatamartContext.Query productHealthSQL = queryHelper.createQueryForGetInfoPerProduct(configuration, nonSegmentQuerySimulationFilters)
  String sql = """ SELECT
                      PRODUCT_HEALTH_DATA.productId          AS 'productId',
                      PRODUCT_HEALTH_DATA.productName        AS 'productName',
                      PRODUCT_HEALTH_DATA.productHealthScore AS 'productHealthScore',
                      PRODUCT_HEALTH_DATA.revenue            AS 'revenue',
                      PRODUCT_HEALTH_DATA.margin             AS 'margin',          
                      PRODUCT_HEALTH_DATA.marginPercent      AS 'marginPercent',
                      PRODUCT_HEALTH_DATA.quantity           AS 'quantity',
                      PRODUCT_HEALTH_DATA.revenueTrend       AS 'revenueTrend',
                      PRODUCT_HEALTH_DATA.marginTrend        AS 'marginTrend',
                      PRODUCT_HEALTH_DATA.quantityTrend      AS 'quantityTrend',
                      PRODUCT_HEALTH_DATA.revenueTrendYTD    AS 'revenueTrendYTD',
                      PRODUCT_HEALTH_DATA.marginTrendYTD     AS 'marginTrendYTD',
                      PRODUCT_HEALTH_DATA.quantityTrendYTD   AS 'quantityTrendYTD'
                   FROM T1 PRODUCT_HEALTH_DATA 
                   ORDER BY productHealthScore DESC , PRODUCT_HEALTH_DATA.productId ASC """

  return queryUtils.getFullQueryData(sql, productHealthSQL)
}
/**
 * Get Cross Selling Opportunity data based on Product Attribute
 * @param configuration
 * @param portfolioDashboardParams
 * @return
 */
List getOpportunityPerProductAttribute(Map configuration, Map portfolioDashboardParams) {
  List data = calculateOpportunities(configuration, portfolioDashboardParams)
  Map groupedByProductAttData = data?.groupBy { it.productAttribute }
  List opportunityByProductAttribute = []
  Set groupedByProductAttDataKeys = groupedByProductAttData?.keySet()?.sort({ a, b -> a <=> b }) //sort by alphabet
  for (key in groupedByProductAttDataKeys) {
    opportunityByProductAttribute.add(calculateOpportunityItem(groupedByProductAttData.get(key), key))
  }

  return opportunityByProductAttribute
}
/**
 * calculate Up-sell/Crosss-sell/Revenue below target based on product attribute and customer segment
 * using SQL to improve performance in case larged data
 * @param configuration
 * @param portfolioDashboardParams
 * @return
 */
protected List calculateOpportunities(Map configuration, Map portfolioDashboardParams) {
  def constant = libs.CustomerInsights.Constant
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def queryUtils = libs.CustomerInsights.QueryUtils
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils

  String currentCustomerId = portfolioDashboardParams.getCustomerId()
  String productAttribute = portfolioDashboardParams.getProductAttribute()
  String customerSegment = calculationCommonUtils.getCustomerSegmentByCustomer(configuration, currentCustomerId)

  DatamartContext.Query productAttributeQuery = queryHelper.createQueryForGetProductAttribute(configuration, productAttribute)
  List segmentQuerySimulationFilters = portfolioDashboardParams.getValueOnSegmentQuerySimulationFilter(configuration, customerSegment)
  DatamartContext.Query transactionQuery = queryHelper.createQueryForSumValuePerCustomerAndProduct(configuration, segmentQuerySimulationFilters)
  String unknown = constant.DEFAULT_VALUE.UNKNOWN

  String sqlBasic = """SELECT TRANS_DATA.customerId                                            AS 'customerid',
                              TRANS_DATA.productId                                             AS 'productid',
                              TRANS_DATA.quantity                                              AS 'quantity',
                              TRANS_DATA.revenue                                               AS 'revenue',
                              TRANS_DATA.revenuePerUnit                                        AS 'revenueperunit',
                              TRANS_DATA.customerSegment                                       AS 'customersegment',
                              PRODUCT_ATT.productAttribute                                     AS 'productattribute',
                              CONCAT(PRODUCT_ATT.productAttribute ,
                                     COALESCE(TRANS_DATA.customerSegment,'${unknown}'))        AS 'productcustomersegment'
                      FROM T1 TRANS_DATA INNER JOIN T2 PRODUCT_ATT ON TRANS_DATA.productId = PRODUCT_ATT.productId """

  String sqlRevenueTarget = """ 
                               SELECT  TransactionData.customersegment       AS 'customersegment',
                                       TransactionData.productid             AS 'productid',
                                       AVG(TransactionData.revenueperunit)   AS 'revenueperunit'
                               FROM (${sqlBasic}) TransactionData 
                               GROUP BY TransactionData.customersegment, TransactionData.productid """

  String sqlUpSell_Sub_01 = """ SELECT Trans.customerid                        AS 'customerid', 
                                       Trans.productcustomersegment            AS 'productcustomersegment',
                                       Trans.productattribute                  AS 'productattribute',
                                       Trans.productid                         AS 'productid',
                                       Trans.revenue                           AS 'revenue',
                                       Trans.quantity                          AS 'quantity',
                                       Trans.revenueperunit                    AS 'revenueperunit',
                                       IIF( Trans.revenueperunit - AVG_PRODUCT_DATA.revenueperunit < 0,
                                            (Trans.revenueperunit - AVG_PRODUCT_DATA.revenueperunit)*Trans.quantity*(-1),
                                            0 )                                AS 'revenuebelowtarget',
                                       IIF( Trans.revenueperunit - AVG_PRODUCT_DATA.revenueperunit < 0,
                                            Trans.revenue, 0)                  AS 'revenueforrevenuebelowtargetpercent',
                                       IIF( Trans.quantity < AVG(Trans.quantity) OVER(PARTITION BY Trans.productcustomersegment, Trans.productid),
                                            (AVG(Trans.quantity) OVER(PARTITION BY Trans.productcustomersegment, Trans.productid) - Trans.quantity)*Trans.revenueperunit,
                                            0 )                                AS 'up_sell'
               FROM (${sqlBasic}) Trans INNER JOIN (${sqlRevenueTarget}) AVG_PRODUCT_DATA ON 
                    COALESCE(Trans.customersegment,'${unknown}') = COALESCE(AVG_PRODUCT_DATA.customersegment,'${unknown}')  
               AND  Trans.productid = AVG_PRODUCT_DATA.productid AND Trans.customersegment = '${customerSegment}' """

  String sqlUpSell = """ SELECT productattribute, productcustomersegment, 
                                SUM(revenue)                    AS 'revenue', 
                                SUM(up_sell)                     AS 'upsell',
                                SUM(revenuebelowtarget)         AS 'revenuebelowtarget',
                                IIF(SUM(revenueforrevenuebelowtargetpercent) <> 0,
                                    SUM(revenuebelowtarget)/SUM(revenueforrevenuebelowtargetpercent),
                                    0)                             AS 'revenuebelowtargetpercent'
                         FROM (${sqlUpSell_Sub_01})
                         WHERE customerid = '${currentCustomerId}'
                         GROUP BY productattribute, productcustomersegment """

  String sqlCrossSell = """ SELECT RESULT.productattribute   AS 'productattribute',
                                   SUM(RESULT.revenue)       AS 'crosssell'
                            FROM (
                                  SELECT TransactionData.productid,
                                         TransactionData.productattribute,
                                         AVG(TransactionData.revenue) as 'revenue'
                                  FROM (${sqlBasic}) TransactionData
                                  WHERE TransactionData.productid NOT IN 
                                                            ( SELECT CUST_DATA.productid 
                                                              FROM (${sqlBasic}) CUST_DATA
                                                            WHERE CUST_DATA.customerId = '${currentCustomerId}')
                                  GROUP BY TransactionData.productid, TransactionData.productattribute
                                 ) RESULT
                            GROUP BY RESULT.productattribute """

  String sql = """ SELECT COALESCE(up_sell_sql.productattribute, cross_sell_sql.productattribute)  AS 'productAttribute',
                          COALESCE(up_sell_sql.revenue, 0)                                         AS 'revenue',
                          COALESCE(up_sell_sql.revenuebelowtarget, 0)                              AS 'revenueBelowTarget',
                          COALESCE(up_sell_sql.revenuebelowtargetpercent, 0)                       AS 'revenueBelowTargetPercent',
                          COALESCE(up_sell_sql.upsell, 0)                                          AS 'upSell',
                          COALESCE(cross_sell_sql.crosssell,0)                                     AS 'crossSell'
                   FROM (${sqlUpSell}) up_sell_sql FULL JOIN (${sqlCrossSell}) cross_sell_sql
                     ON up_sell_sql.productattribute = cross_sell_sql.productattribute 
                   """

  return queryUtils.getFullQueryData(sql, transactionQuery, productAttributeQuery)
}
/**
 * calculate value for each Cross Selling Opportunity item
 * @param opportunityValues
 * @param productIds
 * @param productAttribute
 * @return
 */
protected Map calculateOpportunityItem(List opportunityValues, String productAttribute) {
  def constant = libs.CustomerInsights.Constant
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils
  def roundingUtils = libs.SharedLib.RoundingUtils

  BigDecimal crossSell = BigDecimal.ZERO, upSell = BigDecimal.ZERO, revenue = BigDecimal.ZERO
  BigDecimal revenueBelowTarget = BigDecimal.ZERO, revenueBelowTargetPercent

  for (opportunityItem in opportunityValues) { //use for () to improve performance
    crossSell += (opportunityItem.crossSell ?: BigDecimal.ZERO)
    upSell += (opportunityItem.upSell ?: BigDecimal.ZERO)
    revenue += (opportunityItem.revenue ?: BigDecimal.ZERO)
    revenueBelowTarget += (opportunityItem.revenueBelowTarget ?: BigDecimal.ZERO)
  }

  int decimalPlaces = constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_05
  revenueBelowTarget = roundingUtils.round(revenueBelowTarget, decimalPlaces)
  revenueBelowTargetPercent = revenueBelowTarget ? calculationCommonUtils.getAverageValue(opportunityValues.revenueBelowTargetPercent) : BigDecimal.ZERO

  return [productAttribute         : productAttribute,
          revenue                  : revenue,
          crossSell                : crossSell,
          upSell                   : upSell,
          opportunity              : (crossSell + upSell),
          revenueBelowTarget       : revenueBelowTarget,
          revenueBelowTargetPercent: roundingUtils.round(revenueBelowTargetPercent, decimalPlaces)]
}
/**
 * get revenue below target data for each products of customer
 * @param configuration
 * @param portfolioDashboardParams
 * @return
 */
List getRevenueBelowTargetPerProduct(Map configuration, Map portfolioDashboardParams) {
  def constant = libs.CustomerInsights.Constant
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper
  def queryUtils = libs.CustomerInsights.QueryUtils
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils

  String currentCustomerId = portfolioDashboardParams.getCustomerId()
  String customerSegment = calculationCommonUtils.getCustomerSegmentByCustomer(configuration, currentCustomerId)
  String unknown = constant.DEFAULT_VALUE.UNKNOWN
  List segmentQuerySimulationFilters = portfolioDashboardParams.getValueOnSegmentQuerySimulationFilter(configuration, customerSegment)
  DatamartContext.Query transactionQuery = queryHelper.createQueryForSumValuePerCustomerAndProduct(configuration, segmentQuerySimulationFilters)
  String sqlBasic = """SELECT TRANS_DATA.customerId                                            AS 'customerid',
                              TRANS_DATA.customerSegment                                       AS 'customersegment',
                              TRANS_DATA.productId                                             AS 'productid',
                              TRANS_DATA.productClassificationByMarginPercent                  AS 'productclassificationbymarginpercent',
                              TRANS_DATA.quantity                                              AS 'quantity',
                              TRANS_DATA.revenue                                               AS 'revenue',
                              TRANS_DATA.revenuePerUnit                                        AS 'revenueperunit'
                      FROM T1 TRANS_DATA  """
  String sqlAvgUnitPricePerProductOfCustomerSegment = """ 
                               SELECT  TransactionData.customersegment       AS 'customersegment',
                                       TransactionData.productid             AS 'productid',
                                       AVG(TransactionData.revenueperunit)   AS 'revenueperunit'
                               FROM (${sqlBasic}) TransactionData 
                               GROUP BY TransactionData.customersegment, TransactionData.productid """

  String sqlReCalculation = """
SELECT   CAL_DATA.productid                            AS 'productid',
         CAL_DATA.productclassificationbymarginpercent AS 'productclassificationbymarginpercent',
         IIF( CAL_DATA.revenueperunit - AVG_PRODUCT_DATA.revenueperunit < 0,
              (CAL_DATA.revenueperunit - AVG_PRODUCT_DATA.revenueperunit)*CAL_DATA.quantity*(-1),
              0 )                                      AS 'revenuebelowtarget',
         IIF( CAL_DATA.revenue <> 0 AND (CAL_DATA.revenueperunit - AVG_PRODUCT_DATA.revenueperunit) < 0,
              (CAL_DATA.revenueperunit - AVG_PRODUCT_DATA.revenueperunit)*CAL_DATA.quantity*(-1)/CAL_DATA.revenue, 
              0)                                       AS 'revenuebelowtargetpercent'
FROM (${sqlBasic}) CAL_DATA INNER JOIN (${sqlAvgUnitPricePerProductOfCustomerSegment}) AVG_PRODUCT_DATA
     ON COALESCE(CAL_DATA.customersegment,'${unknown}') = COALESCE(AVG_PRODUCT_DATA.customersegment,'${unknown}')
     AND CAL_DATA.productid = AVG_PRODUCT_DATA.productid
WHERE CAL_DATA.customerid = '${currentCustomerId}' """

  String sql = """ SELECT productid                            AS 'productId',
                          productclassificationbymarginpercent AS 'productClassificationByMarginPercent',
                          revenuebelowtarget                   AS 'revenueBelowTarget',
                          IIF( SUM(revenuebelowtarget) OVER() <> 0,
                               (SUM(revenuebelowtarget) OVER( ORDER BY revenuebelowtarget DESC)/SUM(revenuebelowtarget) OVER()),
                               0)                                   AS 'cumulativeRevenueBelowTargetPercent'
                   FROM (${sqlReCalculation})
                   ORDER BY revenuebelowtarget DESC , productid ASC """

  return queryUtils.getFullQueryData(sql, transactionQuery)
}
/**
 * Get Revenue and Margin per product
 * @param configuration
 * @param portfolioDashboardParams
 * @return
 */
List getDataForRevenueAndMarginPerProduct(Map configuration, Map portfolioDashboardParams) {
  def queryHelper = libs.CustomerInsights.CalculationQueryHelper

  List nonSegmentQuerySimulationFilters = portfolioDashboardParams.getBasicQuerySimulationFilter(configuration)
  DatamartContext.Query query = queryHelper.createQueryForGetInfoPerProduct(configuration, nonSegmentQuerySimulationFilters)

  return api.getDatamartContext().executeQuery(query)?.getData()?.collect()
}
/**
 * Get summary information for customer shown in Customer Product Portfolio
 * @param configuration
 * @param inputParam
 * @return
 */
Map getSummaryInfoForCustomerProductPortfolio(Map configuration, Map portfolioDashboardParams) {
  List nonSegmentQuerySimulationFilters = portfolioDashboardParams.getBasicQuerySimulationFilter(configuration)

  return libs.CustomerInsights.CalculationCommonUtils.getSumValuePerCustomer(configuration, nonSegmentQuerySimulationFilters)
}