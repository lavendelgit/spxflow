import net.pricefx.formulaengine.DatamartContext

/*
  Covert filter to List of Customer
 */

List convertCustomerGroupInputToListOfCustomerId(def customerGroup) {
  if (!customerGroup) {
    return null
  }

  Filter filter = (customerGroup instanceof Filter) ? customerGroup : customerGroup.asFilter()
  List data = libs.SharedLib.StreamUtils.stream("C", "customerId", ["customerId"], [filter])

  return data?.customerId
}

List getAllCustomerIdsWithOtherAttributes(List customerAttributes, String customerIdInput = null) {
  List fields = ["customerId"]
  if (customerAttributes) {
    fields += customerAttributes
  }

  List filters
  if (customerIdInput) {
    filters = [Filter.equal('customerId', customerIdInput)]
  }

  return libs.SharedLib.StreamUtils.stream("C", "customerId", fields, filters)
}

List getAllCustomerIdsWithOtherAttributesFromDataSource(String sourceName, String customerId, List customerAttributes, Filter... filterInput) {
  Map resultFields = customerAttributes?.collectEntries { attribute -> [(attribute): attribute] } ?: [:]

  if (!resultFields) {
    return []
  }

  DatamartContext.Query query = libs.CustomerInsights.QueryUtils.getContextQuery(libs.CustomerInsights.Constant.SOURCE_TYPE.DMDS,
      sourceName,
      resultFields,
      true,
      null,
      *filterInput)

  String sql = buildSQLStatement("T1", customerId, customerAttributes)

  return libs.CustomerInsights.QueryUtils.getFullQueryData(sql, query)
}

List getAllCustomerIdsWithOtherAttributesFromCX(String sourceName, List customerAttributes, String customerIdInput = null) {
  List fields = ["customerId"] + customerAttributes
  if (!customerAttributes) {
    fields += customerAttributes
  }

  List filters = [Filter.equal("name", sourceName)]
  if (customerIdInput) {
    filters.add(Filter.equal('customerId', customerIdInput))
  }

  return libs.SharedLib.StreamUtils.stream("CX", "customerId", fields, filters)
}
/**
 * build sql statement to get value of Customer attributes from DMDS
 * need to have 'ORDER BY customerId' to process case result >1M rows
 * @param tableName
 * @param customerId
 * @param customerAttributes
 * @return
 */
protected String buildSQLStatement(String tableName, String customerId, List customerAttributes) {
  Integer attributeSize = customerAttributes?.size()
  if (!attributeSize) {
    return ""
  }

  String attribute
  String sql = "SELECT "
  for (int index = 0; index < attributeSize; index++) {
    attribute = customerAttributes.get(index)
    if (index == (customerAttributes.size() - 1)) {
      sql += " ${attribute} AS '${attribute}' "
    } else {
      sql += " ${attribute} AS '${attribute}', "
    }
  }
  sql += " FROM $tableName ORDER BY $customerId "

  return sql
}