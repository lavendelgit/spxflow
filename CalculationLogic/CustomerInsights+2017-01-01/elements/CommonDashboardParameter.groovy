/**
 * build common parameters are used in dashboard logic
 * @param timeFilter
 * @param currency --> a Map [currencyCode, currencySymbol]
 * @param period --> a Map [startDate, endDate]
 * @param currentDate
 * @return
 */
Map commonDashboardParam(String timeFilter, Map currency, Map period, Date currentDate) {
  return [getTimeFilter            : { return timeFilter },
          getCurrency              : { return currency },
          getCurrentDate           : { return currentDate },
          getPeriod                : { return period },
          getStartDate             : { return period?.startDate },
          getEndDate               : { return period?.endDate },
          getPeriodSimulationFilter: { Map _configuration, Date _startDate, Date _endDate ->
            return generatePeriodSimulationFilter(_configuration, _startDate, _endDate)
          },
          getPeriodDatamartFilter  : { Map _configuration, Date _startDate, Date _endDate ->
            return generatePeriodDatamartFilter(_configuration, _startDate, _endDate)
          }
  ]
}
/**********************************************
 * Simulation filters
 **********************************************/
protected List generatePeriodSimulationFilter(Map configuration, Date startDate, Date endDate) {
  def dateUtils = libs.CustomerInsights.DateUtils
  String monthFieldName = configuration.PricingMonthFieldName
  String startDateFormatMonth = dateUtils.getMonthName(startDate)
  String endDateFormatMonth = dateUtils.getMonthName(endDate)
  List<Filter> filters = []

  if (startDateFormatMonth) {
    filters.add(Filter.greaterOrEqual(monthFieldName, startDateFormatMonth))
  }

  if (endDateFormatMonth) {
    filters.add(Filter.lessOrEqual(monthFieldName, endDateFormatMonth))
  }

  return filters
}
/**********************************************
 * Datamart filters
 **********************************************/
protected List generatePeriodDatamartFilter(Map configuration, Date startDate, Date endDate) {
  String pricingDateFieldName = configuration.OriginalSource.PricingDateFieldName
  List<Filter> filters = []

  if (startDate) {
    filters.add(Filter.greaterOrEqual(pricingDateFieldName, startDate))
  }

  if (endDate) {
    filters.add(Filter.lessOrEqual(pricingDateFieldName, endDate))
  }

  return filters
}