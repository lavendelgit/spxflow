import net.pricefx.formulaengine.DatamartContext

//---CUSTOMER CLASSIFICATION
Map calculationCustomerClassificationByRevenue(Map configuration, Map simulationParams) {
  if (api.global.CalculationCustomerClassificationByRevenue) {
    return api.global.CalculationCustomerClassificationByRevenue
  }

  List customerClassification = processCustomerClassificationByRevenue(configuration, simulationParams)
  Map processedData = customerClassification?.groupBy { it.customerId }
  api.global.CalculationCustomerClassificationByRevenue = processedData

  return processedData
}

protected List processCustomerClassificationByRevenue(Map configuration, Map simulationParams) {
  def CID_lib = libs.CustomerInsights

  Integer lastMonthAmount = CID_lib.Configuration.getLastMonthAmountFromConfiguration(configuration)
  Map currentPeriod = CID_lib.DateUtils.getPreviousPeriodFromDate(simulationParams.getCurrentDate(), lastMonthAmount)
  Map previousPeriod = CID_lib.DateUtils.addMonthsToPeriod(currentPeriod, (-1) * lastMonthAmount)//last year

  //1. create query for current period
  List currentPeriodFilters = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, currentPeriod.startDate, currentPeriod.endDate)
  DatamartContext.Query currentQuery = CID_lib.ClassificationQueryHelper.createQueryForSumValuePerCustomer(configuration, currentPeriodFilters)
  //2. create query for previous period
  List previousPeriodFilters = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, previousPeriod.startDate, previousPeriod.endDate)
  DatamartContext.Query previousQuery = CID_lib.ClassificationQueryHelper.createQueryForSumValuePerCustomer(configuration, previousPeriodFilters)

  //3. Sql statement
  String cumulativeRevenueContributionFieldName = "cumulativerevenuecontribution"
  String cumulativeSql = """SELECT customerId              AS 'customerid', 
                                  IFNULL(revenue,0)        AS 'revenue',
                                  IFNULL(margin,0)         AS 'margin',
                                  IFNULL(quantity,0)       AS 'quantity',
                                  100*(SUM(revenue) OVER(ORDER BY revenue DESC, customerId) / SUM(revenue) OVER() ) AS '$cumulativeRevenueContributionFieldName'
                           FROM T2 CurrentPeriod 
                           ORDER BY revenue DESC"""

  String customerClassificationSQL = buildClassificationSQL(configuration.CUSTOMER_CLASSIFICATION_REVENUE, cumulativeRevenueContributionFieldName)

  String sqlStatement = """ SELECT  customerid                    AS 'customerId',
   	                                revenue                       AS 'revenue',
   	                                margin                        AS 'margin',
   	                                quantity                      AS 'quantity',
                                    ${customerClassificationSQL}  AS 'classificationByRevenue'
   							            FROM ( ${cumulativeSql} ) CUMULATIVE_SQL
   		                      ORDER BY revenue DESC, customerid """

  return CID_lib.QueryUtils.getFullQueryData(sqlStatement, previousQuery, currentQuery)
}
/**
 * Calculate HealthScore base on revenueTrend and marginTrend
 * @param configuration
 * @param revenueTrend
 * @param marginTrend
 * @return
 */
BigDecimal calculateHealthScore(Map configuration, BigDecimal revenueTrend, BigDecimal marginTrend) {
  if (revenueTrend == null || marginTrend == null) {
    return BigDecimal.ZERO
  }

  Map weightedFactor
  if (api.global.WeightedFactorFromConfiguration) {
    weightedFactor = api.global.WeightedFactorFromConfiguration
  } else {
    weightedFactor = libs.CustomerInsights.Configuration.getWeightedFactorFromConfiguration(configuration)
    api.global.WeightedFactorFromConfiguration = weightedFactor
  }

  BigDecimal revenueWeight = weightedFactor.revenueWeighted as BigDecimal
  BigDecimal marginWeight = weightedFactor.marginWeighted as BigDecimal
  BigDecimal revenueHealthScore = getScoreByValue(configuration.SCORE_CLASSIFICATION, revenueTrend)
  BigDecimal marginHealthScore = getScoreByValue(configuration.SCORE_CLASSIFICATION, marginTrend)

  return (revenueHealthScore * revenueWeight + marginHealthScore * marginWeight)
}
/**
 * calculate Customer Classification By Health Score
 * @param configuration
 * @param inputHealthScore
 * @return
 */
String calculateCustomerClassificationByHealthScore(Map configuration, BigDecimal inputHealthScore) {
  Map customerClassificationHealthScoreTable = configuration.CUSTOMER_CLASSIFICATION_HEALTH_SCORE

  return getClassification(customerClassificationHealthScoreTable, libs.CustomerInsights.Constant.SORT_TYPE.DESCENDING, inputHealthScore)
}
/**
 * calculate Product Classification By Health Score
 * @param configuration
 * @param inputHealthScore
 * @return
 */
String calculateProductClassificationByHealthScore(Map configuration, BigDecimal inputHealthScore) {
  Map productClassificationHealthScoreTable = configuration.PRODUCT_CLASSIFICATION_HEALTH_SCORE

  return getClassification(productClassificationHealthScoreTable, libs.CustomerInsights.Constant.SORT_TYPE.DESCENDING, inputHealthScore)
}
/**
 * Get score base on score classification table and input value
 * @param sortedScoreClassification
 * @param valueInput
 * @return
 */
protected BigDecimal getScoreByValue(Map sortedScoreClassification, BigDecimal valueInput) {
  if (!sortedScoreClassification) {
    return BigDecimal.ZERO
  }
  int decimalPlaces = libs.CustomerInsights.Constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_02
  BigDecimal healthScore, classificationValue
  Map scoreClassificationItem
  for (item in sortedScoreClassification) {
    scoreClassificationItem = item.value.getAt(0)
    classificationValue = libs.SharedLib.RoundingUtils.round(scoreClassificationItem.value as BigDecimal, decimalPlaces)
    if (valueInput >= classificationValue) {
      healthScore = (item.key as BigDecimal)
      break
    }
  }

  return healthScore ?: BigDecimal.ZERO
}
//---PRODUCT CLASSIFICATION
/**
 *
 * @param configuration
 * @param simulationParams
 * @return
 */
Map calculateProductClassification(Map configuration, Map simulationParams) {
  List productClassificationData = processProductClassification(configuration, simulationParams)

  return productClassificationData?.groupBy { it.productId }
}

protected List processProductClassification(Map configuration, Map simulationParams) {
  def CID_lib = libs.CustomerInsights
  Map sortType = CID_lib.Constant.SORT_TYPE

  Integer lastMonthAmount = CID_lib.Configuration.getLastMonthAmountFromConfiguration(configuration)
  Map currentPeriod = CID_lib.DateUtils.getPreviousPeriodFromDate(simulationParams.getCurrentDate(), lastMonthAmount)
  Map previousPeriod = CID_lib.DateUtils.addMonthsToPeriod(currentPeriod, (-1) * lastMonthAmount)//last year
  //1. create query for current period
  List currentPeriodFilters = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, currentPeriod.startDate, currentPeriod.endDate)
  DatamartContext.Query currentQuery = CID_lib.ClassificationQueryHelper.createQueryForSumValuePerProduct(configuration, currentPeriodFilters)
  //2. create query for previous period
  List previousPeriodFilters = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, previousPeriod.startDate, previousPeriod.endDate)
  DatamartContext.Query previousQuery = CID_lib.ClassificationQueryHelper.createQueryForSumValuePerProduct(configuration, previousPeriodFilters)
  //3. Sql statement
  String cumulativeQuantityContributionFieldName = "cumulativequantitycontribution"
  String marginPercentFieldName = "marginpercent"
  String cumulativeSql = """SELECT productId               AS 'productid', 
                                  IFNULL(revenue,0)        AS 'revenue',
                                  IFNULL(margin,0)         AS 'margin',
                                  IIF( IFNULL(revenue,0) <> 0, 
                                       100 * IFNULL(margin,0)/IFNULL(revenue,0),
                                       0)                  AS '$marginPercentFieldName',
                                  IFNULL(quantity,0)       AS 'quantity',
                                  100*(SUM(quantity) OVER(ORDER BY quantity DESC, productId) / SUM(quantity) OVER() ) AS '$cumulativeQuantityContributionFieldName'
                           FROM T2 CurrentPeriod 
                           ORDER BY quantity DESC"""

  String productClassificationByQuantitySQL = buildClassificationSQL(configuration.PRODUCT_CLASSIFICATION_QUANTITY, cumulativeQuantityContributionFieldName, sortType.ASCENDING)
  String productClassificationByMarginPercentSQL = buildClassificationSQL(configuration.PRODUCT_CLASSIFICATION_MARGIN_PERCENT, marginPercentFieldName, sortType.DESCENDING)

  String sqlStatement = """ SELECT  productid                                   AS 'productId',
   	                                revenue                                     AS 'revenue',
   	                                margin                                      AS 'margin',
   	                                quantity                                    AS 'quantity',
                                    ${productClassificationByQuantitySQL}       AS 'classificationByQuantity',
                                    ${productClassificationByMarginPercentSQL}  AS 'classificationByMarginPercent'
   							            FROM ( ${cumulativeSql} ) CUMULATIVE_SQL
   		                      ORDER BY quantity DESC, productid """

  return CID_lib.QueryUtils.getFullQueryData(sqlStatement, previousQuery, currentQuery)
}
//---PRODUCT AVG PRICE
/**
 * get Avg Invoice Price by productId and Customer Class
 * @param configuration
 * @param simulationParams
 * @return
 */
Map calculateAvgPricePerProductAndCustomerClass(Map configuration, Map simulationParams) {
  if (api.global.CalculateAvgPricePerProductAndCustomerClass) {
    return api.global.CalculateAvgPricePerProductAndCustomerClass
  }
  List productWithCustomers = calculateAvgPricePerProductAndCustomer(configuration, simulationParams)
  Map classificationCustomers = calculationCustomerClassificationByRevenue(configuration, simulationParams)
  Map finalData = calculateAvgPricePerProductAndCustomerClassification(productWithCustomers, classificationCustomers)
  api.global.CalculateAvgPricePerProductAndCustomerClass = finalData

  return finalData
}
/**
 * get Avg Invoice Price by productId and CustomerId
 * @param configuration
 * @param simulationParams
 * @return
 */
List calculateAvgPricePerProductAndCustomer(Map configuration, Map simulationParams) {
  if (!api.global.CalculateAvgPricePerProductAndCustomer) {
    api.global.CalculateAvgPricePerProductAndCustomer = getSumValueOnEachProductAndCustomer(configuration, simulationParams)
  }

  return api.global.CalculateAvgPricePerProductAndCustomer
}

protected List getSumValueOnEachProductAndCustomer(Map configuration, Map simulationParams) {
  def queryUtils = libs.CustomerInsights.QueryUtils

  List filters = simulationParams.getBasicQueryDatamartFilter(configuration)
  DatamartContext.Query query = libs.CustomerInsights.ClassificationQueryHelper.createQueryForSumValuePerProductAndCustomer(configuration, filters)
  String sql = """SELECT customerId      AS 'customerId',  
                         productId       AS 'productId',
                         revenue         AS 'revenue',
                         margin          AS 'margin',
                         quantity        AS 'quantity',
                         avgInvoicePrice AS 'avgInvoicePrice'
                  FROM T1 
                  ORDER BY customerId, productId """

  return queryUtils.getFullQueryData(sql, query)
}

protected Map calculateAvgPricePerProductAndCustomerClassification(List productWithCustomers, Map classificationCustomers) {
  String customerClass, productId
  Map finalResult = [:]
  Map classificationCustomer

  BigDecimal avgPriceByCustomerClass
  Map processedDataItem
  Map dataOfProducts = [:]

  for (Map productWithCustomerItem in productWithCustomers) {
    classificationCustomer = classificationCustomers.getAt(productWithCustomerItem.customerId)?.getAt(0)
    customerClass = classificationCustomer?.classificationByRevenue ?: libs.CustomerInsights.Constant.DEFAULT_VALUE.UNKNOWN
    productId = productWithCustomerItem.productId
    updateDataOfProduct(dataOfProducts, productId, customerClass, productWithCustomerItem)
    avgPriceByCustomerClass = dataOfProducts.getAt(productId)?.customerClassData?.getAt(customerClass)?.averagePrice

    processedDataItem = finalResult.getAt(productId) ?: [:]
    processedDataItem.productId = productId
    processedDataItem.overall = dataOfProducts.getAt(productId)?.overall
    processedDataItem.(customerClass as String) = avgPriceByCustomerClass

    finalResult.put(productId, processedDataItem)
  }

  return finalResult
}

protected void updateDataOfProduct(Map dataOfProducts, String productId, String customerClass, Map dataRow) {

  Map productData = dataOfProducts?.getAt(productId) ?: [:]
  Map customerClassData = productData?.customerClassData ?: [:]
  Map customerClassDataItem = customerClassData.getAt(customerClass)
  BigDecimal avgPrice = dataRow.quantity ? dataRow.revenue / dataRow.quantity : BigDecimal.ZERO

  if (customerClassDataItem) {
    customerClassDataItem.totalRevenue += (dataRow.revenue as BigDecimal)
    customerClassDataItem.totalQuantity += (dataRow.quantity as BigDecimal)
    customerClassDataItem.totalAveragePrice += avgPrice
    customerClassDataItem.customerAmount += 1
  } else {
    customerClassDataItem = [:]
    customerClassDataItem.totalRevenue = (dataRow.revenue as BigDecimal)
    customerClassDataItem.totalQuantity = (dataRow.quantity as BigDecimal)
    customerClassDataItem.totalAveragePrice = avgPrice
    customerClassDataItem.customerAmount = 1
  }

  customerClassDataItem.averagePrice = customerClassDataItem.totalAveragePrice ? customerClassDataItem.totalAveragePrice / customerClassDataItem.customerAmount : BigDecimal.ZERO
  customerClassData.(customerClass as String) = customerClassDataItem
  productData.customerClassData = customerClassData

  BigDecimal totalAvgPrice = 0
  int classAmount = 0
  productData.customerClassData.each { String className, Map classData ->
    totalAvgPrice += classData.averagePrice
    classAmount++
  }
  productData.overall = totalAvgPrice ? totalAvgPrice / classAmount : BigDecimal.ZERO

  dataOfProducts?.put(productId, productData)
}

//----CUSTOMER TREND
/**
 * calculate last 12M Trend for Customer
 * @param configuration
 * @param simulationParams
 * @return
 */
Map calculateTrendForCustomers(Map configuration, Map simulationParams) {
  List data = getL12MTrendForCustomer(configuration, simulationParams)

  return data?.groupBy { it.customerId }
}

protected List getL12MTrendForCustomer(Map configuration, Map simulationParams) {
  def CID_lib = libs.CustomerInsights

  List filters = simulationParams.getBasicQueryDatamartFilter(configuration)
  DatamartContext.Query last12MQuery = CID_lib.ClassificationQueryHelper.createQueryForSumValueOnEachMonthPerCustomer(configuration, filters)
  String monthNameOfEndDate = CID_lib.DateUtils.getMonthName(simulationParams.getEndDate())
  String valueOnMonthSQL = """SELECT customerid                                AS 'customerid',
                                     month                                     AS 'month',
                                     revenue                                   AS 'revenue',
                                     margin                                    AS 'margin',
                                     quantity                                  AS 'quantity',
                                     ROW_NUMBER() OVER (PARTITION BY customerid ORDER BY month) AS 'rownumber'
                               FROM T1 """
  String sqlL12MTrends = CID_lib.CalculationCommonUtils.buildL12MTrendCalculationSQL(monthNameOfEndDate, CID_lib.Constant.TIME_PERIOD.MONTH)
  String sqlStatement = """ SELECT currentperiod.customerid AS 'customerId', 
                                   ${sqlL12MTrends}
                            FROM ( ${valueOnMonthSQL} ) currentperiod LEFT JOIN 
                                 ( ${valueOnMonthSQL}) previousperiod 
                                ON currentperiod.customerid = previousperiod.customerid 
                                AND (currentperiod.rownumber - 1) = previousperiod.rownumber  
                            GROUP BY currentperiod.customerid  
                            ORDER BY currentperiod.customerid """

  return CID_lib.QueryUtils.getFullQueryData(sqlStatement, last12MQuery)
}
/**
 * calculate YTD trend for customer
 * @param configuration
 * @param simulationParams
 * @return
 */
Map calculateYTDTrendForCustomers(Map configuration, Map simulationParams) {
  List YTDTrends = getDataForYTDTrendForCustomer(configuration, simulationParams)

  return YTDTrends?.groupBy { it.customerId }
}

protected List getDataForYTDTrendForCustomer(Map configuration, Map simulationParams) {
  def queryUtils = libs.CustomerInsights.QueryUtils
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils
  def dateUtils = libs.CustomerInsights.DateUtils
  def queryHelper = libs.CustomerInsights.ClassificationQueryHelper

  Map timePeriod = libs.CustomerInsights.Constant.TIME_PERIOD
  Map currentPeriod = dateUtils.getPeriod(simulationParams.getCurrentDate(), timePeriod.YTD)
  Map previousPeriod = dateUtils.addYearsToPeriod(currentPeriod, -1)//last year

  List previousPeriodFilter = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, previousPeriod.startDate, previousPeriod.endDate)
  List currentPeriodFilter = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, currentPeriod.startDate, currentPeriod.endDate)

  DatamartContext.Query previousPeriodQuery = queryHelper.createQueryForSumValuePerCustomer(configuration, previousPeriodFilter)
  DatamartContext.Query currentPeriodQuery = queryHelper.createQueryForSumValuePerCustomer(configuration, currentPeriodFilter)
  if (!previousPeriodQuery || !currentPeriodQuery) {
    return null
  }

  String sqlYTDTrends = calculationCommonUtils.buildYTDTrendCalculationSQL()
  String sqlStatement = """ SELECT CurrentPeriod.customerId AS 'customerId',
                                   ${sqlYTDTrends}
                            FROM T2 CurrentPeriod LEFT JOIN T1 PreviousPeriod ON CurrentPeriod.customerId = PreviousPeriod.customerId
                            GROUP BY CurrentPeriod.customerId 
                            ORDER BY CurrentPeriod.customerId """

  return queryUtils.getFullQueryData(sqlStatement, previousPeriodQuery, currentPeriodQuery)
}
//----PRODUCT CUSTOMER TREND----
/**
 * Calculate last 12M trend per product and per customer
 * @param configuration
 * @param simulationParams
 * @return
 */
Map calculateTrendForProductAndCustomer(Map configuration, Map simulationParams) {
  List data = getL12MTrendForProductAndCustomer(configuration, simulationParams)

  return data?.groupBy { getGroupByKeyProductAndCustomer(it.productId, it.customerId) }
}

protected List getL12MTrendForProductAndCustomer(Map configuration, Map simulationParams) {
  def CID_lib = libs.CustomerInsights

  List filters = simulationParams.getBasicQueryDatamartFilter(configuration)
  DatamartContext.Query last12MQuery = CID_lib.ClassificationQueryHelper.createQueryForSumValueOnEachQuarterPerProductAndCustomer(configuration, filters)
  String quarterNameOfEndDate = CID_lib.DateUtils.getQuarterName(simulationParams.getEndDate())
  String valueOnQuarterSQL = """SELECT customerid                                           AS 'customerid', 
                                       productid                                            AS 'productid',
                                       quarter                                              AS 'quarter',
                                       revenue                                              AS 'revenue',
                                       margin                                               AS 'margin',
                                       quantity                                             AS 'quantity',
                                       ROW_NUMBER() OVER (PARTITION BY customerid, productid ORDER BY quarter) AS 'rownumber'
                         FROM T1 """
  String sqlL12MTrends = CID_lib.CalculationCommonUtils.buildL12MTrendCalculationSQL(quarterNameOfEndDate, CID_lib.Constant.TIME_PERIOD.QUARTER)
  String sqlStatement = """ SELECT currentperiod.customerid AS 'customerId', 
                                   currentperiod.productid  AS 'productId',
                                   ${sqlL12MTrends}
                            FROM ( ${valueOnQuarterSQL} ) currentperiod LEFT JOIN
                                 ( ${valueOnQuarterSQL} ) previousperiod 
                               ON   currentperiod.customerid = previousperiod.customerid 
                                AND currentperiod.productid = previousperiod.productid
                                AND (currentperiod.rownumber - 1) = previousperiod.rownumber
                            GROUP BY currentperiod.customerid, currentperiod.productid 
                            ORDER BY currentperiod.customerid, currentperiod.productid """

  return CID_lib.QueryUtils.getFullQueryData(sqlStatement, last12MQuery)
}
/**
 * calculate YTD trend per product and per customer
 * @param configuration
 * @param simulationParams
 * @return
 */
Map calculateYTDTrendForProductAndCustomer(Map configuration, Map simulationParams) {
  List YTDTrends = getDataForYTDTrendForProductAndCustomer(configuration, simulationParams)

  return YTDTrends?.groupBy { getGroupByKeyProductAndCustomer(it.productId, it.customerId) }
}

protected List getDataForYTDTrendForProductAndCustomer(Map configuration, Map simulationParams) {
  def calculationCommonUtils = libs.CustomerInsights.CalculationCommonUtils
  def dateUtils = libs.CustomerInsights.DateUtils
  def queryHelper = libs.CustomerInsights.ClassificationQueryHelper

  Map currentPeriod = dateUtils.getPeriod(simulationParams.getCurrentDate(), libs.CustomerInsights.Constant.TIME_PERIOD.YTD)
  Map previousPeriod = dateUtils.addYearsToPeriod(currentPeriod, -1)//last year

  List previousPeriodFilter = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, previousPeriod.startDate,previousPeriod.endDate)
  List currentPeriodFilter = simulationParams.getCustomPeriodQueryDatamartFilter(configuration, currentPeriod.startDate, currentPeriod.endDate)

  DatamartContext.Query previousPeriodQuery = queryHelper.createQueryForSumValuePerProductAndCustomer(configuration, previousPeriodFilter)
  DatamartContext.Query currentPeriodQuery = queryHelper.createQueryForSumValuePerProductAndCustomer(configuration, currentPeriodFilter)

  if (!previousPeriodQuery || !currentPeriodQuery) {
    return null
  }

  String sqlYTDTrends = calculationCommonUtils.buildYTDTrendCalculationSQL()
  String sqlStatement = """ SELECT CurrentPeriod.CustomerId AS 'customerId', CurrentPeriod.ProductId AS 'productId',
                                   ${sqlYTDTrends}
                            FROM T2 CurrentPeriod LEFT JOIN T1 PreviousPeriod ON CurrentPeriod.customerId = PreviousPeriod.customerId
                                 AND CurrentPeriod.productId = PreviousPeriod.productId
                            GROUP BY CurrentPeriod.customerId, CurrentPeriod.productId 
                            ORDER BY CurrentPeriod.customerId, CurrentPeriod.productId """

  return libs.CustomerInsights.QueryUtils.getFullQueryData(sqlStatement, previousPeriodQuery, currentPeriodQuery)
}
/**
 *
 * @param classificationTable
 * @param orderingType
 * @param inputValue
 * @return
 */
protected String getClassification(Map classificationTable, String orderingType, BigDecimal inputValue) {
  def roundingUtils = libs.SharedLib.RoundingUtils
  def constant = libs.CustomerInsights.Constant
  String classCategory
  Map classification
  BigDecimal categoryValue
  int decimalPlaces = constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_02
  BigDecimal value = roundingUtils.round(inputValue, decimalPlaces)
  String localOrderingType = orderingType ?: constant.SORT_TYPE.ASCENDING
  boolean isAscendingType, isDescendingType

  for (classificationItem in classificationTable) {
    classification = classificationItem.value?.getAt(0)
    categoryValue = roundingUtils.round((classification.value ?: BigDecimal.ZERO) as BigDecimal, decimalPlaces)
    isAscendingType = (localOrderingType == constant.SORT_TYPE.ASCENDING && value <= categoryValue)
    isDescendingType = (localOrderingType == constant.SORT_TYPE.DESCENDING && value >= categoryValue)
    if (isAscendingType || isDescendingType) {
      classCategory = classification.label
      break
    }
  }
  //get the last value of range
  return classCategory ?: classification.label
}

String getGroupByKeyProductAndCustomer(String productId, String customerId) {
  return ((productId ?: "") + (customerId ?: ""))
}
/**
 * build CASE WHEN SQL statement base on classificationTable
 * @param classificationTable
 * @param fieldName
 * @param orderingType
 * @return
 */
protected String buildClassificationSQL(Map classificationTable, String fieldName, String orderingType = null) {
  def roundingUtils = libs.SharedLib.RoundingUtils
  def constant = libs.CustomerInsights.Constant
  int decimalPlaces = constant.ROUNDING_DECIMAL_PLACES.DECIMAL_PLACES_02
  String localOrderingType = orderingType ?: constant.SORT_TYPE.ASCENDING
  Map classification
  BigDecimal categoryValue
  String sql = " CASE "

  for (classificationItem in classificationTable) {
    classification = classificationItem.value?.getAt(0)
    categoryValue = roundingUtils.round((classification.value ?: BigDecimal.ZERO) as BigDecimal, decimalPlaces)

    if (localOrderingType == constant.SORT_TYPE.ASCENDING) {
      sql += " WHEN ${fieldName} <= ${categoryValue} THEN '${classification.label}' "
    } else {
      sql += " WHEN ${fieldName} >= ${categoryValue} THEN '${classification.label}' "
    }
  }
  sql += " ELSE '${classification.label}' END "

  return sql
}
