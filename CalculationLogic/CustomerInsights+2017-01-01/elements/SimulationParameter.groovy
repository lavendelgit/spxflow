/**
 * build parameter used in Simulation logic
 * @param period
 * @param currentDate
 * @return
 */
Map newSimulationParameter(Map period, Date currentDate) {
  Map simulationParam = libs.CustomerInsights.CommonDashboardParameter.commonDashboardParam(null, null, period, currentDate)
  simulationParam << [
      getBasicQueryDatamartFilter       : { Map _configuration ->
        return simulationParam.getPeriodDatamartFilter(_configuration, simulationParam.getStartDate(), simulationParam.getEndDate())
      },
      getCustomPeriodQueryDatamartFilter: { Map _configuration, Date _startDate, Date _endDate ->
        return simulationParam.getPeriodDatamartFilter(_configuration, _startDate, _endDate)
      }
  ]

  return simulationParam
}