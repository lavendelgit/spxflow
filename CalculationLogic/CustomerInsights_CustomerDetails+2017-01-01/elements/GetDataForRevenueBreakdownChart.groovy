Map configuration = api.getElement("Configuration")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")

if (!needToProcess) {
  return null
}

Map rawData = libs.CustomerInsights.CustomerDetailDashboardUtils.getDataForRevenueBreakdownChart(configuration, detailViewDashboardParams)
Map colors = DashboardUtils.COLUMN_BASE_COLORS
List chartData = []

if (rawData.lastRevenue && rawData.currentRevenue) {
  chartData.add([name: "Previous Revenue", y: rawData.lastRevenue, color: colors.PRICE])
  chartData.add([name: "Lost Business", y: rawData.lostBusiness])
  chartData.add([name: "Price Effect", y: rawData.priceEffect])
  chartData.add([name: "Volume Effect", y: rawData.volumeEffect])
  chartData.add([name: "Portfolio Mix Effect", y: rawData.portfolioMixEffect])
  chartData.add([name: "Other Effects", y: rawData.otherEffects])
  chartData.add([name: "New Business", y: rawData.newBusiness])
  chartData.add([name: "Current Revenue", y: rawData.currentRevenue, isSum: true, color: colors.PRICE])
}

return chartData