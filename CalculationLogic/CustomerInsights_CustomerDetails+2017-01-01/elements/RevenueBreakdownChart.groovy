List chartData = api.getElement("GetDataForRevenueBreakdownChart")
Map currencyData = api.getElement("Currency")
def hLib = libs.HighchartsLibrary
def currencyUtils = libs.SIP_Dashboards_Commons.CurrencyUtils

String chartTitle = "Revenue Breakdown"
String dataFormat = currencyUtils.getFormatWithCurrencySymbol(hLib.ConstConfig.TOOLTIP_POINT_Y_FORMAT_TYPES.ABSOLUTE_PRICE, currencyData.currencySymbol)

Map legend = hLib.Legend.newLegend().setEnabled(false)
Map xAxis = hLib.Axis.newAxis().setType(hLib.ConstConfig.AXIS_TYPES.CATEGORY)
Map yAxis = hLib.Axis.newAxis().setTitle(currencyData.currencyCode)
    .setMaxPadding(0.1)
Map plotOptions = [series: [borderWidth: 0,
                            dataLabels : [borderWidth: 0,
                                          color      : "BLACK",
                                          enabled    : true,
                                          format     : dataFormat,
                                          style      : [textOutline: 0],
                                          inside     : false]]]
Map tooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>${dataFormat}</b>")
Map chart = hLib.Chart.newChart().setTitle(chartTitle)
    .setTooltip(tooltip)
    .setLegend(legend)
    .setPlotOptions(plotOptions)

if (chartData) {
  Map mainSeries = hLib.WaterfallSeries.newWaterfallSeries().setName(currencyData.currencyCode)
      .setData(chartData)
      .setXAxis(xAxis)
      .setYAxis(yAxis)
  chart.setSeries(mainSeries)
}

Map chartDef = chart.getHighchartFormatDefinition()

return api.buildHighchart(chartDef)