import groovy.transform.Field

@Field Map LINE_SERIES_COLORS = [GENERIC_RED   : "#FF2400",
                                 LIGHT_BLUE    : "#33BBFF",
                                 LIGHT_PINK    : "#E033FF",
                                 GENERIC_BLUE  : "#3933FF",
                                 GENERIC_YELLOW: "#FFD733"]
@Field Map COLUMN_BASE_COLORS = [PRICE   : "#0080FF",
                                 NEGATIVE: "#BF4040",
                                 POSITIVE: "#00FF00"]
@Field String ABSOLUTE_VALUE_FORMAT = "#,###.0f"
@Field String PERCENT_VALUE_FORMAT = "#,###.2f"

Map createDataWithRegressionForChart(Map configuration, Date currentDate, Map rawData, String valueType) {
  def customerDetailUtils = libs.CustomerInsights.CustomerDetailDashboardUtils
  Map attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  if (!rawData) {
    return null
  }

  Map monthList = getMonthConfig(configuration, currentDate)
  Integer lastMonthAmount = monthList.lastMonthAmount
  Integer nextMonthAmount = monthList.nextMonthAmount
  List lastCategory = monthList.lastMonths ?: []
  List nextCategory = monthList.nextMonths ?: []
  Map regressionFactor = customerDetailUtils.getRegressionFactorFromMapData(rawData.clone(), lastCategory, valueType)
  List lastValues = [], regressions = [], nextValues = []
  BigDecimal typeFactor = attributeTypeCode.MARGIN_PERCENT.equalsIgnoreCase(valueType) ? 100 : 1 // show % value
  Integer index = addDataForLastMonths(lastCategory, rawData, valueType, regressionFactor, typeFactor, lastValues, nextValues, regressions)
  addDataForNextMonths(nextCategory, rawData, valueType, regressionFactor, typeFactor, index, nextValues)

  return [lastMonthValues: lastValues,
          nextMonthValues: nextValues,
          regressions    : regressions,
          lastMonths     : lastCategory,
          nextMonths     : nextCategory,
          lastMonthAmount: lastMonthAmount,
          nextMonthAmount: nextMonthAmount]
}

protected int addDataForLastMonths(List lastCategory, Map inputData, String valueType, Map regressionFactor, BigDecimal typeFactor, List lastValues, List nextValues, List regressions) {
  def commonUtils = libs.CustomerInsights.CalculationCommonUtils
  Map dataWithRegressionItem
  BigDecimal regression
  Integer index = 0
  for (month in lastCategory) {
    dataWithRegressionItem = inputData.get(month)
    index++
    lastValues.add([name: month, y: commonUtils.getValueBaseOnType(dataWithRegressionItem, valueType, true)])
    regression = (index * regressionFactor.slope) + regressionFactor.interception
    regressions.add([name: month, y: (regression * typeFactor)])
    nextValues.add(null)
  }

  return index
}

protected void addDataForNextMonths(List nextCategory, Map inputData, String valueType, Map regressionFactor, BigDecimal typeFactor, int inputIndex, List nextValues) {
  BigDecimal avgValue = calculateAverageValueBasedOnType(inputData, valueType)
  BigDecimal valueOfPreviousMonth, seasonalityAdjustment, regression
  Integer index = inputIndex
  for (month in nextCategory) {
    valueOfPreviousMonth = getValueOfPreviousMonthBasedOnType(inputData, month, valueType)
    seasonalityAdjustment = avgValue ? (valueOfPreviousMonth / avgValue) : 0
    index++
    regression = (index * regressionFactor.slope) + regressionFactor.interception
    regression = regression > 0 ? regression : 0
    nextValues.add([name: month, y: seasonalityAdjustment * (regression * typeFactor)])
  }
}
/**
 * Get configuration data for:
 *    lastMonths: List of month's name in last xx months
 *    nextMonths: List of month's name in next x months
 *    lastMonthAmount: last xx months, get from configuration PP
 *    nextMonthAmount: next x months, get from configuration PP
 * @param configuration
 * @param currentDate
 * @return
 */
Map getMonthConfig(Map configuration, Date currentDate) {
  def configurationUtils = libs.CustomerInsights.Configuration
  def dateUtils = libs.CustomerInsights.DateUtils
  Integer lastMonthAmount = configurationUtils.getLastMonthAmountFromConfiguration(configuration)
  Integer nextMonthAmount = configurationUtils.getNextMonthAmountFromConfiguration(configuration)
  Map lastPeriod = dateUtils.getPreviousPeriodFromDate(currentDate, lastMonthAmount)
  Map nextPeriod = dateUtils.getNextPeriodFromDate(currentDate, nextMonthAmount)
  List lastMonths = dateUtils.getMonthNamesInPeriod(lastPeriod)
  List nextMonths = dateUtils.getMonthNamesInPeriod(nextPeriod)

  return [lastMonths     : lastMonths,
          nextMonths     : nextMonths,
          lastMonthAmount: lastMonthAmount,
          nextMonthAmount: nextMonthAmount]
}

List mergeMonthlyData(Map data) {
  return (data.lastMonths ?: []) + (data.nextMonths ?: [])
}

Map buildTooltipForTrendChart() {
  return libs.HighchartsLibrary.Tooltip.newTooltip()
      .setShared(true)
      .setUseHTML(true)
      .setHeaderFormat("<span style='font-size:10px'><b>{point.key}</b></span><table>")
      .setPointFormat("<tr><td style='padding:0'>{series.name}: </td><td style='padding:0'>{point.y:#,###.0f} {series.userOptions.suffix}</td></tr>")
      .setFooterFormat("</table>")
}

Map buildProductViewChart(Map detailViewDashboardParams, List chartData, String chartTitle) {
  def hLib = libs.HighchartsLibrary
  def dateUtils = libs.CustomerInsights.DateUtils

  String valueDisplayFormat = ABSOLUTE_VALUE_FORMAT
  String percentDisplayFormat = PERCENT_VALUE_FORMAT
  String startDateString = dateUtils.formatDateToString(detailViewDashboardParams.getStartDate())
  String endDateString = dateUtils.formatDateToString(detailViewDashboardParams.getEndDate())
  Map currencyData = detailViewDashboardParams.getCurrency()

  Map legend = hLib.Legend.newLegend().setEnabled(false)
  Map chartSeriesTooltip = hLib.Tooltip.newTooltip()
      .setUseHTML(true)
      .setHeaderFormat("")
      .setPointFormat("""<b>{point.name}</b>
                         <br>Product Count: {point.y}
                         <br>Revenue: {point.revenue: ${valueDisplayFormat}} ${currencyData.currencySymbol}
                         <br>Avg. Margin %: {point.marginPercent: ${percentDisplayFormat}} %""")

  Map chartSeriesDataLabels = hLib.DataLabel.newDataLabel().setEnabled(true)
      .setFormat("<b>{point.name}: {point.y} products</b>")

  Map chartSeries = hLib.PieSeries.newPieSeries().setName("Product Count")
      .setData(chartData)
      .setTooltip(chartSeriesTooltip)
      .setDataLabels(chartSeriesDataLabels)

  return hLib.Chart.newChart()
      .setTitle(chartTitle)
      .setSubtitle("From ${startDateString} To ${endDateString}")
      .setSeries(chartSeries)
      .setLegend(legend)
      .getHighchartFormatDefinition()
}

protected List createProductViewChartData(Map groupedData) {
  if (!groupedData) {
    return []
  }
  List groupedDataItem
  BigDecimal revenue, margin, marginPercent

  return groupedData.collect {
    groupedDataItem = it.value
    revenue = groupedDataItem.sum { it.revenue ?: 0.0 }
    margin = groupedDataItem.sum { it.margin ?: 0.0 }
    marginPercent = revenue ? (100 * margin / revenue) : 0.0

    return [name         : it.key,
            y            : groupedDataItem?.size(),
            revenue      : revenue,
            marginPercent: marginPercent]
  }
}

protected BigDecimal calculateAverageValueBasedOnType(Map data, String valueType) {
  List processedData = convertToListBasedOnType(data, valueType)

  return libs.CustomerInsights.CalculationCommonUtils.getAverageValue(processedData)
}

protected BigDecimal getValueOfPreviousMonthBasedOnType(Map data, String currentMonthName, String valueType) {
  if (!data) {
    return BigDecimal.ZERO
  }

  String previousMonthName = getPreviousMonthNameFromCurrentMonthName(currentMonthName)
  Map previousMonthData = data.get(previousMonthName)
  String attribute = getAttributeNameByType(valueType)

  return (previousMonthData?.getAt(attribute) ?: BigDecimal.ZERO)
}

protected String getPreviousMonthNameFromCurrentMonthName(String currentMonthName) {
  String previousName = ""
  if (currentMonthName) {
    List nameParts = currentMonthName.split("-") as List
    String yearName = nameParts.getAt(0)
    String monthName = nameParts.size() > 1 ? nameParts.getAt(1) : ""
    String strYear = yearName.isNumber() ? (yearName.toInteger() - 1).toString() : yearName
    previousName = monthName.isEmpty() ? strYear : (strYear + "-" + monthName)
  }

  return previousName
}

protected List convertToListBasedOnType(Map data, String valueType) {
  String attribute = getAttributeNameByType(valueType)
  List processedData = []
  BigDecimal currentValue
  for (dataItem in data) {
    currentValue = dataItem.value?.getAt(attribute)
    if (currentValue) {
      processedData.add(currentValue)
    }
  }

  return processedData
}

protected String getAttributeNameByType(String valueType) {
  String attribute
  Map attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  switch (valueType) {
    case attributeTypeCode.REVENUE:
      attribute = "revenue"
      break
    case attributeTypeCode.TRANSACTION_AMOUNT:
      attribute = "transactionAmount"
      break
    case attributeTypeCode.QUANTITY:
      attribute = "quantity"
      break
    case attributeTypeCode.MARGIN:
      attribute = "margin"
      break
    case attributeTypeCode.MARGIN_PERCENT:
      attribute = "marginPercent"
      break
    default: attribute = ""
  }

  return attribute
}