import net.pricefx.server.dto.calculation.ResultHighchart

def sipCommons = libs.SIP_Dashboards_Commons
def commonsConstConfig = sipCommons.ConstConfig
def waterfallUtils = sipCommons.WaterfallUtils

Map waterfallConfig = libs.SIP_Dashboards_Commons.ConstConfig.WATERFALL_DASHBOARD_CONFIG
String selectedModel = commonsConstConfig.WATERFALL_MODEL_ABSOLUTE_NAME
Map currencyData = api.getElement("Currency")
Map chartData = api.getElement("ProcessDataForWaterfall")

boolean isDrilldown = waterfallUtils.isModelWithDrilldown(selectedModel)
String numberFormat = waterfallUtils.getNumberTooltipFormat(selectedModel)
String dataFormat = waterfallUtils.getDataTooltipFormat(selectedModel, numberFormat, currencyData.currencySymbol)
String chartTitle = waterfallUtils.getChartModelTitle(waterfallConfig, selectedModel)
String yAxisLabel = waterfallUtils.getYAxisLabel(selectedModel, currencyData.currencyCode)

return generateWaterfallChart(chartTitle, yAxisLabel, chartData, isDrilldown, dataFormat)

protected ResultHighchart generateWaterfallChart(String chartTitle, String yAxisTitle, Map chartData, Boolean enableDataLabels, String displayFormat) {
  def hLib = libs.HighchartsLibrary
  Map legend = hLib.Legend.newLegend().setEnabled(false)
  Map xAxis = hLib.Axis.newAxis().setType(hLib.ConstConfig.AXIS_TYPES.CATEGORY)
  Map yAxis = hLib.Axis.newAxis().setTitle(yAxisTitle)
      .setMaxPadding(0.1)
  Map plotOptions = [series: [borderWidth: 0,
                              dataLabels : [borderWidth: 0,
                                            color      : 'BLACK',
                                            enabled    : enableDataLabels,
                                            format     : displayFormat,
                                            style      : [textOutline: 0],
                                            inside     : false]]]
  Map tooltip = hLib.Tooltip.newTooltip().setPointFormat("<b>${displayFormat}</b>")
  Map chart = hLib.Chart.newChart().setTitle(chartTitle)
      .setTooltip(tooltip)
      .setLegend(legend)
      .setPlotOptions(plotOptions)

  if (chartData.mainSeriesData) {
    def mainSeries = hLib.WaterfallSeries.newWaterfallSeries().setName("HighLevel")
        .setData(chartData.mainSeriesData)
        .setXAxis(xAxis)
        .setYAxis(yAxis)
    chart.setSeries(mainSeries)
  }

  if (chartData.drilldownData) {
    List drilldownSeries = chartData.drilldownData.collect { Map drilldown ->
      return hLib.WaterfallSeries.newWaterfallSeries().setName(drilldown.name)
          .setId(drilldown.id)
          .setData(drilldown.data)
    }
    chart.setDrilldownSeries(*drilldownSeries)
  }

  Map chartDef = chart.getHighchartFormatDefinition()

  return api.buildHighchart(chartDef)
}