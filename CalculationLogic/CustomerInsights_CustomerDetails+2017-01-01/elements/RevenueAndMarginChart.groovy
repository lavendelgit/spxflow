Map currencyData = api.getElement("Currency")
List chartData = api.getElement("GetDataForRevenueAndMargin")
List category = chartData?.period
List revenueData = chartData?.revenue
List marginData = chartData?.marginPercent
def hLib = libs.HighchartsLibrary
def currencyUtils = libs.SIP_Dashboards_Commons.CurrencyUtils
String percentDisplayFormat = DashboardUtils.PERCENT_VALUE_FORMAT
String valueFormat = currencyUtils.getFormatWithCurrencySymbol(hLib.ConstConfig.TOOLTIP_POINT_Y_FORMAT_TYPES.ABSOLUTE_PRICE, currencyData.currencySymbol)

Map revenueAndMarginChart = hLib.Chart.newChart()
    .setTitle("Revenue and Margin")
Map revenueAxis = hLib.Axis.newAxis().setTitle("Revenue")
Map marginPercentAxis = hLib.Axis.newAxis()
    .setTitle("Margin %")
    .setOpposite(true)
    .setMin(0)
    .setMax(100)
    .setAlignTicks(false)
    .setGridLineWidth(0)
Map axisX = hLib.Axis.newAxis().setCategories(category)

Map columnTooltip = hLib.Tooltip.newTooltip()
    .setHeaderFormat("<span style=\"font-size:10px\"><b>{point.key}</b></span><br>")
    .setPointFormat("{series.name}: ${valueFormat}<br>")
    .setUseHTML(true)
    .setShared(true)
Map seriesCol = hLib.ColumnSeries.newColumnSeries()
    .setName("Revenue")
    .setYAxis(revenueAxis)
    .setXAxis(axisX)
    .setData(revenueData)
    .setTooltip(columnTooltip)

Map lineTooltip = hLib.Tooltip.newTooltip()
    .setHeaderFormat("<span style=\"font-size:10px\"><b>{point.key}</b></span><table>")
    .setPointFormat("{series.name}: {point.y:${percentDisplayFormat}} %")
    .setUseHTML(true)
    .setShared(true)

Map tooltip = hLib.Tooltip.newTooltip()
    .setShared(true)

Map seriesLine = hLib.SplineSeries.newSplineSeries()
    .setName("Margin %")
    .setYAxis(marginPercentAxis)
    .setXAxis(axisX)
    .setData(marginData)
    .setTooltip(lineTooltip)

revenueAndMarginChart.setSeries(seriesCol, seriesLine)
    .setZoomType(hLib.ConstConfig.ZOOM_TYPES.XY)
    .setTooltip(tooltip)
    .setEnableMouseWheelZoomMapNavigation(false)

return api.buildHighchart(revenueAndMarginChart.getHighchartFormatDefinition())