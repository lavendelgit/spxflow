List rawData = api.getElement("GetRawDataForProductViewChart")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")

Map groupedDataByClass = rawData?.groupBy { it.productClassificationByQuantity }
List chartData = DashboardUtils.createProductViewChartData(groupedDataByClass)
Map chartDef = DashboardUtils.buildProductViewChart(detailViewDashboardParams, chartData, "High Volume vs Low Volume Products")

return api.buildHighchart(chartDef)