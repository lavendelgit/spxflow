def timeFilter = api.getElement("TimeFilter")

return libs.CustomerInsights.DateUtils.getPeriod(new Date(), timeFilter)