def customer = api.getElement("Customer")
def timeFilter = api.getElement("TimeFilter")
def dimension = api.getElement("DimensionFilter")
Map currency = api.getElement("Currency")
Date currentDate = new Date()
Map currentPeriod = api.getElement("CurrentPeriod")
List productClassByQuantities = api.getElement("ProductClassificationByQuantityFilter")
List productClassByHealthScores = api.getElement("ProductClassificationByHealthScoreFilter")
//adding based on ticket PFPCS-3674 to show in LargeDealOptimizer
String passedCustomerId = api.input("Customer")
if (passedCustomerId) {
  customer = passedCustomerId
}

return libs.CustomerInsights.DetailViewDashboardParameter.newDetailViewDashboardParam(customer,
    timeFilter,
    currency,
    dimension,
    currentPeriod,
    currentDate,
    productClassByQuantities,
    productClassByHealthScores)