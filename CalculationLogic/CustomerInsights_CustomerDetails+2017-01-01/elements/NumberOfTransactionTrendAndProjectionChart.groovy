Map configuration = api.getElement("Configuration")
Map rawData = api.getElement("GetRawDataForRegressionChart")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")
def constant = libs.CustomerInsights.Constant
Map dataForChart = DashboardUtils.createDataWithRegressionForChart(configuration, detailViewDashboardParams.getCurrentDate(), rawData, constant.ATTRIBUTE_TYPE_CODE.TRANSACTION_AMOUNT) ?: [:]

List buyingFrequencies = dataForChart.lastMonthValues
List forecastBuyingFrequencies = dataForChart.nextMonthValues
List regressions = dataForChart.regressions
List months = DashboardUtils.mergeMonthlyData(dataForChart)

def hLib = libs.HighchartsLibrary
Map legend = hLib.Legend.newLegend().setEnabled(true)
Map axisX = hLib.Axis.newAxis().setCategories(months)
Map axisY = hLib.Axis.newAxis().setTitle("Nr. of Transactions")
Map marker = hLib.Marker.newMarker().setEnabled(false)

Map chartSeriesTooltip = DashboardUtils.buildTooltipForTrendChart()

Map lastMonthChartSeries = hLib.ColumnSeries.newColumnSeries().setName("Nr. of Transactions")
    .setData(buyingFrequencies)
    .setXAxis(axisX)
    .setYAxis(axisY)
Map forecastChartSeries = hLib.ColumnSeries.newColumnSeries().setName("Nr. of Transactions Projection")
    .setData(forecastBuyingFrequencies)
    .setXAxis(axisX)
    .setYAxis(axisY)
Map regressionChartSeries = hLib.LineSeries.newLineSeries().setName("Regression")
    .setData(regressions)
    .setXAxis(axisX)
    .setYAxis(axisY)
    .setMarker(marker)
    .setDashStyle(hLib.ConstConfig.DASHSTYLES.SHORTDOT)
    .setColor(DashboardUtils.LINE_SERIES_COLORS.GENERIC_RED)

Map chartDef = hLib.Chart.newChart().setTitle("Nr. of Transactions Trend Last 12M & Projection")
    .setSeries(lastMonthChartSeries, forecastChartSeries, regressionChartSeries)
    .setLegend(legend)
    .setTooltip(chartSeriesTooltip)
    .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)