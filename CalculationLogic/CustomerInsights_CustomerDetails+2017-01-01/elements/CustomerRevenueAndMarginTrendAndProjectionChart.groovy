Map configuration = api.getElement("Configuration")
Map currencyData = api.getElement("Currency")
Map rawData = api.getElement("GetRawDataForRegressionChart")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")
def constant = libs.CustomerInsights.Constant
Date currentDate = detailViewDashboardParams.getCurrentDate()
Map revenueDataForChart = DashboardUtils.createDataWithRegressionForChart(configuration, currentDate, rawData, constant.ATTRIBUTE_TYPE_CODE.REVENUE) ?: [:]
List revenues = revenueDataForChart.lastMonthValues
List forecastRevenues = revenueDataForChart.nextMonthValues
List regressionRevenues = revenueDataForChart.regressions

List months = DashboardUtils.mergeMonthlyData(revenueDataForChart)

Map marginDataForChart = DashboardUtils.createDataWithRegressionForChart(configuration, currentDate, rawData, constant.ATTRIBUTE_TYPE_CODE.MARGIN) ?: [:]
List forecastMargins = marginDataForChart.nextMonthValues

Map marginPercentDataForChart = DashboardUtils.createDataWithRegressionForChart(configuration, currentDate, rawData, constant.ATTRIBUTE_TYPE_CODE.MARGIN_PERCENT) ?: [:]
List marginPercents = marginPercentDataForChart.lastMonthValues
List forecastMarginPercents = marginPercentDataForChart.nextMonthValues
List regressionMarginPercent = marginPercentDataForChart.regressions
forecastMarginPercents = calculateForecastMarginPercents(forecastRevenues, forecastMargins, forecastMarginPercents)

def hLib = libs.HighchartsLibrary
Map legend = hLib.Legend.newLegend().setEnabled(true)
Map axisX = hLib.Axis.newAxis().setCategories(months)
Map axisY = hLib.Axis.newAxis()
    .setTitle("Revenue")

Map marginAxis = hLib.Axis.newAxis()
    .setTitle("Margin %")
    .setOpposite(true)
    .setLabels([format: "{value} %"])
    .setGridLineWidth(0)

Map chartSeriesTooltip = DashboardUtils.buildTooltipForTrendChart()

Map marker = hLib.Marker.newMarker().setEnabled(false)
Map lastMonthRevenueChartSeries = hLib.ColumnSeries.newColumnSeries().setName("Revenue")
    .setData(revenues)
    .setXAxis(axisX)
    .setYAxis(axisY)
    .setSuffix(currencyData.currencySymbol)

Map forecastRevenueChartSeries = hLib.ColumnSeries.newColumnSeries().setName("Revenue Projection")
    .setData(forecastRevenues)
    .setXAxis(axisX)
    .setYAxis(axisY)
    .setSuffix(currencyData.currencySymbol)

Map regressionRevenueChartSeries = hLib.LineSeries.newLineSeries().setName("Revenue Regression")
    .setData(regressionRevenues)
    .setXAxis(axisX)
    .setYAxis(axisY)
    .setSuffix(currencyData.currencySymbol)
    .setMarker(marker)
    .setDashStyle(hLib.ConstConfig.DASHSTYLES.SHORTDOT)
    .setColor(DashboardUtils.LINE_SERIES_COLORS.GENERIC_RED)

Map lastMonthsMarginPercentSeries = hLib.SplineSeries.newSplineSeries().setName("Margin %")
    .setData(marginPercents)
    .setYAxis(marginAxis)
    .setXAxis(axisX)
    .setSuffix("%")
    .setMarker(marker)
    .setColor(DashboardUtils.LINE_SERIES_COLORS.LIGHT_PINK)
Map forecastMarginPercentSeries = hLib.SplineSeries.newSplineSeries().setName("Margin % Projection")
    .setData(forecastMarginPercents)
    .setYAxis(marginAxis)
    .setXAxis(axisX)
    .setSuffix("%")
    .setMarker(marker)
    .setColor(DashboardUtils.LINE_SERIES_COLORS.GENERIC_YELLOW)
Map marginPercentRegressionSeries = hLib.SplineSeries.newSplineSeries().setName("Margin % Regression")
    .setData(regressionMarginPercent)
    .setYAxis(marginAxis)
    .setXAxis(axisX)
    .setSuffix("%")
    .setMarker(marker)
    .setDashStyle(hLib.ConstConfig.DASHSTYLES.SHORTDOT)
    .setColor(DashboardUtils.LINE_SERIES_COLORS.LIGHT_BLUE)

Map chartDef = hLib.Chart.newChart().setTitle("Customer Revenue And Margin Trend Last 12M & Projection")
    .setSeries(lastMonthRevenueChartSeries,
        lastMonthsMarginPercentSeries,
        forecastRevenueChartSeries,
        forecastMarginPercentSeries,
        regressionRevenueChartSeries,
        marginPercentRegressionSeries)
    .setLegend(legend)
    .setTooltip(chartSeriesTooltip)
    .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)

protected List calculateForecastMarginPercents(List forecastRevenues, List forecastMargins, List currentForecastMarginPercents) {
  Map currentForecastMarginPercentItem
  List newForecastMarginPercents = currentForecastMarginPercents
  Integer i
  BigDecimal forecastRevenue
  for (i = 0; i < forecastRevenues?.size(); i++) {
    if (forecastRevenues.getAt(i)) {
      currentForecastMarginPercentItem = currentForecastMarginPercents.getAt(i)
      forecastRevenue = forecastRevenues.getAt(i).y
      currentForecastMarginPercentItem.y = 100 * (forecastRevenue ? (forecastMargins.getAt(i).y / forecastRevenue) : BigDecimal.ZERO)
      newForecastMarginPercents.set(i, currentForecastMarginPercentItem)
    }
  }

  return newForecastMarginPercents
}