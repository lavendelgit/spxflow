def sipCommons = libs.SIP_Dashboards_Commons
def commonsConstConfig = sipCommons.ConstConfig
def waterfallUtils = sipCommons.WaterfallUtils

Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return [:]
}

Map queryData = api.getElement("QueryDataForWaterfall")
List waterfallConfigurationFields = api.local.WATERFALL_CONFIGURATION_FIELDS
Map sqlConfiguration = api.local.SQL_CONFIGURATION
Map colors = commonsConstConfig.COMMON_WATERFALL_DASHBOARDS_CONFIG.WATERFALL_COLUMN_COLORS.BASE
String selectedModel = commonsConstConfig.WATERFALL_MODEL_ABSOLUTE_NAME
String drilldownName = "Drilldown"
Map dataForChart = waterfallUtils.processWaterfallFieldData(waterfallConfigurationFields, queryData)

return waterfallUtils.getWaterfallChartData(sqlConfiguration, dataForChart, queryData, waterfallConfigurationFields, selectedModel, colors, drilldownName)