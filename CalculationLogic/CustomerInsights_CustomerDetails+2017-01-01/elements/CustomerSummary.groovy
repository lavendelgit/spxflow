def constant = libs.CustomerInsights.Constant
def portletUtils = libs.CustomerInsights.SummaryPortletUtils

Boolean needToProcess = api.getElement("NeedToProcess")
Map configuration = api.getElement("Configuration")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")
List rawDataForRegressionAndRevenueMarginChart = api.getElement("GetRawDataForRegressionAndRevenueMarginChart")

Map passedParams = buildPassedParam(detailViewDashboardParams) //generate params need to pass to portfolio dashboard
String url = generateURL(passedParams)
Map customerInfo = getCustomerInfo(configuration, detailViewDashboardParams, rawDataForRegressionAndRevenueMarginChart, needToProcess)
List tableData = setSummaryDataToTable(configuration, detailViewDashboardParams, customerInfo)

def controller = portletUtils.createControllerWithHtmlTable(configuration, tableData)
controller.addButton(constant.CUSTOMER_DETAIL_VIEW_DASHBOARD.OPEN_PORTFOLIO_DASHBOARD_BUTTON_LABEL, "dashboardPage", null, url)

return controller

/**
 * collect item into a list to build summary data table
 * @param configuration
 * @param detailViewDashboardParams
 * @param customerInfo
 * @return
 */
protected List setSummaryDataToTable(Map configuration, Map detailViewDashboardParams, Map inputCustomerInfo) {
  def portletUtils = libs.CustomerInsights.SummaryPortletUtils
  Map summaryLabels = libs.CustomerInsights.Constant.SUMMARY_PORTLET_LABEL
  //general section
  List tableData = []
  customerInfo = inputCustomerInfo ?: [:]
  Map dimensionLabel = initDimensionLabel(configuration, detailViewDashboardParams, customerInfo.customerId)
  tableData.add(portletUtils.initTextItem(summaryLabels.CUSTOMER.SINGULAR, portletUtils.makeSingleCustomerLabel(customerInfo.customerId, customerInfo.customerName)))
  tableData.add(portletUtils.initTextItem(dimensionLabel.fieldLabel, dimensionLabel.fieldValue))
  tableData.add(portletUtils.initTextItem(summaryLabels.CUSTOMER_SEGMENT, customerInfo.customerSegment))
  tableData.add(portletUtils.initHealthScoreItem(summaryLabels.HEALTH_SCORE, customerInfo.customerHealthScore))
  //column-01
  tableData.add(portletUtils.initTrendItem(summaryLabels.REVENUE_TREND_YTD, customerInfo.customerYTDRevenueTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.MARGIN_TREND_YTD, customerInfo.customerYTDMarginTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.QUANTITY_TREND_YTD, customerInfo.customerYTDQuantityTrend))
  //column-02
  tableData.add(portletUtils.initTrendItem(summaryLabels.REVENUE_TREND_L12M, customerInfo.customerRevenueTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.MARGIN_TREND_L12M, customerInfo.customerMarginTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.QUANTITY_TREND_L12M, customerInfo.customerQuantityTrend))
  //column-03
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_REVENUE, customerInfo.revenue))
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_MARGIN, customerInfo.margin))
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_QUANTITY, customerInfo.quantity))

  return tableData
}

protected Map initDimensionLabel(Map configuration, Map detailViewDashboardParams, String customer) {
  def inputUtils = libs.CustomerInsights.InputUtils
  Map dimensionLabel
  if (customer) { //show dimensionLabel when data is existed
    dimensionLabel = inputUtils.getDimensionFilterLabel(configuration, detailViewDashboardParams.getDimension())
  } else { //hide dimensionLabel when data is existed
    dimensionLabel = inputUtils.getDimensionFilterLabel(configuration, null)
  }

  return dimensionLabel
}

protected String generateURL(Map passedParams) {
  def constant = libs.CustomerInsights.Constant
  String customerDetailDashboardName = constant.CUSTOMER_DETAIL_VIEW_DASHBOARD.NAME
  String portfolioDashboardName = constant.CUSTOMER_PRODUCT_PORTFOLIO_DASHBOARD.NAME
  String url = "${customerDetailDashboardName}/${portfolioDashboardName}"

  if (passedParams) {
    String paramString = passedParams.collect { key, value -> buildStringOfPassedParam(key, value) }.flatten().join("&")
    url += ("?" + paramString)
  }

  return url
}

protected Map buildPassedParam(Map detailViewDashboardParams) {
  Map timeFilter = libs.CustomerInsights.Constant.INPUT_ENTRY.TIME_FILTER
  Map detailFilter = libs.CustomerInsights.Constant.INPUT_ENTRY.CUSTOMER_DETAIL_VIEW_FILTER

  Map passedParam = [:]
  passedParam << [Customer: detailViewDashboardParams.getCustomerId()]
  passedParam << [(timeFilter.NAME): detailViewDashboardParams.getTimeFilter()]

  List productClassByQuantities = detailViewDashboardParams.getProductClassByQuantities()
  List productClassByHealthScores = detailViewDashboardParams.getProductClassByHealthScores()

  if (productClassByQuantities) {
    passedParam << [(detailFilter.PRODUCT_CLASSIFICATION_QUANTITY.NAME): productClassByQuantities]
  }

  if (productClassByHealthScores) {
    passedParam << [(detailFilter.PRODUCT_CLASSIFICATION_HEALTHSCORE.NAME): productClassByHealthScores]
  }

  return passedParam
}

protected Map getCustomerInfo(Map configuration, Map detailViewDashboardParams, List rawDataForRegressionAndRevenueMarginChart, boolean isNeedToProcess = false) {
  if (!isNeedToProcess) {
    return null
  }

  Map customerData = libs.CustomerInsights.CustomerDetailDashboardUtils.getSummaryInfoForCustomerDetailView(configuration, detailViewDashboardParams)
  customerData << getGeneralCustomerInfo(rawDataForRegressionAndRevenueMarginChart)

  return customerData
}

protected def buildStringOfPassedParam(String paramKey, def paramValue) {
  if (paramValue instanceof List) {
    return paramValue.findAll().collect { "$paramKey=$it" }
  } else {
    return "$paramKey=$paramValue"
  }
}

protected Map getGeneralCustomerInfo(List rawDataForRegressionAndRevenueMarginChart) {
  if (!rawDataForRegressionAndRevenueMarginChart) {
    return [:]
  }

  Map item = rawDataForRegressionAndRevenueMarginChart.getAt(0) ?: [:]

  return [customerId     : item.customerId,
          customerName   : item.customerName,
          customerSegment: item.customerSegment]
}