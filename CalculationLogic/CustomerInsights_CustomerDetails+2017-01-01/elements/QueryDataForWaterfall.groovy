import net.pricefx.domain.CustomerGroup
import net.pricefx.domain.ProductGroup

Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return [:]
}

def sipCommons = libs.SIP_Dashboards_Commons
def commonsConstConfig = sipCommons.ConstConfig
def waterfallUtils = sipCommons.WaterfallUtils

Map sqlConfiguration = sipCommons.ConfigurationUtils.getAdvancedConfiguration([:], commonsConstConfig.DASHBOARDS_ADVANCED_CONFIGURATION_NAME)
api.local.SQL_CONFIGURATION = sqlConfiguration
List waterfallConfigurationFields = waterfallUtils.getWaterfallFieldsDefinition()
api.local.WATERFALL_CONFIGURATION_FIELDS = waterfallConfigurationFields
api.logInfo("sqlConfiguration",sqlConfiguration)
api.logInfo("waterfallConfigurationFields",waterfallConfigurationFields)
Map configuration = api.getElement("Configuration")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")

CustomerGroup customers = buildCustomerGroupFromInput(detailViewDashboardParams)
ProductGroup products = buildProductGroupFromInput(configuration, detailViewDashboardParams)

String dateFrom = detailViewDashboardParams.getStartDate()
String dateTo = detailViewDashboardParams.getEndDate()
String targetCurrencyCode = detailViewDashboardParams.getCurrency().currencyCode

return waterfallUtils.getWaterfallQueryResult(sqlConfiguration,
    waterfallConfigurationFields,
    targetCurrencyCode,
    products,
    customers,
    dateFrom,
    dateTo,
    null)

//*********************************************
protected CustomerGroup buildCustomerGroupFromInput(Map detailViewDashboardParams) {
  CustomerGroup customers = new CustomerGroup()
  customers.setCustomerFieldName("customerId")
  customers.setCustomerFieldValue(detailViewDashboardParams.getCustomerId())

  return customers
}

protected ProductGroup buildProductGroupFromInput(Map configuration, Map detailViewDashboardParams) {
  Date endDate = detailViewDashboardParams.getEndDate()
  List filters = detailViewDashboardParams.getCustomPeriodQuerySimulationFilter(configuration, null, endDate)
  Map customerAndProductList = libs.CustomerInsights.CalculationCommonUtils.getCustomerAndProductBaseOnInput(configuration, filters)
  List productIds = customerAndProductList.productIds ?: []
  Map productFilterCriteria = [_constructor: "AdvancedCriteria",
                               operator    : "and",
                               criteria    : [
                                   [
                                       fieldName   : "sku",
                                       operator    : "inSet",
                                       value       : productIds,
                                       _constructor: "AdvancedCriteria"
                                   ]
                               ]
  ]
  ProductGroup productGroup = new ProductGroup()
  productGroup.setProductFilterCriteria(productFilterCriteria)

  return productGroup
}