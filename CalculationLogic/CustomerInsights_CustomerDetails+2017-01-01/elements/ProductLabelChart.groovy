List rawData = api.getElement("GetRawDataForProductViewChart")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")

Map groupedDataByClass = rawData?.groupBy { it.productClassificationByMarginPercent }
List chartData = DashboardUtils.createProductViewChartData(groupedDataByClass)
Map chartDef = DashboardUtils.buildProductViewChart(detailViewDashboardParams, chartData, "Speciality vs Commodity Products")

return api.buildHighchart(chartDef)
