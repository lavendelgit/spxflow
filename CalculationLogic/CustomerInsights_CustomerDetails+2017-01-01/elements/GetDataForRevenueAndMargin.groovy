List rawData = api.getElement("GetRawDataForRegressionAndRevenueMarginChart")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return null
}

return processDataForRevenueAndMarginChart(rawData, detailViewDashboardParams)

protected List processDataForRevenueAndMarginChart(List rawData, Map detailViewDashboardParams) {
  def dateUtils = libs.CustomerInsights.DateUtils
  List processedData = []
  List monthNames = dateUtils.getMonthNamesInPeriod(detailViewDashboardParams.getStartDate(), detailViewDashboardParams.getEndDate())
  Map groupedDataByMonth = rawData.groupBy { it.month }
  Map groupedDataItem

  for (monthName in monthNames) {
    groupedDataItem = groupedDataByMonth.getAt(monthName)?.getAt(0) ?: [:]
    processedData.add([period       : monthName,
                       revenue      : (groupedDataItem.revenue ?: 0.0),
                       margin       : (groupedDataItem.margin ?: 0.0),
                       marginPercent: 100 * (groupedDataItem.marginPercent ?: 0.0)])
  }

  return processedData
}
