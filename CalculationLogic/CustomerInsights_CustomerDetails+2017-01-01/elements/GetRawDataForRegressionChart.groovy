List rawData = api.getElement("GetRawDataForRegressionAndRevenueMarginChart")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return null
}

return processDataForRegressionChart(rawData, detailViewDashboardParams)

protected Map processDataForRegressionChart(List rawData, Map detailViewDashboardParams) {
  def dateUtils = libs.CustomerInsights.DateUtils

  if (!rawData) {
    return [:]
  }

  String currentMonthName = dateUtils.getMonthNamesInPeriod(detailViewDashboardParams.getCurrentDate(), detailViewDashboardParams.getCurrentDate()).getAt(0)
  List sortedDataByMonth = rawData.sort(false, { a, b -> a.month <=> b.month })
  int lastIndexOfData = sortedDataByMonth.size() - 1

  if (sortedDataByMonth.getAt(lastIndexOfData).month == currentMonthName) {
    sortedDataByMonth.remove(lastIndexOfData)
  }

  Map groupedDataByMonth = sortedDataByMonth.groupBy { it.month }

  return groupedDataByMonth.collectEntries { String monthName, List itemData ->
    return [(monthName): itemData.getAt(0)]
  }
}
