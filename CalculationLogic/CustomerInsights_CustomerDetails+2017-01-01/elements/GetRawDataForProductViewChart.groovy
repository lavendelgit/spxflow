Map configuration = api.getElement("Configuration")
Map detailViewDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return null
}

return libs.CustomerInsights.CustomerDetailDashboardUtils.calculateMetricsPerProduct(configuration, detailViewDashboardParams)