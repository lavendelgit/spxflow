
Map getPlantMapping(Object skus){
    List filters = [
            Filter.equal("name", libs.SpxflowLib.Constants.MaterialPlantCost_PX),
            Filter.in("sku",skus)
    ]
    Map MaterialPlantMap = [:]
    MaterialPlantMap = api.find("PX6",0,api.getMaxFindResultsLimit(),null,*filters)?.collectEntries{[(it.sku): [
        plant : it.attribute1,
        StandardCost : it.attribute2,
        Currency : it.attribute3,
        StandardCostInUSD : it.attribute5
]
]
}

    return MaterialPlantMap
}

Map getMaterialArchetypeMapping(Object skus){
    List filters = [
            Filter.equal("name", libs.SpxflowLib.Constants.MaterialArchetypeMapping_PX),
            Filter.in("sku",skus)
    ]
    Map Materialarchetype = [:]
    Materialarchetype = api.find("PX3",0,api.getMaxFindResultsLimit(),null,*filters)?.collectEntries{[(it.sku): [Archetype : it.attribute1,ArchetypeCatgeory:it.attribute2,ArchetypeValue:it.attribute2]]
    }

    return Materialarchetype
}

Map getSAPCurrentPrice(Object skus){
    List filters = [
            Filter.equal("name", libs.SpxflowLib.Constants.SAPCurrentPrice_PX),
            Filter.in("sku",skus)
    ]
    Map MaterialSAPCurrentPrice = [:]
    MaterialSAPCurrentPrice = api.find("PX20",0,api.getMaxFindResultsLimit(),null,*filters)?.collectEntries{[(it.sku): [SAPPrice : it.attribute13,
                                                                                         SAPPriceCurrency : it.attribute2,
                                                                                         SAPPriceUSD : it.attribute10,
                                                                                                                        SAPPriceMUnit : it.attribute1,
            priceUnit : it.attribute11,
    ]]
    }

    return MaterialSAPCurrentPrice
}

Map getProductHierarchy(Object skus){
    List filters = [
            Filter.equal("name", libs.SpxflowLib.Constants.ProductHierarchyMapping_PX),
            Filter.in("sku",skus)
    ]
    MaterialSAPCurrentPrice = api.find("PX6",0,api.getMaxFindResultsLimit(),null,*filters)?.collectEntries{[(it.sku):[
            Brand         : it.attribute1,
            ProductLine   : it.attribute2,
            ProductFamily : it.attribute3,
            ProductSeries : it.attribute4,
            ModelSize     : it.attribute5
    ]
    ]
    }

    return MaterialSAPCurrentPrice
}



















