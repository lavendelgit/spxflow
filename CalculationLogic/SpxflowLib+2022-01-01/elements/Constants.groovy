import groovy.transform.Field

@Field final String HierarchyPlantMapping_PX= "HierarchyPlantMapping"
@Field final String MaterialPlantMapping_PX = "MaterialPlantMapping"
@Field final String MaterialPlantCost_PX = "MaterialPlantCost"
@Field final String RegionalDiscount_PX = "RegionalDiscount"
@Field final String StandardCostMaster_PX = "StandardCost"
@Field final String ArchetypeMargins_PX = "ArchetypeMargins"
@Field final String SAPCurrentPrice_PX = "SAPCurrentPrice"
@Field final String ProductHierarchyMapping_PX = "ProductHierarchyMapping"
@Field final String MaterialArchetypeMapping_PX = "MaterialArchetypeMapping"
@Field final String MaterialSalesOrgMapping_PX = "MaterialSalesOrgMapping"
@Field final String Currencies_PP = "Currencies"
@Field final String PH1Brand_PP = "PH1Brand"
@Field final String PH2ProductLine_PP = "PH2ProductLine"
@Field final String PH3ProductFamily_PP = "PH3ProductFamily"
@Field final String PH4ProductSeries_PP = "PH4ProductSeries"
@Field final String PH5ModelSize_PP = "PH5ModelSize"
@Field final String ProductHierarchyNomenclature_PP = "ProductHierarchyNomenclature"
@Field final String UOMConversion_PP = "UOMConversion"
@Field final String Plant_PP = "Plant"
@Field final String UOM_PP = "UOM"
@Field final String DistributionChannel_PP = "DistributionChannel"
@Field final String RegionalDiscountSequence_PP = "RegionalDiscountSequence"
@Field final String AnnualExchangeRate_PP = "AnnualExchangeRate"
@Field final String MonthlyExchangeRate_PP = "MonthlyExchangeRate"
@Field final String PlantSalesOrgMapping_PP = "PlantSalesOrgMapping"
@Field final String MaterialGroup1_PP = "MaterialGroup1"
@Field final String Countries_PP = "Countries"
@Field final String Industry_PP = "Industry"
@Field final String ItemCategory_PP = "ItemCategory"
@Field final String CustomerGroup_PP = "CustomerGroup"
@Field final String SalesGroup_PP = "SalesGroup"
@Field final String SalesOffice_PP = "SalesOffice"
@Field final String SalesOrganization_PP = "SalesOrganization"
@Field final String ContinentAndCountryMapping_PP = "ContinentAndCountryMapping"
@Field final String ProfiseeDescription_PP = "ProfiseeDescription"
@Field final String Brand_PP = "Brand"
@Field final String Rule8020_PP = "80/20Rule"
@Field final String SkuCategorisation_PP = "SkuBasedA/BCategorisation"
@Field final String VelocityBucketPrice_PP = "VelocityBucketPrice"
@Field final String SalesVelocityBucket_PP = "SalesVelocityBuckets"
@Field final String CustomerSalesOrgMapping_CX = "CustomerSalesOrgMapping"
@Field final String CustomerProfiseeMapping_CX = "CustomerProfiseeMapping"

@Field Map SYMBOLS = [
        DECREASE: "↓",
        INCREASE: "↑",
        FLAT    : ""
]

@Field Map TEXT_COLORS = [
        DECREASE: "white",
        INCREASE: "green",
        FLAT    : null
]

@Field Map BG_COLORS = [
        DECREASE: "#FA8072",
        INCREASE: null,
        FLAT    : null
]

@Field Map SERIES_COLOR = [
        Online : [
                NEGATIVE: "#D0021B",
                POSITIVE: "#4A90E2",
        ],
        Offline: [
                NEGATIVE: "#F5A623",
                POSITIVE: "#4A4A4A",
        ]
]