Map buildColumnSeries(String seriesName,
                      String seriesLabel,
                      BigDecimal purchasePrice,
                      BigDecimal salesPrice,
                      BigDecimal calculatedListPrice,
                      BigDecimal listPrice) {
    def commonUtils = libs.SpxflowLib.CommonUtils
    def SERIES_COLOR = libs.SpxflowLib.Constants.SERIES_COLOR
    return [
            name         : seriesName,
            label        : seriesLabel,
            data         : [
                    commonUtils.nvl(purchasePrice),
                    commonUtils.nvl(salesPrice),
                    commonUtils.nvl(calculatedListPrice),
                    commonUtils.nvl(listPrice)
            ],
            color        : SERIES_COLOR[seriesName]?.POSITIVE,
            negativeColor: SERIES_COLOR[seriesName]?.NEGATIVE
    ]
}


Map getPriceComparisionColumnChartDef(List seriesData,
                                      String currency,
                                      String chartTitle) {
    return [
            chart  : [
                    type: 'column'
            ],
            title  : [
                    text: chartTitle
            ],
            credits: [
                    enabled: false
            ],
            xAxis  : [
                    categories: [
                            'Purchase Price',
                            'Sales Price',
                            'Calculated List Price',
                            'New RP'
                    ],
                    labels    : [
                            style: [
                                    fontSize: '8px'
                            ]
                    ]
            ],
            yAxis  : [
                    title: [
                            text: currency
                    ]
            ],
            legend : [
                    layout       : 'horizontal',
                    verticalAlign: 'bottom',
                    style        : [
                            fontSize: '8px'
                    ]
            ],
            tooltip: [
                    shared     : true,
                    valueSuffix: ' ' + currency
            ],
            series : seriesData
    ]
}