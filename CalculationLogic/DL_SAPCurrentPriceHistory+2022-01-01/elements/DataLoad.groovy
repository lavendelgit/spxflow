
List duplicateRecord = []
metaData = api.find("PXAM",Filter.equal("name",out.SourceTableSelection))?.collectEntries{[(it.fieldName):it.label]}
List deleteDuplicateRecord = []
api.local.duplicateRecords?.each{record->
    duplicateRecord = []
    if(record?.value?.size()>1){
        duplicateRecord = record?.value?.minus(record?.value?.getAt(0))
        loadToDataSource(duplicateRecord)
    }
}
return
void loadToDataSource(Object duplicateRecord){
    def target = api.getDatamartRowSet("target")
    api.trace("duplicateRecord",duplicateRecord)
    Map rowRecord = [:]
    duplicateRecord?.each{ record->
        rowRecord.MaterialNumber = record?.sku
        metaData?.each{attributes->
            if(!attributes?.key!="sku"){
                rowRecord[attributes?.value] = record[attributes?.key]
            }
        }
        target?.addRow(rowRecord)
    }
}