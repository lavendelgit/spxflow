duplicateData = [:]
businessKey = out.TableConfigurations["Keys"]?.split(',')*.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")*.trim()
duplicateDetails = findDuplicateEntrys(out.SourceTableSelection,out.TableConfigurations?.SourceTypeCode,businessKey)
List filter = [Filter.equal("name",out.SourceTableSelection),
               Filter.lessOrEqual("ValidFrom",new Date()?.format("YYYY-MM-dd"))]
businessKey?.each{
    filter << Filter.in(it,duplicateDetails[it])
}
String key
iterator = api.stream(out.TableConfigurations?.SourceTypeCode,"-ValidFrom",*filter)
iterator?.collect{attribute ->
    key = ""
    businessKey?.each{it ->
        key = key?key+"_"+attribute[it] : attribute[it]
    }
    if(!duplicateData[key]){
        duplicateData[key] = []
        duplicateData[key] << attribute
    }else{
        duplicateData[key] << attribute
    }
}
iterator?.close()
api.local.duplicateRecords = duplicateData
return

List findDuplicateEntrys(String sourceTable, String sourceTypeCode, List bKey) {
    List filter = [Filter.equal("name",sourceTable),
                   Filter.greaterOrEqual("lastUpdateDate",out.DataloadCalculation),
    ]
    List filterAttributes = bKey
    iterator = api.stream(sourceTypeCode,"ValidFrom",filterAttributes,*filter)
    duplicateRecords = iterator?.collect{it}
    iterator?.close()
    return duplicateRecords
}