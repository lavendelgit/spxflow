Map opportunityData = api.local.TOP_AND_WORST_SELLING_OPPORTUNITY
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
String productAttribute = api.local.PRODUCT_ATTRIBUTES.getAt(portfolioDashboardParams.getProductAttribute())
Map metaData = initMetaData(productAttribute, opportunityData?.top)

return libs.CustomerInsights.DataTable.generateDataTable(metaData)

protected Map initMetaData(String productAttribute, List boldData) {
  def dataTable = libs.CustomerInsights.DataTable

  return dataTable.newTableMetaData()
      .setTableStructure(createTableStructure(productAttribute))
      .setTablePreferenceName("Opportunity")
      .setData(dataTable.buildBoldData(boldData))
      .getTableMetaData()
}

protected List createTableStructure(String productAttribute) {
  def dataTable = libs.CustomerInsights.DataTable
  def constant = libs.CustomerInsights.Constant
  Map columnLabels = constant.SELLING_OPPORTUNITY_TABLE_COLUMN_LABELS
  Map attributeName = constant.ENTITY_ATTRIBUTE_NAME
  List tableStructure = []
  tableStructure.add(dataTable.createTextColumnConfiguration(productAttribute, attributeName.PRODUCT_ATTRIBUTE, true))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.CROSS_SELL, attributeName.CROSS_SELL))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.UP_SELL, attributeName.UP_SELL))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.OPPORTUNITY, attributeName.OPPORTUNITY))

  return tableStructure
}