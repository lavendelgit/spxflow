Map productAttributes = libs.CustomerInsights.ProductUtils.getProductAttributes()
api.local.PRODUCT_ATTRIBUTES = productAttributes
String optionName = "ProductAttribute"
def productAttributeOption = api.option(optionName, productAttributes?.keySet() as List, productAttributes)
setConfigOption(productAttributes, optionName)

return productAttributeOption

protected void setConfigOption(Map productAttributes, String optionName) {
  def param = api.getParameter(optionName)
  param?.setLabel(libs.CustomerInsights.Constant.INPUT_ENTRY.PRODUCT_ATTRIBUTE.LABEL)
  if (param && !param.getValue()) {
    param.setRequired(true)
    Map.Entry defaultItem = findDefaultOptionItem(productAttributes)
    String defaultValue = defaultItem?.key ?: productAttributes?.keySet().getAt(0)
    param.setValue(defaultValue)
  }
}

protected Map.Entry findDefaultOptionItem(Map productAttributes) {
  String pattern = ".*Product.*Group.*".toLowerCase()

  return productAttributes?.find { (it.value ?: "").toLowerCase() =~ pattern }
}