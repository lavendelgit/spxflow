/*
  using api.local to pass value to other element -> improve performance
 */
Map configuration = api.getElement("Configuration")
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return null
}

List productHealthData = libs.CustomerInsights.CustomerProductPortfolioDashboardUtils.getProductHealthData(configuration, portfolioDashboardParams)
Integer topProduct = portfolioDashboardParams.getTopProduct()
api.local.TOP_AND_WORST_PRODUCT_HEALTH = libs.CustomerInsights.DataTable.getTopAndWorst(productHealthData, topProduct)

return null