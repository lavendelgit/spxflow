/*
  using api.local to pass value to other element -> improve performance
 */
def portfolioUtils = libs.CustomerInsights.CustomerProductPortfolioDashboardUtils
Map configuration = api.getElement("Configuration")
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return null
}
api.local.REVENUE_BELOW_TARGET_PER_PRODUCT_ATTRIBUTE = portfolioUtils.getOpportunityPerProductAttribute(configuration, portfolioDashboardParams)

return null