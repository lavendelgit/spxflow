String chartTitle = "Revenue and Margin Contribution (Products)"
String axisTitle = "No. of Products"
Map data = api.getElement("GetDataForRevenueAndMarginContributionPerProduct") ?: [:]
Map chartDef = getChartDefinition(chartTitle, axisTitle, data.bucketRevenueData, data.bucketMarginData, data.drilldownSeriesData)

return api.buildHighchart(chartDef)

protected Map getChartDefinition(String title, String axisTitle, List bucketRevenueData, List bucketMarginData, List drilldownSeriesData) {
  def hLib = libs.HighchartsLibrary
  Map config = RevenueAndMarginContributionUtils.REVENUE_AND_MARGIN

  Map chart = hLib.Chart.newChart()
  String tooltipFooterFormat = config.tooltip.footerFormat
  String tooltipHeaderFormat = config.tooltip.headerFormat
  String currencySymbol = config.currency.currencySymbol

  Map tooltipMargin = hLib.Tooltip.newTooltip()
      .setHeaderFormat(tooltipHeaderFormat)
      .setPointFormat(generateSeriesTooltipFormat(config.tooltipLabels.margin, axisTitle, currencySymbol))
      .setFooterFormat(tooltipFooterFormat)
      .setUseHTML(config.tooltip.useHTML)

  Map tooltipRevenue = hLib.Tooltip.newTooltip()
      .setHeaderFormat(tooltipHeaderFormat)
      .setPointFormat(generateSeriesTooltipFormat(config.tooltipLabels.revenue, axisTitle, currencySymbol))
      .setFooterFormat(tooltipFooterFormat)
      .setUseHTML(config.tooltip.useHTML)

  Map xAxis = hLib.Axis.newAxis().setType(hLib.ConstConfig.AXIS_TYPES.CATEGORY)

  Map yAxis = hLib.Axis.newAxis().setTitle(axisTitle)

  Map seriesRevenue = hLib.ColumnSeries.newColumnSeries()
      .setName(config.tooltipLabels.revenue)
      .setData(bucketRevenueData)
      .setTooltip(tooltipRevenue)
      .setXAxis(xAxis)
      .setYAxis(yAxis)

  Map seriesMargin = hLib.ColumnSeries.newColumnSeries()
      .setName(config.tooltipLabels.margin)
      .setData(bucketMarginData)
      .setTooltip(tooltipMargin)
      .setXAxis(xAxis)
      .setYAxis(yAxis)

  List drilldownSeries = []
  Map tooltip

  drilldownSeriesData.each {
    tooltip = hLib.Tooltip.newTooltip()
        .setHeaderFormat(it.tooltip.headerFormat)
        .setPointFormat(it.tooltip.pointFormat)
    drilldownSeries.add(hLib.ColumnSeries.newColumnSeries()
        .setName(it.name)
        .setId(it.id)
        .setData(it.data)
        .setTooltip(tooltip))
  }

  chart.setSeries(seriesRevenue, seriesMargin)
      .setTitle(title)
      .setDrilldownSeries(*drilldownSeries)
      .setPlotOptions(config.plotOptions)

  return chart.getHighchartFormatDefinition()
}

protected String generateSeriesTooltipFormat(String tooltipCategory, String tooltipTitle, String currency) {
  return """<tr><td style='padding:0'>$tooltipTitle: {point.y:,.0f}</td><br>
                <td style='padding:0'>Total $tooltipCategory: {point.total:,.0f} $currency</td><br>
                <td style="padding:0">Representing $tooltipCategory: {point.representing:,.0f} $currency</td></tr><br>"""
}