Map configuration = api.getElement("Configuration")
def constant = libs.CustomerInsights.Constant
List topValues = configuration.MASTER_DATA.TOP_WORST_PRODUCTS

return libs.CustomerInsights.InputUtils.createTopWorstList(constant.INPUT_ENTRY.TOP_WORST_PRODUCT.LABEL, topValues)