Map configuration = api.getElement("Configuration")
Map processedData = api.local.TOP_AND_WORST_PRODUCT_HEALTH
Map metaData = initMetaData(configuration, processedData?.top, processedData?.worst)

return libs.CustomerInsights.DataTable.generateDataTable(metaData)

protected Map initMetaData(Map configuration, List topData, List worstData) {
  def dataTable = libs.CustomerInsights.DataTable

  return dataTable.newTableMetaData()
      .setTableStructure(createTableStructure(configuration.CONFIGURATION))
      .setTablePreferenceName("ProductTrends")
      .setData(dataTable.buildTopWorstData(topData, worstData))
      .getTableMetaData()
}

protected List createTableStructure(Map configurationCategory) {
  def dataTable = libs.CustomerInsights.DataTable
  def constant = libs.CustomerInsights.Constant
  Map columnLabels = constant.TREND_TABLE_COLUMN_LABELS
  Map attributeName = constant.ENTITY_ATTRIBUTE_NAME
  List tableStructure = []

  tableStructure.add(dataTable.createProductNameColumnConfiguration())
  tableStructure.add(dataTable.createProductIdColumnConfiguration())
  tableStructure.add(dataTable.createTrendColumnConfiguration(configurationCategory, columnLabels.REVENUE_TREND_L12M, attributeName.REVENUE_TREND))
  tableStructure.add(dataTable.createTrendColumnConfiguration(configurationCategory, columnLabels.MARGIN_TREND_L12M, attributeName.MARGIN_TREND))
  tableStructure.add(dataTable.createTrendColumnConfiguration(configurationCategory, columnLabels.QUANTITY_TREND_L12M, attributeName.QUANTITY_TREND))
  tableStructure.add(dataTable.createTrendColumnConfiguration(configurationCategory, columnLabels.REVENUE_TREND_YTD, attributeName.REVENUE_TREND_YTD))
  tableStructure.add(dataTable.createTrendColumnConfiguration(configurationCategory, columnLabels.MARGIN_TREND_YTD, attributeName.MARGIN_TREND_YTD))
  tableStructure.add(dataTable.createTrendColumnConfiguration(configurationCategory, columnLabels.QUANTITY_TREND_YTD, attributeName.QUANTITY_TREND_YTD))

  return tableStructure
}
