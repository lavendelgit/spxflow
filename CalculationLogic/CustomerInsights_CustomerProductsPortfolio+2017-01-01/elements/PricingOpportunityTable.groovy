Map opportunityData = api.local.TOP_AND_WORST_PRICING_OPPORTUNITY
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
String productAttribute = api.local.PRODUCT_ATTRIBUTES.getAt(portfolioDashboardParams.getProductAttribute())
Map metaData = initMetaData(productAttribute, opportunityData?.top)

return libs.CustomerInsights.DataTable.generateDataTable(metaData)

protected Map initMetaData(String productAttribute, List boldData) {
  def dataTable = libs.CustomerInsights.DataTable

  return dataTable.newTableMetaData()
      .setTableStructure(createTableStructure(productAttribute))
      .setTablePreferenceName("PricingOpportunity")
      .setData(dataTable.buildBoldData(boldData))
      .getTableMetaData()
}

protected List createTableStructure(String productAttribute) {
  def dataTable = libs.CustomerInsights.DataTable
  def constant = libs.CustomerInsights.Constant
  Map columnLabels = constant.PRICING_OPPORTUNITY_TABLE_COLUMN_LABELS
  Map attributeName = constant.ENTITY_ATTRIBUTE_NAME
  List tableStructure = []
  tableStructure.add(dataTable.createTextColumnConfiguration(productAttribute, attributeName.PRODUCT_ATTRIBUTE, true))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.REVENUE_BELOW_TARGET, attributeName.REVENUE_BELOW_TARGET))
  tableStructure.add(dataTable.createPercentColumnConfiguration(columnLabels.REVENUE_BELOW_TARGET_PERCENT, attributeName.REVENUE_BELOW_TARGET_PERCENT))

  return tableStructure
}