Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")

if (!needToProcess) {
  return null
}

List rawData = api.local.REVENUE_BELOW_TARGET_PER_PRODUCT_ATTRIBUTE
Integer topProduct = portfolioDashboardParams.getTopProduct()
List sortedData = rawData.sort { a, b -> return b.revenueBelowTarget <=> a.revenueBelowTarget }
api.local.TOP_AND_WORST_PRICING_OPPORTUNITY = libs.CustomerInsights.DataTable.getTopAndWorst(sortedData, topProduct)

return null