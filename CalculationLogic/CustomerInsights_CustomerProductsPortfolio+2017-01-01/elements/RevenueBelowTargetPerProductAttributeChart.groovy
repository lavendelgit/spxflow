Map currency = api.getElement("Currency")
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
String productAttribute = api.local.PRODUCT_ATTRIBUTES.getAt(portfolioDashboardParams.getProductAttribute())
List productsOfChart = api.local.REVENUE_BELOW_TARGET_PER_PRODUCT_ATTRIBUTE
List chartData = DashboardUtils.createRevenueBelowTargetPerAttributeProductData(productsOfChart)
String chartTitle = "Pricing Opportunity By ${productAttribute}"
String axisYTitle = "Revenue Below Target"
String axisZTitle = "Revenue Below Target (%)"
String columnName = "Revenue Below Target"
String lineName = "Revenue Below Target (%)"
Map titles = DashboardUtils.buildChartTitles(chartTitle, axisYTitle, axisZTitle, columnName, lineName)

return DashboardUtils.buildRevenueBelowTargetPerTopProductsChart(chartData, currency.currencySymbol, titles)