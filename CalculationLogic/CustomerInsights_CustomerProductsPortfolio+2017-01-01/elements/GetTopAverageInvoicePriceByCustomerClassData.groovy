/*
  using api.local to pass value to other element -> improve performance
 */
Map configuration = api.getElement("Configuration")
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")

if (!needToProcess) {
  return null
}

List avgInvoicePriceData = libs.CustomerInsights.CustomerProductPortfolioDashboardUtils.getAverageInvoicePricePerProduct(configuration, portfolioDashboardParams)
Integer topProduct = portfolioDashboardParams.getTopProduct()
api.local.TOP_AND_WORST_AVG_INVOICE_PRICE_BY_CUSTOMER_CLASS = libs.CustomerInsights.DataTable.getTopAndWorst(avgInvoicePriceData, topProduct)

return null