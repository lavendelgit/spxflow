boolean needToProcess = api.getElement("NeedToProcess")
Map configuration = api.getElement("Configuration")
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
def portletUtils = libs.CustomerInsights.SummaryPortletUtils
def portfolioUtils = libs.CustomerInsights.CustomerProductPortfolioDashboardUtils
Map customerInfo, opportunities

if (needToProcess) {
  customerInfo = portfolioUtils.getSummaryInfoForCustomerProductPortfolio(configuration, portfolioDashboardParams)
  opportunities = portletUtils.getSummaryOpportunities(api.local.REVENUE_BELOW_TARGET_PER_PRODUCT_ATTRIBUTE)
}
List tableData = setSummaryDataToTable(customerInfo, opportunities)

return portletUtils.createControllerWithHtmlTable(configuration, tableData)

/**
 * collect item into a list to build summary data table
 * @param inputCustomerInfo
 * @param inputOpportunities
 * @return
 */
protected List setSummaryDataToTable(Map inputCustomerInfo, Map inputOpportunities) {
  def portletUtils = libs.CustomerInsights.SummaryPortletUtils
  Map summaryLabels = libs.CustomerInsights.Constant.SUMMARY_PORTLET_LABEL
  //general section
  List tableData = []
  customerInfo = inputCustomerInfo ?: [:]
  opportunities = inputOpportunities ?: [:]
  tableData.add(portletUtils.initTextItem(summaryLabels.CUSTOMER.SINGULAR, portletUtils.makeSingleCustomerLabel(customerInfo.customerId, customerInfo.customerName)))
  tableData.add(portletUtils.initTextItem(summaryLabels.CUSTOMER_SEGMENT, customerInfo.customerSegment))
  tableData.add(portletUtils.initTextItem("", ""))
  tableData.add(portletUtils.initHealthScoreItem(summaryLabels.HEALTH_SCORE, customerInfo.customerHealthScore))
  //column-01
  tableData.add(portletUtils.initTrendItem(summaryLabels.REVENUE_TREND_YTD, customerInfo.customerYTDRevenueTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.MARGIN_TREND_YTD, customerInfo.customerYTDMarginTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.QUANTITY_TREND_YTD, customerInfo.customerYTDQuantityTrend))
  //column-02
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_REVENUE, customerInfo.revenue))
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_MARGIN, customerInfo.margin))
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_QUANTITY, customerInfo.quantity))
  //column-03
  tableData.add(portletUtils.initNumberItem(summaryLabels.PRICING_OPPORTUNITY, opportunities.pricingOpportunity))
  tableData.add(portletUtils.initNumberItem(summaryLabels.SELLING_OPPORTUNITY, opportunities.sellingOpportunity))
  tableData.add(portletUtils.initNumberItem(summaryLabels.OPPORTUNITY, opportunities.opportunity))

  return tableData
}