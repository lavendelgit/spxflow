import net.pricefx.server.dto.calculation.ResultHighchart

ResultHighchart buildRevenueBelowTargetPerTopProductsChart(List chartData, String currency, Map titles) {
  List category = chartData?.category
  List revenueBelowTargetData = chartData?.revenueBelowTarget
  List revenueBelowTargetPercentData = chartData?.revenueBelowTargetPercent

  def hLib = libs.HighchartsLibrary
  Map revenueBelowTargetByProductChart = hLib.Chart.newChart()
      .setTitle(titles.chartTitle)
  Map revenueBelowTargetAxis = hLib.Axis.newAxis().setTitle(titles.axisYTitle)
  Map cumulativeRevenueBelowTargetPercentAxis = hLib.Axis.newAxis()
      .setTitle(titles.axisZTitle)
      .setOpposite(true)
      .setLabels([format: "{value} %"])
      .setMin(0)
      .setMax(100)
      .setAlignTicks(false)
      .setGridLineWidth(0)
  Map axisX = hLib.Axis.newAxis().setCategories(category)

  Map columnTooltip = hLib.Tooltip.newTooltip()
      .setHeaderFormat("<span style=\"font-size:10px\"><b>{point.key}</b></span><br>")
      .setPointFormat("{series.name}: {point.y:#,###.0f} ${currency}<br>")
      .setUseHTML(true)
      .setShared(true)
  Map seriesCol = hLib.ColumnSeries.newColumnSeries()
      .setName(titles.columnName)
      .setYAxis(revenueBelowTargetAxis)
      .setXAxis(axisX)
      .setData(revenueBelowTargetData)
      .setTooltip(columnTooltip)

  Map lineTooltip = hLib.Tooltip.newTooltip()
      .setHeaderFormat("<span style=\"font-size:10px\"><b>{point.key}</b></span><table>")
      .setPointFormat("{series.name}: {point.y:#,###.2f} %")
      .setUseHTML(true)
      .setShared(true)

  Map tooltip = hLib.Tooltip.newTooltip()
      .setShared(true)
  Map marker = hLib.Marker.newMarker().setEnabled(false)
  Map seriesLine = hLib.SplineSeries.newSplineSeries()
      .setName(titles.lineName)
      .setYAxis(cumulativeRevenueBelowTargetPercentAxis)
      .setXAxis(axisX)
      .setData(revenueBelowTargetPercentData)
      .setTooltip(lineTooltip)
      .setMarker(marker)

  revenueBelowTargetByProductChart.setSeries(seriesCol, seriesLine)
      .setZoomType(hLib.ConstConfig.ZOOM_TYPES.XY)
      .setTooltip(tooltip)
      .setEnableMouseWheelZoomMapNavigation(false)

  return api.buildHighchart(revenueBelowTargetByProductChart.getHighchartFormatDefinition())
}

List createRevenueBelowTargetPerTopProductsData(List productsOfChart) {
  List chartData = []
  for (item in productsOfChart) {
    chartData.add([category                 : (item.productId ?: item.productAttribute),
                   revenueBelowTarget       : item.revenueBelowTarget,
                   revenueBelowTargetPercent: ((item.cumulativeRevenueBelowTargetPercent ?: 0.0) as BigDecimal) * 100])
  }

  return chartData
}

List createRevenueBelowTargetPerAttributeProductData(List productsOfChart) {
  List chartData = []
  for (item in productsOfChart) {
    chartData.add([category                 : item.productAttribute,
                   revenueBelowTarget       : item.revenueBelowTarget,
                   revenueBelowTargetPercent: ((item.revenueBelowTargetPercent ?: 0.0) as BigDecimal) * 100])
  }

  return chartData
}

List createRevenueBelowTargetPerProductTypeData(List productTypeData) {
  List chartData = []
  for (item in productTypeData) {
    chartData.add([name                     : item.productClassification,
                   y                        : item.revenueBelowTarget,
                   revenueBelowTargetPercent: ((item.revenueBelowTargetPercent ?: 0) as BigDecimal) * 100])
  }

  return chartData
}

Map buildChartTitles(String chartTitle, String axisYTitle, String axisZTitle, String columnName, String lineName) {
  return [chartTitle: chartTitle,
          axisYTitle: axisYTitle,
          axisZTitle: axisZTitle,
          columnName: columnName,
          lineName  : lineName]
}