Map processedData = api.local.TOP_AND_WORST_REVENUE_BELOW_TARGET_PER_PRODUCT
Map currency = api.getElement("Currency")
List chartData = DashboardUtils.createRevenueBelowTargetPerTopProductsData(processedData?.worst)
String chartTitle = "Pricing Opportunity By Worst Products"
String axisYTitle = "Revenue Below Target"
String axisZTitle = "Cumulative Revenue Below Target (%)"
String columnName = "Revenue Below Target"
String lineName = "Cumulative Revenue Below Target"
Map titles = DashboardUtils.buildChartTitles(chartTitle, axisYTitle, axisZTitle, columnName, lineName)

return DashboardUtils.buildRevenueBelowTargetPerTopProductsChart(chartData, currency.currencySymbol, titles)