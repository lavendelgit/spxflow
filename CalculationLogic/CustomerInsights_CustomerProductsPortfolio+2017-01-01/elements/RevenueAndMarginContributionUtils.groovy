import groovy.transform.Field

@Field Map REVENUE_AND_MARGIN = [Bucket_Default: [namePrefix  : "Bucket",
                                                  labelSuffix : "%",
                                                  startPercent: 0,
                                                  endPercent  : 100,
                                                  noOfBuckets : 10],
                                 isBar         : false,
                                 series        : ["(seriesRevenue, seriesMargin)"],
                                 tooltip       : [headerFormat: "<span style='font-size:10px'><b>{point.key}</b></span><br><table>",
                                                  footerFormat: "</table>",
                                                  useHTML     : true],
                                 plotOptions   : [column: [borderWidth: 0,
                                                           dataLabels : [borderWidth: 0,
                                                                         color      : "BLACK",
                                                                         enabled    : true,
                                                                         format     : "{point.y:,.0f}",
                                                                         style      : [textOutline: 0],
                                                                         crop       : false,
                                                                         overflow   : "none"]]],
                                 constantTop   : 10,
                                 type          : [revenue: "REVENUE",
                                                  margin : "MARGIN"],
                                 tooltipLabels : [revenue: "Revenue",
                                                  margin : "Margin"],
                                 drilldownType : [customer: [singular: "Customer",
                                                             plural  : "Customers"],
                                                  product : [singular: "Product",
                                                             plural  : "Products"]]]

REVENUE_AND_MARGIN.currency = api.getElement("Currency")
@Field String CATEGORY_NAME = "productName"
@Field String CATEGORY_ID = "productId"

List getBuckets(Map configuration) {
  Integer numberOfBuckets = (configuration.REVENUE_AND_MARGIN_CONTRIBUTION.NUMBER_OF_BUCKETS?.getAt(0)?.value ?: REVENUE_AND_MARGIN.Bucket_Default.noOfBuckets) as Integer
  BigDecimal bucketStartPercent = (configuration.REVENUE_AND_MARGIN_CONTRIBUTION.BUCKET_START_PERCENT?.getAt(0)?.value ?: REVENUE_AND_MARGIN.Bucket_Default.startPercent) as BigDecimal
  BigDecimal bucketEndPercent = (configuration.REVENUE_AND_MARGIN_CONTRIBUTION.BUCKET_END_PERCENT?.getAt(0)?.value ?: REVENUE_AND_MARGIN.Bucket_Default.endPercent) as BigDecimal
  BigDecimal bucketSize = (bucketEndPercent - bucketStartPercent) / numberOfBuckets
  List buckets = []
  Map bucket
  for (int i in 1..numberOfBuckets) {
    bucket = generateBucket(i, bucketStartPercent, bucketEndPercent, bucketSize, numberOfBuckets, REVENUE_AND_MARGIN.Bucket_Default)
    buckets.add(bucket)
  }

  return buckets
}

protected Map generateBucket(int bucketIndex,
                             BigDecimal bucketStart,
                             BigDecimal bucketEnd,
                             BigDecimal bucketSize,
                             int numberOfBuckets,
                             Map bucketConfiguration) {
  BigDecimal bucketValue = bucketIndex == numberOfBuckets ? bucketEnd : bucketStart + bucketIndex * bucketSize
  String bucketLabel = "${libs.SharedLib.RoundingUtils.round(bucketValue, 2)}${bucketConfiguration.labelSuffix}"

  return [name : bucketConfiguration.namePrefix + bucketIndex,
          label: "${bucketLabel}",
          value: bucketValue / 100]
}
/**
 *
 * @param configuration
 * @param rawData
 * @return
 */
Map createRevenueAndMarginContributionChartData(Map configuration, List rawData) {
  Map attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  Map chartData = [bucketRevenueData  : [],
                   bucketMarginData   : [],
                   drilldownSeriesData: []]
  if (!rawData) {
    return chartData
  }

  List marginContributionRanking = rawData.sort(false, { a, b -> (b.margin as BigDecimal) <=> (a.margin as BigDecimal) })
  List revenueContributionRanking = rawData.sort(false, { a, b -> (b.revenue as BigDecimal) <=> (a.revenue as BigDecimal) })
  BigDecimal totalRevenue = revenueContributionRanking.sum { (it.revenue ?: BigDecimal.ZERO) }
  BigDecimal totalMargin = marginContributionRanking.sum { (it.margin ?: BigDecimal.ZERO) }
  List buckets = getBuckets(configuration)
  Map drilldownLabelType = REVENUE_AND_MARGIN.drilldownType.product
  String categoryName = CATEGORY_NAME
  String categoryId = CATEGORY_ID
  int preMarginIdx = 0
  int preRevenueIdx = 0
  Map revenueBucket, marginBucket
  String preBucketLabel

  for (bucket in buckets) {
    revenueBucket = prepareBucketData(attributeTypeCode.REVENUE, buckets, bucket, revenueContributionRanking, totalRevenue, preRevenueIdx, preBucketLabel, drilldownLabelType, categoryName, categoryId)
    chartData.bucketRevenueData.add(revenueBucket.bucketData)
    chartData.drilldownSeriesData.add(revenueBucket.dataForRankingGroup)

    marginBucket = prepareBucketData(attributeTypeCode.MARGIN, buckets, bucket, marginContributionRanking, totalMargin, preMarginIdx, preBucketLabel, drilldownLabelType, categoryName, categoryId)
    chartData.bucketMarginData.add(marginBucket.bucketData)
    chartData.drilldownSeriesData.add(marginBucket.dataForRankingGroup)

    preRevenueIdx = revenueBucket.preIndex
    preMarginIdx = marginBucket.preIndex
    preBucketLabel = bucket.label
  }

  return chartData
}

protected Map prepareBucketData(String type, List buckets, Map bucket, List contributionRanking, BigDecimal totalValue, int preIdx, String preBucketLabel, Map drilldownLabelType, String categoryName, String categoryId) {
  Map attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  String bucketName = bucket?.name
  BigDecimal representingValue = (bucket?.value as BigDecimal) * totalValue
  String fullBucketName = type + bucketName
  Map rankingListByPercent

  if (attributeTypeCode.REVENUE.equalsIgnoreCase(type)) {
    rankingListByPercent = getRankingByPercentValue(contributionRanking.revenue, representingValue)
  } else if (attributeTypeCode.MARGIN.equalsIgnoreCase(type)) {
    rankingListByPercent = getRankingByPercentValue(contributionRanking.margin, representingValue)
  }

  Map bucketRevenueData = generateBucketRowData(rankingListByPercent, representingValue, fullBucketName, buckets, bucket)
  Map dataForRankingGroup = generateDataForDrilldownColumnChart(type, contributionRanking, preIdx, rankingListByPercent.size, preBucketLabel, bucketName, drilldownLabelType, categoryName, categoryId)

  return [dataForRankingGroup: dataForRankingGroup,
          bucketData         : bucketRevenueData,
          preIndex           : rankingListByPercent.size]
}

protected Map getRankingByPercentValue(List ranking, BigDecimal percentValue) {
  Map result = [:]
  BigDecimal totalValue = ranking.sum { (it ?: 0) }
  int size = ranking.size()
  if (percentValue == totalValue) {
    return addSizeAndValue(result, size, totalValue)
  }
  BigDecimal cumulativeValue = 0
  int idx = 0
  while (cumulativeValue < percentValue && idx < size) {
    cumulativeValue += ((ranking.getAt(idx) ?: 0) as BigDecimal)
    idx++
  }

  return addSizeAndValue(result, idx, cumulativeValue)
}

protected Map addSizeAndValue(Map result, int size, BigDecimal value) {
  result << [size: size]
  result << [value: value]
  return result
}

protected Map generateBucketRowData(Map rankingListByPercent, BigDecimal representingValue, String fullBucketName, List buckets, Map bucket) {
  Map row = [name        : bucket.label,
             y           : rankingListByPercent?.size,
             total       : rankingListByPercent?.value,
             representing: representingValue]

  if (buckets?.last() != bucket) {
    row << [drilldown: fullBucketName]
  }

  return row
}

protected Map generateDataForDrilldownColumnChart(String type,
                                                  List contributionRanking,
                                                  int fromIndex,
                                                  int toIndex,
                                                  String preBucketLabel,
                                                  String currentBucketName,
                                                  Map drilldownLabelType,
                                                  String categoryName,
                                                  String categoryId) {
  Map dataForRankingGroup = [:]
  dataForRankingGroup?.name = REVENUE_AND_MARGIN.tooltipLabels.getAt(type.toLowerCase())
  dataForRankingGroup?.id = type + currentBucketName
  dataForRankingGroup?.tooltip = [headerFormat: '',
                                  pointFormat : generateDrilldownPointFormat(type)]
  List rankingItemList = getRankingItemListByIndexForColumn(type,
      contributionRanking,
      fromIndex,
      toIndex,
      preBucketLabel,
      drilldownLabelType,
      categoryName,
      categoryId)

  dataForRankingGroup?.type = "column"
  dataForRankingGroup?.data = rankingItemList

  return dataForRankingGroup
}

protected String generateDrilldownPointFormat(String type) {
  String currencySymbol = REVENUE_AND_MARGIN.currency.currencySymbol
  String highlightSpan = '<span style="color:{point.color};font-weight: bold">'
  Map pointFormatMap = [begin  : '<b>{point.name}</b><br>',
                        revenue: "Revenue: {point.revenue:,.0f} ${currencySymbol}<br>",
                        margin : "Margin: {point.margin:,.0f} ${currencySymbol}<br>Margin %: {point.marginPercent:,.2f} %"]

  return pointFormatMap.inject("") { result, entry ->
    result += (entry.key == type.toLowerCase()) ? highlightSpan + entry.value + "</span>" : entry.value
  }
}

protected List getRankingItemListByIndexForColumn(String type, List revenueRanking, int fromIndex, int toIndex, String preBucketLabel, Map drilldownLabelType, String categoryName, String categoryId) {
  List itemList = []
  int constantTop = 10
  int top10Index = (toIndex - fromIndex) > constantTop ? constantTop : (toIndex - fromIndex)
  String item
  if (fromIndex > 0) {
    String drilldownLabel = fromIndex == 1 ? drilldownLabelType.singular : drilldownLabelType.plural
    item = preBucketLabel + " (${fromIndex} ${drilldownLabel})"
    itemList.add(prepareItem(type, 0, fromIndex, revenueRanking, item))
  }
  for (int i = fromIndex; i < (fromIndex + top10Index); i++) {
    itemList.add(prepareTop10Item(type, revenueRanking.getAt(i), categoryId, categoryName))
  }
  if (top10Index == constantTop && (toIndex - fromIndex) > constantTop) {
    item = "Others (${(toIndex - fromIndex - constantTop)})"
    itemList.add(prepareItem(type, fromIndex + top10Index, toIndex, revenueRanking, item))
  }

  return itemList
}

protected Map prepareTop10Item(String type, Map revenueRanking, String categoryId, String categoryName) {
  String customer = revenueRanking.getAt(categoryId) + " <br> " + revenueRanking.getAt(categoryName)
  BigDecimal value
  Map attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  if (attributeTypeCode.REVENUE.equalsIgnoreCase(type)) {
    value = revenueRanking?.revenue
  } else if (attributeTypeCode.MARGIN.equalsIgnoreCase(type)) {
    value = revenueRanking?.margin
  }

  return [name         : customer,
          y            : value,
          displayData  : value,
          revenue      : revenueRanking?.revenue,
          margin       : revenueRanking?.margin,
          marginPercent: 100 * (revenueRanking?.marginPercent ?: 0)]
}

protected Map prepareItem(String type, int fromIndex, int toIndex, List revenueRanking, String customerName) {
  BigDecimal otherRevenue = BigDecimal.ZERO
  BigDecimal otherMargin = BigDecimal.ZERO
  BigDecimal otherMarginPercent = BigDecimal.ZERO
  for (int i = fromIndex; i < toIndex; i++) {
    otherRevenue += ((revenueRanking?.getAt(i)?.revenue ?: 0) as BigDecimal)
    otherMargin += ((revenueRanking?.getAt(i)?.margin ?: 0) as BigDecimal)
  }
  if (otherRevenue) {
    otherMarginPercent = 100 * (otherMargin / otherRevenue)
  }
  Map attributeTypeCode = libs.CustomerInsights.Constant.ATTRIBUTE_TYPE_CODE
  BigDecimal value
  if (attributeTypeCode.REVENUE.equalsIgnoreCase(type)) {
    value = otherRevenue
  } else if (attributeTypeCode.MARGIN.equalsIgnoreCase(type)) {
    value = otherMargin
  }

  return [name         : customerName,
          y            : value,
          displayData  : value,
          revenue      : otherRevenue,
          margin       : otherMargin,
          marginPercent: otherMarginPercent]
}