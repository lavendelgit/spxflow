Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
List revenueBelowTargetPerProductTypes = api.local.REVENUE_BELOW_TARGET_PER_PRODUCT_TYPE
def dateUtils = libs.CustomerInsights.DateUtils
String startDateString = dateUtils.formatDateToString(portfolioDashboardParams.getStartDate())
String endDateString = dateUtils.formatDateToString(portfolioDashboardParams.getEndDate())
String currencySymbol = api.getElement("Currency").currencySymbol
List chartData = DashboardUtils.createRevenueBelowTargetPerProductTypeData(revenueBelowTargetPerProductTypes)

def hLib = libs.HighchartsLibrary
Map legend = hLib.Legend.newLegend().setEnabled(false)

Map chartSeriesDataLabels = hLib.DataLabel.newDataLabel().setEnabled(true)
    .setFormat("<b>{point.name}: {point.y:,.0f} ${currencySymbol}</b>")

Map chartSeriesTooltip = hLib.Tooltip.newTooltip()
    .setUseHTML(true)
    .setHeaderFormat("")
    .setPointFormat("<b>{point.name}</b><br>Revenue Below Target: {point.y:,.0f} ${currencySymbol}<br>Revenue Below Target %: {point.revenueBelowTargetPercent:,.2f} %")

Map chartSeries = hLib.PieSeries.newPieSeries().setName("Customer Count")
    .setData(chartData)
    .setTooltip(chartSeriesTooltip)
    .setDataLabels(chartSeriesDataLabels)

Map chartDef = hLib.Chart.newChart().setTitle("Pricing Opportunity By Product Type")
    .setSubtitle("From ${startDateString} To ${endDateString}")
    .setSeries(chartSeries)
    .setLegend(legend)
    .getHighchartFormatDefinition()

return api.buildHighchart(chartDef)