/*
  using api.local to pass value to other element -> improve performance
 */
Boolean needToProcess = api.getElement("NeedToProcess")
if (!needToProcess) {
  return null
}

List revenueBelowTargetPerProductType = createRevenueBelowTargetPerProductTypeData(api.local.REVENUE_BELOW_TARGET_PER_PRODUCT as List)
api.local.REVENUE_BELOW_TARGET_PER_PRODUCT_TYPE = revenueBelowTargetPerProductType

return null

protected List createRevenueBelowTargetPerProductTypeData(List revenueBelowTargetData) {
  if (!revenueBelowTargetData) {
    return []
  }
  Map revenueBelowTargetDataGroupedByProductType = revenueBelowTargetData.groupBy {
    it.productClassificationByMarginPercent
  }
  BigDecimal totalRevenueBelowTarget = revenueBelowTargetData.revenueBelowTarget?.sum()
  List revenueBelowTargetPerProductType = []
  Map item
  for (revenueBelowTargetDataItem in revenueBelowTargetDataGroupedByProductType) {
    item = [:]
    item.productClassification = (revenueBelowTargetDataItem.key ?: libs.CustomerInsights.Constant.DEFAULT_VALUE.UNKNOWN)
    item.revenueBelowTarget = revenueBelowTargetDataItem.value?.revenueBelowTarget?.sum()
    item.revenueBelowTargetPercent = totalRevenueBelowTarget ? (item.revenueBelowTarget / totalRevenueBelowTarget) : BigDecimal.ZERO
    revenueBelowTargetPerProductType.add(item)
  }

  return revenueBelowTargetPerProductType
}