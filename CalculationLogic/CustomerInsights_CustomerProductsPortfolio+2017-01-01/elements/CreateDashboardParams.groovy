def customer = api.getElement("Customer")
def timeFilter = api.getElement("TimeFilter")
Integer topProduct = api.getElement("TopProducts") as Integer //must cast
Map currentPeriod = api.getElement("CurrentPeriod")
Map currency = api.getElement("Currency")
String productAttribute = api.getElement("ProductAttribute")
List productClassByQuantities = api.getElement("ProductClassificationByQuantityFilter")
List productClassByHealthScores = api.getElement("ProductClassificationByHealthScoreFilter")
Date currentDate = new Date()

return libs.CustomerInsights.PortfolioDashboardParameter.newPortfolioDashboardParam(customer,
    productAttribute,
    topProduct,
    timeFilter,
    currency,
    currentPeriod,
    currentDate,
    productClassByQuantities,
    productClassByHealthScores)