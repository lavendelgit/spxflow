Map configuration = api.getElement("Configuration")
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")

if (!needToProcess) {
  return null
}

List revenueBelowTargetData = libs.CustomerInsights.CustomerProductPortfolioDashboardUtils.getRevenueBelowTargetPerProduct(configuration, portfolioDashboardParams)
api.local.REVENUE_BELOW_TARGET_PER_PRODUCT = revenueBelowTargetData
Integer topProduct = portfolioDashboardParams.getTopProduct()
api.local.TOP_AND_WORST_REVENUE_BELOW_TARGET_PER_PRODUCT = libs.CustomerInsights.DataTable.getTopAndWorst(revenueBelowTargetData, topProduct)

return null