Map configuration = api.getElement("Configuration")
Map portfolioDashboardParams = api.getElement("CreateDashboardParams")
Boolean needToProcess = api.getElement("NeedToProcess")

if (!needToProcess) {
  return null
}

List rawData = libs.CustomerInsights.CustomerProductPortfolioDashboardUtils.getDataForRevenueAndMarginPerProduct(configuration, portfolioDashboardParams)

return RevenueAndMarginContributionUtils.createRevenueAndMarginContributionChartData(configuration, rawData)