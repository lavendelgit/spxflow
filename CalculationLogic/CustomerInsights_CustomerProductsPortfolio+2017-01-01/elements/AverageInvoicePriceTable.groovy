Map configuration = api.getElement("Configuration")
Map customerClassificationByRevenueRange = configuration.CUSTOMER_CLASSIFICATION_REVENUE
Map processedData = api.local.TOP_AND_WORST_AVG_INVOICE_PRICE_BY_CUSTOMER_CLASS
List classes = customerClassificationByRevenueRange?.keySet()?.collect()
Map metaData = initMetaData(classes, processedData?.top, processedData?.worst)

return libs.CustomerInsights.DataTable.generateDataTable(metaData)

protected Map initMetaData(List classes, List topData, List worstData) {
  def dataTable = libs.CustomerInsights.DataTable

  return dataTable.newTableMetaData()
      .setTableStructure(createTableStructure(classes))
      .setTablePreferenceName("AverageInvoicePrice")
      .setData(dataTable.buildTopWorstData(topData, worstData))
      .getTableMetaData()
}

protected List createTableStructure(List classes) {
  def dataTable = libs.CustomerInsights.DataTable
  def constant = libs.CustomerInsights.Constant
  Map columnLabels = constant.AVERAGE_INVOICE_PRICE_TABLE_COLUMN_LABELS
  Map attributeName = constant.ENTITY_ATTRIBUTE_NAME
  List tableStructure = []
  tableStructure.add(dataTable.createProductNameColumnConfiguration(true))
  tableStructure.add(dataTable.createProductIdColumnConfiguration())

  tableStructure.add(dataTable.createMoneyColumnConfiguration(columnLabels.CUSTOMER_REVENUE_CLASS_A, classes.getAt(0)))
  tableStructure.add(dataTable.createMoneyColumnConfiguration(columnLabels.CUSTOMER_REVENUE_CLASS_B, classes.getAt(1)))
  tableStructure.add(dataTable.createMoneyColumnConfiguration(columnLabels.CUSTOMER_REVENUE_CLASS_C, classes.getAt(2)))
  tableStructure.add(dataTable.createMoneyColumnConfiguration(columnLabels.CUSTOMER_REVENUE_CLASS_D, classes.getAt(3)))
  tableStructure.add(dataTable.createMoneyColumnConfiguration(columnLabels.OVERALL, attributeName.OVERALL_PRICE))
  tableStructure.add(dataTable.createMoneyColumnConfiguration(columnLabels.PRICE_OF_CUSTOMER, attributeName.AVERAGE_PRICE_BY_CUSTOMER))

  return tableStructure
}