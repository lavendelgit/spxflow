String timeFilter = api.getElement("TimeFilter")
Map period = libs.CustomerInsights.DateUtils.getPeriod(new Date(), timeFilter)

return [startDate: period.startDate,
        endDate  : period.endDate]