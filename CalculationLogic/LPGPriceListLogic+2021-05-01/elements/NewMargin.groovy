if (out.OverrideOption == "Margin (%)" && out.OverrideOptionValue) {
    manualOverrideValue = out.OverrideOptionValue as BigDecimal
    return manualOverrideValue / 100
}

if (out.NewListPriceUSD && out.StandardCostInUSD) {
    api.local.priceBuildupMatrix.MarginStringFormula = "((New List Price USD - Standard Cost USD)/New List Price USD"
    api.local.priceBuildupMatrix.MarginStringValue = "((" + out.NewListPriceUSD + "-" + out.StandardCostInUSD + ")/" + out.NewListPriceUSD + ")"
    return (((out.NewListPriceUSD - out.StandardCostInUSD) / out.NewListPriceUSD as BigDecimal))?.setScale(4, BigDecimal.ROUND_HALF_UP)
}
return
