transactionRecords = api.global.transactionRecords?.getAt(out.Material)?.groupBy { (it.SoldtoCountryfromTD) }

regionQuanitiy = [:]

chartData = [
        region       : [],
        totalRevenue : [],
        totalQuantity: [],
        average      : [],
]
dummyTrx = [:]
String regionCode = ""
transactionRecords?.each { it ->
    if (it.key) {
        regionCode = api.global.regionMapping?.getAt(it.key)
        if (dummyTrx?.getAt(regionCode)) {
            it.value?.each { record ->
                dummyTrx[regionCode] << record
            }
        } else {
            dummyTrx[regionCode] = it.value
        }
    }
}
transactionRecords = dummyTrx
seriesData = []
chartData = []
avgSellingPrice = 0
mapData = [:]
transactionRecords?.each{region->
    seriesData = []
    region.value?.each{it->
        if(it.NetValueinUSD&&it.NetCostinUSD){
            mapData = [:]
            avgSellingPrice = (it.NetValueinUSD/(it.OrderQuantity?:1))
            mapData.x = it.NetValueinUSD
            mapData.y = (((it.NetValueinUSD-it.NetCostinUSD)/it.NetValueinUSD)*100)
            mapData.custom = [quantity : it.OrderQuantity,
                              avgSellingPrice:avgSellingPrice ,
                              customerInfo:it.ParentCust]
            seriesData << mapData
        }
    }
    chartData << [
            name : region.key,
            data : seriesData,
    ]
}
def definition = [
        chart      : [
                type    : 'scatter',
                zoomType: 'xy'
        ],
        title      : [
                text: 'Region Based Booking & Margin'
        ],
        subtitle   : [
                text: ''
        ],
        xAxis      : [
                title        : [
                        text: 'Booking'
                ],
                showLastLabel: true
        ],
        yAxis      : [
                title: [
                        text: 'Margin (%)'

                ]
        ],
        plotOptions: [
                scatter: [
                        marker : [
                                radius: 5,
                                symbol: 'circle'
                        ],
                ],
        ],
        tooltip: [
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat : '<b>{point.custom.customerInfo}</b><br>'+'Booking : {point.x} USD <br>' +
                        'Margin : {point.y:.2f}% <br> '+'Quantity :  {point.custom.quantity} <br>'+'Average Selling Price :  {point.custom.avgSellingPrice:.2f} USD <br>'
        ],
        series     : chartData
]
return api.buildHighchart(definition)