BigDecimal listPrice = out.SAPCurrentPriceUSD
api.local.approvelStep = null
out.StandardCostInUSD ?: api.addWarning("Standard Cost is missing")
out.MaxMargin ?: api.addWarning("Max Margin is missing")
out.MinMargin ?: api.addWarning("Min Margin is missing")
if (!listPrice || !out.StandardCostInUSD) return


BigDecimal listPriceMargin = (((listPrice - out.StandardCostInUSD) / listPrice)) ?: 0.0
listPriceMargin = (listPriceMargin)?.setScale(4, BigDecimal.ROUND_HALF_UP)
api.local.priceBuildupMatrix.ActualMarginStringFormula = "((SAP Current Price USD - Standard Cost USD)/SAP Current Price USD)*100"
api.local.priceBuildupMatrix.ActualMarginValueFormula = "((" + listPrice + " - " + out.StandardCostInUSD + ")/" + listPrice + ")*100"
if (listPriceMargin >= (out.MinMargin) && listPriceMargin <= (out.MaxMargin)) {
    api.local.warningReason = "Actual Margin is Within Min/Max Margin Corridor"
            api.local.actualMarginColor = "green"
    return api.attributedResult(listPriceMargin).withTextColor("white").withBackgroundColor("green")
} else if (out.MinMargin && listPriceMargin < out.MinMargin) {
    api.local.actualMarginColor = "red"
    api.addWarning("Actual Margin is less than Min Margin : " + out.MinMargin * 100)
    api.local.approvelStep = "Below Min Margin"
    api.local.PriceReason = "MarginProofPrice"
    dfferenceMargin = ((out.MinMargin - listPriceMargin) *100)+" %"
    api.local.warningReason = "Actual Margin("+ listPriceMargin*100 +"% )is below Min Margin( " + out.MinMargin * 100 +"% )by $dfferenceMargin"
    api.local.marginProofPrice = out.StandardCostInUSD / (1-out.MinMargin)
    api.local.priceBuildupMatrix.MarginProofStringformula = "(Standard Cost USD/(1 - Min Margin)"
    api.local.priceBuildupMatrix.MarginProofValueformula = "("+out.StandardCostInUSD+" / ("+1+"-"+out.MinMargin+")"
    return api.attributedResult(listPriceMargin).withTextColor("white").withBackgroundColor("red")
} else if (out.MaxMargin && listPriceMargin > out.MaxMargin) {
    api.addWarning("Actual Margin is greater than Max Margin : " + (out.MaxMargin ? out.MaxMargin * 100 : 0))
    api.local.approvelStep = "Greater than Max Margin"
    api.local.actualMarginColor = "red"
    api.local.PriceReason = "MarginProofPrice"
    api.local.marginProofPrice = out.StandardCostInUSD / (1-out.MaxMargin)
    dfferenceMargin = ((listPriceMargin - out.MaxMargin) *100)+" %"
    api.local.warningReason = "Actual Margin("+ listPriceMargin*100 +"% )is exceeded Max Margin( " + out.MaxMargin * 100 +"% )by $dfferenceMargin"
    api.local.priceBuildupMatrix.MarginProofStringformula = "(Standard Cost USD/(1 - Max Margin)"
    api.local.priceBuildupMatrix.MarginProofValueformula = "("+out.StandardCostInUSD+" / ("+1+"-"+out.MaxMargin+")"
    return api.attributedResult(listPriceMargin).withTextColor("white").withBackgroundColor("red")
} else if (out.MinMargin && listPriceMargin > out.MinMargin) {
    api.local.warningReason = "Actual Margin is above Min Margin"
    api.local.actualMarginColor = "green"
    return api.attributedResult(listPriceMargin).withTextColor("white").withBackgroundColor("green")
} else if (out.MaxMargin && listPriceMargin < out.MaxMargin) {
    api.local.warningReason = "Actual Margin is below Max Margin"
    api.local.actualMarginColor = "green"
    return api.attributedResult(listPriceMargin).withTextColor("white").withBackgroundColor("green")
}
return listPriceMargin