def transactionData = api.global.transactionRecords?.getAt(out.Material)?.groupBy { (it.CreateondateMonth) }
def categoryData = transactionData?.keySet()?.sort() //list of xAxis values

def seriesData = []
Map chartData = [
        totalCost : [],
        costAverage : [],
        totalRevenue :[],
        revenueAverage :[],
        totalQuantity :[],
        quantityAverage:[],
        discount : []
]
BigDecimal totalQuantity=0
categoryData?.each{month->
    totalQuantity = (transactionData?.getAt(month)?.OrderQuantity?.sum()) as BigDecimal
    chartData.totalQuantity << totalQuantity
    totalRevenue = transactionData?.getAt(month)?.NetValueinUSD?.sum()
    chartData.totalCost << transactionData?.getAt(month)?.NetCostinUSD?.sum()
    chartData.totalRevenue << transactionData?.getAt(month)?.NetValueinUSD?.sum()
    chartData.costAverage << (transactionData?.getAt(month)?.NetCostinUSD?.sum()/totalQuantity)
    chartData.revenueAverage << (transactionData?.getAt(month)?.NetValueinUSD?.sum()/totalQuantity)
    chartData.discount << (1-(totalRevenue/(totalQuantity * (out.SAPCurrentPriceUSD?:1))))*100
    chartData.quantityAverage << ((totalQuantity as BigDecimal)/(transactionData?.getAt(month)?.OrderQuantity?.size()))

}

 seriesData = [[
                          type: "column",
                          name: 'Average Selling Price',
                          data: chartData.revenueAverage,
                  ],[
                          type: "spline",
                          name: 'Average Discount',
                          yAxis: 1,
                          tooltip    : [
                                  pointFormat: '{point.y:.2f} % '
                          ],
                          data: chartData.discount
                  ]
                  ]

def definition = [
        chart      : [
                type: 'column'
        ],
        title      : [
                text: 'Total Averages- Trend'
        ],
        subtitle   : [
                text: ''
        ],
        xAxis      : [
                categories: categoryData
        ],
        yAxis      : [
                [// Primary yAxis
                 title: [
                         text: 'Average Selling Price',
                 ]
                ],
                [// Second yAxis
                 title   : [
                         text: 'Average Discount %',

                 ],
                 opposite: true
                ],

        ],
        plotOptions: [
                line: [
                        dataLabels         : [
                                enabled: true
                        ],
                        enableMouseTracking: false
                ]
        ],
        tooltip    : [
                pointFormat: '{point.y:.2f} USD '
        ],
        series     : seriesData
]

return api.buildHighchart(definition)