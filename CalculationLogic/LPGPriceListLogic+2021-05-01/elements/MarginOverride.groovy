if (out.OverrideOption == "Margin (%)" && out.OverrideOptionValue) {
    listPrice = out.ABPriceChange ?: out.SAPCurrentPriceUSD
    manualOverrideValue = (out.OverrideOptionValue as BigDecimal) / 100
    api.local.priceBuildupMatrix.OverrideMarginStringFormula = "(Standard Cost USD/(1-(Override Value/100)))"
    api.local.priceBuildupMatrix.OverrideMarginValueFormula = "(" + out.StandardCostInUSD + "/(1-(" + manualOverrideValue + "/100)))"
    return (out.StandardCostInUSD / (1 - manualOverrideValue)) ?: 0.0
}
return