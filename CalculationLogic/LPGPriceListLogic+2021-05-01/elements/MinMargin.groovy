if (out.StandardCostInUSD) {
    if (out.MarginValue?.attribute8) {
        return (out.MarginValue?.attribute8 as BigDecimal)?.setScale(4, BigDecimal.ROUND_HALF_UP)
    }
}
return