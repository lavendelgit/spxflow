def ArchetypeRevenue = ((out.ProductLineRevenue?:0.0) * 1000) ?.setScale(2, BigDecimal.ROUND_HALF_UP)

if (ArchetypeRevenue && out.VelocityAttributesValue) {
    value = "<"
    if (ArchetypeRevenue <= ((out.VelocityAttributesValue?.attribute1) * 100) && ((out.VelocityAttributesValue?.attribute2) * 100) >= ArchetypeRevenue) {
        api.local.priceBuildupMatrix.velocityBucketStringFormula = "(Product Line Revenue <=  Bucket 1 % )&&(Product Line Revenue >= Bucket 2 %)"
        api.local.priceBuildupMatrix.velocityBucketValueFormula = "(" + ArchetypeRevenue + " <= " + ((out.VelocityAttributesValue?.attribute1) * 100) + " && " + out.VelocityAttributesValue?.attribute2 * 100 + ">=" + ArchetypeRevenue + ")"
        return "Velocity Bucket1"
    } else if (ArchetypeRevenue <= ((out.VelocityAttributesValue?.attribute2) * 100) && ((out.VelocityAttributesValue?.attribute3) * 100) >= ArchetypeRevenue) {
        api.local.priceBuildupMatrix.velocityBucketStringFormula = "(Product Line Revenue <= Bucket 2 % )&&(Product Line Revenue >= Bucket 3 %)"
        api.local.priceBuildupMatrix.velocityBucketValueFormula = "(" + ArchetypeRevenue + " <= " + ((out.VelocityAttributesValue?.attribute2) * 100) + " && " + out.VelocityAttributesValue?.attribute3 * 100 + ">=" + ArchetypeRevenue + ")"
        return "Velocity Bucket2"
    } else if (ArchetypeRevenue <= ((out.VelocityAttributesValue?.attribute3) * 100) && ((out.VelocityAttributesValue?.attribute4) * 100) >= ArchetypeRevenue) {
        api.local.priceBuildupMatrix.velocityBucketStringFormula = "(Product Line Revenue <= Bucket 3 % )&&(Product Line Revenue >= Bucket 4 %)"
        api.local.priceBuildupMatrix.velocityBucketValueFormula = "(" + ArchetypeRevenue + " <= " + ((out.VelocityAttributesValue?.attribute3) * 100) + " && " + out.VelocityAttributesValue?.attribute4 * 100 + ">=" + ArchetypeRevenue + ")"
        return "Velocity Bucket3"
    } else if (ArchetypeRevenue <= ((out.VelocityAttributesValue?.attribute4) * 100) && ((out.VelocityAttributesValue?.attribute5) * 100) >= ArchetypeRevenue) {
        api.local.priceBuildupMatrix.velocityBucketStringFormula = "(Product Line Revenue <= Bucket 4 % )&&(Product Line Revenue >= Bucket 4 %)"
        api.local.priceBuildupMatrix.velocityBucketValueFormula = "(" + ArchetypeRevenue + " <= " + ((out.VelocityAttributesValue?.attribute4) * 100) + " && " + out.VelocityAttributesValue?.attribute5 * 100 + ">=" + ArchetypeRevenue + ")"
        return "Velocity Bucket4"
    } else if (ArchetypeRevenue <= ((out.VelocityAttributesValue?.attribute5) * 100) && ((out.VelocityAttributesValue?.attribute6) * 100) >= ArchetypeRevenue) {
        api.local.priceBuildupMatrix.velocityBucketStringFormula = "(Product Line Revenue <= Bucket 5 % )&&(Product Line Revenue >= Bucket 6 %)"
        api.local.priceBuildupMatrix.velocityBucketValueFormula = "(" + ArchetypeRevenue + " <= " + ((out.VelocityAttributesValue?.attribute5) * 100) + " && " + out.VelocityAttributesValue?.attribute6 * 100 + ">=" + ArchetypeRevenue + ")"
        return "Velocity Bucket5"
    }
       else if (ArchetypeRevenue >= ((out.VelocityAttributesValue?.attribute6) * 100)){
        api.local.priceBuildupMatrix.velocityBucketStringFormula = "(Product Line Revenue > Bucket 6 % )"
        api.local.priceBuildupMatrix.velocityBucketValueFormula = "(" + ArchetypeRevenue + ">" + (out.VelocityAttributesValue?.attribute6) * 100 + ")"
        return "Velocity Bucket6"
    }
}
return


