List velocityPriceAttributes = [out.Brand ?: "*", out.ProductLineInArchetype ?: "*", out.VelocityBuckets ?: "*"]
return Lib.getVelocityPrice(velocityPriceAttributes)