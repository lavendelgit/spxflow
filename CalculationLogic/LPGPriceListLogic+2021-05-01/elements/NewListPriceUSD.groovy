if (out.OverrideOption == "List Price (USD)" && out.OverrideOptionValue) {
    results = api.currentItem()?.allCalculationResults?.collectEntries { [(it.resultName): it.result] }
    api.local.currentItemList = [:]
    api.local.currentItemList.USDPrice = results?.NewListPriceUSD
    api.local.currentItemList.Margin = results?.NewMargin
    api.local.currentItemList.Price = results?.NewListPrice
    api.local.currentItemList.priceQ = results?.CalculatedNewListPrice
    api.local.currentItemList.PriceCurrency = results?.NewListPriceCurrency
    api.local.currentItemList.MarginStringFormula = "(( List Price USD - Standard Cost USD)/ List Price USD"
    api.local.currentItemList.MarginStringValue = "((" + api.local.currentItemList?.USDPrice + "-" + out.StandardCostInUSD + ")/" + api.local.currentItemList?.USDPrice + ")"
    api.local.PriceReason = "Manual Override of List Price"
    return out.OverrideOptionValue as BigDecimal
}

if (out.OverrideOption == "Margin (%)" && out.MarginOverride) {
    results = api.currentItem()?.allCalculationResults?.collectEntries { [(it.resultName): it.result] }
    api.local.currentItemList = [:]
    api.local.currentItemList.USDPrice = results?.NewListPriceUSD
    api.local.currentItemList.Margin = results?.NewMargin
    api.local.currentItemList.Price = results?.NewListPrice
    api.local.currentItemList.PriceCurrency = results?.NewListPriceCurrency
    api.local.currentItemList.MarginStringFormula = "(( List Price USD - Standard Cost USD)/ List Price USD"
    api.local.currentItemList.MarginStringValue = "((" + api.local.currentItemList?.USDPrice + "-" + out.StandardCostInUSD + ")/" + api.local.currentItemList?.USDPrice + ")"
    api.local.PriceReason = "Manual Override of margin"
    return out.MarginOverride as BigDecimal
}
return out.UpliftedPrice ?: out.SAPCurrentPriceUSD