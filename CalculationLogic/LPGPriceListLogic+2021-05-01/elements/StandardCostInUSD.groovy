def StandardcostUSD = api.global.batch?.getAt(out.Material)?.MaterialPlantMap?.StandardCostInUSD
return libs.SpxflowLib.RoundingUtils.roundNumber(StandardcostUSD, 2) ?: null