if (out.StandardCostInUSD) {
    if (out.MarginValue?.attribute9) {
        return (out.MarginValue?.attribute9 as BigDecimal)?.setScale(4, BigDecimal.ROUND_HALF_UP)
    }
}
return