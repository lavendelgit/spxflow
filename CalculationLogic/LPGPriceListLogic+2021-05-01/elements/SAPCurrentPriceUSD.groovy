def SAPPriceUSD = api.global.batch?.getAt(out.Material)?.MaterialSAPCurrentPrice?.SAPPriceUSD
return libs.SpxflowLib.RoundingUtils.roundNumber(SAPPriceUSD, 2) ?: null



