api.local.priceMatrixRegionalD = api.newMatrix("Country","Current Price-Currency","Applied Discount","Discounted Price","Sequence","Sequence Combination","Sequence Value","Calculation Source","Calculation Formula","Sequence Source","Discount Source")
Map attributeValues = [:]
List discountValue = []
discount = 0
List filter = []
flag = false
List priceListValues = api.global.discount?.attribute1?.findAll{it!="*"}?.unique()?.sort()

attributeValues.Pricelist = "*"
attributeValues.Country = "*"
attributeValues.MaterialGroup1 = out.MaterialGroup1?:"*"
attributeValues.Material = out.Material
attributeValues.ProductHierarchy = out.ProductHierarchy
attributeValues.Brand = out.Brand
attributeValues.ProductLine = out.ProductLine
attributeValues.ProductFamily = out.ProductFamily
attributeValues.ProductSeries = out.ProductSeries
findDiscount(attributeValues,priceListValues)
return api.local.flag?api.local.priceMatrixRegionalD:null

def findDiscount(Map attributeValues, List priceListValues){
    api.global.discount
    sequenceValueFormual = ""
    api.global.country?.each{country->
      
        discountValue = []
        attributeValues.Country = (api.global.countryCodeMapping?.getAt(country?.key))
        for(sequence in api.global.regionalSequence){
            sequenceValueFormual = ""
            priceListValues?.each { pricelist ->
                sequenceValueFormual = pricelist
                discountValue = api.global.discount?.findAll { it.attribute1 == pricelist}
                sequence?.each { attribute ->
                    if (attribute != "Pricelist" && !attribute?.contains("sequence")) {
                        discountValue = discountValue?.findAll { it.getAt(api.global.regionalAttributeMapping?.getAt(attribute)) == attributeValues?.getAt(attribute) }
                        sequenceValueFormual = sequenceValueFormual+"-"+attributeValues?.getAt(attribute)
                    }
                }
                if (discountValue && (discountValue?.getAt(0)?.attribute2==attributeValues.Country || discountValue?.getAt(0)?.attribute2=="*")) {
                    flag = true
                    discount = (discountValue?.getAt(0)?.attribute10 as BigDecimal)
                    exchangeRate = (api.global.exchangeRate?.getAt("USD-" + country?.value))
                    if (out.NewListPriceUSD && exchangeRate) {
                        price = ((out.NewListPriceUSD * exchangeRate ?: 0) as BigDecimal)?.setScale(2, BigDecimal.ROUND_HALF_UP)
                        if (price && discount) {
                            dicountedPrice = (price as BigDecimal) - Math.abs((((price as BigDecimal) / 100) * discount))
                            calculationValueFormula = "(" + out.NewListPriceUSD + "*" + exchangeRate + ") - (((" + out.NewListPriceUSD + "*" + exchangeRate + ")/100) * " + discount + "%)"
                            calculationStringFormula = "( NewListPriceUSD * ExchangeRate(USD-" + country?.value + ")) - (( NewListPriceUSD * ExchangeRate(USD-" + country?.value + ")/100) * Regional Discount %)"
                            discountPrice = libs.SpxflowLib.RoundingUtils.roundNumber(dicountedPrice, 2)
                            sequenceStringFormual = sequence?.findAll { !it.contains("sequence") }?.join('-')
                            api.local.priceMatrixRegionalD.addRow(country?.key,price+" "+country?.value, discount+"%", discountPrice + " " + country?.value, sequence?.find{it?.contains("sequence")}?.split(':')?.getAt(1),sequenceStringFormual,sequenceValueFormual, calculationStringFormula, calculationValueFormula, api.local.priceMatrixRegionalD?.linkCell("Regional Discount Sequence", "pricingParametersPage", ((api.findLookupTable("RegionalDiscountSequence")?.id) as String)), api.local.priceMatrixRegionalD.linkCell("RegionalDiscount PX", "productExtensionsPage", "RegionalDiscount"))
                            api.local.flag = true
                        }
                    }
                }
            }
            if(flag){
                break
            }
    }
    }
}