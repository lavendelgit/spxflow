api.local.pid = out.Material
def prodHier = out.ProductHierarchy
api.local.priceBuildupMatrix = [:]
api.local.PriceReason = ""
def dm
def ctx
if (!api.global.Plantname) {
    api.global.Plantname = [:]

    def PlantList = api.findLookupTableValues(libs.SpxflowLib.Constants.Plant_PP)
    api.global.Plantname = PlantList.collectEntries {
        [(it.name): it.attribute1]
    }
}

if (!api.global.Materialgroup1Desc) {
    api.global.Materialgroup1Desc = [:]

    def MaterialgroupDesc1 = api.findLookupTableValues(libs.SpxflowLib.Constants.MaterialGroup1_PP)
    api.global.Materialgroup1Desc = MaterialgroupDesc1.collectEntries {
        [(it.name): it.attribute1]
    }
}

if (!api.global.Ph1brand) {
    api.global.Ph1brand = [:]

    def PH1 = api.findLookupTableValues(libs.SpxflowLib.Constants.PH1Brand_PP)
    api.global.Ph1brand = PH1.collectEntries {
        [
                (it.name): it.attribute1
        ]
    }
}

if (!api.global.Ph2productLine) {
    api.global.Ph2productLine = [:]

    def PH2 = api.findLookupTableValues(libs.SpxflowLib.Constants.PH2ProductLine_PP)
    api.global.Ph2productLine = PH2.collectEntries {
        [
                (it.name): it.attribute1
        ]
    }
}

if (!api.global.Ph3productFamily) {
    api.global.Ph3productFamily = [:]

    def PH3 = api.findLookupTableValues(libs.SpxflowLib.Constants.PH3ProductFamily_PP)
    api.global.Ph3productFamily = PH3.collectEntries {
        [
                (it.name): it.attribute1
        ]
    }
}

if (!api.global.Ph4productSeries) {
    api.global.Ph4productSeries = [:]

    def PH4 = api.findLookupTableValues(libs.SpxflowLib.Constants.PH4ProductSeries_PP)
    api.global.Ph4productSeries = PH4.collectEntries {
        [
                (it.name): it.attribute1
        ]
    }
}

if (!api.global.Ph5modelSize) {
    api.global.Ph5modelSize = [:]

    def PH5 = api.findLookupTableValues(libs.SpxflowLib.Constants.PH5ModelSize_PP)
    api.global.Ph5modelSize = PH5.collectEntries {
        [
                (it.name): it.attribute1
        ]
    }
}

if (!api.global.pastMonth) {
    Date previousMonthPrice = api.targetDate()
    api.global.trallingMonth = ((api.findLookupTableValues("Configuration")?.find{it.name == "TTM-Month"}.attribute1) as BigDecimal)?:12
    trallingMonth = ((api.global.trallingMonth) * 30)
    api.global.pastMonth = previousMonthPrice?.minus(trallingMonth as Integer)
}

if (!api.global.batch) {
    api.global.batch = [:]
}
if (!api.global.batch[api.local.pid]) {
    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid] as Set
    MaterialPlantMap = libs.SpxflowLib.DataUtils.getPlantMapping(batch)
    Materialarchetype = libs.SpxflowLib.DataUtils.getMaterialArchetypeMapping(batch)
    MaterialSAPCurrentPrice = libs.SpxflowLib.DataUtils.getSAPCurrentPrice(batch)
    materialProductHierarchy = libs.SpxflowLib.DataUtils.getProductHierarchy(batch)
apctx = api.getDatamartContext()
    ds = apctx.getDataSource("ApprovedPrice")
    def approvedQuery =apctx?.newQuery(ds)
            ?.select('MaterialNumber','MaterialNumber')
            ?.select('approvalDate','approvalDate')
            ?.select('NewListPrice','NewListPrice')
            ?.select('NewListPriceCurrency','NewListPriceCurrency')
  			?.select('approvedByName','approvedByName')
            ?.select('ValidFrom','ValidFrom')
    api.global.approvedPrice = apctx?.executeQuery(approvedQuery)?.data?.toResultMatrix()?.getAt("entries")?.groupBy { it.MaterialNumber }

    api.global.regionMapping = api.findLookupTableValues("CountryRegionMapping")?.collectEntries { [(it.name): it.attribute1] }
    List dataSourceFilter = [
            Filter.in("MaterialNumber", batch),
            Filter.greaterOrEqual("Createondate", api.global.pastMonth),

    ]
    ctx = api.getDatamartContext()
    dm = ctx.getDatamart("Transaction")
    def query = ctx?.newQuery(dm)
            ?.select("SourcesystemID", "SourcesystemID")
            ?.select("DOCNumber", "DOCNumber")
            ?.select("SOItem", "SOItem")
            ?.select("Createondate", "Createondate")
            ?.select("MaterialNumber", "MaterialNumber")
            ?.select("NetCostinUSD","NetCostinUSD")
            ?.select("CreateondateMonth","CreateondateMonth")
            ?.select("SoldtoCountryfromTD", "SoldtoCountryfromTD")
            ?.select("ParentCust", "ParentCust")
            ?.select("NetValueinUSD", "NetValueinUSD")
            ?.select("OrderQuantity", "OrderQuantity")
            ?.where(*dataSourceFilter)

    api.global.transactionRecords = ctx?.executeQuery(query)?.data?.toResultMatrix()?.getAt("entries")?.groupBy { it.MaterialNumber }

    batch.each { sku ->
        api.global.batch[sku] = [
                "MaterialPlantMap"       : (MaterialPlantMap[sku] ?: null),
                "Materialarchetype"      : (Materialarchetype[sku] ?: null),
                "MaterialSAPCurrentPrice": (MaterialSAPCurrentPrice[sku] ?: null),
                "ProductHierarchy"       : (materialProductHierarchy[sku] ?: [:]),
                "MaterialNetValue"       : api.global.transactionRecords?.getAt(sku)?.getAt("NetValueinUSD")?.sum() ?: null]


    }
}

if (!api.global.ArchetypeNetvalue) {
    api.global.ArchetypeNetvalue = [:]
    List datamartFilter = [
            Filter.greaterOrEqual("Createondate", api.global.pastMonth),
    ]

    def query = ctx?.newQuery(dm)
            ?.select("ProductLine", "ProductLine")
            ?.select("SUM(NetValueinUSD)", "TotalProductLineNetValueinUSD")
            ?.where(*datamartFilter)
    api.global.ArchetypeNetvalue = ctx?.executeQuery(query)?.data?.collect { it }
}
if (!api.global.ArchetypeMargin) {
    api.global.ArchetypeMargin = [:]

    List filters = [
            Filter.equal("name", libs.SpxflowLib.Constants.ArchetypeMargins_PX),
    ]
    iterater = api.stream("PX10", null, *filters)
    api.global.ArchetypeMargin = iterater?.collect { it }
    iterater?.close()
    api.global.ArchetypeMargins = api.global.ArchetypeMargin?.groupBy { it.sku + "_" + it.attribute1 + "_" + it.attribute2 + "_" + it.attribute3 + "_" + it.attribute4 + "_" + it.attribute5 }
}

if (!api.global.categorisation) {
    api.global.categorisation = [:]

    def Category = api.findLookupTableValues(libs.SpxflowLib.Constants.SkuCategorisation_PP)
    api.global.categorisation = Category?.collectEntries {
        [
                (it.name): it.attribute1
        ]
    }
}

if (!api.global.rule2080) {
    api.global.rule2080 = [:]

    def rule = api.findLookupTableValues(libs.SpxflowLib.Constants.Rule8020_PP)
    api.global.rule2080 = rule?.collect { it }
    api.global.rules2080 = api.global.rule2080?.groupBy { it.key1 + "_" + it.key2 + "_" + it.key3 + "_" + it.key4 }


}

if (!api.global.Salesvelocitybucket) {
    api.global.Salesvelocitybucket = [:]

    def rule = api.findLookupTableValues(libs.SpxflowLib.Constants.SalesVelocityBucket_PP)
    api.global.Salesvelocitybucket = rule?.collect { it }
    api.global.Salesvelocitybuckets = api.global.Salesvelocitybucket?.groupBy { it.key1 + "_" + it.key2 }

}

if (!api.global.exchangeRate) {
    api.global.exchangeRate = [:]
    api.global.exchangeRate = api.findLookupTableValues("MonthlyExchangeRate", "key3")?.collectEntries {
        [(it.key1 + "-" + it.key2): it.attribute1]
    }
    api.global.currencyList = []
    api.global.currencyList = api.findLookupTableValues("CurrencySymbols")?.name
    api.global.country = []
    api.global.country = api.findLookupTableValues("Countries")?.collectEntries{[(it.name):it.attribute1]}
    api.global.countryCodeMapping = [:]
    api.global.countryCodeMapping = api.findLookupTableValues("ContinentAndCountryMapping")?.collectEntries{[(it.attribute1):it.name]}
}


if (!api.global.velocitybucketprice) {
    api.global.velocitybucketprice = [:]
    api.global.velocitybucketprice = api.findLookupTableValues("VelocityBucketPrice")?.groupBy { it.key1 + "_" + it.key2 + "_" + it.key3 }
}

if (!api.global.surcharge) {
    currentDate = api.targetDate()?.format("YYYY-MM-dd")
    api.global.surcharge = [:]
    api.global.surcharge = api.findLookupTableValues("RawMaterialCostSurcharge")?.findAll { it.attribute3 <= currentDate && it.attribute4 >= currentDate }?.groupBy { it.key1 + "_" + it.key2 }

}

if (!api.global.discounts) {
    api.global.regionalAttributeMapping = api.findLookupTableValues("RegionalDiscountAttributesSequence")?.collectEntries{[(it.name):it.value]}?.sort{it.value}
    data = api.findLookupTableValues("RegionalDiscountSequence")
    api.global.regionalSequence = []
    combination =[]
    excludeList = ["version","typedId","lastUpdateByName","createdByName","valueType","type","lookupTableTypedId","createDate","createdBy","lastUpdateDate","lastUpdateBy","id"]
    data?.each {it->
        combination = []
        keys = it.keySet()?.minus(excludeList)
        keys?.each{key->
            if(key=="name"){
                combination<<"sequence:"+it.getAt(key)
            }else{
                combination << it.getAt(key)
            }
        }
        api.global.regionalSequence << combination
    }
    List filter =[
            Filter.equal("name","RegionalDiscount"),
            Filter.lessOrEqual("attribute9",api.targetDate())
    ]
    api.global.discounts = [:]
    api.global.discount = api.stream("PX20",null,*filter)?.collect{it}
}


return