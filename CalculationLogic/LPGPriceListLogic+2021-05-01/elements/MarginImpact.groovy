if(out.ActualMargin && out.NewMargin && api.global.transactionRecords?.getAt(out.Material)){
    quantity = (api.global.transactionRecords?.getAt(out.Material)?.OrderQuantity?.sum()) as BigDecimal
    difference = (out.NewMargin - out.ActualMargin) as BigDecimal
    api.local.priceBuildupMatrix.marginImpactStringFormula = "(New Margin - Actual Margin)"
    api.local.priceBuildupMatrix.marginImpactValueFormula = "("+out.NewMargin+" - "+ out.ActualMargin+")"
    return (difference)
}
return