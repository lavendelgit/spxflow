Map getMargin(List productAttributes) {
    Map sortedCombinations = getSortedCombinations(productAttributes)
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        List margins = api.global.ArchetypeMargins?.getAt(key) as List
        if (margins) {
            return margins?.findAll { it.attribute6 <= out.StandardCostInUSD && it.attribute7 >= out.StandardCostInUSD }?.getAt(0)
        }
    }
}

List getSurcharge(List productAttributes) {
    Map sortedCombinations = getSortedCombinations(productAttributes)
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        margins = api.global.surcharge?.getAt(key)
        if (margins) {
            return margins
        }
    }
}

BigDecimal getDiscount(List productAttributes) {
    Map sortedCombinations = getSortedCombinations(productAttributes)
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        margins = api.global.discount?.getAt(key)
        if (margins) {
            return margins?.attribute10
        }
    }
}

Map getPercent(List productAttributes) {
    Map sortedCombinations = getSortedCombinations(productAttributes)
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        List percent = api.global.rules2080?.getAt(key) as List
        if (percent) {
            return percent?.findAll { it.attribute2 }?.getAt(0)
        }
    }

}

Map getVelocityPrice(List productAttributes) {
    Map sortedCombinations = getSortedCombinations(productAttributes)
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        List price = api.global.velocitybucketprice?.getAt(key) as List
        if (price) {
            return price?.findAll { it.attribute1 }?.getAt(0)
        }
    }

}

Map getVelocity(List productAttributes) {
    Map sortedCombinations = getSortedCombinations(productAttributes)
    String key
    for (combination in sortedCombinations) {
        key = frameKey(combination.value.getAt(0))
        List percent = api.global.Salesvelocitybuckets?.getAt(key) as List
        if (percent) {
            return percent?.findAll { it }?.getAt(0)
        }
    }

}

String frameKey(List keys) {
    String formattedKey
    keys.each {
        key ->
            formattedKey = formattedKey ? (formattedKey += "_" + key) : key
    }
    return formattedKey
}

Map getSortedCombinations(List productAttributes) {
    List combinations = []
    for (attributeValue in productAttributes) {
        combinations << [attributeValue, "*"]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ getWeightage(it, "*") })?.sort { -it.key }

}

Integer getWeightage(List keys, String defaultValue) {
    Integer weightage = 0
    Integer weightageIndex
    keys.eachWithIndex { key, index ->
        weightageIndex = index ? 2.power(index) : 1
        weightage += ((defaultValue.equals(key) ? 0 : weightageIndex))
    }
    return weightage
}

