if(out.SAPCurrentPriceUSD){
    convertedPrice = 0
    exchangeRate = 0
    convertedCountryPrice = [:]
    def pricePointsMatrix = api.newMatrix("Currency","Applied Exchange Rate","Converted Price")
    pricePointsMatrix.addRow("List Price in USD","-",out.NewListPriceUSD+" USD")
    (api.global.country?.values() as List)?.unique()?.each{currency->
        exchangeRate = api.global.exchangeRate?.getAt("USD-"+currency)
        if(exchangeRate){
            convertedPrice = libs.SpxflowLib.RoundingUtils.roundNumber((out.NewListPriceUSD * exchangeRate), 2)
            pricePointsMatrix.addRow("List Price in "+ currency,"USD - "+currency +"("+exchangeRate+")",convertedPrice+" "+ currency)
            convertedCountryPrice["USD - "+currency] = convertedPrice

        }
    }
    return pricePointsMatrix
}
