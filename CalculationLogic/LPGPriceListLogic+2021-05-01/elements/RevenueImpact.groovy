if(out.SAPCurrentPriceUSD && out.NewListPriceUSD && api.global.transactionRecords?.getAt(out.Material)){
    netValueSum = (api.global.transactionRecords?.getAt(out.Material)?.NetValueinUSD?.sum()) as BigDecimal
    difference = (((out.NewListPriceUSD - out.SAPCurrentPriceUSD)/out.SAPCurrentPriceUSD) * netValueSum) as BigDecimal
    api.local.priceBuildupMatrix.revenueImpactStringFormula = "((New List Price USD - Current List Price USD)/Current List Price USD)*(Trailing Months Revenue)"
    api.local.priceBuildupMatrix.revenueImpactValueFormula = "(("+out.NewListPriceUSD+" - "+ out.SAPCurrentPriceUSD+")/"+out.SAPCurrentPriceUSD+")*("+netValueSum+")"
    return difference.setScale(2,BigDecimal.ROUND_HALF_UP)
}
return
