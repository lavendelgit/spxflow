approvedPrice = api.global.approvedPrice?.getAt(out.Material)
if(approvedPrice){
    def matrix = api.newMatrix("Material Number","Result Price","Currency","Approved Date","Approved By","Valid From")
    approvedPrice?.each {it->
        matrix.addRow(it.MaterialNumber,(it.NewListPrice),it.NewListPriceCurrency,it.approvalDate,it.approvedByName,it.ValidFrom)
    }
    return matrix
}
return
