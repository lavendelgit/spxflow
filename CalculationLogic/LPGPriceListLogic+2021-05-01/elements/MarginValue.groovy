List productAttributes = [out.Material ?: "*", out.Brand ?: "*", out.ProductLineInArchetype ?: "*", out.ArcheType ?: "*", out.ArchetypeCategory ?: "*", out.ArchetypeValue ?: "*"]
return Lib.getMargin(productAttributes)
