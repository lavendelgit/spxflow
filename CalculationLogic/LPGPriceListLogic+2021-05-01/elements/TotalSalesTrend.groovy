def transactionData = api.global.transactionRecords?.getAt(out.Material)?.groupBy { (it.CreateondateMonth) }
def categoryData = transactionData?.keySet()?.sort() //list of xAxis values

def seriesData = []
Map chartData = [
        totalCost : [],
        costAverage : [],
        totalRevenue :[],
        revenueAverage :[],
        totalQuantity :[],
        quantityAverage:[]
]
BigDecimal totalQuantity=0
categoryData?.each{month->
    totalQuantity = (transactionData?.getAt(month)?.OrderQuantity?.sum()) as BigDecimal
    chartData.totalQuantity << totalQuantity
    chartData.totalCost << transactionData?.getAt(month)?.NetCostinUSD?.sum()
    chartData.totalRevenue << transactionData?.getAt(month)?.NetValueinUSD?.sum()
    chartData.costAverage << (transactionData?.getAt(month)?.NetCostinUSD?.sum()/totalQuantity)
    chartData.revenueAverage << (transactionData?.getAt(month)?.NetValueinUSD?.sum()/totalQuantity)
    chartData.quantityAverage << ((totalQuantity as BigDecimal)/(transactionData?.getAt(month)?.OrderQuantity?.size()))

}
api.local.MonthchartData = chartData
 seriesData = [ [
                          type: "column",
                          name: 'Total Bookings',
                          data: chartData.totalRevenue,
                       
                  ],
                  [
                          type: "spline",
                          name: 'Total Quantity',
                          yAxis: 1,
                          tooltip    : [
                                  pointFormat: '{point.y} '
                          ],
                          data: chartData.totalQuantity
                  ],
                  ]

def definition = [
        chart      : [
                type: 'column'
        ],
        title      : [
                text: 'Total Sales - Trend'
        ],
        subtitle   : [
                text: ''
        ],
        xAxis      : [
                categories: categoryData
        ],
        yAxis      : [
                [// Primary yAxis
                 title: [
                         text: 'Total Bookings',
                 ],
                 min: 0,
                ],
                [// Second yAxis
                 title   : [
                         text: 'Total Quantity',

                 ],
                 min: 0,
                 opposite: true
                ],

        ],
        plotOptions: [
                line: [
                        dataLabels         : [
                                enabled: true
                        ],
                        enableMouseTracking: false
                ]
        ],
        tooltip    : [
                pointFormat: '{point.y:.2f} USD '
        ],
        series     : seriesData
]

return api.buildHighchart(definition)