price = out.ABPriceChange ?: out.MarginProofPrice
priceValue = out.ABPriceChange ? "Category Price Change" : "Margin Proof Value"
def velocityPrice = out.VelocityPriceValue?.attribute1
if (velocityPrice && price) {
    def velocitybucketprice = (price + (price * velocityPrice))
    api.local.priceBuildupMatrix.velocityPriceMultipler = velocityPrice
    api.local.priceBuildupMatrix.velocityValueStringFormula = "(" + "$priceValue" + "+ (" + "Velocity Price Multiplier * $priceValue ))"
    api.local.priceBuildupMatrix.velocityValueValueFormula = "(" + price + "+ (" + velocityPrice + "*" + price + "))"
    api.local.PriceReason = api.local.PriceReason?api.local.PriceReason+"-VelocityBucketPrice":"VelocityBucketPrice"
    api.local.price = libs.SpxflowLib.RoundingUtils.roundNumber(velocitybucketprice, 2) ?: null
    return api.local.price
}
return