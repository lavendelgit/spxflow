transactionRecords = api.global.transactionRecords?.getAt(out.Material)?.groupBy { (it.SoldtoCountryfromTD) }
if(transactionRecords){
    regionQuanitiy = [:]

    chartData = [
            region       : [],
            totalRevenue : [],
            totalQuantity: [],
            average      : [],
            discount      : []
    ]
    dummyTrx = [:]
    String regionCode = ""
    transactionRecords?.each { it ->
        if (it.key) {
            regionCode = api.global.regionMapping?.getAt(it.key)
            if (dummyTrx?.getAt(regionCode)) {
                it.value?.each { record ->
                    dummyTrx[regionCode] << record
                }
            } else {
                dummyTrx[regionCode] = it.value
            }
        }
    }
    transactionRecords = dummyTrx
    transactionRecords?.each { it ->
        chartData.region << it.key
        totalRevenue = (it.value?.NetValueinUSD?.grep())?.sum()
        totalQuantity = (it.value?.OrderQuantity?.grep())?.sum()
        transactionSize = (it.value?.NetValueinUSD?.grep())?.size()
        chartData.totalRevenue << totalRevenue
        chartData.average << (totalRevenue / totalQuantity)
        chartData.discount << ((1-(totalRevenue/api.global.trallingMonth as BigDecimal))/(out.SAPCurrentPriceUSD?:1))*100
        chartData.totalQuantity << totalQuantity
    }
    api.local.chart = chartData
    def categoryData = chartData.region //list of xAxis values
    def seriesData = [[
                              type: "column",
                              name: 'Total Bookings', //name of the series
                              data: chartData.totalRevenue,
                      ],
                      [
                              type : "scatter",
                              name : 'Total Quantity',
                              yAxis: 1,
                              tooltip    : [
                                      pointFormat: '{point.y}'
                              ],
                              data : chartData.totalQuantity,
                              color: '#fa0000',

                      ],
    ]

    def definition = [
            chart      : [
                    type: 'column'
            ],
            title      : [
                    text: 'Total Sales'
            ],
            subtitle   : [
                    text: ''
            ],
            xAxis      : [
                    categories: categoryData
            ],
            yAxis      : [
                    [// Primary yAxis
                     title: [
                             text: 'Total Bookings',
                     ],
                     min: 0,
                    ],
                    [// Second yAxis
                     title   : [
                             text: 'Total Quantity',
                     ],
                     min: 0,
                     opposite: true
                    ],

            ],
            plotOptions: [
                    line: [
                            dataLabels         : [
                                    enabled: true
                            ],
                            enableMouseTracking: false
                    ]
            ],
            tooltip    : [
                    pointFormat: '{point.y:.2f} USD '
            ],
            series     : seriesData
    ]

    return api.buildHighchart(definition)

}
