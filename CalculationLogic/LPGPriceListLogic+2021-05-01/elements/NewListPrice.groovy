if (out.NewListPriceUSD && out.SAPCurrentPriceCurrency != "USD") {
    def newlistprice = out.NewListPriceUSD * (api.global.exchangeRate?.getAt("USD-" + out.SAPCurrentPriceCurrency) as BigDecimal)
    return libs.SpxflowLib.RoundingUtils.roundNumber(newlistprice, 2) ?: null
}
return out.NewListPriceUSD