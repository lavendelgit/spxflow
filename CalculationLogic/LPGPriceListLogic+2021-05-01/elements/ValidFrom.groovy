import java.text.SimpleDateFormat

def date = new Date()
standardFormat = new SimpleDateFormat("yyyy-MM-dd")
priceListDate = api.dateUserEntry("ValidFrom")
defaultDate = standardFormat.format(date)
parameter = api.getParameter("ValidFrom")
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
    parameter.setValue(defaultDate)
}
return priceListDate