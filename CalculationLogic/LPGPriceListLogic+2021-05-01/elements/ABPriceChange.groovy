if (out.MarginProofPrice && out.CategoryPercentIncrease) {
    def abpricechange = (out.MarginProofPrice + (out.CategoryPercentIncrease * out.MarginProofPrice))
    api.local.priceBuildupMatrix.abPriceChangeStringFormula = " Margin Proof Price + (Categorisation Percentage Increase * Margin Proof Price)"
    api.local.priceBuildupMatrix.abPriceChangeValueFormula = "(" + out.MarginProofPrice + "+ (" + out.CategoryPercentIncrease + "*" + out.MarginProofPrice + "))"
    api.local.PriceReason = api.local.PriceReason?api.local.PriceReason+"-ABPriceChange":"ABPriceChange"
    api.local.price = libs.SpxflowLib.RoundingUtils.roundNumber(abpricechange, 2) ?: null
    return api.local.price
}
return