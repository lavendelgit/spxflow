import net.pricefx.server.dto.calculation.ResultMatrix

count = 0
List productAttributes = [out.Brand ?: "*", out.ProductLine ? (out.ProductLine?.minus(out.Brand)) : "*"]

surchargeData = Lib.getSurcharge(productAttributes)
surchargeData = surchargeData?.findAll{(it.key3==out.Material||it.key3=="*")&&(it.key4==out.ArcheType||it.key4=="*")}

ResultMatrix matrix = api.newMatrix("Surcharge Name", "Applied Surcharge", "Uplifted Price", "Comment", "Valid From", "Valid To", "Source", "Formula")
upgradePrice = out.VelocityBucketBasedPrice ?: (out.ABPriceChange ?: out.MarginProofPrice)
name = out.VelocityBucketBasedPrice ? "Velocity Bucket Based Price" : (out.ABPriceChange ? "Category Price Change" : "Margin Proof Price")
surchargeData?.each {
    name = count == 0 ? name : previousname
    formula = it.attribute1 ? "(" + upgradePrice + "+(" + it.attribute1 + "*" + upgradePrice + "))" : upgradePrice + "+" + it.attribute2
    Source = it.attribute1 ? "( $name +( Surcharge Percentage * $name))" : "($name + Surcharge Value adder in USD )"
    matrix.addRow(it.key5, it.attribute1 ? ((it.attribute1 * 100) + "%") : it.attribute2, it.attribute1 ? (upgradePrice + (it.attribute1 * upgradePrice)) : upgradePrice + it.attribute2, it.attribute5, it.attribute3, it.attribute4, Source, formula)
    upgradePrice = it.attribute1 ? (upgradePrice + (it.attribute1 * upgradePrice)) : upgradePrice + it.attribute2
    count = 1
    previousname = it.key5 + " Uplifted Price"
}
api.local.surchargeData = surchargeData
api.local.surchargeMatrix = matrix
api.local.price = upgradePrice
if (surchargeData) {
    return matrix
}
return