List ruleAttributes = [out.Brand ?: "*", out.ProductLineInArchetype ?: "*", out.Material ?: "*", out.Categorisation ?: "*"]
return Lib.getPercent(ruleAttributes)