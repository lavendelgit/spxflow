out.MaterialTotalNetValue ?: api.addWarning("Material NetValue not found")
out.ProductLineTotalNetValue ?: api.addWarning("Archetype NetValue not found")
if ((out.MaterialTotalNetValue && out.ProductLineTotalNetValue)) {
    def ArchetypeRevenue = (out.MaterialTotalNetValue / out.ProductLineTotalNetValue) * 1000
    //api.local.priceBuildupMatrix.ProductLineRevenueStringFormula = "(Material Total Net Value / Product Line Total Net Value)"
    //api.local.priceBuildupMatrix.ProductLineRevenueValueFormula = "(" + out.MaterialTotalNetValue + " / " + out.ProductLineTotalNetValue + ")"
    api.local.priceBuildupMatrix.ProductLineRevenueStringFormula = "(Material Total Net Value / Product Line Total Net Value) *1000"
    api.local.priceBuildupMatrix.ProductLineRevenueValueFormula = "(" + out.MaterialTotalNetValue + " / " + out.ProductLineTotalNetValue + ")" + "*" + 1000
    return (ArchetypeRevenue as BigDecimal)?.setScale(6, BigDecimal.ROUND_HALF_UP)
}
return
