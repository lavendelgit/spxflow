import groovy.transform.Field
import net.pricefx.common.api.FieldFormatType
import net.pricefx.server.dto.calculation.ResultMatrix

Map currentMatrix = [:]
Map calculationResult = [:]
@Field Map OPERATORS = [
        EQUALS : '=',
        IN     : '∈',
        MIN    : '⋘',
        MAX    : '⋙',
        AVG    : 'μ',
        SUM    : '∑',
        SECTION: ''
]

String productSKU = out.Product?.sku

def roundingUtils = libs.SpxflowLib.RoundingUtils

ResultMatrix resultMatrix = api.newMatrix("Name", " ", "Value", "Source", "Formula")
resultMatrix.setEnableClientFilter(false)
resultMatrix.setDisableSorting(true)
resultMatrix.addRow(newRow(resultMatrix, 'PRICE LIST SUMMARY', 'SECTION', "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Material', 'EQUALS', out.Material + "-" + out.Description, resultMatrix.linkCell("Product Master", "productsPage", out.Material), ""))
resultMatrix.addRow(newRow(resultMatrix, 'MaterialType', 'EQUALS', out.MaterialType, resultMatrix.linkCell("Product Master", "productsPage", out.Material), ""))
resultMatrix.addRow(newRow(resultMatrix, 'IndustrySector', 'EQUALS', out.IndustrySector, resultMatrix.linkCell("Product Master", "productsPage", out.Material), ""))
resultMatrix.addRow(newRow(resultMatrix, 'ItemCategoryGroup', 'EQUALS', out.ItemCategoryGroup, resultMatrix.linkCell("Product Master", "productsPage", out.Material), ""))
resultMatrix.addRow(newRow(resultMatrix, 'MaterialGroup', 'EQUALS', out.MaterialGroup, resultMatrix.linkCell("Product Master", "productsPage", out.Material), ""))
resultMatrix.addRow(newRow(resultMatrix, 'MaterialGroup1', 'EQUALS', (out.MaterialGroup1 ?: "") + " - " + out.MaterialGroup1Description ?: "", resultMatrix.linkCell("Product Master", "productsPage", out.Material), ""))
resultMatrix.addRow(newRow(resultMatrix, 'ProductHierarchy', 'EQUALS', out.ProductHierarchy, resultMatrix.linkCell("Product Master", "productsPage", out.Material), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Brand', 'EQUALS', (out.Brand ?: "") + " - " + (out.BrandDescription ?: ""), resultMatrix.linkCell("Product Hierarchy Mapping PX", "productExtensionsPage", "ProductHierarchyMapping"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'ProductLine', 'EQUALS', (out.ProductLine ?: "") + " - " + (out.ProductLineDescription ?: ""), resultMatrix.linkCell("Product Hierarchy Mapping PX", "productExtensionsPage", "ProductHierarchyMapping"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'ProductFamily', 'EQUALS', (out.ProductFamily ?: "") + " - " + (out.ProductFamilyDescription ?: ""), resultMatrix.linkCell("Product Hierarchy Mapping PX", "productExtensionsPage", "ProductHierarchyMapping"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'ProductSeries', 'EQUALS', (out.ProductSeries ?: "") + " - " + (out.ProductSeriesDescription ?: ""), resultMatrix.linkCell("Product Hierarchy Mapping PX", "productExtensionsPage", "ProductHierarchyMapping"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'ModelSize', 'EQUALS', (out.ModelSize ?: "") + " - " + (out.ModelSizeDescription ?: ""), resultMatrix.linkCell("Product Hierarchy Mapping PX", "productExtensionsPage", "ProductHierarchyMapping"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Plant', 'EQUALS', (out.Plant ?: "") + " - " + (out.PlantName ?: ""), resultMatrix.linkCell("MaterialPlantCost PX", "productExtensionsPage", "MaterialPlantCost"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'StandardCost', 'EQUALS', (out.StandardCost ?: 0) + " " + (out.CostCurrency ?: ""), resultMatrix.linkCell("MaterialPlantCost PX", "productExtensionsPage", "MaterialPlantCost"), ""))
out.CostCurrency != "USD" && out.StandardCost ? resultMatrix.addRow(newRow(resultMatrix, 'Exchange Rate ' + out.CostCurrency + " - USD", 'EQUALS', api.global.exchangeRate?.getAt(out.CostCurrency + "-USD"), resultMatrix.linkCell("Monthly Exchange Rate CP", "pricingParametersPage", (api.findLookupTable("MonthlyExchangeRate")?.id) as String), "")) : ""
resultMatrix.addRow(newRow(resultMatrix, 'Standard Cost In USD', 'EQUALS', (out.StandardCostInUSD ?: 0) + " USD", resultMatrix.linkCell("MaterialPlantCost PX", "productExtensionsPage", "MaterialPlantCost"), out.CostCurrency != "USD" && out.StandardCost ? (out.StandardCost + "*" + api.global.exchangeRate?.getAt(out.CostCurrency + "-USD")) : ""))
resultMatrix.addRow(newRow(resultMatrix, 'Archetype', 'EQUALS', out.ArcheType, resultMatrix.linkCell("MaterialArchetypeMapping PX", "productExtensionsPage", "MaterialArchetypeMapping"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Price Unit', 'EQUALS', out.PriceUnit?:"", resultMatrix.linkCell("SAPCurrentPrice PX", "productExtensionsPage", "SAPCurrentPrice"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'SAP Current Price includes Price Unit', 'EQUALS', out.SAPCurrentPriceIncludesPerUnit + " " + out.SAPCurrentPriceCurrency, resultMatrix.linkCell("SAPCurrentPrice PX", "productExtensionsPage", "SAPCurrentPrice"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'SAP Current Price', 'EQUALS', out.SAPCurrentPrice + " " + out.SAPCurrentPriceCurrency, resultMatrix.linkCell("SAPCurrentPrice PX", "productExtensionsPage", "SAPCurrentPrice"), ""))
out.SAPCurrentPriceCurrency != "USD" ? resultMatrix.addRow(newRow(resultMatrix, 'Exchange Rate ' + out.SAPCurrentPriceCurrency + " - USD", 'EQUALS', api.global.exchangeRate?.getAt(out.SAPCurrentPriceCurrency + "-USD"), resultMatrix.linkCell("Monthly Exchange Rate CP", "pricingParametersPage", (api.findLookupTable("MonthlyExchangeRate")?.id) as String), "")) : ""
resultMatrix.addRow(newRow(resultMatrix, 'SAPCurrentPrice USD', 'EQUALS', out.SAPCurrentPriceUSD + " USD", resultMatrix.linkCell("SAPCurrentPrice PX", "productExtensionsPage", "SAPCurrentPrice"), out.SAPCurrentPriceCurrency != "USD" ? (out.SAPCurrentPrice + "*" + api.global.exchangeRate?.getAt(out.SAPCurrentPriceCurrency + "-USD")) : ""))
resultMatrix.addRow(newRow(resultMatrix, 'ArchetypeCategory', 'EQUALS', out.ArchetypeCategory, resultMatrix.linkCell("ArchetypeMargins PX", "productExtensionsPage", "ArchetypeMargins"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'ArchetypeValue', 'EQUALS', out.ArchetypeValue, resultMatrix.linkCell("ArchetypeMargins PX", "productExtensionsPage", "ArchetypeMargins"), ""))
resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'ACTUAL MARGIN & MIN-MAX VALIDATION', 'SECTION', "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'MarginCostBucket', 'EQUALS', (out.MarginValue?.attribute6 ?: "") + "-" + (out.MarginValue?.attribute7 ?: ""), resultMatrix.linkCell("ArchetypeMargins PX", "productExtensionsPage", "ArchetypeMargins"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Min Margin', 'EQUALS', out.MinMargin ? (out.MinMargin * 100) + "%" : "", resultMatrix.linkCell("ArchetypeMargins PX", "productExtensionsPage", "ArchetypeMargins"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Max Margin', 'EQUALS', out.MaxMargin ? (out.MaxMargin * 100) + "%" : "", resultMatrix.linkCell("ArchetypeMargins PX", "productExtensionsPage", "ArchetypeMargins"), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Actual Margin %', 'EQUALS', out.ActualMargin ? (out.ActualMargin * 100) + "%" : "", api.local.priceBuildupMatrix.ActualMarginStringFormula, api.local.priceBuildupMatrix.ActualMarginValueFormula))
resultMatrix.addRow(newRow(resultMatrix, 'Margin Proof Price', 'EQUALS', out.MarginProofPrice ?(out.MarginProofPrice+" USD"): "", (api.local.priceBuildupMatrix.MarginProofStringformula?:resultMatrix.linkCell("SAPCurrentPrice PX", "productExtensionsPage", "SAPCurrentPrice")), (api.local.priceBuildupMatrix.MarginProofValueformula?:"")))

//
resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, '80/20 RULE and CATEGORISATION', 'SECTION', "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Categorisation', 'EQUALS', out.Categorisation, resultMatrix.linkCell("Sku Based A/B Categorisation CP", "pricingParametersPage", ((api.findLookupTable("SkuBasedA/BCategorisation")?.id) as String)), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Categorisation Percentage Increase', 'EQUALS', (out.CategoryPercentIncrease ? (out.CategoryPercentIncrease * 100)+"%" : ""), resultMatrix.linkCell("80/20 Rule CP", "pricingParametersPage", ((api.findLookupTable("80/20Rule")?.id) as String)), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Categorisation Price Change', 'EQUALS', out.ABPriceChange ? out.ABPriceChange + " USD" : "", api.local.priceBuildupMatrix.abPriceChangeStringFormula, api.local.priceBuildupMatrix.abPriceChangeValueFormula))
//
resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'SALES BUCKET VELOCITY', 'SECTION', "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Material Total Net Value', 'EQUALS', out.MaterialTotalNetValue?out.MaterialTotalNetValue+" USD":"",  resultMatrix.linkCell("Transaction Data mart","datamartsPage","10.DM"), "SUM (12 M Trailing Material Revenue)"))
resultMatrix.addRow(newRow(resultMatrix, 'Product Line Total Net Value', 'EQUALS', out.ProductLineTotalNetValue?out.ProductLineTotalNetValue+" USD":"", resultMatrix.linkCell("Transaction Data mart","datamartsPage","10.DM"), "SUM (12 M Trailing ProductLine Revenue)"))
//resultMatrix.addRow(newRow(resultMatrix, 'Product Line Revenue %', 'EQUALS', (out.ProductLineRevenue?((out.ProductLineRevenue)+"%"):""), api.local.priceBuildupMatrix.ProductLineRevenueStringFormula, api.local.priceBuildupMatrix.ProductLineRevenueValueFormula))
resultMatrix.addRow(newRow(resultMatrix, 'Proportion of Product Line Revenue(*1000)', 'EQUALS', (out.ProductLineRevenue?((out.ProductLineRevenue*1000)+"%"):""), api.local.priceBuildupMatrix.ProductLineRevenueStringFormula, api.local.priceBuildupMatrix.ProductLineRevenueValueFormula))
resultMatrix.addRow(newRow(resultMatrix, 'Velocity Bucket', 'EQUALS', out.VelocityBuckets, api.local.priceBuildupMatrix.velocityBucketStringFormula, api.local.priceBuildupMatrix.velocityBucketValueFormula))
resultMatrix.addRow(newRow(resultMatrix, 'Velocity Bucket Multiplier', 'EQUALS', api.local.priceBuildupMatrix.velocityPriceMultipler?(( api.local.priceBuildupMatrix.velocityPriceMultipler*100)+"%"):"", resultMatrix.linkCell("Velocity Bucket Price CP","pricingParametersPage",((api.findLookupTable("VelocityBucketPrice")?.id) as String)), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Velocity Bucket Based Price', 'EQUALS', out.VelocityBucketBasedPrice?(out.VelocityBucketBasedPrice+" USD"):"", api.local.priceBuildupMatrix.velocityValueStringFormula, api.local.priceBuildupMatrix.velocityValueValueFormula))//
//
resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'SURCHARGE', 'SECTION', "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Surcharge Matrix', 'EQUALS', api.local.surchargeMatrix, resultMatrix.linkCell("Raw Material Cost Surcharge CP", "pricingParametersPage", ((api.findLookupTable("RawMaterialCostSurcharge")?.id) as String)), ""))
resultMatrix.addRow(newRow(resultMatrix, 'Surcharge Uplift Price', 'EQUALS', out.SurchargePrice ? (out.SurchargePrice + " USD") : "", "", ""))

if (out.OverrideOption && out.OverrideOptionValue) {
    listPrice = api.local.currentItemList.USDPrice
    margin = api.local.currentItemList.Margin
    qList = api.local.currentItemList.priceQ
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'RESULT PRICE', 'SECTION', "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price USD', 'EQUALS', listPrice ? listPrice + " USD" : "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'New Margin %', 'EQUALS', margin ? (margin * 100) + "%" : "", api.local.currentItemList.MarginStringFormula, api.local.currentItemList.MarginStringValue))
    out.SAPCurrentPriceCurrency != "USD" ? resultMatrix.addRow(newRow(resultMatrix, 'Exchange Rate  : USD - ' + out.SAPCurrentPriceCurrency, 'EQUALS', api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency), resultMatrix.linkCell("Monthly Exchange Rate CP", "pricingParametersPage", (api.findLookupTable("MonthlyExchangeRate")?.id) as String), "")) : ""
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price', 'EQUALS', api.local.currentItemList.Price ? api.local.currentItemList.Price + " " + api.local.currentItemList.PriceCurrency : "", api.local.currentItemList.PriceCurrency != "USD" ? "(" + api.local.currentItemList.USDPrice + "*" + api.global.exchangeRate?.getAt("USD-" + api.local.currentItemList.PriceCurrency) + ")" : "", api.local.currentItemList.PriceCurrency != "USD" ? (listPrice + "*" + api.global.exchangeRate?.getAt("USD-" + api.local.currentItemList.PriceCurrency)) : ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price includes Price Unit', 'EQUALS', api.local.currentItemList.priceQ ? api.local.currentItemList.priceQ + " " + api.local.currentItemList.PriceCurrency : "", api.local.currentItemList.PriceCurrency != "USD" ? "(" + api.local.currentItemList.USDPrice + "*" + api.global.exchangeRate?.getAt("USD-" + api.local.currentItemList.PriceCurrency) + ")*"+out.PriceUnit : "", api.local.currentItemList.PriceCurrency != "USD" ? (listPrice + "*" + api.global.exchangeRate?.getAt("USD-" + api.local.currentItemList.PriceCurrency)+"*"+out.PriceUnit) : ""))
} else {
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'RESULT PRICE', 'SECTION', "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price USD', 'EQUALS', out.NewListPriceUSD ? out.NewListPriceUSD + " USD" : "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'New Margin %', 'EQUALS', out.NewMargin ? (out.NewMargin * 100) + "%" : "", api.local.priceBuildupMatrix.MarginStringFormula, api.local.priceBuildupMatrix.MarginStringValue))
    out.SAPCurrentPriceCurrency != "USD" ? resultMatrix.addRow(newRow(resultMatrix, 'Exchange Rate  : USD - ' + out.SAPCurrentPriceCurrency, 'EQUALS', api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency), resultMatrix.linkCell("Monthly Exchange Rate CP", "pricingParametersPage", (api.findLookupTable("MonthlyExchangeRate")?.id) as String), "")) : ""
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price', 'EQUALS', out.NewListPrice ? out.NewListPrice + " " + out.NewListPriceCurrency : "", out.NewListPriceCurrency != "USD" ? "( Calculated New List Price USD *  Monthly Exchange Rate)" : "", out.NewListPriceCurrency != "USD" ? (out.NewListPriceUSD + "*" + api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency)) : ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price includes Price Unit', 'EQUALS', out.CalculatedNewListPrice ? out.CalculatedNewListPrice + " " + out.NewListPriceCurrency : "", out.NewListPriceCurrency != "USD" ? "( Calculated New List Price USD *  Monthly Exchange Rate)*Price Unit" : "", out.NewListPriceCurrency != "USD" ? (out.NewListPriceUSD + "*" + api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency)+"*"+out.PriceUnit) : ""))
}

if (out.OverrideOption == "List Price (USD)" && out.OverrideOptionValue) {
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'OVERRIDE RESULT PRICE : LIST PRICE (USD)', 'SECTION', "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Override New List Price USD', 'EQUALS', out.NewListPriceUSD ? out.NewListPriceUSD + " USD" : "", "Override Value", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'New Margin %', 'EQUALS', out.NewMargin ? (out.NewMargin * 100) + "%" : "", api.local.priceBuildupMatrix.MarginStringFormula, api.local.priceBuildupMatrix.MarginStringValue))
    out.SAPCurrentPriceCurrency != "USD" ? resultMatrix.addRow(newRow(resultMatrix, 'Exchange Rate ' + out.SAPCurrentPriceCurrency + " - USD", 'EQUALS', api.global.exchangeRate?.getAt(out.SAPCurrentPriceCurrency + "-USD"), resultMatrix.linkCell("Monthly Exchange Rate CP", "pricingParametersPage", (api.findLookupTable("MonthlyExchangeRate")?.id) as String), "")) : ""
    resultMatrix.addRow(newRow(resultMatrix, 'New List Price', 'EQUALS', out.NewListPrice ? out.NewListPrice + " " + out.NewListPriceCurrency : "", out.NewListPriceCurrency != "USD" ? "( Calculated New List Price USD *  Monthly Exchange Rate)" : "", out.NewListPriceCurrency != "USD" ? (out.NewListPriceUSD + "*" + api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency)) : ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price includes Price Unit', 'EQUALS', out.CalculatedNewListPrice ? out.CalculatedNewListPrice + " " + out.NewListPriceCurrency : "", out.NewListPriceCurrency != "USD" ? "( Calculated New List Price USD *  Monthly Exchange Rate)*Price Unit" : "", out.NewListPriceCurrency != "USD" ? (out.NewListPriceUSD + "*" + api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency)+"*"+out.PriceUnit) : ""))
}
if (out.OverrideOption == "Margin (%)" && out.OverrideOptionValue) {
    resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'OVERRIDE RESULT PRICE : MARGIN (%)', 'SECTION', "", "", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Override New Margin %', 'EQUALS', out.NewMargin ? ((out.NewMargin * 100) + "%") : "", "Override Value", ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price USD', 'EQUALS', out.NewListPriceUSD ? out.NewListPriceUSD + " USD" : "", api.local.priceBuildupMatrix.OverrideMarginStringFormula, api.local.priceBuildupMatrix.OverrideMarginValueFormula))
    out.SAPCurrentPriceCurrency != "USD" ? resultMatrix.addRow(newRow(resultMatrix, 'Exchange Rate  : USD - ' + out.SAPCurrentPriceCurrency, 'EQUALS', api.global.exchangeRate?.getAt("USD-" + out.SAPCurrentPriceCurrency), resultMatrix.linkCell("Monthly Exchange Rate CP", "pricingParametersPage", (api.findLookupTable("MonthlyExchangeRate")?.id) as String), "")) : ""
    resultMatrix.addRow(newRow(resultMatrix, 'New List Price', 'EQUALS', out.NewListPrice ? out.NewListPrice + " " + out.NewListPriceCurrency : "", out.NewListPriceCurrency != "USD" ? "(" + out.NewListPriceUSD + "*" + api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency) + ")" : "", out.NewListPriceCurrency != "USD" ? (out.NewListPriceUSD + "*" + api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency)) : ""))
    resultMatrix.addRow(newRow(resultMatrix, 'Calculated New List Price includes Price Unit', 'EQUALS', out.CalculatedNewListPrice ? out.CalculatedNewListPrice + " " + out.NewListPriceCurrency : "", out.NewListPriceCurrency != "USD" ? "( Calculated New List Price USD *  Monthly Exchange Rate)*Price Unit" : "", out.NewListPriceCurrency != "USD" ? (out.NewListPriceUSD + "*" + api.global.exchangeRate?.getAt("USD-" + out.NewListPriceCurrency)+"*"+out.PriceUnit) : ""))
}

resultMatrix.addRow(newRow(resultMatrix, "", "", "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'IMPACTS', 'SECTION', "", "", ""))
resultMatrix.addRow(newRow(resultMatrix, 'Revenue Impact', 'EQUALS', out.RevenueImpact ? out.RevenueImpact + " USD" : "", api.local.priceBuildupMatrix.revenueImpactStringFormula, api.local.priceBuildupMatrix.revenueImpactValueFormula))
//resultMatrix.addRow(newRow(resultMatrix, 'Margin Impact %', 'EQUALS', out.MarginImpact ?(out.MarginImpact*100)+"%": "", api.local.priceBuildupMatrix.marginImpactStringFormula, api.local.priceBuildupMatrix.marginImpactValueFormula))
resultMatrix.addRow(newRow(resultMatrix, 'Margin Impact ', 'EQUALS', out.MarginImpact ?: "", api.local.priceBuildupMatrix.marginImpactStringFormula, api.local.priceBuildupMatrix.marginImpactValueFormula))



return resultMatrix


Map newRow(ResultMatrix resultMatrix, String name, String operator, result, source, String formula) {

    if (name?.contains("Margin %")) {

        [
                Name   : getNameCell(resultMatrix, name, operator),
                " "    : getOperatorCell(resultMatrix, operator),
                Value  : getValueCell(resultMatrix, name, result),
                Source : source,
                Formula: formula
        ]
    } else {
        return [
                Name   : getNameCell(resultMatrix, name, operator),
                " "    : getOperatorCell(resultMatrix, operator),
                Value  : result,
                Source : source,
                Formula: formula
        ]
    }
}

def getOperatorCell(ResultMatrix resultMatrix, String operator) {
    //OPERATORS[operator]
    return resultMatrix.styledCell(OPERATORS[operator])
            .withColor("orange")
            .withFontWeight("bold")
            .withAlignment("center")
            .withCSS("font-size: 26px;color:orange;")
}

def getNameCell(ResultMatrix resultMatrix, String sectionName, String operator) {
    if (operator == 'SECTION') {
        return resultMatrix.styledCell(sectionName)
                .withColor("grey")
                .withFontWeight("bold")
                .withAlignment("left")
                .withCSS("font-size: 20px;")
    }
    return resultMatrix.styledCell(sectionName)
}

def getValueCell(ResultMatrix resultMatrix, String sectionName, def result) {
    List marginValues = ["Actual Margin %"]
    if (sectionName?.contains("%") && result && !sectionName?.contains("Calculated")) {
        if (marginValues?.contains(sectionName)) {
            return resultMatrix.styledCell(result)
                    .withColor(api.local.actualMarginColor)
                    .withFontWeight("bold")
                    .withAlignment("left")
                    .withCSS("font-size: 12px;")
        } else if (result?.contains("-")) {
            return resultMatrix.styledCell(result + "↓")
                    .withColor("red")
                    .withFontWeight("bold")
                    .withAlignment("left")
                    .withCSS("font-size: 12px;")
        } else {
            return resultMatrix.styledCell(result + "↑")
                    .withColor("green")
                    .withFontWeight("bold")
                    .withAlignment("left")
                    .withCSS("font-size: 12px;")
        }
    } else if (result) {
        return resultMatrix.styledCell(result)
                .withColor("green")
                .withFontWeight("bold")
                .withAlignment("left")
                .withCSS("font-size: 15px;")
    }

}

