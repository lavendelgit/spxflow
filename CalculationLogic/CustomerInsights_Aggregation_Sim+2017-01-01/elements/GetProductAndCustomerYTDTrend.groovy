def classificationUtils = libs.CustomerInsights.Classification
Map trend = libs.SharedLib.CacheUtils.getOrSet("PRODUCT_CUSTOMER_YTD_TREND", [], {
  Map configuration = api.getElement("Configuration")
  Map simulationParams = api.getElement("CreateSimulationParams")

  return classificationUtils.calculateYTDTrendForProductAndCustomer(configuration, simulationParams)
})
String customerId = api.getElement("CustomerId")
String productId = api.getElement("ProductId")
String groupByKey = classificationUtils.getGroupByKeyProductAndCustomer(productId, customerId)
api.local.Product_Customer_YTD_Trends = trend.get(groupByKey)?.getAt(0)

return null