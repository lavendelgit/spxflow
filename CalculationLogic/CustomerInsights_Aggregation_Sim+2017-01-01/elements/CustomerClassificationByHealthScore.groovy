Map configuration = api.getElement("Configuration")
BigDecimal customerHealthScore = api.getElement("CustomerHealthScore")

return libs.CustomerInsights.Classification.calculateCustomerClassificationByHealthScore(configuration, customerHealthScore)