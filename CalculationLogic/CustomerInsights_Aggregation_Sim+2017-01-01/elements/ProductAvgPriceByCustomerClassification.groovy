Map productAvgPrice = api.local.Product_Average_Price_Info
String customerClass = api.getElement("CustomerClassificationByRevenue")

return productAvgPrice?.get(customerClass)