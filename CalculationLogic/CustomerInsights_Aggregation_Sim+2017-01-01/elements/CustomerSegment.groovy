def constant = libs.CustomerInsights.Constant
def customerSegmentUtils = libs.CustomerInsights.CustomerSegmentUtils

Map customerSegmentInfo = libs.SharedLib.CacheUtils.getOrSet("CUSTOMER_SEGMENT_BY_CUSTOMER_ID", [], {
  Map configuration = api.getElement("Configuration")

  return customerSegmentUtils.getCustomerSegment(configuration.customerSegments)
})
String customerId = api.getElement("CustomerId")

return customerSegmentInfo.get(customerId)?.getAt(0)?.customerSegment ?: constant.DEFAULT_VALUE.UNKNOWN