Map configuration = api.getElement("Configuration")
Map customerTrend = api.local.Customer_L12M_Trends
BigDecimal revenueTrendL12M = customerTrend?.revenueTrend
BigDecimal marginTrendL12M = customerTrend?.marginTrend

return libs.CustomerInsights.Classification.calculateHealthScore(configuration, revenueTrendL12M, marginTrendL12M)