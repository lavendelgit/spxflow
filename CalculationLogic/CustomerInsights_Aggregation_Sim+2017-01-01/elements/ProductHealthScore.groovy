Map configuration = api.getElement("Configuration")
Map productAndCustomerTrend = api.local.Product_Customer_L12M_Trends
BigDecimal revenueTrendL12M = productAndCustomerTrend?.revenueTrend
BigDecimal marginTrendL12M = productAndCustomerTrend?.marginTrend

return libs.CustomerInsights.Classification.calculateHealthScore(configuration, revenueTrendL12M, marginTrendL12M)