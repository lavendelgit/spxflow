Map trend = libs.SharedLib.CacheUtils.getOrSet("CUSTOMER_TREND", [], {
  Map configuration = api.getElement("Configuration")
  Map simulationParams = api.getElement("CreateSimulationParams")
  def classificationUtils = libs.CustomerInsights.Classification

  return classificationUtils.calculateTrendForCustomers(configuration, simulationParams)
})
String customerId = api.getElement("CustomerId")
api.local.Customer_L12M_Trends = trend.get(customerId)?.getAt(0)

return null