BigDecimal avgPriceByCustomer = api.getElement("GetProductAvgPriceByCustomer")

return (avgPriceByCustomer ?: BigDecimal.ZERO)