return libs.SharedLib.CacheUtils.getOrSet("SIMULATION_INPUT_PARAMS", [], {
  Map configuration = api.getElement("Configuration")
  Integer lastMonthAmount = libs.CustomerInsights.Configuration.getLastMonthAmountFromConfiguration(configuration)
  Date currentDate = new Date()
  Map period = libs.CustomerInsights.DateUtils.getPreviousPeriodFromDate(currentDate, lastMonthAmount)

  return libs.CustomerInsights.SimulationParameter.newSimulationParameter(period, currentDate)
})