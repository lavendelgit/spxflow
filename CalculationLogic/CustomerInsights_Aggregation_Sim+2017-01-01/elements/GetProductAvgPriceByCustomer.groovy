def classificationUtils = libs.CustomerInsights.Classification

Map productAvgPriceInfo = libs.SharedLib.CacheUtils.getOrSet("AVG_PRICE_PER_PRODUCT_CUSTOMER", [], {
  Map configuration = api.getElement("Configuration")
  Map simulationParams = api.getElement("CreateSimulationParams")
  List productAvgPrice = classificationUtils.calculateAvgPricePerProductAndCustomer(configuration, simulationParams)

  return productAvgPrice.groupBy { classificationUtils.getGroupByKeyProductAndCustomer(it.productId, it.customerId) }
})

String productId = api.getElement("ProductId")
String customerId = api.getElement("CustomerId")
String groupedKey = classificationUtils.getGroupByKeyProductAndCustomer(productId, customerId)

return productAvgPriceInfo.get(groupedKey)?.getAt(0)?.avgInvoicePrice