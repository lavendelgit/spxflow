Map trend = libs.SharedLib.CacheUtils.getOrSet("CUSTOMER_YTD_TREND", [], {
  Map configuration = api.getElement("Configuration")
  Map simulationParams = api.getElement("CreateSimulationParams")
  def classificationUtils = libs.CustomerInsights.Classification

  return classificationUtils.calculateYTDTrendForCustomers(configuration, simulationParams)
})
String customerId = api.getElement("CustomerId")
api.local.Customer_YTD_Trends = trend.get(customerId)?.getAt(0)

return null