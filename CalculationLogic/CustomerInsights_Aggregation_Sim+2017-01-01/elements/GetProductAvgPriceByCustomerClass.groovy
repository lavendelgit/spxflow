Map productAvgPriceInfo = libs.SharedLib.CacheUtils.getOrSet("PRODUCT_AVG_PRICE_CUSTOMER_CLASS", [], {
  Map configuration = api.getElement("Configuration")
  Map simulationParams = api.getElement("CreateSimulationParams")
  def classificationUtils = libs.CustomerInsights.Classification

  return classificationUtils.calculateAvgPricePerProductAndCustomerClass(configuration, simulationParams)
})
String productId = api.getElement("ProductId")
api.local.Product_Average_Price_Info = productAvgPriceInfo.get(productId)

return null