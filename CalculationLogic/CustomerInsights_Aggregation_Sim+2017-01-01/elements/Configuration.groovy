return libs.SharedLib.CacheUtils.getOrSet("CONFIGURATION", [], {
  return libs.CustomerInsights.Configuration.getAllConfigurationData()
})