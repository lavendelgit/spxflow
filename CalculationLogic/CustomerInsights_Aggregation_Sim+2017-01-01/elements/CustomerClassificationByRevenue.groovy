Map customerClassificationInfo = libs.SharedLib.CacheUtils.getOrSet("CUSTOMER_CLASSIFICATION", [], {
  Map configuration = api.getElement("Configuration")
  Map simulationParams = api.getElement("CreateSimulationParams")
  def classificationUtils = libs.CustomerInsights.Classification

  return classificationUtils.calculationCustomerClassificationByRevenue(configuration, simulationParams)
})
String customerId = api.getElement("CustomerId")

return customerClassificationInfo.get(customerId)?.getAt(0)?.classificationByRevenue