Map productClassificationInfo = libs.SharedLib.CacheUtils.getOrSet("PRODUCT_CLASSIFICATION", [], {
  Map configuration = api.getElement("Configuration")
  Map simulationParams = api.getElement("CreateSimulationParams")
  def classificationUtils = libs.CustomerInsights.Classification

  return classificationUtils.calculateProductClassification(configuration, simulationParams)
})
String productId = api.getElement("ProductId")
Map productClass = productClassificationInfo.get(productId)?.getAt(0)

api.local.Product_Classification_Info = [classificationByQuantity     : productClass?.classificationByQuantity,
                                         classificationByMarginPercent: productClass?.classificationByMarginPercent]

return null