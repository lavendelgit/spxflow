Map configuration = api.getElement("Configuration")
BigDecimal productHealthScore = api.getElement("ProductHealthScore")

return libs.CustomerInsights.Classification.calculateProductClassificationByHealthScore(configuration, productHealthScore)