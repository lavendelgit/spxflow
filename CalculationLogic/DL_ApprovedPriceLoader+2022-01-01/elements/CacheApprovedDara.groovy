api.local.approvedPrice = [:]

List filter =[
        Filter.equal("approvalState","APPROVED"),
        Filter.in("priceGridId",api.local.ids),
        Filter.greaterOrEqual("approvalDate",out.LastDLRun),

]
cacheIterator = api.stream("PGI","sku",*filter)
api.local.approvedPrice = cacheIterator?.collect{it}
cacheIterator?.close()
return