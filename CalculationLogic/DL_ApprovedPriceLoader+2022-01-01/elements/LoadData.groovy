loadApprovedData(api.local.approvedPrice)
return


void loadApprovedData(approvedRecords){
    List loadedSkus = []
    api.logInfo("approvedRecords",approvedRecords)
    Map approvedRecord = [:]
    def target = api.getDatamartRowSet("target")
    for(records  in approvedRecords){
        approvedRecord.MaterialNumber = records?.getAt("sku")
        approvedRecord.priceGridId = records?.getAt("priceGridId")
        approvedRecord.approvalDate = records?.getAt("approvalDate")
        approvedRecord.approvalState = records?.getAt("approvalState")
        approvedRecord.approvedByName = records?.getAt("approvedByName")
        approvedRecord.ResultPrice = records?.getAt("resultPrice")
        approvedRecord.unitOfMeasure = records?.getAt("unitOfMeasure")
        records?.calculationResults?.each {attributes->
            approvedRecord?.put(attributes?.resultName,attributes?.result)
        }
        target?.addRow(approvedRecord)
        //loadedSkus << approvedRecord?.MaterialNumber+approvedRecord?.approvalDate+approvedRecord?.ValidFrom
    }
}
