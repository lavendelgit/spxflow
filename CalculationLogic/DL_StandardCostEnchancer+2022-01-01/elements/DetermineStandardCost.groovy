Map plantData = [:]
Map costMap = [:]
api.local.costflag = ""
if(out.CurrentItem?.ProductHierarchy){
    Map plantMapping = api.global.productPlantMapping[out.CurrentItem?.ProductHierarchy]
    for(level in api.global.levelMapping){
        if(plantMapping?.getAt(level)){
            costMap = getCost(plantMapping[level])
            if(costMap){
                return costMap
            }
        }
        }
    }
return

Map getCost(def plant) {
    Map costData = [:]
    data = api.global.materialPlantCost[out.CurrentItem?.MaterialNumber + "_" + plant] as Map
    api.trace("data",data)
    String date = (data?.getAt("attribute6"))//?.format("YYYY-MM-dd")) as String
    if (date <= (out.CurrentItem?.Createondate?.format("YYYY-MM-dd") as String) && date) {
        plantData = data
        costAttribute = plantData?.attribute2 == "S" ? "attribute4" : "attribute3"
        if (plantData?.getAt(costAttribute)) {
            api.local.costflag = "CostFromPXLatestCost"
            costData = [
                    Plant    : plantData?.attribute1,
                    Cost     : (plantData[costAttribute] as BigDecimal)?.setScale(2, BigDecimal.ROUND_HALF_UP),
                    PriceUnit: plantData?.attribute5,
                    Currency : api.global.plantCurrencyMapping[plantData?.attribute1],
            ]
            return costData
        }
    } else if (api.global.StandardCostHistory?.getAt(out.CurrentItem?.MaterialNumber + "_" + plant)) {
        plantData = api.global.StandardCostHistory?.getAt(out.CurrentItem?.MaterialNumber + "_" + plant)?.find { ((it.ValidFrom)?.format("YYYY-MM-dd") as String) <= (out.CurrentItem?.Createondate?.format("YYYY-MM-dd") as String) }
        api.trace("plantData",plantData)
        costAttribute = plantData?.PriceControlIndicator == "S" ? "StandardPrice" : "MovingAveragePricePeriodic"
        if (plantData?.getAt(costAttribute)) {
            api.local.costflag = "CostFromPXHistoricalStandardCost "
            costData = [
                    Plant    : plantData?.ValuationArea,
                    Cost     : (plantData[costAttribute] as BigDecimal)?.setScale(2, BigDecimal.ROUND_HALF_UP),
                    PriceUnit: plantData?.PriceUnit,
                    Currency : api.global.plantCurrencyMapping[plantData?.ValuationArea],
            ]
            return costData
        }
    }else{
        if(data && !date){
            plantData = data
            costAttribute = plantData?.attribute2 == "S" ? "attribute4" : "attribute3"
            if (plantData?.getAt(costAttribute)) {
                api.local.costflag = "CostFromPX-WithoutValidFrom"
                costData = [
                        Plant    : plantData?.attribute1,
                        Cost     : (plantData[costAttribute] as BigDecimal)?.setScale(2, BigDecimal.ROUND_HALF_UP),
                        PriceUnit: plantData?.attribute5,
                        Currency : api.global.plantCurrencyMapping[plantData?.attribute1],
                ]
                return costData
            }
        }


    }
}
