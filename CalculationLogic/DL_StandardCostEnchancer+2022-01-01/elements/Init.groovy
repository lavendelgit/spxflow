if(!api.global.StandardCostHistory){
    apctx = api.getDatamartContext()
    ds = apctx.getDataSource("StandardCostHistory")
    def approvedQuery =apctx?.newQuery(ds)
            ?.select('MaterialNumber','MaterialNumber')
            ?.select('ValuationArea','ValuationArea')
            ?.select('PriceControlIndicator','PriceControlIndicator')
            ?.select('MovingAveragePricePeriodic','MovingAveragePricePeriodic')
            ?.select('StandardPrice','StandardPrice')
            ?.select('PriceUnit','PriceUnit')
            ?.select('ValidFrom','ValidFrom')
            ?.orderBy('ValidFrom DESC')
    api.global.StandardCostHistory  = apctx?.executeQuery(approvedQuery)?.data?.toResultMatrix()?.getAt("entries")?.groupBy { it.MaterialNumber+"_"+it.ValuationArea }
}

if(!api.global.productPlantMapping){
    streamRecords = api.stream("PX6",null,Filter.equal("name","HierarchyPlantMapping"))
    api.global.productPlantMapping = streamRecords?.collectEntries{it-> [(it.attribute1):it]}
    streamRecords?.close()
}
if(!api.global.levelMapping){
    api.global.levelMapping = api.findLookupTableValues("HierarchyPlantMappingConfiguration")?.attribute1
}

if(!api.global.materialPlantCost){
    streamRecords = api.stream("PX10",null,Filter.equal("name","StandardCost"))
    api.global.materialPlantCost = streamRecords?.collectEntries{it-> [(it.sku+"_"+it.attribute1):it]}
    streamRecords?.close()
}

if(!api.global.plantCurrencyMapping){
    api.global.plantCurrencyMapping = api.findLookupTableValues("Plant")?.collectEntries{[(it.name):it.attribute9]}
}


if (!api.global.exchangeRate){
    api.global.exchangeRate=[:]
    api.global.exchangeRate = api.findLookupTableValues("MonthlyExchangeRate","key3")?.collectEntries{
        [(it.key1 + "-" + it.key2):it.attribute1]
    }
}

return