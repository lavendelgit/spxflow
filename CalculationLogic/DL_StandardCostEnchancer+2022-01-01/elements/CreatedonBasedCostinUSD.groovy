if(out.StandardCost){
    def exchangeRate = api.global.exchangeRate?.getAt(out.Currency+"-"+"USD")
    if(out.Currency == "USD")
    {
        return out.StandardCost as BigDecimal
    }
    else{
        return ((out.StandardCost * exchangeRate) as BigDecimal)?.setScale(2,BigDecimal.ROUND_HALF_UP)
    }
}
return