
Map totals = api.local.maps?.findAll{it.key?.contains("Total")}
pgId = out.ID
List pricePoints = ["Trailing Revenue","Existing margins","Additional Revenue","Additional Revenue %","New Margin","Margin change"]
BigDecimal revenue
BigDecimal margin
BigDecimal newMargin
BigDecimal marginDiff
BigDecimal addMargin
String precentMargin = ""
pricePoints?.each { it ->
    api.removePricegridCalculationOutput(pgId,(it as String))
    switch (it) {
        case "Trailing Revenue":
            api.setPricegridCalculationOutput(pgId, (it as String), (it as String),  totals?.getAt("TotalRevenue")+" USD" as String, "")
            break
        case "Existing margins":
            margin = (((totals?.getAt("TotalRevenue") - (totals?.getAt("TotalCost")))/totals?.getAt("TotalRevenue")))?.setScale(4,BigDecimal.ROUND_HALF_UP)
            api.setPricegridCalculationOutput(pgId, (it as String), ("Existing Margins"),(((margin*100)?.setScale(2,BigDecimal.ROUND_HALF_UP))+" %")  as String, "")
            break
        case "Additional Revenue":
            revenue = Math.abs(api.local.finalProfit?.sum())?.round()
            api.setPricegridCalculationOutput(pgId, (it as String), (it as String),(revenue+" USD")  as String, "")
            break
        case "Additional Revenue %":
            addMargin =(api.local.finalProfit?.sum()/totals?.getAt("TotalRevenue"))?.setScale(4,BigDecimal.ROUND_HALF_UP)
            api.setPricegridCalculationOutput(pgId, (it as String), (it as String),(((addMargin*100)?.setScale(2,BigDecimal.ROUND_HALF_UP))+" %")  as String, "")
            break
        case "New Margin":
            newMargin = ((((totals?.getAt("TotalRevenue")+ revenue)- (totals?.getAt("TotalCost")))/(totals?.getAt("TotalRevenue")+revenue)))?.setScale(4,BigDecimal.ROUND_HALF_UP)
            api.setPricegridCalculationOutput(pgId, (it as String), (it as String),(((newMargin*100)?.setScale(2,BigDecimal.ROUND_HALF_UP))+" %")  as String, "")
            break
        case "Margin change":
            marginDiff = (newMargin - margin)?.setScale(4,BigDecimal.ROUND_HALF_UP)
            precentMargin = marginDiff < 0 ? +((marginDiff*100)?.setScale(2,BigDecimal.ROUND_HALF_UP))+" %  ↓ ":((marginDiff*100)?.setScale(2,BigDecimal.ROUND_HALF_UP))+" %  ↑"
            api.setPricegridCalculationOutput(pgId, (it as String), ("Margin Change"),(precentMargin)  as String, "")
            break
    }
}
return
/*Date previousMonthPrice = api.targetDate()
pastDate = previousMonthPrice?.minus(365)

List dataSourceFilter = [
        Filter.in("MaterialNumber", products),
        Filter.greaterOrEqual("Createondate", pastDate),
]

ctx = api.getDatamartContext()
dm = ctx.getDatamart("Transaction")
def query = ctx?.newQuery(dm)
        ?.select("MaterialNumber", "MaterialNumber")
        ?.select("Description", "Description")
        ?.select("SUM(NetValueinUSD)", "NetValueinUSD")
        ?.orderBy("NetValueinUSD DESC")
        ?.where(*dataSourceFilter)

api.local.transactionRecords = ctx?.executeQuery(query)?.data?.toResultMatrix()?.getAt("entries")?.collectEntries {
    [(it.MaterialNumber): [price: it.NetValueinUSD, description: it.Description]
    ]
}*//*


api.setPricegridCalculationOutput(pgId, "Material", "Material ", "12M Trailing Revenue", "")
label = ""
api.local.transactionRecords?.each { it ->
    label = (it.value?.description) ? ("-" + it.value?.description as String) : ""
    api.setPricegridCalculationOutput(pgId, (it.key as String), ((it.key + label) as String), (it.value?.price as String), "")
}
return*/
