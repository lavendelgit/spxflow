List<Object> getLPGLineItems(def lpgId) {
    def result = []
    int i = 0
    int s = api.getMaxFindResultsLimit()
    while (true) {
        def items = api.find("PGI", i, s, null, Filter.equal("priceGridId", lpgId))
        result.addAll(items)
        i += s
        if (items.size() < s) {
            break
        }
    }
    return result
}

BigDecimal getPercentageRatio(BigDecimal value, BigDecimal percentageBase) {
    return value != null && percentageBase ? 100 * value / percentageBase : null
}