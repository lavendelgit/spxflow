// price grid id
Long pgId = out.ID
// raw line items
List<Object> rawLineItems = Lib.getLPGLineItems(pgId)

List<java.util.Map> lineItems = []
rawLineItems.each {
    java.util.Map lineItem = [
            product    : it.sku,
            productName: it.label,

    ]
    lineItem << it.allCalculationResults.collectEntries { [(it.resultName): it.result] }
    lineItems << lineItem
}

// create inputs
List<String> products = lineItems?.collect { it.product + ":" + it.productName }?.unique()?.sort()

api.global.lineItems = lineItems
api.global.products = products


products = api.global.lineItems?.product
/*
Integer days = ((api.findLookupTableValues("DataMartLookupConfiguration",Filter.equal("name","TTM-Month"))?.getAt(0)?.attribute1) as BigDecimal) * 30
Date previousMonthPrice = api.targetDate()
pastDate = previousMonthPrice?.minus(days)

List dataSourceFilter = [
        Filter.in("MaterialNumber", products),
        Filter.greaterOrEqual("Createondate", pastDate),
]

ctx = api.getDatamartContext()
dm = ctx.getDatamart("Transaction")
def query = ctx?.newQuery(dm)
        ?.select("MaterialNumber", "MaterialNumber")
        ?.select("Description", "Description")
        ?.select("SUM(NetValueinUSD)", "NetValueinUSD")
        ?.select("OrderQuantity","OrderQuantity")
        ?.orderBy("NetValueinUSD DESC")
        ?.where(*dataSourceFilter)

api.local.transactionRecords = ctx?.executeQuery(query)?.data?.toResultMatrix()?.getAt("entries")?.collectEntries {
    [(it.MaterialNumber?:""): [price: it.NetValueinUSD, description: it.Description, quanitiy :it.OrderQuantity]
    ]
}
*/

api.option("ValueFormat", ["Absolute","Percentage"])

return
