// price grid id
Long pgId = out.ID
// lineItems
//List<java.util.Map> lineItems = api.global.lineItems


java.util.Map priceGridData = api.find("PG", 0, 1, null, Filter.equal("id", pgId))?.get(0)
java.util.Map headerInputs = api.jsonDecode(priceGridData?.configuration)?.headerInputs?.collectEntries {
    [(it.name): it.value]
}?.findAll {
    it.value != null
}


// if product is not selected, displaying the first one
String configuration = headerInputs?.ValueFormat?:"Absolute"
String format =headerInputs?.ValueFormat?(headerInputs?.ValueFormat=="Absolute"?"USD":"%"):"USD"
String toolTip =headerInputs?.ValueFormat?(headerInputs?.ValueFormat=="Absolute"?'<b>{point.y:,.0f} {point.suffix}</b> ':'<b>{point.y:,.2f} {point.suffix}</b> '):'<b>{point.y:,.0f} {point.suffix}</b> '
Integer MinValue = headerInputs?.ValueFormat?(headerInputs?.ValueFormat=="Absolute"?1000:75):1000

filteredLineItems = api.global.lineItems
api.local.quantity = Math?.round(filteredLineItems?.TTMQ?.grep()?.sum()?:0)
//BigDecimal quantity = filteredLineItems?.TrailingQuantity?.sum()?:1
/*products?.each { product ->
    sku = (product?.split(':')?.getAt(0) as String)
    filteredLineItems << lineItems.find { it.product == sku }
}*/



// filter lineitems based on product
//List<java.util.Map> filteredLineItems = lineItems.findAll { it.product == (product?.split('-')?.getAt(0) as String )}
pricePoints = ["TrailingRevenue", "MarginProofPrice","ABPriceChange", "VelocityBucketBasedPrice", "SurchargePrice","OverrideValue", "StandardCostInUSD", "Profit"]
// prepare chart structure

chartData = [:]
pricePoints?.each {it->
    chartData[it] = []
}
BigDecimal baseValue = Math?.round(filteredLineItems?.TTM?.grep()?.sum()?:0)
BigDecimal baseValueinUSD = Math?.round(filteredLineItems?.SAPCurrentPriceUSD?.grep()?.sum()?:0)

// collect chart values
costValue = 0
TQ = 0
overrideValue = 0
api.local.maps = [:]
    pricePoints?.each { it ->
        costValue = 0
        TQ = 0
        switch (it) {
            case "TrailingRevenue":
                filteredLineItems?.each{records ->
                    api.local.maps.putAt(records?.product+"Revenue",records?.TTM)
                }
                chartData[it] = Math?.round(baseValue)
                api.local.maps.putAt("TotalRevenue",Math?.round(baseValue))
                break
            case "StandardCostInUSD":
                filteredLineItems?.each{records ->
                    TQ = records?.StandardCostInUSD?(records?.StandardCostInUSD * (records?.TTMQ?:0)):0
                    costValue = costValue+(TQ?:0)
                }
                chartData[it] = costValue

                api.local.maps.putAt("TotalCost",costValue)
                break
            case "MarginProofPrice":
                filteredLineItems?.each{records ->
                    TQ = records?.MarginProofPrice?(((records?.MarginProofPrice - (records?.SAPCurrentPriceUSD?:0))/(records?.SAPCurrentPriceUSD?:1))*(records?.TTM?:1)):0
                    //api.trace("TQ",TQ)
                    costValue = costValue+(TQ?:0)
                    api.local.maps.putAt(records?.product+"MarginImpact",TQ)
                }
                chartData[it] = costValue
                api.local.maps.putAt("TotalMarginImpact",costValue)
                break
            case "ABPriceChange":
                filteredLineItems?.each{records ->
                    TQ = records?.CategoryPercentIncrease?(records?.CategoryPercentIncrease * (((api.local.maps?.getAt(records?.product+"MarginImpact")?:0)+(records?.TTM?:0))?:1)):0
                    costValue = costValue+(TQ?:0)
                    api.local.maps.putAt(records?.product+"ABImpact",TQ)
                }
                chartData[it] = costValue
                api.local.maps.putAt("TotalABImpact",costValue)
                break
            case "VelocityBucketBasedPrice":
                filteredLineItems?.each{records ->
                    TQ = records?.VelocityBucketBasedPrice?(records?.VelocityBucketBasedPrice/(records?.ABPriceChange?:records?.MarginProofPrice)-1) * (((api.local.maps?.getAt(records?.product+"MarginImpact")?:0)+(api.local.maps?.getAt(records?.product+"ABImpact")?:0)+(records?.TTM?:0))?:1):0
                    costValue = costValue+(TQ?:0)
                    api.local.maps.putAt(records?.product+"VelocityImpact",TQ)
                }
                chartData[it] = costValue
                api.local.maps.putAt("TotalVelocityImpact",costValue)
                break
            case "SurchargePrice":
                filteredLineItems?.each{records ->
                    TQ = 0
                    if(records?.SurchargePrice){
                        TQ = ((records?.SurchargePrice?(records?.SurchargePrice/(records?.VelocityBucketBasedPrice?:records?.ABPriceChange)):1) - 1) * (((api.local.maps?.getAt(records?.product+"MarginImpact")?:0)+(api.local.maps?.getAt(records?.product+"ABImpact")?:0)+(records?.TTM?:0)+(api.local.maps?.getAt(records?.product+"VelocityImpact")?:0))?:1)
                    }
                     costValue = costValue+(TQ?:0)
                    api.local.maps.putAt(records?.product+"SurchargeImpact",TQ)
                }
                chartData[it] = costValue
                api.local.maps.putAt("TotalSurchargeImpact",costValue)
                break
            case "OverrideValue":
                filteredLineItems?.each{records ->
                    TQ = 0
                    if(records?.OverrideOptionValue!=""&&records?.OverrideOptionValue){
                        TQ = records?.OverrideOptionValue!=""?(records?.NewListPriceUSD/((records?.SurchargePrice?:(records?.VelocityBucketBasedPrice?:records?.ABPriceChange?:1)) - 1)?:1) * (((api.local.maps?.getAt(records?.product+"MarginImpact")?:0)+(api.local.maps?.getAt(records?.product+"ABImpact")?:0)+(records?.TTM?:0)+(api.local.maps?.getAt(records?.product+"VelocityImpact")?:0)+(api.local.maps?.getAt(records?.product+"SurchargeImpact")?:0))?:1):0

                    }
                     costValue = costValue+(TQ?:0)
                    api.local.maps.putAt(records?.product+"OverrideImpact",TQ)
                }
                chartData[it] = costValue
                api.local.maps.putAt("TotalOverrideImpact",costValue)
                break
        }
    }

api.local.finalProfit = []
def data = []
BigDecimal value = 0
api.local.previousValue = baseValue

chartData?.each { it ->
    if(it.key!="Profit"&&it.key !="TrailingRevenue"&& it.key!="StandardCostInUSD"){
        value = getPercentageRatio(((it.value?:0) as BigDecimal),baseValue,baseValueinUSD,configuration,(it.key as String))
    }
    switch (it.key) {
        case "MarginProofPrice":
            data << [
                    name : 'MIN - MAX Margin Impact',
                    y    : value?:0,
                    color: '#c2bd5b',
                    suffix : format,
            ]
            break
        case "ABPriceChange":
            data << [
                    name : '80 - 20 Impact',
                    y    :  value?:0,
                    color: '#95c25b',
                    suffix : format,
            ]
            break
        case "VelocityBucketBasedPrice":
            data << [
                    name : 'Velocity Impact',
                    y    : value?:0,
                    color: '#2dcf48',
                    suffix : format,
            ]
            break
        case "SurchargePrice":
            data << [
                    name : 'Surcharge Impact',
                    y    : value?:0,
                    color: '#5d8f65',
                    suffix : format,
            ]
            break
        case "OverrideValue":
            data << [
                    name : 'Override',
                    y    :(value)?:0,
                    color: '#bd0d1f',
                    suffix : format,
            ]
            break
        case "Profit":
            data << [
                    name : 'Additional Revenue',
                    isSum: true,
                    //y    :  Math.abs(api.local.finalProfit?.sum()),//(chartData?.getAt("StandardCostInUSD") && chartData?.getAt("NewListPriceUSD")) ? (chartData?.getAt("NewListPriceUSD")) - (chartData?.getAt("StandardCostInUSD")):0,
                    color: '#f5930a',
                    suffix : format,
            ]
            break
    }
}

def definition = [
        chart   : [
                type: 'waterfall'
        ],

        title   : [
                text: 'Pricing Points  '
        ],

        subtitle: [
                text: ''
        ],

        xAxis   : [
                type: 'category', //when type is 'category', series.data.name will become X-axis value
        ],

        yAxis   : [
                title: [
                        enabled: false
                ],

        ],

        legend  : [
                enabled: false
        ],

        tooltip : [
                pointFormat: toolTip
        ],

        series  : [
                [
                        data        : data,
                        dataLabels  : [
                                enabled: true,
                                format : toolTip,
                                style  : [
                                        fontWeight: 'bold'
                                ]
                        ],
                        pointPadding: 0
                ]
        ],
        credits : [
                enabled: false
        ]
]

api.setPricegridCalculationChart(definition, pgId)
return

BigDecimal getPercentageRatio(BigDecimal value, BigDecimal TTR,BigDecimal percentageBase, String configuration, String pricePoints) {
    precentage = 0
    if(configuration=="Absolute"){
        BigDecimal percentage = 0
        percentage =  (value / TTR )
        api.local.previousValue = value? api.local.previousValue+value:(api.local.previousValue)
        precentage = (((percentage))?.setScale(4,BigDecimal.ROUND_HALF_UP))*100
        api.local.maps.putAt("TotalPercentage"+pricePoints,precentage)
        api.local.finalProfit << value?:0
        return value
    }else{
        BigDecimal percentage = 0
        percentage =  (value / TTR )
        api.local.previousValue = value? api.local.previousValue+value:(api.local.previousValue)
        precentage = (((percentage))?.setScale(4,BigDecimal.ROUND_HALF_UP))*100
        api.local.maps.putAt("TotalPercentage"+pricePoints,precentage)
        api.local.finalProfit << value?:0
        return precentage
    }
}
