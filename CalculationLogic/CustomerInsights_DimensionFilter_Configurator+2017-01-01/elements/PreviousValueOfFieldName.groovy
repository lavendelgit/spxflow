import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry(InputType.HIDDEN, "PreviousFieldNameValue")
String fieldNamValue = api.input("FieldName")
configuratorEntry.getFirstInput().setValue(fieldNamValue)

return configuratorEntry