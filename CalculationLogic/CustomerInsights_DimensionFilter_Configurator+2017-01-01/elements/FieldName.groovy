import net.pricefx.server.dto.calculation.ConfiguratorEntry

Map configuration = api.getElement("Configuration")
Map dimensionFilter = libs.CustomerInsights.Constant.DIMENSION_FILTER_CONFIG

Map optionItem = getOptionItems(configuration)
List optionValues = optionItem.keySet().collect()

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry(InputType.OPTION, "FieldName")
configuratorEntry.getFirstInput().setParameterConfig([labels: optionItem])
configuratorEntry.getFirstInput().setValueOptions(optionValues)
configuratorEntry.getFirstInput().setLabel(dimensionFilter.FIELD_NAME_LABEL)

return configuratorEntry

protected Map getOptionItems(Map configuration) {
  String sourceType = configuration.SourceType
  String sourceName = configuration.SourceName
  String owner = libs.CustomerInsights.Constant.DEFAULT_VALUE.SIMULATION_OWNER //owner default of simulation

  return libs.CustomerInsights.Configuration.getDatasourceFields(sourceType, sourceName, owner, true)
}