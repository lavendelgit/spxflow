import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ContextParameter

Map configuration = api.getElement("Configuration")
String fieldNameValue = api.input("FieldName")
String previousFieldNameValue = api.input("PreviousFieldNameValue")
ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry(InputType.OPTION, "Value")

ContextParameter valueField = configuratorEntry?.getFirstInput()
valueField.setLabel(libs.CustomerInsights.Constant.DIMENSION_FILTER_CONFIG.FIELD_VALUE_LABEL)
setValueOption(configuration, valueField, fieldNameValue, previousFieldNameValue)

return configuratorEntry

protected void setValueOption(Map configuration, def option, String currentValue, String previousValue) {
  String sourceType = configuration.SourceType
  String sourceName = configuration.SourceName

  if (currentValue) {
    List fieldValues = libs.CustomerInsights.Configuration.getDistinctValueOfField(sourceType, sourceName, currentValue)
    if (previousValue != currentValue) {
      option.setValue(null)
    }
    if (!option.getValue()) {
      option.setRequired(true)
    }
    option.setValueOptions(fieldValues?.findAll())

  } else {
    option.setValue(null)
  }
}