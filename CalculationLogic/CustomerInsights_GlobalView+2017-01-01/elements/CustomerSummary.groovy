Map configuration = api.getElement("Configuration")
def customers = api.getElement("Customers")
def customerGlobalViewParams = api.getElement("CreateDashboardParams")
def portletUtils = libs.CustomerInsights.SummaryPortletUtils

Map customerInfo = libs.CustomerInsights.CustomerGlobalDashboardUtils.getSummaryInfoForCustomerGlobalView(configuration, customerGlobalViewParams)
Map opportunities = portletUtils.getSummaryOpportunities(api.local.CUSTOMER_DATA)
List tableData = setSummaryDataToTable(configuration, customerGlobalViewParams, customers, customerInfo, opportunities)

return portletUtils.createControllerWithHtmlTable(configuration, tableData)

/**
 * generate customer Name
 * If customer is selected but no data found → display Customers: “N/A” (null).
 * If no customer selected, but data found → display Customers: empty
 * @param customerData
 * @param customers
 * @return
 */
protected String generateCustomerName(List customerData, def customers) {
  String customerName
  if (customerData?.size() > 0) {
    customerName = customers ? libs.CustomerInsights.SummaryPortletUtils.makeMultiCustomerLabel(customerData) : ""
  } else {
    customerName = customers ? null : ""
  }

  return customerName
}
/**
 * collect item into a list to build summary data table
 * @param configuration
 * @param customerGlobalViewParams
 * @param customers
 * @param customerInfo
 * @param opportunities
 * @return
 */
protected List setSummaryDataToTable(Map configuration, Map customerGlobalViewParams, def customers, Map customerInfo, Map opportunities) {
  def inputUtils = libs.CustomerInsights.InputUtils
  def portletUtils = libs.CustomerInsights.SummaryPortletUtils
  Map summaryLabels = libs.CustomerInsights.Constant.SUMMARY_PORTLET_LABEL
  //general section
  List tableData = []
  Map dimensionLabel = inputUtils.getDimensionFilterLabel(configuration, customerGlobalViewParams.getDimension())
  tableData.add(portletUtils.initTextItem(summaryLabels.CUSTOMER.PLURAL, generateCustomerName(api.local.CUSTOMER_DATA, customers)))
  tableData.add(portletUtils.initTextItem("", ""))
  tableData.add(portletUtils.initTextItem(dimensionLabel.fieldLabel, dimensionLabel.fieldValue))
  tableData.add(portletUtils.initHealthScoreItem(summaryLabels.HEALTH_SCORE, customerInfo.customerHealthScore))
  //column-01
  tableData.add(portletUtils.initTrendItem(summaryLabels.REVENUE_TREND_YTD, customerInfo.customerYTDRevenueTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.MARGIN_TREND_YTD, customerInfo.customerYTDMarginTrend))
  tableData.add(portletUtils.initTrendItem(summaryLabels.QUANTITY_TREND_YTD, customerInfo.customerYTDQuantityTrend))
  //column-02
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_REVENUE, customerInfo.revenue))
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_MARGIN, customerInfo.margin))
  tableData.add(portletUtils.initNumberItem(summaryLabels.TOTAL_QUANTITY, customerInfo.quantity))
  //column-03
  tableData.add(portletUtils.initNumberItem(summaryLabels.PRICING_OPPORTUNITY, opportunities.pricingOpportunity))
  tableData.add(portletUtils.initNumberItem(summaryLabels.SELLING_OPPORTUNITY, opportunities.sellingOpportunity))
  tableData.add(portletUtils.initNumberItem(summaryLabels.OPPORTUNITY, opportunities.opportunity))

  return tableData
}