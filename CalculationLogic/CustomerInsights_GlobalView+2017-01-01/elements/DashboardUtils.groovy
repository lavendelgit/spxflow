import net.pricefx.server.dto.calculation.ResultHighchart
/**
 * Open one dashboard in a new tab
 * @param customerGlobalViewParams
 * @param eventName
 * @param dashboardName
 * @return
 */
def openDashboard(Map customerGlobalViewParams, String eventName, String dashboardName) {
  Map inputEntry = libs.CustomerInsights.Constant.INPUT_ENTRY
  Map configDefaultValue = libs.CustomerInsights.Constant.DEFAULT_VALUE
  String timeFilterName = inputEntry.TIME_FILTER.NAME
  String event = api.dashboardWideEvent(eventName)

  return api.dashboard(dashboardName)
      .setParam(timeFilterName, customerGlobalViewParams.getTimeFilter())
      .openInTabOrRecalculateOn(event)
      .withEventDataAttr(configDefaultValue.EVENT_DATA_ATTRIBUTE_NAME).asParam(configDefaultValue.EVENT_DATA_PARAMETER_NAME)
}
/**
 * create data chart for Customer Classification Charts
 * @param groupedClassificationData
 * @return
 */
List createCustomerClassificationDataChart(Map groupedClassificationData) {
  List groupedClassificationDataItem
  Map newItem

  return groupedClassificationData?.collect {
    groupedClassificationDataItem = it.value
    newItem = summarizeData(groupedClassificationDataItem) ?: [:]

    return [name         : "Class " + it.key,
            y            : groupedClassificationDataItem?.size(),
            revenue      : newItem.revenue,
            margin       : newItem.margin,
            quantity     : newItem.quantity,
            marginPercent: newItem.marginPercent]
  }
}

protected Map summarizeData(List data) {
  BigDecimal revenue = BigDecimal.ZERO, margin = BigDecimal.ZERO, quantity = BigDecimal.ZERO, marginPercent
  for (item in data) {
    revenue += (item.revenue ?: BigDecimal.ZERO)
    margin += (item.margin ?: BigDecimal.ZERO)
    quantity += (item.quantity ?: BigDecimal.ZERO)
  }
  marginPercent = revenue ? (100 * margin / revenue) : BigDecimal.ZERO

  return [revenue      : revenue,
          quantity     : quantity,
          margin       : margin,
          marginPercent: marginPercent]
}

ResultHighchart buildCustomerClassificationChart(List chartData, String chartTitle, Map customerGlobalViewParams) {
  def dateUtils = libs.CustomerInsights.DateUtils
  String startDateString = dateUtils.formatDateToString(customerGlobalViewParams.getStartDate())
  String endDateString = dateUtils.formatDateToString(customerGlobalViewParams.getEndDate())
  String currencySymbol = customerGlobalViewParams.getCurrency().currencySymbol
  def hLib = libs.HighchartsLibrary
  Map legend = hLib.Legend.newLegend().setEnabled(false)

  Map chartSeriesDataLabels = hLib.DataLabel.newDataLabel().setEnabled(true)
      .setFormat("<b>{point.name}: {point.y:,.0f} customers</b>")

  Map chartSeriesTooltip = hLib.Tooltip.newTooltip()
      .setUseHTML(true)
      .setHeaderFormat("")
      .setPointFormat("""<b>{point.name}</b><br>Customer Count: {point.y}<br>Revenue: {point.revenue:,.0f} $currencySymbol<br>
                                     <br>Quantity: {point.quantity:,.0f} <br>Avg. Margin %: {point.marginPercent:,.2f} %""")

  Map chartSeries = hLib.PieSeries.newPieSeries().setName("Customer Count")
      .setData(chartData)
      .setTooltip(chartSeriesTooltip)
      .setDataLabels(chartSeriesDataLabels)

  Map chartDef = hLib.Chart.newChart().setTitle(chartTitle)
      .setSubtitle("From ${startDateString} To ${endDateString}")
      .setSeries(chartSeries)
      .setLegend(legend)
      .getHighchartFormatDefinition()

  return api.buildHighchart(chartDef)
}

Map createDetailViewEmbeddedDashboardInfo() {
  def constant = libs.CustomerInsights.Constant

  return libs.CustomerInsights.DataTable.buildEmbeddedDashboardInfo(constant.CUSTOMER_DETAIL_VIEW_DASHBOARD.EVENT_NAME,
      constant.COMMON_COLUMN_LABELS.CUSTOMER_ID,
      constant.DEFAULT_VALUE.EVENT_DATA_ATTRIBUTE_NAME)
}

Map createProductPortfolioEmbeddedDashboardInfo() {
  def constant = libs.CustomerInsights.Constant

  return libs.CustomerInsights.DataTable.buildEmbeddedDashboardInfo(constant.CUSTOMER_PRODUCT_PORTFOLIO_DASHBOARD.EVENT_NAME,
      constant.COMMON_COLUMN_LABELS.CUSTOMER_ID,
      constant.DEFAULT_VALUE.EVENT_DATA_ATTRIBUTE_NAME)
}