/**
 * Calculate Customer Health Score and Customer Classification based on last 12 months
 * Using api.local to pass value to other element -> improve performance
 */
Map configuration = api.getElement("Configuration")
Map customerGlobalViewParams = api.getElement("CreateDashboardParams")
api.local.CUSTOMER_DATA = libs.CustomerInsights.CustomerGlobalDashboardUtils.calculateMetricsPerCustomer(configuration, customerGlobalViewParams)

return null