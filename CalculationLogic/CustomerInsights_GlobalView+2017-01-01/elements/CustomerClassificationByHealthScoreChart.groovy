Map customerGlobalViewParams = api.getElement("CreateDashboardParams")
Map processedData = api.local.TOP_AND_WORST
List classificationData = processedData.all
Map groupedClassificationData = classificationData?.groupBy { it.customerClassificationByHealthScore }
List chartData = DashboardUtils.createCustomerClassificationDataChart(groupedClassificationData)
String chartTitle = libs.CustomerInsights.Constant.CHART_TITLE.customerClassificationByHealthScore

return DashboardUtils.buildCustomerClassificationChart(chartData, chartTitle, customerGlobalViewParams)