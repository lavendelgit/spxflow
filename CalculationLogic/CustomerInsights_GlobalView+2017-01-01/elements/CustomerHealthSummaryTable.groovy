Map configuration = api.getElement("Configuration")
Map processedData = api.local.TOP_AND_WORST
Map metaData = initMetaData(configuration, processedData.top, processedData.worst)

return libs.CustomerInsights.DataTable.generateDataTable(metaData)

protected Map initMetaData(Map configuration, List topData, List worstData) {
  def dataTable = libs.CustomerInsights.DataTable

  return dataTable.newTableMetaData()
      .setTableStructure(createTableStructure(configuration.CONFIGURATION))
      .setTablePreferenceName("CustomerHealthSummary")
      .setEmbeddedDashboardInfomation(DashboardUtils.createDetailViewEmbeddedDashboardInfo())
      .setData(dataTable.buildTopWorstData(topData, worstData))
      .getTableMetaData()
}

protected List createTableStructure(Map configurationCategory) {
  def dataTable = libs.CustomerInsights.DataTable
  def constant = libs.CustomerInsights.Constant
  Map columnLabels = constant.CUSTOMER_HEALTH_SCORE_TABLE_COLUMN_LABELS
  Map attributeName = constant.ENTITY_ATTRIBUTE_NAME
  List tableStructure = []

  tableStructure.add(dataTable.createCustomerNameColumnConfiguration())
  tableStructure.add(dataTable.createCustomerIdColumnConfiguration())
  tableStructure.add(dataTable.createHealthScoreColumnConfiguration(configurationCategory, columnLabels.HEALTH_SCORE, attributeName.CUSTOMER_HEALTH_SCORE))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.REVENUE, attributeName.REVENUE))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.MARGIN, attributeName.MARGIN))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.QUANTITY, attributeName.QUANTITY))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.BUYING_FREQUENCY, attributeName.BUYING_FREQUENCY))
  tableStructure.add(dataTable.createPercentColumnConfiguration(columnLabels.MARGIN_PERCENT, attributeName.AVERAGE_MARGIN_PERCENT))

  return tableStructure
}