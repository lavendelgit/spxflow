/**
 * Gets top/worst customer to show in tables
 * Using api.local to pass value to other element -> improve performance
 */
List customerData = api.local.CUSTOMER_DATA
Integer topCustomer = api.getElement("CreateDashboardParams").getTopCustomer()
api.local.TOP_AND_WORST = libs.CustomerInsights.DataTable.getTopAndWorst(customerData, topCustomer)

return null