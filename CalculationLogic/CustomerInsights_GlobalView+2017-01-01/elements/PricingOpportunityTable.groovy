Map processedData = api.local.TOP_AND_WORST_PRICING_OPPORTUNITY
Map metaData = initMetaData(processedData.top)

return libs.CustomerInsights.DataTable.generateDataTable(metaData)

protected Map initMetaData(List boldData) {
  def dataTable = libs.CustomerInsights.DataTable

  return dataTable.newTableMetaData()
      .setTableStructure(createTableStructure())
      .setTablePreferenceName("CustomerPricingOpportunity")
      .setEmbeddedDashboardInfomation(DashboardUtils.createProductPortfolioEmbeddedDashboardInfo())
      .setData(dataTable.buildBoldData(boldData))
      .getTableMetaData()
}

protected List createTableStructure() {
  def dataTable = libs.CustomerInsights.DataTable
  def constant = libs.CustomerInsights.Constant
  Map columnLabels = constant.PRICING_OPPORTUNITY_TABLE_COLUMN_LABELS
  Map attributeName = constant.ENTITY_ATTRIBUTE_NAME
  List tableStructure = []
  tableStructure.add(dataTable.createCustomerNameColumnConfiguration(true))
  tableStructure.add(dataTable.createCustomerIdColumnConfiguration())
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.REVENUE_BELOW_TARGET, attributeName.REVENUE_BELOW_TARGET))
  tableStructure.add(dataTable.createPercentColumnConfiguration(columnLabels.REVENUE_BELOW_TARGET_PERCENT, attributeName.REVENUE_BELOW_TARGET_PERCENT))
  tableStructure.add(dataTable.createPercentColumnConfiguration(columnLabels.PRODUCT_BUYING_PERCENT, attributeName.PRODUCT_BUYING_PERCENT))

  return tableStructure
}