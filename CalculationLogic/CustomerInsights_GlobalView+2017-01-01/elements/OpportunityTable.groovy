Map processedData = api.local.TOP_AND_WORST_OPPORTUNITY
Map metaData = initMetaData(processedData.top)

return libs.CustomerInsights.DataTable.generateDataTable(metaData)

protected Map initMetaData(List boldData) {
  def dataTable = libs.CustomerInsights.DataTable

  return dataTable.newTableMetaData()
      .setTableStructure(createTableStructure())
      .setTablePreferenceName("CustomerOpportunity")
      .setEmbeddedDashboardInfomation(DashboardUtils.createProductPortfolioEmbeddedDashboardInfo())
      .setData(dataTable.buildBoldData(boldData))
      .getTableMetaData()
}

protected List createTableStructure() {
  def dataTable = libs.CustomerInsights.DataTable
  def constant = libs.CustomerInsights.Constant
  Map columnLabels = constant.SELLING_OPPORTUNITY_TABLE_COLUMN_LABELS
  Map attributeName = constant.ENTITY_ATTRIBUTE_NAME
  List tableStructure = []
  tableStructure.add(dataTable.createCustomerNameColumnConfiguration(true))
  tableStructure.add(dataTable.createCustomerIdColumnConfiguration())
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.CROSS_SELL, attributeName.CROSS_SELL))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.UP_SELL, attributeName.UP_SELL))
  tableStructure.add(dataTable.createNumberColumnConfiguration(columnLabels.OPPORTUNITY, attributeName.OPPORTUNITY))

  return tableStructure
}