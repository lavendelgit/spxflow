List rawData = api.local.CUSTOMER_DATA
List sortedData = rawData?.sort(false, { a, b -> (b.revenueBelowTarget) <=> (a.revenueBelowTarget) })
Integer topCustomer = api.getElement("CreateDashboardParams").getTopCustomer()
api.local.TOP_AND_WORST_PRICING_OPPORTUNITY = libs.CustomerInsights.DataTable.getTopAndWorst(sortedData, topCustomer)

return null