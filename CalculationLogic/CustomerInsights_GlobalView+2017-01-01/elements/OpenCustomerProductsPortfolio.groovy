Map customerGlobalViewParams = api.getElement("CreateDashboardParams")
def constant = libs.CustomerInsights.Constant

return DashboardUtils.openDashboard(customerGlobalViewParams,
    constant.CUSTOMER_PRODUCT_PORTFOLIO_DASHBOARD.EVENT_NAME,
    constant.CUSTOMER_PRODUCT_PORTFOLIO_DASHBOARD.NAME)