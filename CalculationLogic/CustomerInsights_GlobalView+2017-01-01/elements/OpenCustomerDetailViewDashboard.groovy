Map customerGlobalViewParams = api.getElement("CreateDashboardParams")
def constant = libs.CustomerInsights.Constant

return DashboardUtils.openDashboard(customerGlobalViewParams,
    constant.CUSTOMER_DETAIL_VIEW_DASHBOARD.EVENT_NAME,
    constant.CUSTOMER_DETAIL_VIEW_DASHBOARD.NAME)