def customers = api.getElement("Customers")
def timeFilter = api.getElement("TimeFilter")
Integer topCustomer = api.getElement("TopCustomers") as Integer
String kpi = api.getElement("KPI")
def dimension = api.getElement("DimensionFilter")
Map currentPeriod = api.getElement("CurrentPeriod")
Map currency = api.getElement("Currency")
List customerClassByRevenues = api.getElement("CustomerClassByRevenueFilter")
List customerClassByHealthScores = api.getElement("CustomerClassByHealthScoreFilter")
Date currentDate = new Date()

return libs.CustomerInsights.GlobalViewDashboardParameter.newGlobalViewDashboardParam(customers,
    timeFilter,
    kpi,
    topCustomer,
    currency,
    dimension,
    currentPeriod,
    currentDate,
    customerClassByRevenues,
    customerClassByHealthScores)