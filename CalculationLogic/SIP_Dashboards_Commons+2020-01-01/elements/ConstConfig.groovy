import groovy.transform.Field
import net.pricefx.common.api.FieldFormatType

/**
 * Stores the name of the PP table used for retrieving the currency symbols
 * Structure of an entry is:
 * [name : "EUR",
 *  value: "€"]
 */
@Field CURRENCY_SYMBOL_PP_NAME = "CurrencySymbols"

/**
 * Stores the configuration of the PP table responsible for storing the user defined default filters for all the dashboards.
 */
@Field DASHBOARDS_DEFAULT_FILTERS_PP_CONFIG = [NAME         : "SIP_DefaultFilterValues",
                                               COLUMN_CONFIG: [FILTER_NAME : "key1",
                                                               USER_NAME   : "key2",
                                                               FILTER_VALUE: "attributeExtension___FilterValue"]]

/**
 * Name of the Advanced Configuration for the package, contains all DM field mappings.
 */
@Field DASHBOARDS_ADVANCED_CONFIGURATION_NAME = "SIP_AdvancedConfiguration"

/**
 * Name of the Advanced Configuration for the SIP_Dashboards_Commons, contains information about the ccy DS.
 */
@Field COMMONS_ADVANCED_CONFIGURATION_NAME = "SIP_Commons_AdvancedConfiguration"

/**
 * Defines the key of Empty dimension used in Causality dashboards.
 * Allows to aggregate over only one product or customer dimension.
 */
@Field EMPTY_DIMENSION_KEY = "none"

/**
 * Defines the value of Empty dimension used in Causality dashboards.
 * Allows to aggregate over only one product or customer dimension.
 */
@Field EMPTY_DIMENSION_VALUE = "None"

/**
 * Defines the value of an entry that is marked as "used" in PP tables.
 * Used mainly in Regional Revenue and Margin Dashboard
 */
@Field IS_USED_VALUE = "Yes"

/**
 * Provides an EMPTY_DIMENSION map that is used in the QueryUtils getNameLabelPairs method to retrieve available aggregation dimensions
 */
@Field EMPTY_DIMENSION = [(EMPTY_DIMENSION_KEY): EMPTY_DIMENSION_VALUE]

/**
 * Defines the key used to describe the margin contribution KPI in various dashboards.
 */
@Field MARGIN_CONTRIBUTION_PERCENTAGE_KPI_KEY = "marginContributionPercentage"

/**
 * Defines the key used to describe the revenue contribution KPI in various dashboards.
 */
@Field REVENUE_CONTRIBUTION_PERCENTAGE_KPI_KEY = "revenueContributionPercentage"

/**
 * Defines the key used to describe the item name KPI in various dashboards.
 */
@Field NAME_KPI_KEY = "itemName"

/**
 * Defines the key used to describe the item number (or ID) KPI in various dashboards.
 */
@Field ITEM_NUMBER_KPI_KEY = "itemNumber"

/**
 * Defines the key used to describe the revenue KPI in various dashboards.
 */
@Field REVENUE_KPI_KEY = "revenue"

/**
 * Defines the key used to describe the margin percentage KPI in various dashboards.
 */
@Field MARGIN_PERCENTAGE_KPI_KEY = "marginPercentage"

/**
 * Defines the key used to describe the volume KPI in various dashboards.
 */
@Field VOLUME_KPI_KEY = "volume"

/**
 * Defines the key used to describe the margin KPI in various dashboards.
 */
@Field MARGIN_KPI_KEY = "margin"

/**
 * Defines the key used to describe the KPI in Outliers Dashboard.
 */
@Field KPI_KEY = "kpi"

/**
 * Defines the key used to describe the bucket in Outliers Dashboard.
 */
@Field BUCKET_KEY = "bucket"


/**
 * Defines the key used to describe the min max calculation model in Outliers Dashboard.
 */
@Field MAX_MIN_SPLIT_MODEL_KEY = "minMaxSplit"

/**
 * Defines the key used to describe the equal split calculation model in Outliers Dashboard.
 */
@Field EQUAL_SPLIT_MODEL_KEY = "equalSplit"

/**
 * Defines the key used to describe the contribution calculation model in Outliers Dashboard.
 */
@Field CONTRIBUTION_MODEL_KEY = "contribution"

/**
 * Holds the configuration for the Outliers Dashboard configurator logic.
 */
@Field OUTLIERS_CONFIGURATOR_CONFIG = [NAME: "Configurator_Outliers"]

/**
 * Holds all configuration of the Outliers Dashboard.
 * The configuration contains
 * - FIELDS - definition of all KPI used by the dashboard and their formats
 * - BASE_FIELDS - defines which fields are used in both of the Performance matrices
 * - PIE_CHART_DATA_COMMON_COLUMNS - defines which fields are used in both of the pie charts
 * - ADDITIONAL_DATA_PER_DIMENSION - defines additional fields to be used by pie charts and performance matrices depending on the displayed dimension
 * - MODELS - contains additional information for the used calculation models
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 */
@Field OUTLIERS_DASHBOARD_CONFIG = [FIELDS                       : [(MARGIN_CONTRIBUTION_PERCENTAGE_KPI_KEY) : [LABEL        : "Margin Contribution %",
                                                                                                                RESULT_MATRIX: FieldFormatType.PERCENT],
                                                                    (REVENUE_CONTRIBUTION_PERCENTAGE_KPI_KEY): [LABEL        : "Revenue Contribution %",
                                                                                                                RESULT_MATRIX: FieldFormatType.PERCENT],
                                                                    (NAME_KPI_KEY)                           : [LABEL        : "Name",
                                                                                                                RESULT_MATRIX: null],
                                                                    (ITEM_NUMBER_KPI_KEY)                    : [LABEL        : "Number",
                                                                                                                RESULT_MATRIX: null],
                                                                    (REVENUE_KPI_KEY)                        : [LABEL        : "Revenue",
                                                                                                                RESULT_MATRIX: FieldFormatType.MONEY],
                                                                    (MARGIN_PERCENTAGE_KPI_KEY)              : [LABEL        : "Margin %",
                                                                                                                RESULT_MATRIX: FieldFormatType.PERCENT],
                                                                    (VOLUME_KPI_KEY)                         : [LABEL        : "Volume",
                                                                                                                RESULT_MATRIX: FieldFormatType.NUMERIC],
                                                                    (MARGIN_KPI_KEY)                         : [LABEL        : "Margin",
                                                                                                                RESULT_MATRIX: FieldFormatType.MONEY]],
                                    BASE_FIELDS                  : [NAME_KPI_KEY,
                                                                    ITEM_NUMBER_KPI_KEY,
                                                                    REVENUE_KPI_KEY,
                                                                    MARGIN_KPI_KEY,
                                                                    MARGIN_PERCENTAGE_KPI_KEY,
                                                                    MARGIN_CONTRIBUTION_PERCENTAGE_KPI_KEY,
                                                                    REVENUE_CONTRIBUTION_PERCENTAGE_KPI_KEY],
                                    PIE_CHART_DATA_COMMON_COLUMNS: [REVENUE_KPI_KEY,
                                                                    REVENUE_CONTRIBUTION_PERCENTAGE_KPI_KEY,
                                                                    MARGIN_KPI_KEY,
                                                                    MARGIN_PERCENTAGE_KPI_KEY,
                                                                    MARGIN_CONTRIBUTION_PERCENTAGE_KPI_KEY],
                                    ADDITIONAL_DATA_PER_DIMENSION: [PRODUCT : [VOLUME_KPI_KEY],
                                                                    CUSTOMER: []],
                                    MODELS                       : [(CONTRIBUTION_MODEL_KEY): [PP_NAME: "OutliersContributionModelThresholds"]],
                                    INPUTS                       : [PRODUCT             : [LABEL        : "Product(s)",
                                                                                           UNIQUE_KEY   : "outliersPRODUCT",
                                                                                           DEFAULT_VALUE: null],
                                                                    CUSTOMER            : [LABEL        : "Customer(s)",
                                                                                           UNIQUE_KEY   : "outliersCUSTOMER",
                                                                                           DEFAULT_VALUE: null],
                                                                    DATE_FROM           : [LABEL        : "Date From",
                                                                                           UNIQUE_KEY   : "outliersDATE_FROM",
                                                                                           DEFAULT_VALUE: null],
                                                                    DATE_TO             : [LABEL        : "Date To",
                                                                                           UNIQUE_KEY   : "outliersDATE_TO",
                                                                                           DEFAULT_VALUE: null],
                                                                    NO_OF_RESULTS       : [LABEL            : "Top Product(s)/ Customer(s)",
                                                                                           NO_CUSTOMER_LABEL: "Top Product(s)",
                                                                                           UNIQUE_KEY       : "outliersNO_OF_RESULTS",
                                                                                           VALUES           : ["5", "10", "25", "50", "100"],
                                                                                           DEFAULT_VALUE    : "5"],
                                                                    PRODUCT_AGGREGATION : [LABEL               : "Product Aggregation",
                                                                                           UNIQUE_KEY          : "outliersPRODUCT_AGGREGATION",
                                                                                           ENTRY_CATEGORY      : "Product",
                                                                                           ENTRY_CATEGORY_FIELD: "productId"],
                                                                    CUSTOMER_AGGREGATION: [LABEL               : "Customer Aggregation",
                                                                                           UNIQUE_KEY          : "outliersCUSTOMER_AGGREGATION",
                                                                                           ENTRY_CATEGORY      : "Customer",
                                                                                           ENTRY_CATEGORY_FIELD: "customerId"],
                                                                    MODELS              : [LABEL        : "Calculation Model",
                                                                                           UNIQUE_KEY   : "outliersMODELS",
                                                                                           VALUES       : [(MAX_MIN_SPLIT_MODEL_KEY): "(Max - Min) Split",
                                                                                                           (EQUAL_SPLIT_MODEL_KEY)  : "Split Equally",
                                                                                                           (CONTRIBUTION_MODEL_KEY) : "Contribution"],
                                                                                           DEFAULT_VALUE: MAX_MIN_SPLIT_MODEL_KEY],
                                                                    KPI                 : [LABEL     : "KPI",
                                                                                           UNIQUE_KEY: "outliersKPI",
                                                                                           MODELS    : [(MAX_MIN_SPLIT_MODEL_KEY): [VALUES       : [REVENUE_KPI_KEY, REVENUE_CONTRIBUTION_PERCENTAGE_KPI_KEY, MARGIN_KPI_KEY, MARGIN_PERCENTAGE_KPI_KEY, MARGIN_CONTRIBUTION_PERCENTAGE_KPI_KEY],
                                                                                                                                    DEFAULT_VALUE: REVENUE_KPI_KEY],
                                                                                                        (EQUAL_SPLIT_MODEL_KEY)  : [VALUES       : [REVENUE_KPI_KEY, MARGIN_KPI_KEY],
                                                                                                                                    DEFAULT_VALUE: REVENUE_KPI_KEY],
                                                                                                        (CONTRIBUTION_MODEL_KEY) : [VALUES       : [REVENUE_CONTRIBUTION_PERCENTAGE_KPI_KEY, MARGIN_CONTRIBUTION_PERCENTAGE_KPI_KEY],
                                                                                                                                    DEFAULT_VALUE: REVENUE_CONTRIBUTION_PERCENTAGE_KPI_KEY]]],
                                                                    CURRENCY            : [LABEL     : "Currency",
                                                                                           UNIQUE_KEY: "revenueBreakdownCURRENCY"]]]

/**
 * Holds all configuration of the Revenue and Margin Dashboard.
 * The configuration contains
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 */
@Field REVENUE_AND_MARGIN_DASHBOARD_CONFIG = [INPUTS: [PRODUCT             : [UNIQUE_KEY   : "revenueAndMarginPRODUCT",
                                                                              LABEL        : "Product(s)",
                                                                              DEFAULT_VALUE: null],
                                                       CUSTOMER            : [UNIQUE_KEY   : "revenueAndMarginCUSTOMER",
                                                                              LABEL        : "Customer(s)",
                                                                              DEFAULT_VALUE: null],
                                                       DATE_FROM           : [UNIQUE_KEY   : "revenueAndMarginDATE_FROM",
                                                                              LABEL        : "Date From",
                                                                              DEFAULT_VALUE: null],
                                                       DATE_TO             : [UNIQUE_KEY   : "revenueAndMarginDATE_TO",
                                                                              LABEL        : "Date To",
                                                                              DEFAULT_VALUE: null],
                                                       TIME_PERIOD         : [UNIQUE_KEY   : "revenueAndMarginTIME_PERIOD",
                                                                              LABEL        : "Time Period",
                                                                              VALUES       : ["Week", "Month", "Quarter", "Year"],
                                                                              DEFAULT_VALUE: "Quarter"],
                                                       BAND_BY_CUSTOMER    : [UNIQUE_KEY          : "revenueAndMarginBAND_BY_CUSTOMER",
                                                                              LABEL               : "Band By For Customer",
                                                                              ENTRY_CATEGORY      : "Customer",
                                                                              ENTRY_CATEGORY_FIELD: "customerId"],
                                                       BAND_BY_PRODUCT     : [UNIQUE_KEY          : "revenueAndMarginBAND_BY_PRODUCT",
                                                                              LABEL               : "Band By For Product",
                                                                              ENTRY_CATEGORY      : "Product",
                                                                              ENTRY_CATEGORY_FIELD: "productId"],
                                                       PRODUCT_AGGREGATION : [UNIQUE_KEY          : "revenueAndMarginPRODUCT_AGGREGATION",
                                                                              LABEL               : "Product Aggregation",
                                                                              ENTRY_CATEGORY      : "Product",
                                                                              ENTRY_CATEGORY_FIELD: "productId"],
                                                       CUSTOMER_AGGREGATION: [UNIQUE_KEY          : "revenueAndMarginCUSTOMER_AGGREGATION",
                                                                              LABEL               : "Customer Aggregation",
                                                                              ENTRY_CATEGORY      : "Customer",
                                                                              ENTRY_CATEGORY_FIELD: "customerId"],
                                                       AXIS_TYPE           : [UNIQUE_KEY: "revenueAndMarginAXIS_TYPE",
                                                                              LABEL     : "Column chart axis type"],
                                                       CURRENCY            : [LABEL     : "Currency",
                                                                              UNIQUE_KEY: "revenueBreakdownCURRENCY"]]]

/**
 * Defines the key used to describe the world hierarchy in the Regional Revenue and Margin dashboard.
 */
@Field WORLD_HIERARCHY_KEY = "world"

/**
 * Defines the key used to describe the continent hierarchy in the Regional Revenue and Margin dashboard.
 */
@Field CONTINENT_HIERARCHY_KEY = "continent"

/**
 * Defines the key used to describe the country hierarchy in the Regional Revenue and Margin dashboard.
 */
@Field COUNTRY_HIERARCHY_KEY = "country"

/**
 * Defines the key used to describe the region hierarchy in the Regional Revenue and Margin dashboard.
 */
@Field REGION_HIERARCHY_KEY = "region"

/**
 * Defines the key used to describe the sector hierarchy in the Regional Revenue and Margin dashboard.
 */
@Field SECTOR_HIERARCHY_KEY = "sector"

/**
 * Holds the configuration for the Regional Revenue and Margin Dashboard configurator
 * Defines a pattern that's used for the world checkbox.
 */
@Field REGIONAL_REVENUE_AND_MARGIN_CONFIGURATOR_CONFIG = [NAME  : "Configurator_RegionAndCountry",
                                                          INPUTS: [(WORLD_HIERARCHY_KEY): [PATTERN      : "Display %s map",
                                                                                           DEFAULT_VALUE: true]]]

/**
 * Defines all hierarchies used by the Regional Revenue and Margin Dashboard.
 * Each hierarchy has:
 * - NAME
 * - SQL_FIELD - which points to appropriate key in the DASHBOARDS_ADVANCED_CONFIGURATION_NAME
 * - CONTAINS - which defines the child hierarchy
 */
@Field REGIONAL_REVENUE_AND_MARGIN_HIERARCHY_CONFIG = [(WORLD_HIERARCHY_KEY)    : [NAME     : "World",
                                                                                   SQL_FIELD: null,
                                                                                   CONTAINS : CONTINENT_HIERARCHY_KEY],
                                                       (CONTINENT_HIERARCHY_KEY): [NAME     : "Continent",
                                                                                   SQL_FIELD: "continent",
                                                                                   CONTAINS : COUNTRY_HIERARCHY_KEY],
                                                       (COUNTRY_HIERARCHY_KEY)  : [NAME     : "Country",
                                                                                   SQL_FIELD: "country",
                                                                                   CONTAINS : REGION_HIERARCHY_KEY],
                                                       (REGION_HIERARCHY_KEY)   : [NAME     : "Region",
                                                                                   SQL_FIELD: "region",
                                                                                   CONTAINS : SECTOR_HIERARCHY_KEY],
                                                       (SECTOR_HIERARCHY_KEY)   : [NAME     : "Sector",
                                                                                   SQL_FIELD: "sector",
                                                                                   CONTAINS : null]]

/**
 * Holds information about the SIP_MapHierarchyConfig PP table.
 * The table holds all information about currently used hierarchies.
 * Based on the values setup different hierarchies are enabled or disabled in the Regional Revenue and Margin configurator.
 */
@Field REGIONAL_REVENUE_AND_MARGIN_HIERARCHY_PP = [NAME         : "SIP_MapHierarchyConfig",
                                                   TYPE         : "MLTV",
                                                   COLUMN_CONFIG: [HIERARCHY: "name",
                                                                   LABEL    : "attribute1",
                                                                   IS_USED  : "attribute2"],
                                                   IS_USED      : IS_USED_VALUE]

/**
 * Holds information about the SIP_MapCodeOverrides PP table.
 * The table holds all information about the overrides for default ISO_CODES as defined by the user.
 * Used to allow custom names being stored in the DM and not force the ISO_CODE approach.
 */
@Field REGIONAL_REVENUE_AND_MARGIN_MAP_CODES_PP = [NAME         : "SIP_MapCodeOverrides",
                                                   TYPE         : "MLTV2",
                                                   COLUMN_CONFIG: [HIERARCHY_LEVEL: "key1",
                                                                   ISO_CODE       : "key2",
                                                                   DM_FIELD_LABEL : "attribute1",
                                                                   DISPLAY_LABEL  : "attribute2"]]

/**
 * Holds information about the SIP_GeoOverrides PP table.
 * The table holds all information about the geoOverrides defined by the user.
 */
@Field REGIONAL_REVENUE_AND_MARGIN_GEO_OVERRIDES_PP = [NAME         : "SIP_GeoOverrides",
                                                       TYPE         : "MLTV",
                                                       COLUMN_CONFIG: [ISO_CODE         : "name",
                                                                       PARENT_ISO_CODE  : "attribute1",
                                                                       OVERRIDE_ISO_CODE: "attribute2"]]

/**
 * Holds information about the SIP_Population PP table.
 * The table holds all information about the population of each hierarchy level.
 */
@Field REGIONAL_REVENUE_AND_MARGIN_POPULATION_PP = [NAME         : "SIP_Population",
                                                    TYPE         : "MLTV4",
                                                    COLUMN_CONFIG: [(CONTINENT_HIERARCHY_KEY): "key1",
                                                                    (COUNTRY_HIERARCHY_KEY)  : "key2",
                                                                    (REGION_HIERARCHY_KEY)   : "key3",
                                                                    (SECTOR_HIERARCHY_KEY)   : "key4",
                                                                    POPULATION               : "attribute1"],
                                                    IS_USED      : IS_USED_VALUE]
/**
 * Defines the key used to describe the quantity KPI in Regional Revenue and Margin dashboard.
 */
@Field QUANTITY_KPI_KEY = "quantity"

/**
 * Defines the key used to describe the deviation from average price KPI in Regional Revenue and Margin dashboard.
 */
@Field DEVIATION_KPI_KEY = "deviationWAP"

/**
 * Defines the key used to describe the revenue per customer KPI in Regional Revenue and Margin dashboard.
 */
@Field REVENUE_PER_CUSTOMER_KPI_KEY = "revenuePerCustomer"

/**
 * Defines the key used to describe the revenue per X people KPI in Regional Revenue and Margin dashboard.
 */
@Field REVENUE_PER_POPULATION_KPI_KEY = "revenuePerPopulation"

/**
 * Defines the key used to describe the margin per customer KPI in Regional Revenue and Margin dashboard.
 */
@Field MARGIN_PER_CUSTOMER_KPI_KEY = "marginPerCustomer"

/**
 * Defines the key used to describe the margin per X people KPI in Regional Revenue and Margin dashboard.
 */
@Field MARGIN_PER_POPULATION_KPI_KEY = "marginPerPopulation"

/**
 * Holds all configuration of the Regional Revenue and Margin dashboard.
 * The configuration contains
 * - KPI - definition of all KPI used by the dashboard, their formats and rounding
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 * - POPULATION_CALCULATION_CONSTANT - constant user for revenue/margin per X people calculations
 */
@Field REGIONAL_REVENUE_AND_MARGIN_DASHBOARD_CONFIG = [KPI                            : [(QUANTITY_KPI_KEY)              : [LABEL   : "Quantity",
                                                                                                                            ROUNDING: 0,
                                                                                                                            PATTERN : "{%s:,.0f}"],
                                                                                         (REVENUE_KPI_KEY)               : [LABEL   : "Revenue",
                                                                                                                            ROUNDING: 0,
                                                                                                                            PATTERN : "{%s:,.0f} %s"],
                                                                                         (MARGIN_KPI_KEY)                : [LABEL   : "Margin",
                                                                                                                            ROUNDING: 0,
                                                                                                                            PATTERN : "{%s:,.0f} %s"],
                                                                                         (MARGIN_PERCENTAGE_KPI_KEY)     : [LABEL   : "Margin %",
                                                                                                                            ROUNDING: 2,
                                                                                                                            PATTERN : "{%s:,.2f} %%"],
                                                                                         (DEVIATION_KPI_KEY)             : [LABEL   : "Deviation from weighted average price",
                                                                                                                            ROUNDING: 6,
                                                                                                                            PATTERN : "{%s:,.2f} %s"],
                                                                                         (REVENUE_PER_CUSTOMER_KPI_KEY)  : [LABEL                 : "Revenue per customer",
                                                                                                                            ROUNDING              : 0,
                                                                                                                            PATTERN               : "{%s:,.0f} %s",
                                                                                                                            REQUIRES_CUSTOMER_DATA: true],
                                                                                         (MARGIN_PER_CUSTOMER_KPI_KEY)   : [LABEL                 : "Margin per customer",
                                                                                                                            ROUNDING              : 0,
                                                                                                                            PATTERN               : "{%s:,.0f} %s",
                                                                                                                            REQUIRES_CUSTOMER_DATA: true],
                                                                                         (REVENUE_PER_POPULATION_KPI_KEY): [LABEL   : "Revenue per 1000 people",
                                                                                                                            ROUNDING: 0,
                                                                                                                            PATTERN : "{%s:,.0f} %s"],
                                                                                         (MARGIN_PER_POPULATION_KPI_KEY) : [LABEL   : "Margin per 1000 people",
                                                                                                                            ROUNDING: 0,
                                                                                                                            PATTERN : "{%s:,.0f} %s"]],
                                                       INPUTS                         : [PRODUCT  : [LABEL        : "Product(s)",
                                                                                                     UNIQUE_KEY   : "regionalPRODUCT",
                                                                                                     DEFAULT_VALUE: null],
                                                                                         CUSTOMER : [LABEL        : "Customer(s)",
                                                                                                     UNIQUE_KEY   : "regionalCUSTOMER",
                                                                                                     DEFAULT_VALUE: null],
                                                                                         DATE_FROM: [LABEL        : "Date From",
                                                                                                     UNIQUE_KEY   : "regionalDATE_FROM",
                                                                                                     DEFAULT_VALUE: null],
                                                                                         DATE_TO  : [LABEL        : "Date To",
                                                                                                     UNIQUE_KEY   : "regionalDATE_TO",
                                                                                                     DEFAULT_VALUE: null],
                                                                                         KPI      : [LABEL        : "KPI",
                                                                                                     UNIQUE_KEY   : "regionalKPI",
                                                                                                     VALUES       : [QUANTITY_KPI_KEY, REVENUE_KPI_KEY, MARGIN_KPI_KEY,
                                                                                                                     MARGIN_PERCENTAGE_KPI_KEY, DEVIATION_KPI_KEY, REVENUE_PER_CUSTOMER_KPI_KEY,
                                                                                                                     REVENUE_PER_CUSTOMER_KPI_KEY, REVENUE_PER_POPULATION_KPI_KEY, MARGIN_PER_POPULATION_KPI_KEY],
                                                                                                     DEFAULT_VALUE: REVENUE_KPI_KEY],
                                                                                         CURRENCY : [LABEL     : "Currency",
                                                                                                     UNIQUE_KEY: "revenueBreakdownCURRENCY"]],
                                                       POPULATION_CALCULATION_CONSTANT: 1000]

/**
 * Holds all configuration that is common for both the Waterfall and Comparison Waterfall dashboards.
 * The configuration contains
 * - KPI - definition of all KPI used by the dashboard, their formats and rounding
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 * - POPULATION_CALCULATION_CONSTANT - constant user for revenue/margin per X people calculations
 */
@Field COMMON_WATERFALL_DASHBOARDS_CONFIG = [
        WATERFALL_COLUMN_COLORS: [BASE     : [PRICE   : "#0080FF",
                                              NEGATIVE: "#BF4040",
                                              POSITIVE: "#00FF00"],
                                  SECONDARY: [PRICE   : "#1f2a8d",
                                              NEGATIVE: "#dd0074",
                                              POSITIVE: "#99da00"]]
]

/**
 * Defines the key used to describe the absolute calculation model in Waterfall and Waterfall Comparison Dashboard.
 */
@Field WATERFALL_MODEL_ABSOLUTE_NAME = "absolute"

/**
 * Defines the key used to describe the percentage calculation model in Waterfall and Waterfall Comparison Dashboard.
 */
@Field WATERFALL_MODEL_PERCENTAGE_NAME = "percentage"

/**
 * Defines the key used to describe the detail calculation model in Waterfall Dashboard.
 */
@Field WATERFALL_MODEL_DETAIL_NAME = "detail"

/**
 * Defines the key used to describe the absoluteUnit calculation model in Waterfall and Waterfall Comparison Dashboard.
 */
@Field WATERFALL_MODEL_ABSOLUTE_UNIT_NAME = "absoluteUnit"

/**
 * Holds all configuration of the Waterfall dashboard.
 * The configuration contains
 * - MODELS - definition of all calculation models supported by the dashboard and their additional configuration (like titles)
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 */
@Field WATERFALL_DASHBOARD_CONFIG = [MODELS: [(WATERFALL_MODEL_ABSOLUTE_NAME)     : [LABEL: "Absolute",
                                                                                     TITLE: "Waterfall With Absolute Value"],
                                              (WATERFALL_MODEL_DETAIL_NAME)       : [LABEL: "Absolute Detail",
                                                                                     TITLE: "Waterfall With Absolute Detail Value"],
                                              (WATERFALL_MODEL_ABSOLUTE_UNIT_NAME): [LABEL: "By Absolute Unit",
                                                                                     TITLE: "Waterfall By Absolute Unit Value"],
                                              (WATERFALL_MODEL_PERCENTAGE_NAME)   : [LABEL: "Percentage",
                                                                                     TITLE: "Waterfall With Percentage Value"]],
                                     INPUTS: [PRODUCT  : [LABEL        : "Product(s)",
                                                          UNIQUE_KEY   : "waterfallPRODUCT",
                                                          DEFAULT_VALUE: null],
                                              CUSTOMER : [LABEL        : "Customer(s)",
                                                          UNIQUE_KEY   : "waterfallCUSTOMER",
                                                          DEFAULT_VALUE: null],
                                              DATE_FROM: [LABEL        : "Date From",
                                                          UNIQUE_KEY   : "waterfallDATE_FROM",
                                                          DEFAULT_VALUE: null],
                                              DATE_TO  : [LABEL        : "Date To",
                                                          UNIQUE_KEY   : "waterfallDATE_TO",
                                                          DEFAULT_VALUE: null],
                                              MODELS   : [LABEL        : "Waterfall Model",
                                                          UNIQUE_KEY   : "waterfallMODELS",
                                                          DEFAULT_VALUE: WATERFALL_MODEL_ABSOLUTE_NAME],
                                              CURRENCY : [LABEL     : "Currency",
                                                          UNIQUE_KEY: "revenueBreakdownCURRENCY"]]]

/**
 * Defines the key used to describe the product comparison dimension in Waterfall Comparison Dashboard.
 */
@Field COMPARISON_DIMENSION_PRODUCT_KEY = "product"

/**
 * Defines the key used to describe the customer comparison dimension in Waterfall Comparison Dashboard.
 */
@Field COMPARISON_DIMENSION_CUSTOMER_KEY = "customer"

/**
 * Defines the key used to describe the date comparison dimension in Waterfall Comparison Dashboard.
 */
@Field COMPARISON_DIMENSION_DATE_KEY = "date"

/**
 * Holds all configuration of the Waterfall Comparison dashboard.
 * The configuration contains
 * - MODELS - definition of all calculation models supported by the dashboard and their additional configuration (like titles)
 * - COMPARISON_DIMENSIONS - definition of all comparison dimensions supported by the dashboard and their additional configuration (like titles)
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 */
@Field WATERFALL_COMPARISON_DASHBOARD_CONFIG = [MODELS               : [(WATERFALL_MODEL_ABSOLUTE_NAME)     : [LABEL: "Absolute",
                                                                                                               TITLE: "Absolute Value"],
                                                                        (WATERFALL_MODEL_ABSOLUTE_UNIT_NAME): [LABEL: "By Absolute Unit",
                                                                                                               TITLE: "By Absolute Unit Value"],
                                                                        (WATERFALL_MODEL_PERCENTAGE_NAME)   : [LABEL: "Percentage",
                                                                                                               TITLE: "Percentage Value"]],
                                                COMPARISON_DIMENSIONS: [(COMPARISON_DIMENSION_PRODUCT_KEY) : [LABEL: "Product",
                                                                                                              TITLE: "Product(s)"],
                                                                        (COMPARISON_DIMENSION_CUSTOMER_KEY): [LABEL: "Customer",
                                                                                                              TITLE: "Customer(s)"],
                                                                        (COMPARISON_DIMENSION_DATE_KEY)    : [LABEL: "Date",
                                                                                                              TITLE: "Date"]],
                                                INPUTS               : [PRODUCT              : [LABEL        : "Product(s)",
                                                                                                UNIQUE_KEY   : "waterfallComparisonPRODUCT",
                                                                                                SUFFIXES     : [FIRST : " (1)",
                                                                                                                SECOND: " (2)"],
                                                                                                DEFAULT_VALUE: null],
                                                                        CUSTOMER             : [LABEL        : "Customer(s)",
                                                                                                UNIQUE_KEY   : "waterfallComparisonCUSTOMER",
                                                                                                SUFFIXES     : [FIRST : " (1)",
                                                                                                                SECOND: " (2)"],
                                                                                                DEFAULT_VALUE: null],
                                                                        DATE_FROM            : [LABEL        : "Date From",
                                                                                                UNIQUE_KEY   : "waterfallComparisonDATE_FROM",
                                                                                                SUFFIXES     : [FIRST : " (1)",
                                                                                                                SECOND: " (2)"],
                                                                                                DEFAULT_VALUE: [FIRST : [YEAR_ADJUSTMENT: -1,
                                                                                                                         MONTH          : 0,
                                                                                                                         DAY            : 1],
                                                                                                                SECOND: [YEAR_ADJUSTMENT: -2,
                                                                                                                         MONTH          : 0,
                                                                                                                         DAY            : 1]]],
                                                                        DATE_TO              : [LABEL        : "Date To",
                                                                                                UNIQUE_KEY   : "waterfallComparisonDATE_TO",
                                                                                                SUFFIXES     : [FIRST : " (1)",
                                                                                                                SECOND: " (2)"],
                                                                                                DEFAULT_VALUE: [FIRST : [YEAR_ADJUSTMENT: -1,
                                                                                                                         MONTH          : 11,
                                                                                                                         DAY            : 31],
                                                                                                                SECOND: [YEAR_ADJUSTMENT: -2,
                                                                                                                         MONTH          : 11,
                                                                                                                         DAY            : 31]]],
                                                                        COMPARISON_DIMENSIONS: [LABEL        : "Comparison Type",
                                                                                                UNIQUE_KEY   : "waterfallComparisonCOMPARISON_DIMENSIONS",
                                                                                                DEFAULT_VALUE: COMPARISON_DIMENSION_DATE_KEY],
                                                                        MODELS               : [LABEL        : "Waterfall Model",
                                                                                                UNIQUE_KEY   : "waterfallComparisonMODELS",
                                                                                                DEFAULT_VALUE: WATERFALL_MODEL_ABSOLUTE_NAME],
                                                                        CURRENCY             : [LABEL     : "Currency",
                                                                                                UNIQUE_KEY: "revenueBreakdownCURRENCY"]]]

/**
 * Defines a year format used by various dashboards
 */
@Field YEAR_FORMAT = "yyyy"

/**
 * Defines the name of the System generated prefix for Year column in DM, generated by setting the column function as PricingDate
 */
@Field YEAR_FIELD_SUFFIX = "Year"

/**
 * Holds all configuration of the Revenue Breakdown dashboard.
 * The configuration contains
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 */
@Field REVENUE_BREAKDOWN_DASHBOARD_CONFIG = [INPUTS: [PRODUCT             : [LABEL        : "Product(s)",
                                                                             UNIQUE_KEY   : "revenueBreakdownPRODUCT",
                                                                             DEFAULT_VALUE: null],
                                                      PRODUCT_AGGREGATION : [LABEL               : "Product Aggregation",
                                                                             UNIQUE_KEY          : "revenueBreakdownPRODUCT_AGGREGATION",
                                                                             ENTRY_CATEGORY      : "Product",
                                                                             ENTRY_CATEGORY_FIELD: "productId"],
                                                      CUSTOMER            : [LABEL        : "Customer(s)",
                                                                             UNIQUE_KEY   : "revenueBreakdownCUSTOMER",
                                                                             DEFAULT_VALUE: null],
                                                      CUSTOMER_AGGREGATION: [LABEL               : "Customer Aggregation",
                                                                             UNIQUE_KEY          : "revenueBreakdownCUSTOMER_AGGREGATION",
                                                                             ENTRY_CATEGORY      : "Customer",
                                                                             ENTRY_CATEGORY_FIELD: "customerId"],
                                                      YEAR                : [LABEL        : "Year",
                                                                             UNIQUE_KEY   : "revenueBreakdownYEAR",
                                                                             FIELD_SUFFIX : YEAR_FIELD_SUFFIX,
                                                                             FORMAT       : YEAR_FORMAT,
                                                                             DEFAULT_VALUE: {
                                                                                 return libs.SharedLib.DateUtils.currentYear()
                                                                             }],
                                                      COMPARISON_YEAR     : [LABEL        : "Comparison Year",
                                                                             UNIQUE_KEY   : "revenueBreakdownCOMPARISON_YEAR",
                                                                             DATE         : [FORMAT: "yyyy-MM-dd"],
                                                                             FIELD_SUFFIX : YEAR_FIELD_SUFFIX,
                                                                             FORMAT       : YEAR_FORMAT,
                                                                             DEFAULT_VALUE: {
                                                                                 return libs.SharedLib.DateUtils.currentYear() - 1
                                                                             }],
                                                      PERIOD              : [LABEL        : "Quarter",
                                                                             UNIQUE_KEY   : "revenueBreakdownPERIOD",
                                                                             VALUES       : ["Q1", "Q2", "Q3", "Q4", EMPTY_DIMENSION_VALUE],
                                                                             DEFAULT_VALUE: {
                                                                                 return libs.SIP_Dashboards_Commons.InputUtils.getCurrentQuarter()
                                                                             }],
                                                      COMPARISON_PERIOD   : [LABEL        : "Comparison Quarter",
                                                                             UNIQUE_KEY   : "revenueBreakdownCOMPARISON_PERIOD",
                                                                             VALUES       : ["Q1", "Q2", "Q3", "Q4", EMPTY_DIMENSION_VALUE],
                                                                             DEFAULT_VALUE: {
                                                                                 return libs.SIP_Dashboards_Commons.InputUtils.getCurrentQuarter()
                                                                             }],
                                                      SHOW_AS_PERCENTAGE  : [LABEL        : "Show Percentage (%)",
                                                                             UNIQUE_KEY   : "revenueBreakdownSHOW_AS_PERCENTAGE",
                                                                             DEFAULT_VALUE: false],
                                                      CURRENCY            : [LABEL     : "Currency",
                                                                             UNIQUE_KEY: "revenueBreakdownCURRENCY"]]]

/**
 * Defines the key used to describe the net calculation model used in the Margin Breakdown Dashboard.
 */
@Field MARGIN_BREAKDOWN_NET_CHART_KEY = "net"

/**
 * Defines the key used to describe the gross calculation model used in the Margin Breakdown Dashboard.
 */
@Field MARGIN_BREAKDOWN_GROSS_CHART_KEY = "gross"

/**
 * Defines the key used to describe the averages calculation model used in the Margin Breakdown Dashboard.
 */
@Field MARGIN_BREAKDOWN_AVERAGES_CHART_KEY = "averages"

/**
 * Defines the key used to describe the most used calculation model used in the Margin Breakdown Dashboard.
 */
@Field MARGIN_BREAKDOWN_MOST_USED_CHART_KEY = "mostUsed"

/**
 * Holds all configuration of the Margin Breakdown dashboard.
 * The configuration contains
 * - MODELS - definition of all calculation models supported by the dashboard and their additional configuration (like titles)
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 */
@Field MARGIN_BREAKDOWN_DASHBOARD_CONFIG = [MODELS: [(MARGIN_BREAKDOWN_NET_CHART_KEY)      : [LABEL: "Net",
                                                                                              TITLE: "Net Margin Breakdown"],
                                                     (MARGIN_BREAKDOWN_GROSS_CHART_KEY)    : [LABEL: "Gross",
                                                                                              TITLE: "Gross Margin Breakdown"],
                                                     (MARGIN_BREAKDOWN_AVERAGES_CHART_KEY) : [LABEL: "Averages",
                                                                                              TITLE: "Averages Margin Breakdown"],
                                                     (MARGIN_BREAKDOWN_MOST_USED_CHART_KEY): [LABEL: "Most Used",
                                                                                              TITLE: "Most Used Margin Breakdown"]],
                                            INPUTS: [PRODUCT             : [LABEL        : "Product(s)",
                                                                            UNIQUE_KEY   : "marginBreakdownPRODUCT",
                                                                            DEFAULT_VALUE: null],
                                                     PRODUCT_AGGREGATION : [LABEL               : "Product Aggregation",
                                                                            UNIQUE_KEY          : "marginBreakdownPRODUCT_AGGREGATION",
                                                                            ENTRY_CATEGORY      : "Product",
                                                                            ENTRY_CATEGORY_FIELD: "productId"],
                                                     CUSTOMER            : [LABEL        : "Customer(s)",
                                                                            UNIQUE_KEY   : "marginBreakdownCUSTOMER",
                                                                            DEFAULT_VALUE: null],
                                                     CUSTOMER_AGGREGATION: [LABEL               : "Customer Aggregation",
                                                                            UNIQUE_KEY          : "marginBreakdownCUSTOMER_AGGREGATION",
                                                                            ENTRY_CATEGORY      : "Customer",
                                                                            ENTRY_CATEGORY_FIELD: "customerId"],
                                                     YEAR                : [LABEL        : "Year",
                                                                            UNIQUE_KEY   : "marginBreakdownYEAR",
                                                                            FIELD_SUFFIX : YEAR_FIELD_SUFFIX,
                                                                            FORMAT       : YEAR_FORMAT,
                                                                            DEFAULT_VALUE: {
                                                                                return libs.SharedLib.DateUtils.currentYear()
                                                                            }],
                                                     COMPARISON_YEAR     : [LABEL        : "Comparison Year",
                                                                            UNIQUE_KEY   : "marginBreakdownCOMPARISON_YEAR",
                                                                            DATE         : [FORMAT: "yyyy-MM-dd"],
                                                                            FIELD_SUFFIX : YEAR_FIELD_SUFFIX,
                                                                            FORMAT       : YEAR_FORMAT,
                                                                            DEFAULT_VALUE: {
                                                                                return libs.SharedLib.DateUtils.currentYear() - 1
                                                                            }],
                                                     PERIOD              : [LABEL        : "Quarter",
                                                                            UNIQUE_KEY   : "marginBreakdownPERIOD",
                                                                            VALUES       : ["Q1", "Q2", "Q3", "Q4", EMPTY_DIMENSION_VALUE],
                                                                            DEFAULT_VALUE: {
                                                                                return libs.SIP_Dashboards_Commons.InputUtils.getCurrentQuarter()
                                                                            }],
                                                     COMPARISON_PERIOD   : [LABEL        : "Comparison Quarter",
                                                                            UNIQUE_KEY   : "marginBreakdownCOMPARISON_PERIOD",
                                                                            VALUES       : ["Q1", "Q2", "Q3", "Q4", EMPTY_DIMENSION_VALUE],
                                                                            DEFAULT_VALUE: {
                                                                                return libs.SIP_Dashboards_Commons.InputUtils.getCurrentQuarter()
                                                                            }],
                                                     SHOW_AS_PERCENTAGE  : [LABEL        : "Show Percentage (%)",
                                                                            UNIQUE_KEY   : "marginBreakdownSHOW_AS_PERCENTAGE",
                                                                            DEFAULT_VALUE: false],
                                                     MODELS              : [LABEL        : "Calculation Type",
                                                                            UNIQUE_KEY   : "marginBreakdownMODELS",
                                                                            DEFAULT_VALUE: MARGIN_BREAKDOWN_NET_CHART_KEY],
                                                     CURRENCY            : [LABEL     : "Currency",
                                                                            UNIQUE_KEY: "revenueBreakdownCURRENCY"]]]

/**
 * Holds all configuration of the Waterfall dashboard.
 * The configuration contains
 * - INPUTS - contains all information about available user inputs used in the dashboard. Used to generate default filters.
 */
@Field CAUSALITY_DASHBOARD_CONFIG = [INPUTS: [PRODUCT             : [LABEL        : "Product(s)",
                                                                     UNIQUE_KEY   : "causalityPRODUCT",
                                                                     DEFAULT_VALUE: null],
                                              PRODUCT_AGGREGATION : [LABEL               : "Product Aggregation",
                                                                     UNIQUE_KEY          : "causalityPRODUCT_AGGREGATION",
                                                                     ENTRY_CATEGORY      : "Product",
                                                                     ENTRY_CATEGORY_FIELD: "productId"],
                                              CUSTOMER            : [LABEL        : "Customer(s)",
                                                                     UNIQUE_KEY   : "causalityCUSTOMER",
                                                                     DEFAULT_VALUE: null],
                                              CUSTOMER_AGGREGATION: [LABEL               : "Customer Aggregation",
                                                                     UNIQUE_KEY          : "causalityCUSTOMER_AGGREGATION",
                                                                     ENTRY_CATEGORY      : "Customer",
                                                                     ENTRY_CATEGORY_FIELD: "customerId"],
                                              YEAR                : [LABEL        : "Year",
                                                                     UNIQUE_KEY   : "causalityYEAR",
                                                                     FIELD_SUFFIX : YEAR_FIELD_SUFFIX,
                                                                     FORMAT       : YEAR_FORMAT,
                                                                     DEFAULT_VALUE: {
                                                                         return libs.SharedLib.DateUtils.currentYear()
                                                                     }],
                                              COMPARISON_YEAR     : [LABEL        : "Comparison Year",
                                                                     UNIQUE_KEY   : "causalityCOMPARISON_YEAR",
                                                                     DATE         : [FORMAT          : "yyyy-MM-dd",
                                                                                     MONTHS_BACK_FROM: -3,
                                                                                     MONTHS_BACK_TO  : -12],
                                                                     FIELD_SUFFIX : YEAR_FIELD_SUFFIX,
                                                                     FORMAT       : YEAR_FORMAT,
                                                                     DEFAULT_VALUE: {
                                                                         return libs.SharedLib.DateUtils.currentYear() - 1
                                                                     }],
                                              PERIOD              : [LABEL        : "Quarter",
                                                                     UNIQUE_KEY   : "causalityPERIOD",
                                                                     VALUES       : ["Q1", "Q2", "Q3", "Q4", EMPTY_DIMENSION_VALUE],
                                                                     DEFAULT_VALUE: {
                                                                         return libs.SIP_Dashboards_Commons.InputUtils.getCurrentQuarter()
                                                                     }],
                                              COMPARISON_PERIOD   : [LABEL        : "Comparison Quarter",
                                                                     UNIQUE_KEY   : "causalityCOMPARISON_PERIOD",
                                                                     VALUES       : ["Q1", "Q2", "Q3", "Q4", EMPTY_DIMENSION_VALUE],
                                                                     DEFAULT_VALUE: {
                                                                         return libs.SIP_Dashboards_Commons.InputUtils.getCurrentQuarter()
                                                                     }],
                                              SHOW_AS_PERCENTAGE  : [LABEL        : "Show Percentage (%)",
                                                                     UNIQUE_KEY   : "causalitySHOW_AS_PERCENTAGE",
                                                                     DEFAULT_VALUE: false],
                                              NO_OF_RESULTS       : [LABEL            : "Top Product(s)/ Customer(s)",
                                                                     NO_CUSTOMER_LABEL: "Top Product(s)",
                                                                     UNIQUE_KEY       : "causalityNO_OF_RESULTS",
                                                                     VALUES           : ["5", "10", "25", "50"],
                                                                     DEFAULT_VALUE    : "10"],
                                              CURRENCY            : [LABEL     : "Currency",
                                                                     UNIQUE_KEY: "revenueBreakdownCURRENCY"]]]

/**
 * Defines the key used to describe the General Filter configurator in the DefaultFilters
 */
@Field GENERAL_FILTER_NAME = "General Filter"

/**
 * Defines the key used to describe the Outliers configurator in the DefaultFilters
 */
@Field OUTLIERS_NAME = "Outliers Dashboard"

/**
 * Defines the key used to describe the Revenue and Margin configurator in the DefaultFilters
 */
@Field REVENUE_AND_MARGIN_NAME = "Revenue and Margin"

/**
 * Defines the key used to describe the Regional Revenue and Margin configurator in the DefaultFilters
 */
@Field REVENUE_AND_MARGIN_MAP_NAME = "Regional Revenue and Margin"

/**
 * Defines the key used to describe the Waterfall configurator in the DefaultFilters
 */
@Field WATERFALL_NAME = "Waterfall"

/**
 * Defines the key used to describe the Comparison Waterfall configurator in the DefaultFilters
 */
@Field COMPARISON_WATERFALL_NAME = "Comparison Waterfall"

/**
 * Defines the key used to describe the Margin Breakdown configurator in the DefaultFilters
 */
@Field MARGIN_BREAKDOWN_NAME = "Margin Breakdown"

/**
 * Defines the key used to describe the Revenue Breakdown configurator in the DefaultFilters
 */
@Field REVENUE_BREAKDOWN_NAME = "Revenue Breakdown"

/**
 * Defines the key used to describe the Causality Dashboard configurator in the DefaultFilters
 */
@Field PRODUCT_CUSTOMER_CAUSALITY_NAME = "Causality Dashboard"

/**
 * Defines the order in which the configurators for the Default Filters will be displayed.
 * The GENERAL_FILTER is always displayed first.
 */
@Field DEFINED_DASHBOARD_FILTERS = [REVENUE_AND_MARGIN_NAME,
                                    REVENUE_AND_MARGIN_MAP_NAME,
                                    OUTLIERS_NAME,
                                    WATERFALL_NAME,
                                    COMPARISON_WATERFALL_NAME,
                                    REVENUE_BREAKDOWN_NAME,
                                    MARGIN_BREAKDOWN_NAME,
                                    PRODUCT_CUSTOMER_CAUSALITY_NAME]

@Field DEFAULT_CONFIGURATOR_CONFIG = [INPUTS: [DASHBOARD_SELECTION: [LABEL     : "Dashboard",
                                                                     UNIQUE_KEY: "defaultConfiguratorDASHBOARD_SELECTION"],
                                               GENERAL_FILTER     : [LABEL     : "General Filter",
                                                                     UNIQUE_KEY: "defaultConfiguratorGENERAL_FILTER"]]]

/**
 * Retrieves additional configuration for the Revenue And Margin axis type.
 * The configuration has to be created like this because it's not possible to add fields from other libraries to @Field definitions directly.
 * @return additional configuration for axis type based on the libs.HighchartsLibrary.ConstConfig.AXIS_TYPES
 */
Map getRevenueAndMarginAxisTypeConfig() {
    Map axisTypes = libs.HighchartsLibrary.ConstConfig.AXIS_TYPES

    return [VALUES       : [(axisTypes.LINEAR)     : "Linear",
                            (axisTypes.LOGARITHMIC): "Logarithmic"],
            DEFAULT_VALUE: axisTypes.LINEAR]
}

/**
 * Retrieves the list of available configurators to be used by the DefaultFilters configurator.
 * The list is build using elements from libs.SIP_Dashboards_Commons.ConstConfig
 * @return list of available configurators to be used by the DefaultFilters configurator
 */
List getDefinedFilters() {
    def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig

    return [commonsConstConfig.GENERAL_FILTER_NAME] + commonsConstConfig.DEFINED_DASHBOARD_FILTERS
}
