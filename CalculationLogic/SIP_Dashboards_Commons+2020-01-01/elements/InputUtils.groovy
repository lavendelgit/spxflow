import net.pricefx.formulaengine.DatamartContext

/**
 * Retrieves the datamart filter user entry, used to generate GeneralFilters input on the dashboards.
 * User default values are applied if applicable.
 * @param datamartName datamart that is the source for the datamartFilterBuilderUserEntry
 * @return value of Filter retrieved from datamartFilterBuilderUserEntry
 */
Filter getDatamartFilterUserEntry(String datamartName) {
    Map inputConfig = libs.SIP_Dashboards_Commons.ConstConfig.DEFAULT_CONFIGURATOR_CONFIG.INPUTS.GENERAL_FILTER
    String userEntryLabel = inputConfig.LABEL
    def userEntry = api.datamartFilterBuilderUserEntry(userEntryLabel, datamartName)
    Map generalFilterUserValue = getGeneralFilterConfiguration()

    return applyInputDefaultValue(userEntry, userEntryLabel, null, generalFilterUserValue)
}

/**
 * Retrieves the default input configuration for a given dashboard as created by the DefaultFilters configurator wizard.
 * The values returned are dependent on the currently logged in user.
 * The values are stored in SIP_DefaultFilterValues PP table
 * @param dashboardName dashboard for which the configuration should be retrieved.
 *                      The list of dashboard names is stored in libs.SIP_Dashboards_Commons.ConstConfig
 * @return Map containing all user defined default input values for the given dashboard
 */
Map getDefaultInputConfiguration(String dashboardName) {
    Map columnConfig = libs.SIP_Dashboards_Commons.ConstConfig.DASHBOARDS_DEFAULT_FILTERS_PP_CONFIG.COLUMN_CONFIG

    def ppEntry = lookupDefaultFiltersPP(dashboardName)?.getAt(0)
    String filterValue = ppEntry?.getAt(columnConfig.FILTER_VALUE)

    return api.jsonDecode(filterValue)
}

/**
 * Used to retrieve the whole structure for the DefaultFilters configuration wizard.
 * Used when assigning initial values in the DefaultFilters configuration wizard.
 * @return all values stored in the SIP_DefaultFilterValues PP table
 */
Map getDefaultInputConfiguration() {
    Map columnConfig = libs.SIP_Dashboards_Commons.ConstConfig.DASHBOARDS_DEFAULT_FILTERS_PP_CONFIG.COLUMN_CONFIG
    List ppEntries = lookupDefaultFiltersPP()

    return ppEntries.collectEntries {
        return [(it.getAt(columnConfig.FILTER_NAME)): it.getAt(columnConfig.FILTER_VALUE)]
    }
}

/**
 * Creates the product group entry input.
 * @param inputName name of the input
 * @param defaultValue the default value, usually userDefault value coming from the default filters.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getProductGroupUserEntry(String inputName, Map defaultValue, Map userDefaultValue = null, boolean allowNulls = false) {
    def productEntry = api.datamartProductGroupEntry(inputName)
    if (!productEntry?.asFilter()) {
        productEntry = null
    }
    return applyInputDefaultValue(productEntry, inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Creates the customer group entry input.
 * @param inputName name of the input
 * @param defaultValue the default value, usually userDefault value coming from the default filters.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getCustomerGroupUserEntry(String inputName, Map defaultValue, Map userDefaultValue = null, boolean allowNulls = false) {
    def customerEntry = api.datamartCustomerGroupEntry(inputName)
  if (!customerEntry?.asFilter()) {
        customerEntry = null
    }
    return applyInputDefaultValue(customerEntry, inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Creates the date from user entry input.
 * The value of a default date from entry is always year back.
 * @param inputName name of the input
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getDefaultDateFromUserEntry(String inputName, def userDefaultValue = null, boolean allowNulls = false) {
    java.util.Calendar calendar = java.util.Calendar.getInstance()
    calendar.add(java.util.Calendar.YEAR, -1)
    def defaultValue = calendar.getTime()

    return getDateUserEntry(inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Creates the date to user entry input.
 * The value of a default date from entry is always current year.
 * @param inputName name of the input
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getDefaultDateToUserEntry(String inputName, def userDefaultValue = null, boolean allowNulls = false) {
    def defaultValue = java.util.Calendar.getInstance().getTime()

    return getDateUserEntry(inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Creates the date user entry input.
 * @param inputName name of the input
 * @param defaultValue the default value, usually userDefault value coming from the default filters.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getDateUserEntry(String inputName, def defaultValue, def userDefaultValue = null, boolean allowNulls = false) {
    def dateEntry = api.dateUserEntry(inputName)

    return applyInputDefaultValue(dateEntry, inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Creates the options user entry input.
 * @param inputName name of the input
 * @param values values available for selection in the input
 * @param defaultValue the default value, usually userDefault value coming from the default filters.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getOptionsUserEntry(String inputName, List values, String defaultValue, String userDefaultValue = null, boolean allowNulls = false) {
    def optionsEntry = api.option(inputName, values)

    return applyInputDefaultValue(optionsEntry, inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Creates the options user entry input.
 * @param inputName name of the input
 * @param values values available for selection in the input, these are the values returned
 * @param labels labels for the values, Map of structure [value : label]
 * @param defaultValue the default value, usually userDefault value coming from the default filters.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getOptionsUserEntry(String inputName, List values, Map labels, String defaultValue, String userDefaultValue = null, boolean allowNulls = false) {
    def optionsEntry = api.option(inputName, values, labels)

    return applyInputDefaultValue(optionsEntry, inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Creates the boolean user entry input.
 * @param inputName name of the input
 * @param defaultValue the default value, usually userDefault value coming from the default filters.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return value of the user input
 */
def getBooleanUserEntry(String inputName, boolean defaultValue, boolean userDefaultValue = null, boolean allowNulls = false) {
    def booleanEntry = api.booleanUserEntry(inputName)

    return applyInputDefaultValue(booleanEntry, inputName, defaultValue, userDefaultValue, allowNulls)
}

/**
 * Applies the default value for a given user input.
 * User default value always has priority.
 * @param userEntry Object defining the processed user entry
 * @param inputName name of the input
 * @param defaultValue the default value, usually userDefault value coming from the default filters.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @param allowNulls defines whether the input considers null values as a valid input
 * @return Object defining the processed user entry with default value
 */
def applyInputDefaultValue(def userEntry, String inputName, def defaultValue, def userDefaultValue = null, boolean allowNulls = false) {
    def param = api.getParameter(inputName)
    def selectedDefaultValue = userDefaultValue ?: defaultValue
    if (param != null && param?.getValue() == null) {
        param.setValue(selectedDefaultValue)
    }

    return allowNulls ?
            userEntry :
            (userEntry ?: defaultValue)
}

/**
 * Retrieves the current quarter in DM friendly format.
 * @return current quarter in a format that's generated by system PricingDate function
 */
String getCurrentQuarter() {
    int currentMonth = Calendar.getInstance().get(Calendar.MONTH)

    return "Q${(currentMonth / 4 as int) + 1}"
}

/**
 * Retrieves the values stored in the SIP_DefaultFilterValues PP table.
 * If the dashboard name value is provided it is used as an additional filter.
 * The default filter for user is always applied.
 * @param dashboardName dashboard for which the configuration should be retrieved.
 *                      The list of dashboard names is stored in libs.SIP_Dashboards_Commons.ConstConfig
 * @return List containing raw PP entries data from the SIP_DefaultFilterValues PP table
 */
protected List lookupDefaultFiltersPP(String dashboardName = null) {
    Map defaultFiltersPPConfig = libs.SIP_Dashboards_Commons.ConstConfig.DASHBOARDS_DEFAULT_FILTERS_PP_CONFIG
    Map columnConfig = defaultFiltersPPConfig.COLUMN_CONFIG
    String userName = api.user().loginName

    List filters = [Filter.equal(columnConfig.USER_NAME, userName)]

    if (dashboardName) {
        filters += Filter.equal(columnConfig.FILTER_NAME, dashboardName)
    }

    return api.findLookupTableValues(defaultFiltersPPConfig.NAME, *filters)
}

/**
 * Retrieves the user value defined in the DefaultFilters configurator wizard for the GeneralFilter used in the dashboards.
 * The value returned is dependent on the currently logged in user.
 * @return user value of GeneralFilter
 */
protected Map getGeneralFilterConfiguration() {
    def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig
    Map inputConfig = commonsConstConfig.DEFAULT_CONFIGURATOR_CONFIG.INPUTS.GENERAL_FILTER
    Map columnConfig = commonsConstConfig.DASHBOARDS_DEFAULT_FILTERS_PP_CONFIG.COLUMN_CONFIG

    def ppEntry = lookupDefaultFiltersPP(commonsConstConfig.GENERAL_FILTER_NAME)?.getAt(0)
    String filterValue = ppEntry?.getAt(columnConfig.FILTER_VALUE)

    return api.jsonDecode(filterValue)?.getAt(inputConfig.UNIQUE_KEY)
}

/**
 * Retrieves the selected value from the options, in case of no selected value a default one is set.
 * Used in aggregation inputs.
 * @param optionLabel name of the options user entry
 * @param dimensions list of the values displayed in the entry
 * @param selected currently selected user value
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @return default or selected value depending on the data availability
 */
protected def getSelectedOrDefaultValue(String optionLabel, Map<String, String> dimensions, def selected, String userDefaultValue) {
    def param = api.getParameter(optionLabel)

    if (param == null || param.getValue() != null) {
        return selected
    }

    String firstDimension = dimensions.keySet().getAt(0)
    param.setValue(userDefaultValue ?: firstDimension)

    return firstDimension
}

/**
 * Creates the aggregation input that's used in various dashboards to provide additional level of data aggregation.
 * Contains list of all defined dimensions in the sqlConfiguration
 * @param dimensions list of the values displayed in the entry
 * @param optionLabel label of the entry
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 * @return value of the aggregation input
 */
protected def createAggregationInput(Map dimensions, String optionLabel, String userDefaultValue) {
    def selectedByUser = api.option(optionLabel, dimensions.keySet() as List, dimensions)

    return getSelectedOrDefaultValue(optionLabel, dimensions, selectedByUser, userDefaultValue)
}

/**
 * Retrieves a Map of all the fields that can be used by the aggregation inputs.
 * Only the fields that are marked as dimension in the provided datamart can be used.
 * @param datamartName name of the datamart
 * @param owner defines whether the input is of Product or Customer type
 * @return Map containing the names and labels for the available dimensions to be used in aggregation inputs.
 */
protected Map getDatamartFieldMap(String datamartName, String owner) {
    DatamartContext ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(datamartName)
    Map fields = [:]

    List fieldList = dm.fc()?.fields
    List ownedFields = fieldList?.findAll {
        it?.owningFC?.toLowerCase() == owner?.toLowerCase() && it?.dimension == true
    }
    for (item in ownedFields) {
        fields << [(item?.name): item?.label]
    }

    return fields
}

/**
 * Retrieves the label for a given DM field.
 * @param datamartName name of the datamart
 * @param fieldName field for which the DM label should be returned. Has to be a dimension
 * @return if the field has been found then field label is returned, null otherwise
 */
protected String getGroupableFieldLabel(String datamartName, String fieldName) {
    DatamartContext ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(datamartName)
    List fieldList = dm.fc()?.fields
    def field = fieldList?.find { it?.name?.toLowerCase() == fieldName?.toLowerCase() && it?.dimension == true }

    return field ? field.label : null
}