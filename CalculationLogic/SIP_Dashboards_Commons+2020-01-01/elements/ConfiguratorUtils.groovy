import net.pricefx.server.dto.calculation.ConfiguratorEntry

/**
 * Sets the default value or user default value for a given configurator.
 * Only configurators with one input entry are supported by this method.
 * @param configuratorEntry configurator entry that contains one input parameter to be used for the default value
 * @param defaultValue default value to be used in case no userDefaultValue is defined and no value has been selected by the user.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 */
void setConfiguratorEntryDefaultValue(def configuratorEntry, def defaultValue, def userDefaultValue = null) {
    def firstInput = configuratorEntry?.getFirstInput()

    if (configuratorEntry != null && firstInput) {
        setParameterDefaultValue(firstInput, defaultValue, userDefaultValue)
    }
}

/**
 * Sets the default value for a given ContextParameter.
 * The value set depends on data availability.
 * In case both default value and userDefaultValue are present the userDefaultValue always has priority.
 * @param parameter parameter for which the default value should be set
 * @param defaultValue default value to be used in case no userDefaultValue is defined and no value has been selected by the user.
 * @param userDefaultValue value defined by the user for the given entry in the Default Filters configurator wizard.
 */
void setParameterDefaultValue(def parameter, def defaultValue, def userDefaultValue = null) {
    if (parameter != null && parameter.getValue() == null) {
        parameter.setValue(userDefaultValue != null ? userDefaultValue : defaultValue)
    }
}

/**
 * Used in DefaultFilters configurator wizard to set the stored value during the initial render of the configurator
 * @param parameter parameter for which the default value should be set
 * @param defaultValue default value to be used in case no userDefaultValue is defined and no value has been selected by the user.
 * @param isInit special flag used in the DefaultFilter wizard that defines whether the configurator is initialized
 * @param fallbackValue fallback value in case default value is not provided
 */
void setParameterInitialValue(def parameter, def defaultValue, Boolean isInit, def fallbackValue = null) {
    if(isInit == null) {
        parameter.setValue(defaultValue != null ? defaultValue : fallbackValue)
    }
}

/**
 * Validates whether the given configurator parameter value is in one of the values that are allowed,
 * if not the value is set to null.
 * Used to reset the configurator parameters in various configurators.
 * @param configuratorParameter parameter for which the check should be made
 * @param allowedValues list of all values that the provided configurator parameter can have
 */
void assertCurrentConfiguratorValue(def configuratorParameter, List allowedValues) {
    def storedConfiguratorValue = configuratorParameter?.getValue()

    if (storedConfiguratorValue != null && !allowedValues.contains(storedConfiguratorValue)) {
        configuratorParameter.setValue(null)
    }
}

/**
 * Sets the noRefresh flag to true on a given configurator entry
 * @param configuratorEntry configurator entry to set the noRefresh flag for
 */
void setNoRefreshParameter(ConfiguratorEntry configuratorEntry) {
    configuratorEntry?.getFirstInput()?.addParameterConfigEntry("noRefresh", true)
}

/**
 * Defines the values and labels values based on a strict structure of the const config provided.
 * The constConfig provided must be of such structure:
 * [(WATERFALL_MODEL_ABSOLUTE_NAME)      : [LABEL: "Absolute"],
 *  (WATERFALL_MODEL_DETAIL_NAME)        : [LABEL: "Absolute Detail"],
 *  (WATERFALL_MODEL_ABSOLUTE_UNIT_NAME) : [LABEL: "By Absolute Unit"],
 *  (WATERFALL_MODEL_PERCENTAGE_NAME)    : [LABEL: "Percentage]]
 * The key will be used as a key returned from the entry, the LABEL will be used for mapping between key-display label.
 * @param configuratorConstConfig constConfig of a given configurator retrieved from libs.SIP_Dashboards_Commons.ConstConfig element
 * @return a Map of values and labels to be used in an option input
 */
Map getDashboardInputOptionKeyLabelConfig(Map configuratorConstConfig) {
    return [values: configuratorConstConfig.keySet() as List,
            labels: configuratorConstConfig.collectEntries { [(it.key): it.value.LABEL] }]
}