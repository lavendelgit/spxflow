import groovy.transform.Field

/**
 * Holds all information about the available map codes for hierarchy level: world
 * Only those hierarchies mentioned in this Map can be drilled down.
 * Information stored:
 * - LABEL - default label used in case user has not defined a custom one
 * - ISO_CODE - by default null, because world does not contain an ISO_CODE, it's there for unification purposes
 * - MAP_CODE - HighMaps code that defines the map to be used for a given entry
 */
@Field Map WORLD_MAP_CODES = [WORLD_CONTINENTS: [LABEL   : "World continents",
                                                 ISO_CODE: null,
                                                 MAP_CODE: "custom/world-continents"]]

/**
 * Holds all information about the available map codes for hierarchy level: continent
 * Only those hierarchies mentioned in this Map can be drilled down.
 * Information stored:
 * - LABEL - default label used in case user has not defined a custom one
 * - ISO_CODE - iso code for the defined entry
 * - MAP_CODE - HighMaps code that defines the map to be used for a given entry
 */
@Field Map CONTINENT_MAP_CODES = [EU: [LABEL   : "Europe",
                                       ISO_CODE: "EU",
                                       MAP_CODE: "custom/europe"],
                                  NA: [LABEL   : "North America",
                                       ISO_CODE: "NA",
                                       MAP_CODE: "custom/north-america"],
                                  AS: [LABEL   : "Asia",
                                       ISO_CODE: "AS",
                                       MAP_CODE: "custom/asia"],
                                  OC: [LABEL   : "Oceania",
                                       ISO_CODE: "OC",
                                       MAP_CODE: "custom/oceania"],
                                  AF: [LABEL   : "Africa",
                                       ISO_CODE: "AF",
                                       MAP_CODE: "custom/africa"],
                                  SA: [LABEL   : "South America",
                                       ISO_CODE: "SA",
                                       MAP_CODE: "custom/south-america"]]

/**
 * Holds all information about the available map codes for hierarchy level: world
 * Only those hierarchies mentioned in this Map can be drilled down.
 * Information stored:
 * - PARENT - defines the higher level hierarchy that contains this entry. Used to create parent-child relations.
 * - LABEL - default label used in case user has not defined a custom one
 * - ISO_CODE - iso code for the defined entry
 * - MAP_CODE - HighMaps code that defines the map to be used for a given entry
 */
@Field Map COUNTRY_MAP_CODES = [US: [PARENT  : CONTINENT_MAP_CODES.NA,
                                     LABEL   : "United States of America",
                                     ISO_CODE: "US",
                                     MAP_CODE: "countries/us/us-all"],
                                CA: [PARENT  : CONTINENT_MAP_CODES.NA,
                                     LABEL   : "Canada",
                                     ISO_CODE: "CA",
                                     MAP_CODE: "countries/ca/ca-all"],
                                CN: [PARENT  : CONTINENT_MAP_CODES.AS,
                                     LABEL   : "China",
                                     ISO_CODE: "CN",
                                     MAP_CODE: "countries/cn/cn-all"],
                                JP: [PARENT  : CONTINENT_MAP_CODES.AS,
                                     LABEL   : "Japan",
                                     ISO_CODE: "JP",
                                     MAP_CODE: "countries/jp/jp-all"],
                                KR: [PARENT  : CONTINENT_MAP_CODES.AS,
                                     LABEL   : "South Korea",
                                     ISO_CODE: "KR",
                                     MAP_CODE: "countries/kr/kr-all"],
                                AU: [PARENT  : CONTINENT_MAP_CODES.OC,
                                     LABEL   : "Australia",
                                     ISO_CODE: "AU",
                                     MAP_CODE: "countries/au/au-all"],
                                NZ: [PARENT  : CONTINENT_MAP_CODES.OC,
                                     LABEL   : "New Zealand",
                                     ISO_CODE: "NZ",
                                     MAP_CODE: "countries/nz/nz-all"],
                                CZ: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Czech Republic",
                                     ISO_CODE: "CZ",
                                     MAP_CODE: "countries/cz/cz-all"],
                                DK: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Denmark",
                                     ISO_CODE: "DK",
                                     MAP_CODE: "countries/dk/dk-all"],
                                FI: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Finland",
                                     ISO_CODE: "FI",
                                     MAP_CODE: "countries/fi/fi-all"],
                                FR: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "France",
                                     ISO_CODE: "FR",
                                     MAP_CODE: "countries/fr/fr-all"],
                                DE: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Germany",
                                     ISO_CODE: "DE",
                                     MAP_CODE: "countries/de/de-all"],
                                HU: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Hungary",
                                     ISO_CODE: "HU",
                                     MAP_CODE: "countries/hu/hu-all"],
                                IT: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Italy",
                                     ISO_CODE: "IT",
                                     MAP_CODE: "countries/it/it-all"],
                                NL: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Netherlands",
                                     ISO_CODE: "NL",
                                     MAP_CODE: "countries/nl/nl-all"],
                                PL: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Poland",
                                     ISO_CODE: "PL",
                                     MAP_CODE: "countries/pl/pl-all"],
                                ES: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Spain",
                                     ISO_CODE: "ES",
                                     MAP_CODE: "countries/es/es-all"],
                                SE: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Sweden",
                                     ISO_CODE: "SE",
                                     MAP_CODE: "countries/se/se-all"],
                                CH: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "Switzerland",
                                     ISO_CODE: "CH",
                                     MAP_CODE: "countries/ch/ch-all"],
                                GB: [PARENT  : CONTINENT_MAP_CODES.EU,
                                     LABEL   : "United Kingdom",
                                     ISO_CODE: "GB",
                                     MAP_CODE: "countries/gb/gb-all"]]

/**
 * Holds all information about the available map codes for hierarchy level: region
 * Only those hierarchies mentioned in this Map can be drilled down.
 * Information stored:
 * - PARENT - defines the higher level hierarchy that contains this entry. Used to create parent-child relations.
 * - LABEL - default label used in case user has not defined a custom one
 * - ISO_CODE - iso code for the defined entry
 * - MAP_CODE - HighMaps code that defines the map to be used for a given entry
 */
@Field Map REGION_MAP_CODES = [:]


/**
 * Holds all information about the available map codes for hierarchy level: sector
 * Only those hierarchies mentioned in this Map can be drilled down.
 * Information stored:
 * - PARENT - defines the higher level hierarchy that contains this entry. Used to create parent-child relations.
 * - LABEL - default label used in case user has not defined a custom one
 * - ISO_CODE - iso code for the defined entry
 * - MAP_CODE - HighMaps code that defines the map to be used for a given entry
 */
@Field Map SECTOR_MAP_CODES = [:]