/**
 * Displays configurator that allows the setup the input user default values for the Revenue and Margin Dashboard
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry()
Map inputConfig = libs.SIP_Dashboards_Commons.ConstConfig.REVENUE_AND_MARGIN_DASHBOARD_CONFIG.INPUTS

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.REVENUE_AND_MARGIN_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.REVENUE_AND_MARGIN_NAME, existingDefaultFilters)

def productParameter = configuratorEntry.createParameter(InputType.PRODUCTGROUP, inputConfig.PRODUCT.UNIQUE_KEY)
        .setLabel(inputConfig.PRODUCT.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(productParameter, existingValue?.getAt(inputConfig.PRODUCT.UNIQUE_KEY), isInit)

if (isCustomerDataUsed) {
    def customerParameter = configuratorEntry.createParameter(InputType.CUSTOMERGROUP, inputConfig.CUSTOMER.UNIQUE_KEY)
            .setLabel(inputConfig.CUSTOMER.LABEL)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(customerParameter, existingValue?.getAt(inputConfig.CUSTOMER.UNIQUE_KEY), isInit)
}

def dateToParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_FROM.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_FROM.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateToParameter, existingValue?.getAt(inputConfig.DATE_FROM.UNIQUE_KEY), isInit)

def dateFromParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_TO.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_TO.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateFromParameter, existingValue?.getAt(inputConfig.DATE_TO.UNIQUE_KEY), isInit)

def timePeriodParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.TIME_PERIOD.UNIQUE_KEY)
        .setValueOptions(inputConfig.TIME_PERIOD.VALUES)
        .setLabel(inputConfig.TIME_PERIOD.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(timePeriodParameter, existingValue?.getAt(inputConfig.TIME_PERIOD.UNIQUE_KEY), isInit)

InputConfiguratorUtils.addAggregationInputParametersToConfigurator(configuratorEntry,
        inputConfig,
        existingValue,
        isCustomerDataUsed,
        isInit)

addBandByAggregationParameters(configuratorEntry,
        inputConfig,
        existingValue,
        isCustomerDataUsed,
        isInit)

Map axisTypeConfig = libs.SIP_Dashboards_Commons.ConstConfig.getRevenueAndMarginAxisTypeConfig()
List axisTypeKeys = axisTypeConfig.VALUES.keySet() as List
def axisTypeParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.AXIS_TYPE.UNIQUE_KEY)
        .setValueOptions(axisTypeKeys)
        .setLabel(inputConfig.AXIS_TYPE.LABEL)
axisTypeParameter.addParameterConfigEntry("labels", axisTypeConfig.VALUES)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(axisTypeParameter, existingValue?.getAt(inputConfig.AXIS_TYPE.UNIQUE_KEY), isInit)

return configuratorEntry

protected void addBandByAggregationParameters(ConfiguratorEntry configurator,
                                              Map inputConfig,
                                              Map existingConfiguratorValues,
                                              boolean isCustomerDataUsed,
                                              Boolean isInit) {
    InputConfiguratorUtils.addAggregationParameterToConfigurator(configurator, inputConfig.BAND_BY_PRODUCT, Configuration.PRODUCT_DIMENSIONS, existingConfiguratorValues, isInit)
    if (isCustomerDataUsed) {
        InputConfiguratorUtils.addAggregationParameterToConfigurator(configurator, inputConfig.BAND_BY_CUSTOMER, Configuration.CUSTOMER_DIMENSIONS, existingConfiguratorValues, isInit)
    }
}