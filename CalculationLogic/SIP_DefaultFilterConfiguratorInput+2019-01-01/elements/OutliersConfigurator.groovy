/**
 * Displays configurator that allows the setup the input user default values for the Outliers Dashboard
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def commonsLib = libs.SIP_Dashboards_Commons
def commonsConstConfig = commonsLib.ConstConfig
Map dashboardConfig = commonsConstConfig.OUTLIERS_DASHBOARD_CONFIG

Map inputConfig = dashboardConfig.INPUTS
Map sqlConfiguration = Configuration.SQL_CONFIGURATION

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.OUTLIERS_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.OUTLIERS_NAME, existingDefaultFilters)

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry()

def productParameter = configuratorEntry.createParameter(InputType.PRODUCTGROUP, inputConfig.PRODUCT.UNIQUE_KEY)
        .setLabel(inputConfig.PRODUCT.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(productParameter, existingValue?.getAt(inputConfig.PRODUCT.UNIQUE_KEY), isInit)

if(isCustomerDataUsed)
{
    def customerParameter = configuratorEntry.createParameter(InputType.CUSTOMERGROUP, inputConfig.CUSTOMER.UNIQUE_KEY)
            .setLabel(inputConfig.CUSTOMER.LABEL)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(customerParameter, existingValue?.getAt(inputConfig.CUSTOMER.UNIQUE_KEY), isInit)
}

InputConfiguratorUtils.addAggregationInputParametersToConfigurator(configuratorEntry,
        inputConfig,
        existingValue,
        isCustomerDataUsed,
        isInit)

def dateFromParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_FROM.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_FROM.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateFromParameter, existingValue?.getAt(inputConfig.DATE_FROM.UNIQUE_KEY), isInit)

def dateToParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_TO.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_TO.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateToParameter, existingValue?.getAt(inputConfig.DATE_TO.UNIQUE_KEY), isInit)

def noOfResultsParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.NO_OF_RESULTS.UNIQUE_KEY)
        .setLabel(isCustomerDataUsed ? inputConfig.NO_OF_RESULTS.LABEL : inputConfig.NO_OF_RESULTS.NO_CUSTOMER_LABEL)
        .setValueOptions(inputConfig.NO_OF_RESULTS.VALUES)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterDefaultValue(noOfResultsParameter, inputConfig.NO_OF_RESULTS.DEFAULT_VALUE, existingValue?.getAt(inputConfig.NO_OF_RESULTS.UNIQUE_KEY))

def modelParameter = libs.SIP_Dashboards_Commons.OutliersUtils.addModelSelectionParameter(configuratorEntry, existingValue?.getAt(inputConfig.MODELS.UNIQUE_KEY))

libs.SIP_Dashboards_Commons.OutliersUtils.addKPISelectionParameter(configuratorEntry, modelParameter.getValue(), existingValue?.getAt(inputConfig.KPI.UNIQUE_KEY))

return configuratorEntry