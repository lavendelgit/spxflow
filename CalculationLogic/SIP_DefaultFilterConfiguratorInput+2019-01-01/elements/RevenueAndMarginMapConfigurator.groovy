/**
 * Displays configurator that allows the setup the input user default values for the Regional Revenue and Margin Dashboard
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry()
Map inputConfig = libs.SIP_Dashboards_Commons.ConstConfig.REGIONAL_REVENUE_AND_MARGIN_DASHBOARD_CONFIG.INPUTS

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.REVENUE_AND_MARGIN_MAP_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.REVENUE_AND_MARGIN_MAP_NAME, existingDefaultFilters)

def productParameter = configuratorEntry.createParameter(InputType.PRODUCTGROUP, inputConfig.PRODUCT.UNIQUE_KEY)
        .setLabel(inputConfig.PRODUCT.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(productParameter, existingValue?.getAt(inputConfig.PRODUCT.UNIQUE_KEY), isInit)

if (isCustomerDataUsed) {
    def customerParameter = configuratorEntry.createParameter(InputType.CUSTOMERGROUP, inputConfig.CUSTOMER.UNIQUE_KEY)
            .setLabel(inputConfig.CUSTOMER.LABEL)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(customerParameter, existingValue?.getAt(inputConfig.CUSTOMER.UNIQUE_KEY), isInit)
}

def dateToParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_FROM.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_FROM.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateToParameter, existingValue?.getAt(inputConfig.DATE_FROM.UNIQUE_KEY), isInit)

def dateFromParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_TO.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_TO.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateFromParameter, existingValue?.getAt(inputConfig.DATE_TO.UNIQUE_KEY), isInit)

Map kpiValues = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.getSupportedKPI(isCustomerDataUsed)
Map kpiOptionsConfig = libs.SIP_Dashboards_Commons.ConfiguratorUtils.getDashboardInputOptionKeyLabelConfig(kpiValues)
def kpiParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.KPI.UNIQUE_KEY)
        .setLabel(inputConfig.KPI.LABEL)
        .setValueOptions(kpiOptionsConfig.values)
kpiParameter.addParameterConfigEntry("labels", kpiOptionsConfig.labels)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(kpiParameter, existingValue?.getAt(inputConfig.KPI.UNIQUE_KEY), isInit)

Map mapCodeOverrides = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.getMapCodeOverrides()
Map usedFields = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.getUsedHierarchyFields()
List queryData = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.getConfiguratorQueryData(usedFields)
Map processedData = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.processConfiguratorQueryData(queryData, mapCodeOverrides)

def worldParameter = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.addWorldConfiguratorParameter(configuratorEntry, usedFields, existingValue?.getAt(commonsConstConfig.WORLD_HIERARCHY_KEY))
boolean worldEntryValue = worldParameter?.getValue()

Map availableContinentsDefinition = processedData.continent
def continentParameter = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.addContinentConfiguratorParameter(configuratorEntry,
        usedFields,
        worldEntryValue,
        availableContinentsDefinition,
        existingValue?.getAt(commonsConstConfig.CONTINENT_HIERARCHY_KEY),
        isInit)

Map availableCountriesDefinition = processedData.country
String selectedContinent = continentParameter?.getValue()

def countryParameter = libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.addCountryConfiguratorParameter(configuratorEntry,
        usedFields,
        worldEntryValue,
        selectedContinent,
        availableCountriesDefinition,
        existingValue?.getAt(commonsConstConfig.COUNTRY_HIERARCHY_KEY),
        isInit)

Map availableRegionsDefinition = processedData.region

String selectedCountry = countryParameter?.getValue()

libs.SIP_Dashboards_Commons.RegionalRevenueAndMarginUtils.addRegionConfiguratorParameter(configuratorEntry,
        usedFields,
        worldEntryValue,
        selectedCountry,
        availableRegionsDefinition,
        existingValue?.getAt(commonsConstConfig.REGION_HIERARCHY_KEY),
        isInit)

return configuratorEntry