/**
 * Displays configurator that allows the setup of the GeneralFilter for all the dashboards
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig
Map sqlConfiguration = Configuration.SQL_CONFIGURATION
Map inputConfig = commonsConstConfig.DEFAULT_CONFIGURATOR_CONFIG.INPUTS.GENERAL_FILTER

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.GENERAL_FILTER_NAME, existingDefaultFilters).getAt(inputConfig.UNIQUE_KEY)

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry()
configuratorEntry.createParameter(InputType.DMFILTERBUILDER, inputConfig.UNIQUE_KEY)
        .setLabel(inputConfig.LABEL)
        .addParameterConfigEntry("dmSourceName", sqlConfiguration.datamartName)

libs.SIP_Dashboards_Commons.ConfiguratorUtils.setConfiguratorEntryDefaultValue(configuratorEntry, existingValue)

return configuratorEntry