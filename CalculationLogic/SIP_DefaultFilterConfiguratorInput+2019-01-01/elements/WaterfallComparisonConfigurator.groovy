/**
 * Displays configurator that allows the setup the input user default values for the Waterfall Comparison Dashboard
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def waterfallUtils = libs.SIP_Dashboards_Commons.WaterfallUtils
def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig
def dashboardConfig = commonsConstConfig.WATERFALL_COMPARISON_DASHBOARD_CONFIG
Map inputConfig = dashboardConfig.INPUTS

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.COMPARISON_WATERFALL_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.COMPARISON_WATERFALL_NAME, existingDefaultFilters)

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry()
def dimensionParameter = waterfallUtils.addComparisonDimensionParameter(configuratorEntry, isCustomerDataUsed, existingValue?.getAt(inputConfig.COMPARISON_DIMENSIONS.UNIQUE_KEY))
String comparisonDimensionKey = dimensionParameter.getValue()

addProductParameters(configuratorEntry, comparisonDimensionKey, commonsConstConfig, inputConfig.PRODUCT, existingValue, isInit)
if (isCustomerDataUsed) {
    addCustomerParameters(configuratorEntry, comparisonDimensionKey, commonsConstConfig, inputConfig.CUSTOMER, existingValue, isInit)
}
addPrimaryDateParameters(configuratorEntry, comparisonDimensionKey, commonsConstConfig, inputConfig.DATE_FROM, inputConfig.DATE_TO, existingValue, isInit)
addSecondaryDateParameters(configuratorEntry, comparisonDimensionKey, commonsConstConfig, inputConfig.DATE_FROM, inputConfig.DATE_TO, existingValue, isInit)

Map optionsConfig = libs.SIP_Dashboards_Commons.ConfiguratorUtils.getDashboardInputOptionKeyLabelConfig(dashboardConfig.MODELS)
def modelParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.MODELS.UNIQUE_KEY)
        .setValueOptions(optionsConfig.values)
        .setLabel(inputConfig.MODELS.LABEL)
modelParameter.addParameterConfigEntry("labels", optionsConfig.labels)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(modelParameter, existingValue?.getAt(inputConfig.MODELS.UNIQUE_KEY), isInit)

return configuratorEntry

protected void addProductParameters(ConfiguratorEntry configurator,
                                    String comparisonDimensionKey,
                                    def commonsConstConfig,
                                    Map inputConfig,
                                    Map existingValue,
                                    Boolean isInit) {
    libs.SIP_Dashboards_Commons.WaterfallUtils.addPrimaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_PRODUCT_KEY,
            inputConfig.LABEL + inputConfig.SUFFIXES.FIRST,
            inputConfig.LABEL,
            inputConfig.UNIQUE_KEY + inputConfig.SUFFIXES.FIRST,
            inputConfig.UNIQUE_KEY,
            extractExistingValueOfPrimaryInput(existingValue, inputConfig),
            null,
            isInit)

    libs.SIP_Dashboards_Commons.WaterfallUtils.addSecondaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_PRODUCT_KEY,
            inputConfig.LABEL + inputConfig.SUFFIXES.SECOND,
            inputConfig.UNIQUE_KEY + inputConfig.SUFFIXES.SECOND,
            extractExistingValueOfSecondaryInput(existingValue, inputConfig),
            null,
            isInit)
}

protected void addCustomerParameters(ConfiguratorEntry configurator,
                                     String comparisonDimensionKey,
                                     def commonsConstConfig,
                                     Map inputConfig,
                                     Map existingValue,
                                     Boolean isInit) {
    libs.SIP_Dashboards_Commons.WaterfallUtils.addPrimaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_CUSTOMER_KEY,
            inputConfig.LABEL + inputConfig.SUFFIXES.FIRST,
            inputConfig.LABEL,
            inputConfig.UNIQUE_KEY + inputConfig.SUFFIXES.FIRST,
            inputConfig.UNIQUE_KEY,
            extractExistingValueOfPrimaryInput(existingValue, inputConfig),
            null,
            isInit)

    libs.SIP_Dashboards_Commons.WaterfallUtils.addSecondaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_CUSTOMER_KEY,
            inputConfig.LABEL + inputConfig.SUFFIXES.SECOND,
            inputConfig.UNIQUE_KEY + inputConfig.SUFFIXES.SECOND,
            extractExistingValueOfSecondaryInput(existingValue, inputConfig),
            null,
            isInit)
}

protected void addPrimaryDateParameters(ConfiguratorEntry configurator,
                                        String comparisonDimensionKey,
                                        def commonsConstConfig,
                                        Map dateFromConfig,
                                        Map dateToConfig,
                                        Map existingValue,
                                        Boolean isInit) {
    libs.SIP_Dashboards_Commons.WaterfallUtils.addPrimaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_DATE_KEY,
            dateFromConfig.LABEL + dateFromConfig.SUFFIXES.FIRST,
            dateFromConfig.LABEL,
            dateFromConfig.UNIQUE_KEY + dateFromConfig.SUFFIXES.FIRST,
            dateFromConfig.UNIQUE_KEY,
            extractExistingValueOfPrimaryInput(existingValue, dateFromConfig),
            null,
            isInit)

    libs.SIP_Dashboards_Commons.WaterfallUtils.addPrimaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_DATE_KEY,
            dateToConfig.LABEL + dateToConfig.SUFFIXES.FIRST,
            dateToConfig.LABEL,
            dateToConfig.UNIQUE_KEY + dateToConfig.SUFFIXES.FIRST,
            dateToConfig.UNIQUE_KEY,
            extractExistingValueOfPrimaryInput(existingValue, dateToConfig),
            null,
            isInit)
}

protected void addSecondaryDateParameters(ConfiguratorEntry configurator,
                                          String comparisonDimensionKey,
                                          def commonsConstConfig,
                                          Map dateFromConfig,
                                          Map dateToConfig,
                                          Map existingValue,
                                          Boolean isInit) {
    libs.SIP_Dashboards_Commons.WaterfallUtils.addSecondaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_DATE_KEY,
            dateFromConfig.LABEL + dateFromConfig.SUFFIXES.SECOND,
            dateFromConfig.UNIQUE_KEY + dateFromConfig.SUFFIXES.SECOND,
            extractExistingValueOfSecondaryInput(existingValue, dateFromConfig),
            null,
            isInit)

    libs.SIP_Dashboards_Commons.WaterfallUtils.addSecondaryComparisonParameter(configurator,
            comparisonDimensionKey,
            commonsConstConfig.COMPARISON_DIMENSION_DATE_KEY,
            dateToConfig.LABEL + dateToConfig.SUFFIXES.SECOND,
            dateToConfig.UNIQUE_KEY + dateToConfig.SUFFIXES.SECOND,
            extractExistingValueOfSecondaryInput(existingValue, dateToConfig),
            null,
            isInit)
}


protected def extractExistingValueOfPrimaryInput(Map existingValue, Map inputConfig) {
    def storedInputValue = existingValue?.getAt(inputConfig.UNIQUE_KEY)
    if (!(storedInputValue instanceof Map)) {
        return storedInputValue
    }

    return storedInputValue?.FIRST ?: existingValue?.getAt(inputConfig.UNIQUE_KEY)
}

protected def extractExistingValueOfSecondaryInput(Map existingValue, Map inputConfig) {
    def storedInputValue = existingValue?.getAt(inputConfig.UNIQUE_KEY)
    if (!(storedInputValue instanceof Map)) {
        return storedInputValue
    }

    return storedInputValue?.SECOND
}
