/**
 * Based on the provided SelectedConfiguration value displays and marks the given configurator as initialized.
 */
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def constConfig = libs.SIP_Dashboards_Commons.ConstConfig

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
String selectedConfiguration = api.getElement("SelectedConfiguration")

InputConfiguratorUtils.markConfiguratorAsInitialized(selectedConfiguration, initializationStateConfiguratorArray)

switch (selectedConfiguration) {
    case constConfig.GENERAL_FILTER_NAME:
        return api.getElement("DefaultConfigurator")
    case constConfig.OUTLIERS_NAME:
        return api.getElement("OutliersConfigurator")
    case constConfig.REVENUE_AND_MARGIN_NAME:
        return api.getElement("RevenueAndMarginConfigurator")
    case constConfig.REVENUE_AND_MARGIN_MAP_NAME:
        return api.getElement("RevenueAndMarginMapConfigurator")
    case constConfig.WATERFALL_NAME:
        return api.getElement("WaterfallConfigurator")
    case constConfig.COMPARISON_WATERFALL_NAME:
        return api.getElement("WaterfallComparisonConfigurator")
    case constConfig.REVENUE_BREAKDOWN_NAME:
        return api.getElement("RevenueBreakdownConfigurator")
    case constConfig.MARGIN_BREAKDOWN_NAME:
        return api.getElement("MarginBreakdownConfigurator")
    case constConfig.PRODUCT_CUSTOMER_CAUSALITY_NAME:
        return api.getElement("ProductCustomerCausalityConfigurator")
}