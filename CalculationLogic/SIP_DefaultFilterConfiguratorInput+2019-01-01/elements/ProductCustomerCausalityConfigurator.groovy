/**
 * Displays configurator that allows the setup the input user default values for the Causality Dashboard
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig

Map dashboardConfig = libs.SIP_Dashboards_Commons.ConstConfig.CAUSALITY_DASHBOARD_CONFIG
Map inputConfig = dashboardConfig.INPUTS

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.PRODUCT_CUSTOMER_CAUSALITY_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.PRODUCT_CUSTOMER_CAUSALITY_NAME, existingDefaultFilters)

ConfiguratorEntry configuratorEntry = InputConfiguratorUtils.getBreakdownConfigurator(inputConfig, existingValue, isCustomerDataUsed, isInit)

def noOfResultsParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.NO_OF_RESULTS.UNIQUE_KEY)
        .setValueOptions(inputConfig.NO_OF_RESULTS.VALUES)
        .setLabel(isCustomerDataUsed ? inputConfig.NO_OF_RESULTS.LABEL : inputConfig.NO_OF_RESULTS.NO_CUSTOMER_LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(noOfResultsParameter, existingValue?.getAt(inputConfig.NO_OF_RESULTS.UNIQUE_KEY), isInit)

return configuratorEntry
