/**
 * Checks whether customer data is mapped and should be used by the dashboard.
 */
return Configuration.SQL_CONFIGURATION.customerId as boolean