import groovy.transform.Field

def sipCommons = libs.SIP_Dashboards_Commons
def commonsConstConfig = sipCommons.ConstConfig

/**
 * Configuration for datamart fields, used in queries. Structure is defined in AdvancedConfiguration/SIP_AdvancedConfiguration
 */
@Field Map SQL_CONFIGURATION
SQL_CONFIGURATION = sipCommons.ConfigurationUtils.getAdvancedConfiguration([:], commonsConstConfig.DASHBOARDS_ADVANCED_CONFIGURATION_NAME)

/**
 * Configuration for product aggregation dimensions that are used in the configurator. Values are defined by the user and stored in SIP_AdvancedConfiguration
 */
@Field PRODUCT_DIMENSIONS
PRODUCT_DIMENSIONS = libs.SIP_Dashboards_Commons.QueryUtils.getNameLabelPairs(SQL_CONFIGURATION.datamartName, SQL_CONFIGURATION.productDimensions ?: [SQL_CONFIGURATION.getAt("productId")], true)

/**
 * Configuration for customer aggregation dimensions that are used in the configurator. Values are defined by the user and stored in SIP_AdvancedConfiguration
 */
@Field CUSTOMER_DIMENSIONS
CUSTOMER_DIMENSIONS = libs.SIP_Dashboards_Commons.QueryUtils.getNameLabelPairs(SQL_CONFIGURATION.datamartName, SQL_CONFIGURATION.customerDimensions ?: [SQL_CONFIGURATION.getAt("customerId")], true)