/**
 * Prepares a configurator entry array object containing hidden configurators that are used to store the initialization state
 * of each of the dashboard configurators.
 */
import net.pricefx.common.api.InputType

List sortedFilters = libs.SIP_Dashboards_Commons.ConstConfig.getDefinedFilters()

List configuratorArrays = sortedFilters.collect {
    return api.createConfiguratorEntry(InputType.HIDDEN, InputConfiguratorUtils.getHiddenConfiguratorName(it))
}

return api.createConfiguratorEntryArray(*configuratorArrays)
