/**
 * Allows the selection which DefaultFilters configurator should be displayed.*
 */
import net.pricefx.server.dto.calculation.ConfiguratorEntry

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig
List sortedFilters = commonsConstConfig.getDefinedFilters()
Map inputConfig = commonsConstConfig.DEFAULT_CONFIGURATOR_CONFIG.INPUTS.DASHBOARD_SELECTION

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry()
def parameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.UNIQUE_KEY)
        .setLabel(inputConfig.LABEL)
        .setValueOptions(sortedFilters)

if (parameter != null && parameter.getValue() == null) {
    parameter.setValue(commonsConstConfig.GENERAL_FILTER_NAME)
}

return configuratorEntry