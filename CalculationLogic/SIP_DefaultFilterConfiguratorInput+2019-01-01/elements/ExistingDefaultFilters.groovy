/**
 * Retrieves the whole structure for the SIP_DefaultFilters PP table to be used by respective configurators.
 */
return libs.SIP_Dashboards_Commons.InputUtils.getDefaultInputConfiguration()