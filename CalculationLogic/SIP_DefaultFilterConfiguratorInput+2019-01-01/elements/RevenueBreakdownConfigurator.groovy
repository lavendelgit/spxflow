/**
 * Displays configurator that allows the setup the input user default values for the Revenue Breakdown Dashboard
 */
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig
Map inputsConfig = commonsConstConfig.REVENUE_BREAKDOWN_DASHBOARD_CONFIG.INPUTS

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.REVENUE_BREAKDOWN_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.REVENUE_BREAKDOWN_NAME, existingDefaultFilters)

return InputConfiguratorUtils.getBreakdownConfigurator(inputsConfig, existingValue, isCustomerDataUsed, isInit)