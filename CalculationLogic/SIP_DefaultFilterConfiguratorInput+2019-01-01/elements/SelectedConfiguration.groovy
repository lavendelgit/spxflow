/**
 * Retrieves the value of the selected ConfigurationSelection configurator entry
 */
return api.getElement("ConfigurationSelection").getFirstInput().getValue()