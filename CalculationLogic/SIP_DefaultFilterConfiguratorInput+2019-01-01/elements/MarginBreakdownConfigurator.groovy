/**
 * Displays configurator that allows the setup the input user default values for the Margin Breakdown Dashboard
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig
Map inputsConfig = commonsConstConfig.MARGIN_BREAKDOWN_DASHBOARD_CONFIG.INPUTS

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.MARGIN_BREAKDOWN_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.MARGIN_BREAKDOWN_NAME, existingDefaultFilters)

ConfiguratorEntry configuratorEntry = InputConfiguratorUtils.getBreakdownConfigurator(inputsConfig, existingValue, isCustomerDataUsed, isInit)
Map dashboardConfig = libs.SIP_Dashboards_Commons.ConstConfig.MARGIN_BREAKDOWN_DASHBOARD_CONFIG
Map inputConfig = dashboardConfig.INPUTS

Map optionsConfig = libs.SIP_Dashboards_Commons.ConfiguratorUtils.getDashboardInputOptionKeyLabelConfig(dashboardConfig.MODELS)
def modelParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.MODELS.UNIQUE_KEY)
        .setLabel(inputConfig.MODELS.LABEL)
        .setValueOptions(optionsConfig.values)
modelParameter.addParameterConfigEntry("labels", optionsConfig.labels)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(modelParameter, existingValue?.getAt(inputConfig.MODELS.UNIQUE_KEY), isInit)

return configuratorEntry