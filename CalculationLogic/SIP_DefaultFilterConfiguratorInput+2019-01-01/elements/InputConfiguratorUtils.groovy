import net.pricefx.common.api.InputType
import net.pricefx.formulaengine.DatamartContext
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

/**
 * Retrieves the configurator for Causality Dashboards
 * @param inputConfig input config for the entry stored in libs.SIP_Dashboards_Commons.ConstConfig
 * @param existingConfiguratorValues existing values for a given configurator as returned by InputConfiguratorUtils.getDefaultFilterConfig
 * @param isCustomerDataUsed states whether the customer data is mapped and used by the package
 * @param isInit special flag used in the DefaultFilter wizard that defines whether the configurator is initialized
 * @return Causality Dashboards Default configurator populated based on existingConfiguratorValues and the inputConfig
 */
def getBreakdownConfigurator(Map inputConfig, Map existingConfiguratorValues, boolean isCustomerDataUsed, Boolean isInit) {
    ConfiguratorEntry configurator = api.createConfiguratorEntry()
    Map sqlConfiguration = Configuration.SQL_CONFIGURATION
    DatamartContext datamartContext = api.getDatamartContext()

    def productParameter = configurator.createParameter(InputType.PRODUCTGROUP, inputConfig.PRODUCT.UNIQUE_KEY)
            .setLabel(inputConfig.PRODUCT.LABEL)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(productParameter, existingConfiguratorValues?.getAt(inputConfig.PRODUCT.UNIQUE_KEY), isInit)

    if (isCustomerDataUsed) {
        def customerParameter = configurator.createParameter(InputType.CUSTOMERGROUP, inputConfig.CUSTOMER.UNIQUE_KEY)
                .setLabel(inputConfig.CUSTOMER.LABEL)
        libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(customerParameter, existingConfiguratorValues?.getAt(inputConfig.CUSTOMER.UNIQUE_KEY), isInit)
    }

    addAggregationInputParametersToConfigurator(configurator,
            inputConfig,
            existingConfiguratorValues,
            isCustomerDataUsed,
            isInit)

    def yearParameter = configurator.createParameter(InputType.DMDIMFILTER, inputConfig.YEAR.UNIQUE_KEY)
            .setLabel(inputConfig.YEAR.LABEL)
    yearParameter.setURL(getDatamartURL(sqlConfiguration, datamartContext, inputConfig.YEAR.FIELD_SUFFIX))
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(yearParameter, existingConfiguratorValues?.getAt(inputConfig.YEAR.UNIQUE_KEY), isInit)

    def periodParameter = configurator.createParameter(InputType.OPTION, inputConfig.PERIOD.UNIQUE_KEY)
            .setLabel(inputConfig.PERIOD.LABEL)
            .setValueOptions(inputConfig.PERIOD.VALUES)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(periodParameter, existingConfiguratorValues?.getAt(inputConfig.PERIOD.UNIQUE_KEY), isInit)

    def comparisonYearParameter = configurator.createParameter(InputType.DMDIMFILTER, inputConfig.COMPARISON_YEAR.UNIQUE_KEY)
            .setLabel(inputConfig.COMPARISON_YEAR.LABEL)
    comparisonYearParameter.setURL(getDatamartURL(sqlConfiguration, datamartContext, inputConfig.COMPARISON_YEAR.FIELD_SUFFIX))
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(comparisonYearParameter, existingConfiguratorValues?.getAt(inputConfig.COMPARISON_YEAR.UNIQUE_KEY), isInit)

    def comparisonPeriodParameter = configurator.createParameter(InputType.OPTION, inputConfig.COMPARISON_PERIOD.UNIQUE_KEY)
            .setLabel(inputConfig.COMPARISON_PERIOD.LABEL)
            .setValueOptions(inputConfig.COMPARISON_PERIOD.VALUES)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(comparisonPeriodParameter, existingConfiguratorValues?.getAt(inputConfig.COMPARISON_PERIOD.UNIQUE_KEY), isInit)

    def showAsPercentageParameter = configurator.createParameter(InputType.BOOLEAN, inputConfig.SHOW_AS_PERCENTAGE.UNIQUE_KEY)
            .setLabel(inputConfig.SHOW_AS_PERCENTAGE.LABEL)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(showAsPercentageParameter, existingConfiguratorValues?.getAt(inputConfig.SHOW_AS_PERCENTAGE.UNIQUE_KEY), isInit)


    return configurator
}

/**
 * Adds product and customer aggregation input parameters to the configurator
 * Customer aggregation added only if the customer data is used.
 * @param configurator the configurator entry to which the parameter will be appended
 * @param sqlConfiguration sql configuration containing data mart fields mapping definition. Stored in SIP_AdvancedConfiguration.
 * @param inputConfig input config for the entry stored in libs.SIP_Dashboards_Commons.ConstConfig
 * @param existingConfiguratorValues existing values for a given configurator as returned by InputConfiguratorUtils.getDefaultFilterConfig
 * @param isCustomerDataUsed states whether the customer data is mapped and used by the package
 * @param isInit special flag used in the DefaultFilter wizard that defines whether the configurator is initialized
 */
void addAggregationInputParametersToConfigurator(ConfiguratorEntry configurator,
                                                 Map inputConfig,
                                                 Map existingConfiguratorValues,
                                                 boolean isCustomerDataUsed,
                                                 Boolean isInit) {
    addAggregationParameterToConfigurator(configurator, inputConfig.PRODUCT_AGGREGATION, Configuration.PRODUCT_DIMENSIONS, existingConfiguratorValues, isInit)
    if (isCustomerDataUsed) {
        addAggregationParameterToConfigurator(configurator, inputConfig.CUSTOMER_AGGREGATION, Configuration.CUSTOMER_DIMENSIONS, existingConfiguratorValues, isInit)
    }
}

/**
 * Adds a single aggregation input to a given configurator.
 * @param configurator the configurator entry to which the parameter will be appended
 * @param inputConfig input config for the entry stored in libs.SIP_Dashboards_Commons.ConstConfig
 * @param sqlConfiguration sql configuration containing data mart fields mapping definition. Stored in SIP_AdvancedConfiguration.
 * @param existingConfiguratorValues existing values for a given configurator as returned by InputConfiguratorUtils.getDefaultFilterConfig
 * @param isInit special flag used in the DefaultFilter wizard that defines whether the configurator is initialized
 */
void addAggregationParameterToConfigurator(ConfiguratorEntry configurator,
                                           Map inputConfig,
                                           Map dimensions,
                                           Map existingConfiguratorValues,
                                           Boolean isInit) {
    def input = configurator.createParameter(InputType.OPTION, inputConfig.UNIQUE_KEY)
            .setLabel(inputConfig.LABEL)
            .setValueOptions(dimensions.keySet() as List)
    input.addParameterConfigEntry("labels", dimensions)

    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(input, existingConfiguratorValues.getAt(inputConfig.UNIQUE_KEY), isInit)
}

/**
 * Retrieves the existingConfiguratorValues that are used for population of the configurator entries in the init state
 * @param filterName name of the dashboard for which the filters should be retrieved
 * @param existingDefaultFilters Map containing all definitions of the filters. Retrieved from ExistingDefaultFilters
 * @return Map containing the definitions of user filters for the given dashboard
 */
Map getDefaultFilterConfig(String filterName, Map existingDefaultFilters) {
    return api.jsonDecode(existingDefaultFilters.getAt(filterName))
}

/**
 * Retrieves the hidden configurator used to check whether the DefaultFilters wizard has been initialized
 * @param dashboardName name of the dashboard for which the init state should be retrieved
 * @param configuratorEntryArray configurator array of hidden configurators as defined by InitializeCheck
 * @return ConfiguratorEntry defining the init state for a given dashboard
 */
ConfiguratorEntry getHiddenConfigurator(String dashboardName, ConfiguratorEntryArray configuratorEntryArray) {
    return configuratorEntryArray.entries.find { it.inputs[0].name == getHiddenConfiguratorName(dashboardName) }
}

/**
 * Retrieves the name of the hidden configurator for a given dashboard.
 * @param dashboardName name of the dashboard for which the configurator name should be retrieved
 * @return name of the hidden configurator
 */
String getHiddenConfiguratorName(String dashboardName) {
    return dashboardName + "Init"
}

/**
 * Marks the given configurator for a given dashboard as initialized.
 * This means that the values of the user defined filters will no longer be applied there as it's been already rendered by the user.
 * @param dashboardName name of the dashboard for which the init state should be set
 * @param configuratorEntryArray configurator array of hidden configurators as defined by InitializeCheck
 */
void markConfiguratorAsInitialized(String dashboardName, ConfiguratorEntryArray configuratorEntryArray) {
    ConfiguratorEntry hiddenConfigurator = getHiddenConfigurator(dashboardName, configuratorEntryArray)
    if (hiddenConfigurator.getFirstInput().getValue() == null) {
        hiddenConfigurator.getFirstInput().setValue(true)
    }
}

/**
 * Retrieves the datamart URL for a given field suffix.
 * Used to retrieve values for Year and Comparison year configurator parameters for Causality Dashboards
 * @param sqlConfiguration sql configuration containing data mart fields mapping definition. Stored in SIP_AdvancedConfiguration.
 * @param datamartContext data mart context object.
 * @param fieldSuffix defines the suffix for the pricingDate field that should be fetched from the DM. Year by default.
 * @return URL that is used to retrieve values for Year and Comparison year configurator parameters
 */
protected String getDatamartURL(Map sqlConfiguration, DatamartContext datamartContext, String fieldSuffix) {
    def datamart = datamartContext.getDatamart(sqlConfiguration.datamartName)
    String dateColumnName = sqlConfiguration.pricingDate
    String typedId = datamart.fc.typedId

    return "${typedId}/${dateColumnName + fieldSuffix}"
}