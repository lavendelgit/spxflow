/**
 * Displays configurator that allows the setup the input user default values for the Waterfall Dashboard
 */
import net.pricefx.common.api.InputType
import net.pricefx.server.dto.calculation.ConfiguratorEntry
import net.pricefx.server.dto.calculation.ConfiguratorEntryArray

def commonsConstConfig = libs.SIP_Dashboards_Commons.ConstConfig

ConfiguratorEntry configuratorEntry = api.createConfiguratorEntry()
Map dashboardConfig = libs.SIP_Dashboards_Commons.ConstConfig.WATERFALL_DASHBOARD_CONFIG
Map inputConfig = dashboardConfig.INPUTS

ConfiguratorEntryArray initializationStateConfiguratorArray = api.getElement("InitializeCheck")
ConfiguratorEntry hiddenConfigurator = InputConfiguratorUtils.getHiddenConfigurator(libs.SIP_Dashboards_Commons.ConstConfig.WATERFALL_NAME, initializationStateConfiguratorArray)
Boolean isInit = hiddenConfigurator?.getFirstInput()?.getValue()

boolean isCustomerDataUsed = api.getElement("IsCustomerDataUsed")

Map existingDefaultFilters = api.getElement("ExistingDefaultFilters")
Map existingValue = InputConfiguratorUtils.getDefaultFilterConfig(commonsConstConfig.WATERFALL_NAME, existingDefaultFilters)

def productParameter = configuratorEntry.createParameter(InputType.PRODUCTGROUP, inputConfig.PRODUCT.UNIQUE_KEY)
        .setLabel(inputConfig.PRODUCT.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(productParameter, existingValue?.getAt(inputConfig.PRODUCT.UNIQUE_KEY), isInit)

if (isCustomerDataUsed) {
    def customerParameter = configuratorEntry.createParameter(InputType.CUSTOMERGROUP, inputConfig.CUSTOMER.UNIQUE_KEY)
            .setLabel(inputConfig.CUSTOMER.LABEL)
    libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(customerParameter, existingValue?.getAt(inputConfig.CUSTOMER.UNIQUE_KEY), isInit)
}

def dateToParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_FROM.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_FROM.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateToParameter, existingValue?.getAt(inputConfig.DATE_FROM.UNIQUE_KEY), isInit)

def dateFromParameter = configuratorEntry.createParameter(InputType.DATEUSERENTRY, inputConfig.DATE_TO.UNIQUE_KEY)
        .setLabel(inputConfig.DATE_TO.LABEL)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(dateFromParameter, existingValue?.getAt(inputConfig.DATE_TO.UNIQUE_KEY), isInit)

Map optionsConfig = libs.SIP_Dashboards_Commons.ConfiguratorUtils.getDashboardInputOptionKeyLabelConfig(dashboardConfig.MODELS)
def modelParameter = configuratorEntry.createParameter(InputType.OPTION, inputConfig.MODELS.UNIQUE_KEY)
        .setLabel(inputConfig.MODELS.LABEL)
        .setValueOptions(optionsConfig.values)
modelParameter.addParameterConfigEntry("labels", optionsConfig.labels)
libs.SIP_Dashboards_Commons.ConfiguratorUtils.setParameterInitialValue(modelParameter, existingValue?.getAt(inputConfig.MODELS.UNIQUE_KEY), isInit)

return configuratorEntry