def archivingConfiguration = out.ArchiveConfiguration
for(configuration in archivingConfiguration){
    processArchiving(configuration)
}
return

def processArchiving(configuration){
    if(configuration?.SourceTable && configuration?.DataSource){
        duplicateData = [:]
        businessKey = configuration["Keys"]?.split(',')*.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")*.trim()
        duplicateDetails = findDuplicateEntrys(configuration?.SourceTable,configuration?.SourceTypeCode,businessKey)
        List filter = [Filter.equal("name",configuration?.SourceTable),
        Filter.lessOrEqual("ValidFrom",new Date()?.format("YYYY-MM-dd"))
        ]
        businessKey?.each{
            filter << Filter.in(it,duplicateDetails[it])
        }
        String key
        iterarter = api.stream(configuration?.SourceTypeCode,"-ValidFrom",*filter)
        iterarter?.collect{attribute ->
            key = ""
            businessKey?.each{it ->
                key = key?key+"_"+attribute[it] : attribute[it]
            }
            if(!duplicateData[key]){
                duplicateData[key] = []
                duplicateData[key] << attribute
            }else{
                duplicateData[key] << attribute
            }
        }
        iterarter?.close()
        delete(duplicateData,configuration?.SourceTypeCode)
    }
}

List findDuplicateEntrys(String sourceTable, String sourceTypeCode, List bKey) {
    List filter = [Filter.equal("name",sourceTable),
                  Filter.greaterOrEqual("lastUpdateDate",out.CFLastRun),
    ]
    List filterAttributes = bKey
    iteratorData = api.stream(sourceTypeCode,"-ValidFrom",filterAttributes,*filter)
    duplicateRecords = iteratorData?.collect{it}
    iteratorData?.close()
    return duplicateRecords
}

void delete(Object duplicateRecords, String typeCode){
  def records
    for(duplicateRecord in duplicateRecords){
        if(duplicateRecord?.value?.size()>1) {
            records = duplicateRecord?.value?.minus(duplicateRecord?.value?.getAt(0))
            records?.each { record ->
                api.delete(typeCode, record)
            }
        }
    }
}