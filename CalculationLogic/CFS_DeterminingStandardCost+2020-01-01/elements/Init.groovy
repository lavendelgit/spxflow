import net.pricefx.formulaengine.AbstractProducer

if(!api.global.productPlantMapping){
    streamRecords = api.stream("PX6",null,Filter.equal("name","HierarchyPlantMapping"))
    api.global.productPlantMapping = streamRecords?.collectEntries{it-> [(it.attribute1):it]}
    streamRecords?.close()
}
if(!api.global.levelMapping){
    api.global.levelMapping = api.findLookupTableValues("HierarchyPlantMappingConfiguration")?.attribute1
}

if(!api.global.materialPlantCost){
    streamRecords = api.stream("PX10",null,Filter.equal("name","StandardCost"))
    api.global.materialPlantCost = streamRecords?.collectEntries{it-> [(it.sku+"_"+it.attribute1):it]}
    streamRecords?.close()
}

if(!api.global.plantCurrencyMapping){
    api.global.plantCurrencyMapping = api.findLookupTableValues("Plant")?.collectEntries{[(it.name):it.attribute9]}
}


if (!api.global.exchangeRate){
    api.global.exchangeRate=[:]
    api.global.exchangeRate = api.findLookupTableValues("MonthlyExchangeRate","key3")?.collectEntries{
        [(it.key1 + "-" + it.key2):it.attribute1]
    }
}

return