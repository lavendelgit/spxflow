Map plantData = [:]
Map costMap
if(out.ProductHierrachy){
    Map plantMapping = api.global.productPlantMapping[out.ProductHierrachy]
    for(level in api.global.levelMapping){
        if(plantMapping?.getAt(level)){
            plantData = api.global.materialPlantCost[out.MaterialNumber+"_"+plantMapping[level]]
            costAttribute = plantData?.attribute2 == "S"? "attribute4" : "attribute3"
            if(plantData?.getAt(costAttribute) && !costMap){
                costMap = [
                        Plant : plantData?.attribute1,
                        Cost : plantData[costAttribute],
                        PriceUnit : plantData?.attribute5,
                        Currency : api.global.plantCurrencyMapping[plantData?.attribute1],
                        ValidFrom : plantData?.attribute6
                ]
                return costMap
            }
        }
    }
}
return