if(out.StandardCost){
    def exchangeRate = api.global.exchangeRate?.getAt(out.Currency+"-"+out.ToCurrency)
    if(out.Currency == out.ToCurrency)
    {
        return out.StandardCost
    }
    else{
        return out.StandardCost * exchangeRate
    }
}
return