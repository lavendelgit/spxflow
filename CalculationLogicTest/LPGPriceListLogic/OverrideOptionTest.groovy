package LPGPriceListLogic

import net.pricefx.tdd4c.TestRun
import spock.lang.Specification

class OverrideOptionTest extends Specification {

    def LOGIC_DIR = "LPGPriceListLogic+2021-05-01"
    def ELEMENT_NAME = "OverrideOption"


    def "test case description"() {
        when:
        TestRun testRun = TestRun.builder()
                .withLogicTestDouble("api", [])
                .buildElementTest(LOGIC_DIR, ELEMENT_NAME)

        and:
        Script script = testRun.getElementScript()

        then:
        testRun.execute()
                .getElementTestResult() != null
    }
}
